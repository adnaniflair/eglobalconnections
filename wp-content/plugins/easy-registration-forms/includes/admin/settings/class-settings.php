<?php
/**
 * Form Settings
 *
 * @package    ERForms
 * @author     ERForms
 * @since      1.0.0
 */
class ERForms_Settings {

	/**
	 * Primary class constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'admin_init', array( $this, 'init' ) );
                add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
	}

	/**
	 *
	 * @since 1.0.0
	 */
	public function init() {
                
		// Check what page we are on
		$page = isset( $_GET['page'] ) ? $_GET['page'] : '';
                
		// Only load if we are actually on the builder
		if ( 'erforms-settings' === $page ) {
                    add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
                    add_action( 'erforms_admin_page',array( $this, 'output') );
		}
	}
        
        /**
	 * Enqueue assets for the overview page.
	 *
	 * @since 1.0.0
	 */
	public function enqueues() {
            // Hook for addons.
            do_action( 'erf_admin_settings_enqueue' );
	}
        
	/**
	 * Load the appropriate files to build the page.
	 *
	 * @since 1.0.0
	 */
	public function output() {
           $options= $this->save_settings(); 
           include 'html/settings.php';
	}
        
        private function save_settings(){ 
            $options_model= erforms()->options;
            $options= $options_model->get_options('erf_gsettings');
            
            if(isset($_POST['erf_save_settings'])){
                $options['rc_site_key'] = sanitize_text_field($_POST['rc_site_key']);
                $options['rc_secret_key'] =   sanitize_text_field($_POST['rc_secret_key']);
                $upload_dir =   sanitize_text_field($_POST['upload_dir']);
                $options['upload_dir'] =   empty($upload_dir) ? 'erf_uploads' : $upload_dir;
                $options['recaptcha_configured'] =   empty($_POST['recaptcha_configured']) ? 0 : 1;
                $options['en_wc_my_account'] =   empty($_POST['en_wc_my_account']) ? 0 : 1;
                $options['default_register_url'] =   empty($_POST['default_register_url']) ? 0 : absint($_POST['default_register_url']);
                $options['after_login_redirect_url']= esc_url($_POST['after_login_redirect_url']);
                $options['social_login']= $_POST['social_login'];
                $options['js_libraries']= $_POST['js_libraries'];
                $payment_methods= isset($_POST['payment_methods']) ? $_POST['payment_methods'] : array();
                $options['payment_methods']= $payment_methods;
                $options['currency']= $_POST['currency'];
                $options['en_role_redirection'] =   empty($_POST['en_role_redirection']) ? 0 : absint($_POST['en_role_redirection']);
                $options['en_login_recaptcha'] =   empty($_POST['en_login_recaptcha']) ? 0 : absint($_POST['en_login_recaptcha']);
                $roles= get_editable_roles();
                foreach($roles as $key=>$role){
                    $options[$key.'_login_redirection']= sanitize_text_field($_POST[$key.'_login_redirection']);
                }
                
                $options['en_payment_pending_email'] =   isset($_POST['en_payment_pending_email']) ? 1 : 0;
                $options['en_payment_completed_email'] =   isset($_POST['en_payment_completed_email']) ? 1 : 0;
                $options['payment_pending_email'] =   isset($_POST['payment_pending_email']) ?  wp_kses_post(stripslashes($_POST['payment_pending_email'])) : '';
                $options['payment_completed_email'] =   isset($_POST['payment_completed_email']) ? wp_kses_post(stripslashes($_POST['payment_completed_email'])) : '';
                $options['note_user_email_sub']= isset($_POST['note_user_email_sub']) ? sanitize_text_field($_POST['note_user_email_sub']) : '';
                $options['note_user_email_from_name']= isset($_POST['note_user_email_from_name']) ? sanitize_text_field($_POST['note_user_email_from_name']) : '';
                $options['note_user_email_from']= isset($_POST['note_user_email_from']) ? sanitize_text_field($_POST['note_user_email_from']) : '';
                $options['en_note_user_email']= isset($_POST['en_note_user_email']) ? 1 : 0;
                $options['note_user_email']= isset($_POST['note_user_email']) ? wp_kses_post(stripslashes($_POST['note_user_email'])) : '';
                
                $options['en_pay_completed_admin_email'] = isset($_POST['en_pay_completed_admin_email']) ? 1 : 0;
                $options['completed_pay_admin_email_from'] = isset($_POST['completed_pay_admin_email_from']) ? sanitize_text_field($_POST['completed_pay_admin_email_from']) : '';
                $options['completed_pay_admin_email_from_name'] = isset($_POST['completed_pay_admin_email_from_name']) ? sanitize_text_field($_POST['completed_pay_admin_email_from_name']) : '';
                $options['completed_pay_admin_email_subject'] = isset($_POST['completed_pay_admin_email_subject']) ? sanitize_text_field($_POST['completed_pay_admin_email_subject']) : '';
                $options['pay_completed_admin_email'] = isset($_POST['pay_completed_admin_email']) ? wp_kses_post(stripslashes($_POST['pay_completed_admin_email'])) : '';
                $options['completed_pay_admin_email_to']= isset($_POST['completed_pay_admin_email_to']) ? sanitize_text_field($_POST['completed_pay_admin_email_to']) : '';
                
                // Display settings for login form
                $options['login_layout']= isset($_POST['login_layout']) ? sanitize_text_field($_POST['login_layout']) : '';
                $options['login_field_style']= isset($_POST['login_field_style']) ? sanitize_text_field($_POST['login_field_style']) : '';
                $options['login_label_position']= isset($_POST['login_label_position']) ? sanitize_text_field($_POST['login_label_position']) : '';
                
                $options= apply_filters('erf_before_save_settings',$options);
                $options_model->save_options($options);
            }
            
            if(isset($_POST['savec'])){// Save and Close
                $tab= isset($_POST['erf_gs_tab']) ? $_POST['erf_gs_tab'] : '';
                $url= admin_url('admin.php?page=erforms-settings');
                if(!empty($tab)){
                    $url .= '&tab='.$tab;
                }
                erforms_redirect($url);
                exit;
            }
            
            return $options;
        }
        /**
    * Enqueue assets for the overview page.
    *
    * @since 1.0.0
    */
        
}

new ERForms_Settings;
