<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.iflair.com/
 * @since      1.0.0
 *
 * @package    Wishlist
 * @subpackage Wishlist/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wishlist
 * @subpackage Wishlist/public
 * @author     iFlair Pvt Ltd <adnan.limdiwala@iflair.com>
 */
class Wishlist_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wishlist_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wishlist_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wishlist-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wishlist_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wishlist_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wishlist-public.js', array( 'jquery' ), $this->version, false );

	}

	// //product Short List Functions.
	// add_action( 'wp_ajax_product_wish_list', 'ege_product_wish_list_user_update' );
	// add_action( 'wp_ajax_nopriv_product_wish_list', 'ege_product_wish_list_user_update');

	public function ege_product_wish_list_user_update(){
		$userid 	= get_current_user_id();
		$educatorid = $_REQUEST['educatorid'];
		$wishdate 	= $_REQUEST['wishdata'];
		$meta_key   = 'product_short_list';
		echo $wishdate;
		if(	$wishdate  == 'add'){
		    $educatorarray = get_user_meta($userid,$meta_key,true);
		   // print_r($educatorarray);
		    if(empty($educatorarray)){
			    $originalarray=array($educatorid);
			    $metavalue = maybe_serialize($originalarray);
			 }else{
			 	$originaleducatorarray=maybe_unserialize($educatorarray);
			 	//echo "adding".$educatorid;
			 	//$thereid ='';
			 	//$thereid = array_search($educatorid,$originaleducatorarray);
				array_push($originaleducatorarray,$educatorid);
			    $metavalue = maybe_serialize(array_unique($originaleducatorarray));
			 }
		    	update_user_meta($userid,$meta_key,$metavalue);
		}else{
		   $educatorarray = get_user_meta($userid,$meta_key,true);
		   $originaleducatorarray=maybe_unserialize($educatorarray);
	 	   $thereid = array_search($educatorid,$originaleducatorarray);
		   unset($originaleducatorarray[$thereid]);
		   //print_r($originaleducatorarray);
		   $metavalue = maybe_serialize(array_unique($originaleducatorarray));

			 if(!empty($originaleducatorarray)){
		    	$metavalue = maybe_serialize(array_unique($originaleducatorarray));
			}else{
				$metavalue = '';
			}
					 //	print_r($originaleducatorarray);

	    	update_user_meta($userid,$meta_key,$metavalue);

		}
		//print_r(array_unique($originaleducatorarray));
		wp_die();
	}

	// Product Short List HtML.
	public function ege_product_short_list_html(){
		$added 			= "add to shortlist";
	    if(is_user_logged_in()) {
		    $userid         = get_current_user_id();
		    $meta_key       = 'product_short_list';
		    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
		    $wishclass      = '';
		    $wishclassajax      = 'add';
		    //$wishsortlistclass   = 'fa-heart';
		    if(is_array($educatorarray)){
			    if(in_array(get_the_id(), $educatorarray)){
			    $wishclass = 'active';
			    $wishclassajax = 'remove';
			    //$wishsortlistclass = 'fa-heart-o';
			    $added = "shortlisted";    
		    }
			}

	}else{
      		$ipaddress   = getRealIpAddr();
			$educatorarray = maybe_unserialize(get_book_wislist($ipaddress));
			$educatorarray = maybe_unserialize($educatorarray[0]);
		    $wishclass      = '';
		    $wishclassajax      = 'add';
		    //$wishsortlistclass   = 'fa-heart';
		    if(is_array($educatorarray)){
			    if(in_array(get_the_id(), $educatorarray)){
			    $wishclass = 'active';
			    $wishclassajax = 'remove';
			    //$wishsortlistclass = 'fa-heart-o';
			    $added = "shortlisted";    
		    	}
			}

      }   
	if(is_user_logged_in()) {
	    ?>
	<span class="product-heart-icon" data-wish="<?php echo $wishclassajax;  ?>" data-id="<?php echo get_the_id();?>">
	    <a href="javascript:void(0);"  class="like-ico course-heart-o"><i class="fa fa-heart <?php echo $wishclass;  ?> "></i></a>
	    <div class="addtoshort-btn"><a href="#"><?php echo $added; ?></a></div>
	</span>
	<?php }else{ ?>
	<span class="product-heart-icon" data-wish="<?php echo $wishclassajax;  ?>" data-id="<?php echo get_the_id();?>">
	    <a href="javascript:void(0);"  class="like-ico course-heart-o"><i class="fa fa-heart <?php echo $wishclass;  ?> "></i></a>
	    <div class="addtoshort-btn"><a href="#"><?php echo $added; ?></a></div>
	</span>
	<?php }  
	}


	// Educator Add In Favorait List.
	public function ege_educator_wish_list_user_update(){

		$userid 	= get_current_user_id();
		$educatorid = $_REQUEST['educatorid'];
		$wishdate 	= $_REQUEST['wishdata'];
		$meta_key   = 'educator_short_list';
		if(	$wishdate  == 'add'){
		    $educatorarray = get_user_meta($userid,$meta_key,true);
		    if(empty($educatorarray)){
			    $originalarray=array($educatorid);
			    $metavalue = maybe_serialize($originalarray);

			 }else{
			 	$originaleducatorarray=maybe_unserialize($educatorarray);
			 	//echo "adding".$educatorid;
			 	//$thereid ='';
			 	//$thereid = array_search($educatorid,$originaleducatorarray);
				array_push($originaleducatorarray,$educatorid);

			    $metavalue = maybe_serialize(array_unique($originaleducatorarray));

			 }
		    	update_user_meta($userid,$meta_key,$metavalue);
		}else{
		   $educatorarray = get_user_meta($userid,$meta_key,true);
		   $originaleducatorarray=maybe_unserialize($educatorarray);
	 	   $thereid = array_search($educatorid,$originaleducatorarray);
		   unset($originaleducatorarray[$thereid]);
		   print_r($originaleducatorarray);
		   $metavalue = maybe_serialize(array_unique($originaleducatorarray));

			 if(!empty($originaleducatorarray)){
		    	$metavalue = maybe_serialize(array_unique($originaleducatorarray));
			}else{
				$metavalue = '';
			}
					 //	print_r($originaleducatorarray);

	    	update_user_meta($userid,$meta_key,$metavalue);

		}

		// $wishdate
	 //    array_push($a,"blue","saran","yellow");
	 //    $value = "saran"; 
	 //    echo $id = array_search($value,$a);
	 //    unset($a[$id]);
	 //   // print_r(maybe_serialize($a));
	 //    $arrayseralize = maybe_serialize($a);
	    //print_r(maybe_unserialize($arrayseralize));

		echo "Work";

		wp_die();

	}


// Courses add in short list
// add_action( 'wp_ajax_course_wish_list', 'ege_course_wish_list_list_user_update' );
// add_action( 'wp_ajax_nopriv_course_wish_list', 'ege_course_wish_list_list_user_update');

	public function ege_course_wish_list_list_user_update(){
		$userid 	= get_current_user_id();
		$educatorid = $_REQUEST['courseid'];
		$wishdate 	= $_REQUEST['wishdata'];
		$meta_key   = 'course_short_list';
		echo $wishdate;
		if(	$wishdate  == 'add'){
		    $educatorarray = get_user_meta($userid,$meta_key,true);
		    print_r($educatorarray);
		    if(empty($educatorarray)){
			    $originalarray=array($educatorid);
			    $metavalue = maybe_serialize($originalarray);
			 }else{
			 	$originaleducatorarray=maybe_unserialize($educatorarray);
			 	//echo "adding".$educatorid;
			 	//$thereid ='';
			 	//$thereid = array_search($educatorid,$originaleducatorarray);
				array_push($originaleducatorarray,$educatorid);
			    $metavalue = maybe_serialize(array_unique($originaleducatorarray));
			 }
		    	update_user_meta($userid,$meta_key,$metavalue);
		}else{
		   $educatorarray = get_user_meta($userid,$meta_key,true);
		   $originaleducatorarray=maybe_unserialize($educatorarray);
	 	   $thereid = array_search($educatorid,$originaleducatorarray);
		   unset($originaleducatorarray[$thereid]);
		   print_r($originaleducatorarray);
		   $metavalue = maybe_serialize(array_unique($originaleducatorarray));

			 if(!empty($originaleducatorarray)){
		    	$metavalue = maybe_serialize(array_unique($originaleducatorarray));
			}else{
				$metavalue = '';
			}
					 //	print_r($originaleducatorarray);

	    	update_user_meta($userid,$meta_key,$metavalue);

		}
		//print_r(array_unique($originaleducatorarray));
		wp_die();

	}

// Course Short List Html Hook.
public function ege_course_short_list_html(){
    	$added 			= "add to shortlist";
	    if(is_user_logged_in()) {
		    $userid         = get_current_user_id();
		    $meta_key       = 'course_short_list';
		    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
		    $wishclass      = '';
		    $wishclassajax      = 'add';
		    //$wishsortlistclass   = 'fa-heart';
		    if(is_array($educatorarray)){
			    if(in_array(get_the_id(), $educatorarray)){
			    $wishclass = 'active';
			    //$wishsortlistclass = 'fa-heart-o';
			    $added = "shortlisted";  
			    $wishclassajax = 'remove';  
		    }
		}

    }else{
      		$ipaddress   = getRealIpAddr();
			$educatorarray = maybe_unserialize(get_course_wislist($ipaddress));
			$educatorarray = maybe_unserialize($educatorarray[0]);
		    $wishclass      = '';
		    $wishclassajax      = 'add';
		    //$wishsortlistclass   = 'fa-heart';
		    if(is_array($educatorarray)){
			    if(in_array(get_the_id(), $educatorarray)){
			    $wishclass = 'active';
			    $wishclassajax = 'remove';
			    //$wishsortlistclass = 'fa-heart-o';
			    $added = "shortlisted";    
		    	}
			}

      }  
	if(is_user_logged_in()) {
	    ?>
	<span class="course-heart-icon" data-wish="<?php echo  $wishclassajax; ?>" data-id="<?php echo get_the_id();?>">
	    <a href="javascript:void(0);"  class="course-heart-click"><i class="fa fa-heart <?php echo $wishclass;  ?> "></i></a>
	    <div class="addtoshort-btn"><a href="#"><?php echo $added; ?></a></div>
	</span>
	<?php }else{ ?>
	<span class="course-heart-icon" data-wish="<?php echo  $wishclassajax; ?>" data-id="<?php echo get_the_id();?>">
	    <a href="javascript:void(0);"  class="course-heart-click"><i class="fa fa-heart <?php echo $wishclass;  ?> "></i></a>
	    <div class="addtoshort-btn"><a href="#"><?php echo $added; ?></a></div>
	</span>

	<?php } 
}

// Educator Short List Html Hook.
public function ege_educator_short_list_html(){
    	$added 			= "add to shortlist";
	    if(is_user_logged_in()) {
		    $userid         = get_current_user_id();
		    $meta_key       = 'educator_short_list';
		    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
		    $wishclass      = '';
		    $wishclassajax      = 'add';
		    //$wishsortlistclass   = 'fa-heart';
		    if(is_array($educatorarray)){
			    if(in_array(get_the_id(), $educatorarray)){
			    $wishclass = 'active';
			    $wishclassajax = 'remove';
			    //$wishsortlistclass = 'fa-heart-o';
			    $added = "shortlisted";    
		    }
		  }
      }else{
      		$ipaddress   = getRealIpAddr();
			$educatorarray = maybe_unserialize(get_educator_wislist($ipaddress));
			$educatorarray = maybe_unserialize($educatorarray[0]);
		    $wishclass      = '';
		    $wishclassajax      = 'add';
		    //$wishsortlistclass   = 'fa-heart';
		    if(is_array($educatorarray)){
			    if(in_array(get_the_id(), $educatorarray)){
			    $wishclass = 'active';
			    $wishclassajax = 'remove';
			    //$wishsortlistclass = 'fa-heart-o';
			    $added = "shortlisted";    
		    	}
			}

      } 
	if(is_user_logged_in()) {
	    ?>
	<span class="educator-heart-icon" data-wish="<?php echo $wishclassajax;  ?>" data-id="<?php echo get_the_id();?>">
	    <a href="javascript:void(0);"  class="like-ico course-heart-o"><i class="fa fa-heart <?php echo $wishclass;  ?> "></i></a>
	    <div class="addtoshort-btn"><a href="#"><?php echo $added; ?></a></div>
	</span>
	<?php }else{ 
	?>
	<span class="educator-heart-icon" data-wish="<?php echo $wishclassajax; ?>" data-id="<?php echo get_the_id();?>">
	    <a href="javascript:void(0);"  class="like-ico course-heart-o"><i class="fa fa-heart <?php echo $wishclass;  ?> "></i></a>
	    <div class="addtoshort-btn"><a href="#"><?php echo $added; ?></a></div>
	</span>

	<?php } 
}

// Non login user wishlist database
// Educator Add In Favorait List.
public function ege_educator_wish_list_user_nonlogin_update(){
	//	$userid 	= get_current_user_id();
	$educatorid  = $_REQUEST['educatorid'];
	$wishdate 	= $_REQUEST['wishdata'];
	$ipaddress   = getRealIpAddr();
	$datecreated 		= date('Y-m-d');
	if(	$wishdate  == 'add'){
	       $educatorarray = get_educator_wislist($ipaddress);
		// global $wpdb;
		// $table_name = $wpdb->prefix ."nonlogin_wishlist";
	 //    $arr = get_educator_wislist($ipaddress );
    
		// $where = '';
		// if($productid){
		//   $where = "WHERE `type` LIKE 'educatorwishlist' and `ip`=".$productid;
		// }
		  if(empty($educatorarray)){
			    $originalarray=array($educatorid);
			    $metavalue = maybe_serialize($originalarray);
				echo add_educator_wislist($metavalue);
			   	}else{
			   	$educatorarray = $educatorarray[0];		 	
			 	$originaleducatorarray=maybe_unserialize($educatorarray);
			 	//echo "adding".$educatorid;
			 	//$thereid ='';
			 	//$thereid = array_search($educatorid,$originaleducatorarray);

				if(!empty($educatorarray)){
				$originaleducatorarray=maybe_unserialize($educatorarray);
				//echo "adding".$educatorid;
				//$thereid ='';
				//$thereid = array_search($educatorid,$originaleducatorarray);
				array_push($originaleducatorarray,$educatorid);
				$metavalue = maybe_serialize(array_unique($originaleducatorarray));
				}else{
				$originalarray=array($educatorid);
				$metavalue = maybe_serialize($originalarray);
				}
				// array_push($originaleducatorarray,$educatorid);
			 //    $metavalue = maybe_serialize(array_unique($originaleducatorarray));
				update_educator_wislist($ipaddress, $metavalue);
		  	}
		}else{
		   $educatorarray = get_educator_wislist($ipaddress);
		   $educatorarray = $educatorarray[0];
		   $originaleducatorarray=maybe_unserialize($educatorarray);
	 	   $thereid = array_search($educatorid,$originaleducatorarray);
		   unset($originaleducatorarray[$thereid]);
		   print_r($originaleducatorarray);
		   $metavalue = maybe_serialize(array_unique($originaleducatorarray));
		   if(!empty($originaleducatorarray)){
		    	$metavalue = maybe_serialize(array_unique($originaleducatorarray));
			}else{
				$metavalue = '';
			}
					 //	print_r($originaleducatorarray);
			update_educator_wislist($ipaddress, $metavalue);
		}

		wp_die();

	}

// Educator Add In Favorait List.
public function ege_course_wish_list_user_nonlogin_update(){
	//	$userid 	= get_current_user_id();
	$educatorid  = $_REQUEST['courseid'];
	$wishdate 	= $_REQUEST['wishdata'];
	echo $ipaddress   = getRealIpAddr();
	$datecreated 		= date('Y-m-d');
	if(	$wishdate  == 'add'){
	       $educatorarray = get_course_wislist($ipaddress);
	      // print_r($educatorarray);
		// global $wpdb;
		// $table_name = $wpdb->prefix ."nonlogin_wishlist";
	 //    $arr = get_educator_wislist($ipaddress );
    
		// $where = '';
		// if($productid){
		//   $where = "WHERE `type` LIKE 'educatorwishlist' and `ip`=".$productid;
		// }
		  if(empty($educatorarray)){
			    $originalarray=array($educatorid);
			    $metavalue = maybe_serialize($originalarray);
				echo add_course_wislist($metavalue);
			   	}else{
			   	$educatorarray = $educatorarray[0];
			   	if(!empty($educatorarray)){
			 		$originaleducatorarray=maybe_unserialize($educatorarray);
			 	//echo "adding".$educatorid;
			 	//$thereid ='';
			 	//$thereid = array_search($educatorid,$originaleducatorarray);
					array_push($originaleducatorarray,$educatorid);
				    $metavalue = maybe_serialize(array_unique($originaleducatorarray));
				}else{
					$originalarray=array($educatorid);
					$metavalue = maybe_serialize($originalarray);
				}
				update_course_wislist($ipaddress,$metavalue);
		  	}
		}else{
		   $educatorarray = get_course_wislist($ipaddress);
		   $educatorarray = $educatorarray[0];
		   $originaleducatorarray=maybe_unserialize($educatorarray);
	 	   $thereid = array_search($educatorid,$originaleducatorarray);
		   unset($originaleducatorarray[$thereid]);
		   print_r($originaleducatorarray);
		   $metavalue = maybe_serialize(array_unique($originaleducatorarray));
		   if(!empty($originaleducatorarray)){
		    	$metavalue = maybe_serialize(array_unique($originaleducatorarray));
			}else{
				$metavalue = '';
			}
					 //	print_r($originaleducatorarray);
			update_course_wislist($ipaddress, $metavalue);
		}

		wp_die();

	}

// Educator Add In Favorait List.
public function ege_book_wish_list_user_nonlogin_update(){
	//	$userid 	= get_current_user_id();
	$educatorid  = $_REQUEST['educatorid'];
	$wishdate 	= $_REQUEST['wishdata'];
	echo $ipaddress   = getRealIpAddr();
	$datecreated 		= date('Y-m-d');
	if(	$wishdate  == 'add'){
	       $educatorarray = get_book_wislist($ipaddress);
	       print_r($educatorarray);
		// global $wpdb;
		// $table_name = $wpdb->prefix ."nonlogin_wishlist";
	 //    $arr = get_educator_wislist($ipaddress );
    
		// $where = '';
		// if($productid){
		//   $where = "WHERE `type` LIKE 'educatorwishlist' and `ip`=".$productid;
		// }
		  if(empty($educatorarray)){
			    $originalarray=array($educatorid);
			    $metavalue = maybe_serialize($originalarray);
				echo add_book_wislist($metavalue);
			   	}else{
			   	$educatorarray = $educatorarray[0];		 	
			 	$originaleducatorarray=maybe_unserialize($educatorarray);
			 	//echo "adding".$educatorid;
			 	//$thereid ='';
			 	//$thereid = array_search($educatorid,$originaleducatorarray);
			    if(!empty($educatorarray)){
			 		$originaleducatorarray=maybe_unserialize($educatorarray);
			 	//echo "adding".$educatorid;
			 	//$thereid ='';
			 	//$thereid = array_search($educatorid,$originaleducatorarray);
					array_push($originaleducatorarray,$educatorid);
				    $metavalue = maybe_serialize(array_unique($originaleducatorarray));
				}else{
					$originalarray=array($educatorid);
					$metavalue = maybe_serialize($originalarray);
				}
			    update_book_wislist($ipaddress, $metavalue);
		  	}
		}else{
		   $educatorarray = get_book_wislist($ipaddress);
		   $educatorarray = $educatorarray[0];
		   $originaleducatorarray=maybe_unserialize($educatorarray);
	 	   $thereid = array_search($educatorid,$originaleducatorarray);
		   unset($originaleducatorarray[$thereid]);
		   print_r($originaleducatorarray);
		   $metavalue = maybe_serialize(array_unique($originaleducatorarray));
		   if(!empty($originaleducatorarray)){
		    	$metavalue = maybe_serialize(array_unique($originaleducatorarray));
			}else{
				$metavalue = '';
			}
			update_book_wislist($ipaddress, $metavalue);
		}

		wp_die();

	}
}