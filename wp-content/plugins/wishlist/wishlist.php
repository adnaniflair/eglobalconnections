<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.iflair.com/
 * @since             2.0.0
 * @package           Wishlist
 *
 * @wordpress-plugin
 * Plugin Name:       Wish list
 * Plugin URI:        wishlist
 * Description:       This plugin use for wishlist for product,course and educator
 * Version:           2.0.0
 * Author:            iFlair Pvt Ltd
 * Author URI:        https://www.iflair.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wishlist
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WISHLIST_VERSION', '2.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wishlist-activator.php
 */
function activate_wishlist() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wishlist-activator.php';
	Wishlist_Activator::activate();
}


/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wishlist-deactivator.php
 */
function deactivate_wishlist() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wishlist-deactivator.php';
	Wishlist_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wishlist' );
register_deactivation_hook( __FILE__, 'deactivate_wishlist' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wishlist.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wishlist() {

	$plugin = new Wishlist();
	$plugin->run();

}
run_wishlist();
/**
 * Ip address get 
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function getRealIpAddr(){
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function get_educator_wislist($productid=null){
  global $wpdb;
  $table_name = $wpdb->prefix ."nonlogin_wishlist";
  // $where = '';
  // if($productid){
  //   $where = "WHERE `type` LIKE 'educatorwishlist' and `ip`=".$productid;
  // }
  $coursereview = $wpdb->get_results( "SELECT wishlist FROM `ege_nonlogin_wishlist` WHERE `ip` LIKE '$productid' AND `type` LIKE 'educatorwishlist' LIMIT 1" ,ARRAY_A );
  if(!empty($coursereview)){
    $coursearray =array_column($coursereview,'wishlist');
  }else{
    $coursearray =array();
  }
  return $coursearray;
}

function add_educator_wislist($productid=null){
  global $wpdb;
  $datecreated = date('Y-m-d');
  $ipaddress   = getRealIpAddr();

  $table = $wpdb->prefix.'nonlogin_wishlist';
  $data = array('ip'=>$ipaddress,'type'=>'educatorwishlist','wishlist' =>$productid,'createdate'=>$datecreated);
  $format = array('%s','%s','%s','%s');
  $insert_id =$wpdb->insert($table,$data,$format);
  // $coursearray =array_column($coursereview);
  return $insert_id;
}

function update_educator_wislist($productid=null,$value1){
  global $wpdb;
  $table_name = $wpdb->prefix . "nonlogin_wishlist";
  $wpdb->update( 
  'ege_nonlogin_wishlist', 
  array( 
  'wishlist' => $value1, // string
  ), 
  array( 'ip' => $productid,
  'type'=>'educatorwishlist' ), 
  array( 
  '%s',   // value1
  ), 
  array( '%s' ) 
  );
  // $coursearray =array_column($coursereview);
  return $coursereview;
}


// Course wish listing

function get_course_wislist($productid=null){
  global $wpdb;
  $table_name = $wpdb->prefix ."nonlogin_wishlist";
  // $where = '';
  // if($productid){
  //   $where = "WHERE `type` LIKE 'educatorwishlist' and `ip`=".$productid;
  // }
  $coursereview = $wpdb->get_results( "SELECT wishlist FROM `ege_nonlogin_wishlist` WHERE `ip` LIKE '$productid' AND `type` LIKE 'coursewishlist' LIMIT 1" ,ARRAY_A );
  if(!empty($coursereview)){
    $coursearray =array_column($coursereview,'wishlist');
  }else{
    $coursearray =array();
  }
  return $coursearray;
}

function add_course_wislist($productid=null){
  global $wpdb;
  $datecreated = date('Y-m-d');
  $ipaddress   = getRealIpAddr();

  $table = $wpdb->prefix.'nonlogin_wishlist';
  $data = array('ip'=>$ipaddress,'type'=>'coursewishlist','wishlist' =>$productid,'createdate'=>$datecreated);
  $format = array('%s','%s','%s','%s');
  $insert_id =$wpdb->insert($table,$data,$format);
  // $coursearray =array_column($coursereview);
  return $insert_id;
}

function update_course_wislist($productid=null,$value1){
  global $wpdb;
  $table_name = $wpdb->prefix . "nonlogin_wishlist";
  $wpdb->update( 
  'ege_nonlogin_wishlist', 
  array( 
  'wishlist' => $value1, // string
  ), 
  array( 'ip' => $productid,
  'type'=>'coursewishlist' ), 
  array( 
  '%s',   // value1
  ), 
  array( '%s' ) 
  );
  // $coursearray =array_column($coursereview);
  return $coursereview;
}
//product wish listing
function get_book_wislist($productid=null){
  global $wpdb;
  $table_name = $wpdb->prefix ."nonlogin_wishlist";

  $coursereview = $wpdb->get_results( "SELECT wishlist FROM `ege_nonlogin_wishlist` WHERE `ip` LIKE '$productid' AND `type` LIKE 'bookwishlist' LIMIT 1" ,ARRAY_A );
  if(!empty($coursereview)){
    $coursearray =array_column($coursereview,'wishlist');
  }else{
    $coursearray =array();
  }
  return $coursearray;
}

function add_book_wislist($productid=null){
  global $wpdb;
  $datecreated = date('Y-m-d');
  $ipaddress   = getRealIpAddr();

  $table = $wpdb->prefix.'nonlogin_wishlist';
  $data = array('ip'=>$ipaddress,'type'=>'bookwishlist','wishlist' =>$productid,'createdate'=>$datecreated);
  $format = array('%s','%s','%s','%s');
  $insert_id =$wpdb->insert($table,$data,$format);
  // $coursearray =array_column($coursereview);
  return $insert_id;
}

function update_book_wislist($productid=null,$value1){
  global $wpdb;
  $table_name = $wpdb->prefix . "nonlogin_wishlist";
  $wpdb->update( 
  'ege_nonlogin_wishlist', 
  array( 
  'wishlist' => $value1, // string
  ), 
  array( 'ip' => $productid,
  'type'=>'bookwishlist' ), 
  array( 
  '%s',   // value1
  ), 
  array( '%s' ) 
  );
  // $coursearray =array_column($coursereview);
  return $coursereview;
}