<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <title>eGlobalconnect - Home</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/favicon.ico ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/owl.carousel.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/animate.css">
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/aos.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/jquery.easy_slides.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/chosen.min.css" rel="stylesheet" />
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/style.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/responsive.css" rel="stylesheet">
		<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="overflow-hiden" style="overflow: hidden;">  
	<header>
		<div class="wrapper">
			<div class="logo-wrap">
				<a href="<?php echo home_url(); ?>" title="eGlobalconnect">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/logo.png" alt="eGlobalconnect">
				</a>
			</div>
			<nav class="menu">
					<?php
					wp_nav_menu( array(
					'theme_location' => 'top',
					'container'=>false,
					'menu_class'=>''
					) );
					?>
				<?php /*
				<ul>

					<li>
						<a href="#" title="Universities">Universities</a>
					</li>
					<li>
						<a href="#" title="educators">Educators</a>
					</li>
					<li>
						<a href="#" title="resources">Resources</a>
					</li>
					<li>
						<a href="#" title="contact">Contact</a>
					</li>
					<a href="#" class="serach-icon" title="search">
						<i class="fa fa-search"></i>
					</a>
				</ul> */ ?>
			</nav>
			<div class="header-right">
				<a href="<?php echo site_url(); ?>/wp-admin/" class="profile">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/user.png">Login
				</a>
				<div class="lan-dropdown">
					<a href="#" class="lan-toggle"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/flag.png">En</a>
				</div>
			</div>
		</div>
	</header>
<script type="text/javascript">
var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>