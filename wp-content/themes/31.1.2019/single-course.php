<?php

get_header(); 
$courseviews = pvc_get_post_views(get_the_id());
update_post_meta(get_the_id(),'post_views_counter',$courseviews);


$unvisityid 	 = get_field('select_university',get_the_id());
$universityname = get_the_title($unvisityid);
$terms  		 = get_the_terms($unvisityid,'Country');
$unvisitylogo = get_field('university_logo',$unvisityid);
//$countryname    = $termsname = $terms[0]->name;

$countryname =  '';
$countryflag = '';
if(!empty($terms)){
    $countryname =  $termsname = $terms[0]->name;
    $termsid     = $terms[0]->term_id;
    $countryflag = ege_countryflagurl($termsid);
}


wp_reset_postdata(); 
//Tab Sections Code
$corsebannerimages = get_field('banner_image');
if(empty($corsebannerimages)){
$corsebannerimages = site_url().'/wp-content/uploads/2018/12/inner-banner.jpg';
}

$course_outline_title = get_field('course_outline_title');
$course_outline_sort_descriptions = get_field('course_outline_sort_descriptions');
$course_descriptions = get_field('course_descriptions');

$learning_teaching_title = get_field('learning_teaching_title');
$learning_teaching_short_descriptions = get_field('learning_teaching_short_descriptions');
$learning_teaching_descriptions = get_field('learning_teaching_descriptions');

$career_possiblities_tiltle = get_field('career_possiblities_tiltle');
$career_possiblities_short_descriptions = get_field('career_possiblities_short_descriptions');
$career_possiblities_descriptions = get_field('career_possiblities_descriptions');

$entry_credit_tiltle = get_field('entry_credit_tiltle');
$entry_credit_short_descriptions = get_field('entry_credit_short_descriptions');

$reviews_title = get_field('reviews_title');
$reviews_descriptions = get_field('reviews_descriptions');

$video_title = get_field('video_title');
$video_descriptions = get_field('video_descriptions');
$gallery_videos = get_field('gallery_videos');

//Review Sections 
global $wpdb;
$table_name = $wpdb->prefix . "review";

$avarage = 0;
$totalaverage =0;
$totalcoursereviws =0;
//SELECT * FROM `ege_review` WHERE `course_id` = 15
$coursereview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE `course_id` = 15 " );
if(!empty($coursereview)){
$totalcoursereviws =  count($coursereview);
$totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE `course_id` = 15 ");
$teching_quality = $wpdb->get_row("SELECT AVG(`teching_quality`) as avarage FROM $table_name WHERE `course_id` = 15 ");
$school_facilites = $wpdb->get_row("SELECT AVG(`school_facilites`) as avarage FROM $table_name WHERE `course_id` = 15 ");
$locations = $wpdb->get_row("SELECT AVG(`locations`) as avarage FROM $table_name WHERE `course_id` = 15 ");
$career_assistance = $wpdb->get_row("SELECT AVG(`career_assistance`) as avarage FROM $table_name WHERE `course_id` = 15 ");
$value_of_money = $wpdb->get_row("SELECT AVG(`value_of_money`) as avarage FROM $table_name WHERE `course_id` = 15 ");
//echo $avarage     = number_format((float)$totalrating->avarage, 2, '.', '');
// echo "<pre>";
// print_r($coursereview);
// echo "</pre>";

$avarage = $totalrating->avarage;
$teching_quality_avrage     = wp_star_rating_avarage_float($teching_quality->avarage);
$school_facilites_avrage    = wp_star_rating_avarage_float($school_facilites->avarage);
$locations_avrage           = wp_star_rating_avarage_float($locations->avarage);
$career_assistance_avrage   = wp_star_rating_avarage_float($career_assistance->avarage);
$value_of_money_avrage      = wp_star_rating_avarage_float($value_of_money->avarage);
$totalaverage                = wp_star_rating_avarage_float($avarage);
}
$args = array(
   'rating' => $avarage,
   'type' => 'rating',
   'number' => 5,
);

//$ratings = round( $avarage / 10, 0 ) / 2;
//$average = 3;

if(is_user_logged_in()) {
    $userid         = get_current_user_id();
    $meta_key       = 'course_short_list';
    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
    $wishclass      = 'add';
    $wishsortlistclass   = 'fa-heart';
    if(is_array($educatorarray)){
        if(in_array(get_the_id(), $educatorarray)){
            $wishclass = 'remove';
            $wishsortlistclass = 'fa-heart-o';
        }
    }
}
?>

    <section class="inner-bannerbox topgreen-border" style="background-image: url(<?php echo $corsebannerimages; ?>)">
        <div class="wrapper">
            <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">
                    <ul class="cf">
                        <li><a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a></li>
                        <li><a href="<?php echo get_permalink($unvisityid);  ?>"><?php echo $universityname; ?></a></li>
                       <?php /*
                        <li><a href="#">University</a></li>
                        <li><a href="#">UK</a></li>
                        */ ?>
                        <li><label> <?php echo get_the_title(); ?></label></li>

                    </ul>
                </div>
                <div class="innerbanner-block cf">
                    <?php if(!empty($unvisitylogo)){ ?>
                    <div class="inrbanner-leftimg">
                        <img src="<?php echo $unvisitylogo; ?>" alt="" />
                    </div>
                    <?php } ?>
                    <div class="inrbaner-rightcont">
                        <div class="baner-titbox">
                            <h3><?php echo get_the_title(); ?></h3>
                            <?php if(is_user_logged_in()) { ?>
                            <span data-wish="<?php echo $wishclass; ?>" class="educator-wish-list"><i class="fa <?php echo $wishsortlistclass; ?>"></i></span>
                            <?php } ?>
                            <?php /*
                            <span data-wish="add" class="educator-wish-list red-heart" ><i class="fa fa-heart-o"></i></span>
                            */ ?>
                        </div>
                        <div class="banner-country-flag">
                            <span>at <?php echo $universityname; ?></span>
                            <div class="course-country-banner">
                                 <?php if(!empty($countryflag)) {?>   
                                 <span><img src="<?php echo  $countryflag; ?>"></span>
                                <?php } ?>
                                <span class="dottd-btmborder"><?php echo $countryname; ?></span>
                            </div>
                        </div>
                        <div class="school-detail-bottom">
                            <div class="rate-star">
                                <?php wp_star_rating($args); ?>
                                <a class="ratenum-like" href="javascript:void(0)"  title="like">
                                    <span class="white-brd rating-number-box"><?php echo $totalaverage;  ?></span>
                                </a>
                            </div>
                            <div class="review-texts">
                                <span class="noborder-bold"><?php echo $totalcoursereviws; ?> reviews</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="innerpage-menu-box greenbgcolor">
        <div class="wrapper cf">
            <div class="sticky-imgtit titimgsticky">
                <?php if(!empty($unvisitylogo)) { ?>
                <div class="stikimg-small">
                  <img src="<?php echo $unvisitylogo ?>" alt="" width="60"/>
                </div>
                <?php } ?>
                <div class="sticky-tibox">
                   <div class="table">
                       <div class="table-cell">
                            <h4><?php echo get_the_title(); ?></h4>
                       </div>
                    </div>
                </div>
            </div>
            <div class="enquiry-rightbox">
                <a href="#" class="btn booknow-btn">Book now</a>
                <a href="javascript:void(0);" class="btn enquiry-btn openpopup">Enquire now</a>
            </div>

            <div class="enquiry-form-popup">
                 <div class="maxwidth-box">
                     <div class="padding whitecolor">
                         <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                         <?php echo do_shortcode('[contact-form-7 id="129" title="Contact form 1"]'); ?>
                     </div>    
                 </div>
            </div>
            <div class="iner-left-menubox" data-aos="fade-right" data-aos-duration="1200">
                <a href="javascript:void(0);" id="innermenu-click" class="innermenu-linkbox">
                   <span></span>
                   <span></span>
                   <span></span>
                </a>
                <ul class="cf in-responsive">
                    <li><a href="#go-aboutid">Course Summary</a></li>
                    <?php if(!empty($course_outline_title) && !empty($course_descriptions)){?>
                    <li><a href="#go-courseid">Course Outline</a></li>
                    <?php } 
                    if(!empty($learning_teaching_title) && !empty($learning_teaching_descriptions)){?>
                    <li><a href="#go-learning">Learning & Teaching</a></li>
                    <?php } 
                    if(!empty($career_possiblities_tiltle) && !empty($career_possiblities_descriptions)){?>
                    <li><a href="#go-career">Career Possiblities</a></li>
                    <?php } ?>
                    <li><a href="#go-rankid">Entry & Credit</a></li>
                    <li><a href="#go-reviewid">Reviews</a></li>
                </ul>
            </div>
        </div>
    </section>

    <section class="about-top-contebox" id="go-aboutid">
        <div class="wrapper cf">
            <div class="title-contentbox" data-aos="fade-right" data-aos-duration="600">
                <h1> Course Summary </h1>
                <?php
                    //$NumberFour = 4.1;
                    //$total = 5;
                   

                ?>
                <?php echo wpautop(get_the_content()); ?>
                <div class="hidecont-box">
                    <p>
                    If you are changing direction after starting in another career area, you will possess valuable work experience and professional awareness. Many employers actively seek candidates with these benefits. Our network of contacts, a dedicated employability service and a reputation in the legal profession mean we also have an outstanding track-record of finding students legal employment.Many employers favour GDL students in an increasingly competitive legal job market. Through studying another degree subject, you will have gained many transferable skills. By taking the conversion route into law, you are showing motivation and determination by choosing law at a later stage than some others. This looks good to employers.
                </p>
                <p>
                    If you are changing direction after starting in another career area, you will possess valuable work experience and professional awareness. Many employers actively seek candidates with these benefits. Our network of contacts, a dedicated employability service and a reputation in the legal profession mean we also have an outstanding track-record of finding students legal employment.Many employers favour GDL students in an increasingly competitive legal job market. Through studying another degree subject, you will have gained many transferable skills. By taking the conversion route into law, you are showing motivation and determination by choosing law at a later stage than some others. This looks good to employers.
                </p>    
                </div>
                <div class="seemore-btnbox" style="display:none;">
                    <a href="javascript:void(0);" class="seemore-btn" id="loadmore"><span class="plusicon"><i class="fa fa-plus-circle"></i>see more</span><span class="minus-icon"><i class="fa fa-minus-circle"></i>less more</span></a>
                </div>
            </div>
        </div>
    </section>
    <?php if(!empty($course_descriptions)) {?>

    <section class="course-section greybg" id="go-courseid">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2><?php echo $course_outline_title; ?></h2>
                <p><?php echo $course_outline_sort_descriptions; ?></p>
            </div>
            <div class="content-mainbox" data-aos="fade-left" data-aos-duration="1200">
                <?php echo $course_descriptions; ?>
            </div>
        </div>
    </section>
    <?php } ?>
    <?php if(!empty($learning_teaching_descriptions)) {?>
    <section class="course-section " id="go-learning">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2><?php echo $learning_teaching_title; ?></h2>
                <p><?php echo $learning_teaching_short_descriptions; ?></p>
            </div>
            <div class="content-mainbox" data-aos="fade-left" data-aos-duration="1200">
                <?php echo $learning_teaching_descriptions; ?>
            </div>
        </div>
    </section>
    <?php } ?>
    <?php if(!empty($career_possiblities_descriptions)) {?>
    <section class="course-section greybg" id="go-career">
    <div class="wrapper cf">
        <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
            <h2><?php echo $career_possiblities_tiltle; ?></h2>
            <p><?php echo $career_possiblities_short_descriptions; ?></p>
        </div>
        <div class="content-mainbox" data-aos="fade-left" data-aos-duration="1200">
            <?php echo $career_possiblities_descriptions; ?>
        </div>
    </div>
    </section>
    <?php } ?>



    <section class="course-section ranking-section" id="go-rankid">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2><?php echo $entry_credit_tiltle; ?></h2>
                <p><?php echo $entry_credit_short_descriptions; ?></p>
            </div>
            <div class="our-ranking-box cf" data-aos="fade-left" data-aos-duration="1200">
                <div class="ourrank-leftbox">
                    <h3>Average student ranking</h3>
                    <div class="cf">
                        <div class="school-detail-bottom all-reviebox cf">
                            <div class="rate-star">
                                <?php wp_star_rating($args); ?>


                                <a class="ratenum-like" href="javascript" title="like">
                                    <span class="rating-number-box"><?php echo $totalaverage; ?></span>
                                </a>
                            </div>
                            <div class="review-texts">
                                <span><?php echo $totalcoursereviws; ?> reviews</span>
                            </div>
                        </div>
                    </div>
                    <div class="cf rankcout-box">
                        <div class="leftrevie-box">
                            <span><?php echo $totalcoursereviws; ?></span>
                            <p>reviews</p>
                        </div>
                        <div class="leftcourserevie-box">
                            <span>137</span>
                            <p>educator reviews</p>
                        </div>
                    </div>
                    <div class="rows cf rankicon-boxs" >
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/campus-culcture.png" width="38" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-uparow.png" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-earlyicon.png" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-usericon.png" alt="">
                        </div>

                    </div>
                </div>


   <div id="chartContainer" class="ourrank-rightbox" ></div>
            </div>
        </div>
    </section>

    <section class="our-review-section" id="go-reviewid">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2><?php echo $reviews_title; ?></h2>
                <p><?php echo $reviews_descriptions;  ?></p>
            </div>
            <div class="ourreiv-blockbox" data-aos="fade-left" data-aos-duration="1200">
                <?php  foreach ($coursereview as $value) {
                    # code...
                   $userid          = $value->userid;
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                  // $user            = get_user_by( 'email',$useremail);
                   //print_r($user);
                   $firstname       =  $user_info->first_name;
                   $lastname        =$user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $descriptions    = $value->descriptions;
                   $overal_rating    = $value->overal_rating;
                  // echo get_avatar( $userid ); 

                   ?>
                <div class="reviwebox-block cf">
                    <div class="left-riveimg-box equal-height">
                        <span class="rie-imgicons"><?php echo get_avatar( $userid ,52); ?></span>
                        <div class="revrat-box">
                            <h4><?php echo $userlogin; ?></h4>
                            <div class="cf">
                                <div class="school-detail-bottom">

                                    <div class="rate-star">
                                            <?php if(!empty($overal_rating) ){
                                            $totalavaragereaming = 5;
                                            ?>
                                            <span class="like-count">
                                            <?php for($i=1; $i<=$overal_rating; $i++ ){ ?>
                                            <i class="fa fa-star"></i>
                                            <?php } ?>
                                            <?php for($i=$overal_rating; $i<$totalavaragereaming; $i++ ){ ?>
                                            <i class="fa fa-star-o"></i>
                                            <?php  } ?>
                                            </span>
                                            <?php  } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-rivecontbox equal-height">
                        <div class="table" >
                            <div class="table-cell">
                                <p><?php echo substr($descriptions,'0','172').'...'; ?><a href="" class="arrow-links">read more</a></p>
                            </div>
                        </div>
                        <a href="#" class="helpfull-btn">Helpful</a>
                    </div>
                </div>
                   <?php
                } ?>

                <div class="reviwebox-block cf">
                    <div class="left-riveimg-box equal-height">
                        <span class="rie-imgicons"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/review-imgicon.png"></span>
                        <div class="revrat-box">
                            <h4>Victoriya Markova</h4>
                            <div class="cf">
                                <div class="school-detail-bottom">
                                    <div class="rate-star">
                                        <span class="like-count">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-rivecontbox equal-height">
                        <div class="table">
                            <div class="table-cell">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation... <a href="" class="arrow-links">read more</a></p>
                            </div>
                        </div>
                        <a href="#" class="helpfull-btn">Helpful</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    // check if the repeater field has rows of data
    if( have_rows('gallery_videos') ):
        $video_title = get_field('video_title');
        $video_descriptions = get_field('video_descriptions');
        ?>
    <section class="course-section videogallery-section" id="go-videoid">
        <div class="wrapper cf">
            <div class="top-title-contbox aos-init" data-aos="fade-right" data-aos-duration="1200">
                <?php if(!empty($video_title)) { ?>
                <h2><?php echo $video_title;  ?></h2>
                <?php } ?>
                <?php if(!empty($video_descriptions )) {?>
                <p><?php echo $video_descriptions;  ?></p>
            <?php } ?>
            </div>
            <div class="videogally-box">
                <div class="slider slider_circle_10">
                <?php
                // check if the repeater field has rows of data

                // loop through the rows of data
                while ( have_rows('gallery_videos') ) : the_row();
                    ?>
                  <div>
                    <iframe width="619" height="313" src="<?php echo get_sub_field('add_video_url'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>

                <?php

                endwhile;


                // no rows found
                    ?>
        
                  <div>
                    <iframe width="619" height="313" src="<?php echo get_sub_field('add_video_url'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>

                  <div class="next_button"><i class="fa fa-angle-right"></i></div>  
                  <div class="prev_button"><i class="fa fa-angle-left"></i></div>  
                </div>
            </div> 
        </div>
    </section>

    <?php
    endif;
//SELECT `teching_quality` / sum(`teching_quality`) *100 as percenage FROM `ege_review`

    ?>
    <?php
        if($teching_quality_avrage != 0){
             $teching_quality_avrage_percentage = ( $teching_quality_avrage / 5 ) * 100;
         }
         if($school_facilites_avrage != 0){
             $school_facilites_percentage = ( $school_facilites_avrage / 5 ) * 100;
         }
         if($locations_avrage != 0){
             $locations_avrage_percentage = ( $locations_avrage / 5 ) * 100;
         }
         if($career_assistance_avrage != 0){
             $career_assistance_avrage_percentage = ( $career_assistance_avrage / 5 ) * 100;
         } 
        if($value_of_money_avrage != 0){
            $value_of_money_avrage_percentage = ( $value_of_money_avrage / 5 ) * 100;
        }

    ?>
    <?php
    $args = array(
   'rating' => 3.5,
   'type' => 'rating',
   'number' => 5,
);
?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/dist/canvasjs.min.js"></script>
    <script>
         jQuery(document).ready(function($) {
        jQuery('.course-name').children().val('<?php echo get_the_title(); ?>');
        //jQuery('.educator-name').val('<?php echo $universityname; ?>');
        jQuery('.course-name').children().attr('readonly','readonly');
        jQuery(".educator-wish-list").live("click", function(){
                //alert('Add To Wish');
                ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
                $thisclass = jQuery(this); 
                $wishclass = jQuery(this).data('wish');
               // alert($wishclass);
          

                $educatorid = <?php echo get_the_id(); ?>;
                $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                'action': 'course_wish_list',
                'courseid' : $educatorid,
                'wishdata'   : $wishclass,
                },
                success:function(data) {
                // This outputs the result of the ajax request
               // alert(data);
                if($wishclass == 'add'){

                    jQuery($thisclass).children().removeClass('fa-heart');
                    jQuery($thisclass).children().addClass('fa-heart-o');
                    jQuery($thisclass).data('wish','remove');
                }else{

                    jQuery($thisclass).children().addClass('fa-heart');
                    jQuery($thisclass).children().removeClass('fa-heart-o');
                    jQuery($thisclass).data('wish','add');


                }


                },
                error: function(errorThrown){
                console.log(errorThrown);
                }

                });        
    
     }); 

    });    
    window.onload = function () {

     CanvasJS.addColorSet("greenShades",
                [//colorSet Array

                "#9eedf0",
                "#b0c2d9",
                "#ffcbbb",
                "#7ecd97",
                "#dd9acf"                
                ]);
    var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: false,
    colorSet: "greenShades",
    theme: "light1", // "light1", "light2", "dark1", "dark2"
toolTip:{
   contentFormatter: function ( e ) {
               return "<div class='main-chart-id'><span>" + e.entries[0].dataPoint.label +"</span><p>"+e.entries[0].dataPoint.y +"%</p><div class='ad  ada'>" +  e.entries[0].dataPoint.html + "</div>";  
   }  
 },
    axisX:{
        labelFontSize: 15,
        labelFontColor: "black",
        gridColor: "orange",

        //labelFontWeight: "bold"

      },
    axisY: {
    suffix: "%",
    labelFontSize: 15,
    labelFontColor: "black",
    scaleBreaks: {
    customBreaks: [{
    startValue: 100,
    endValue: 100
    }]
    },
    includeZero: false
    },
    data: [{
    type: "column",
    fontSize: 25,
    yValueFormatString: "#,##0\"%\"",
  
    // toolTipContent : '<div class="main-chart-id">{label}: {y} <div class="main-chart"> <span class="like-count-graph">Average rate = </span><b>{xa}</b></div> {html}</div>',

    dataPoints: [
    { label: "Teaching quality", y:<?php echo $teching_quality_avrage_percentage; ?>,xa:<?php echo $teching_quality_avrage; ?>,html:'<?php  wp_star_rating_chart($teching_quality_avrage); ?>' },
    { label: "School Facilities", y: <?php echo $school_facilites_percentage; ?>,xa:<?php echo $school_facilites_avrage; ?>,html:'<?php  wp_star_rating_chart($school_facilites_avrage); ?>'},
    { label: "Location", y: <?php echo $locations_avrage_percentage; ?>,xa:<?php echo $locations_avrage; ?>,html:'<?php  wp_star_rating_chart($locations_avrage); ?>'},
    { label: "Carrer assistance", y: <?php echo $career_assistance_avrage_percentage; ?>,xa:<?php echo $career_assistance_avrage; ?>,html:'<?php  wp_star_rating_chart($career_assistance_avrage); ?>'},
    { label: "Value for money", y: <?php echo $value_of_money_avrage_percentage; ?> ,xa:<?php echo $value_of_money_avrage; ?>,html:'<?php  wp_star_rating_chart($value_of_money_avrage); ?>'},

    ]
    }],

    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1    
    });
    chart.render();


    }
    </script>

<?php get_footer();
 