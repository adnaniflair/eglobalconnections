<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage eglobaleducation
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function eglobaleducation_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/eglobaleducation
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'eglobaleducation' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'eglobaleducation' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'eglobaleducation-featured-image', 2000, 1200, true );

	add_image_size( 'eglobaleducation-thumbnail-avatar', 100, 100, true );




	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'eglobaleducation' ),
		'social' => __( 'Social Links Menu', 'eglobaleducation' ),
		'abouts-us' => __( 'About Us Links Menu', 'eglobaleducation' ),
		'Policies' => __( 'Policies Links Menu', 'eglobaleducation' ),
		'Contact-us' => __( 'Contact Us Links Menu', 'eglobaleducation' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', eglobaleducation_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'eglobaleducation' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'eglobaleducation' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'eglobaleducation' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'eglobaleducation' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'eglobaleducation' ),
				'items' => array(
					'link_yelp',
					'link_facecourse',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	/**
	 * Filters Twenty Seventeen array of starter content.
	 *
	 * @since Twenty Seventeen 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'eglobaleducation_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'eglobaleducation_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function eglobaleducation_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( eglobaleducation_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Twenty Seventeen content width of the theme.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'eglobaleducation_content_width', $content_width );
}
add_action( 'template_redirect', 'eglobaleducation_content_width', 0 );

/**
 * Register custom fonts.
 */
function eglobaleducation_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'eglobaleducation' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function eglobaleducation_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'eglobaleducation-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'eglobaleducation_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function eglobaleducation_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'eglobaleducation' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'eglobaleducation' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'eglobaleducation' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'eglobaleducation' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'eglobaleducation' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'eglobaleducation' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'eglobaleducation_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function eglobaleducation_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'eglobaleducation' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'eglobaleducation_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function eglobaleducation_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'eglobaleducation_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function eglobaleducation_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'eglobaleducation_pingback_header' );

/**
 * Display custom color CSS.
 */
function eglobaleducation_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo eglobaleducation_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'eglobaleducation_colors_css_wrap' );

/**
 * Enqueue scripts and styles.
 */
function eglobaleducation_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'eglobaleducation-fonts', eglobaleducation_fonts_url(), array(), null );

	// Theme stylesheet.
	wp_enqueue_style( 'eglobaleducation-style', get_stylesheet_uri() );

	// Load the dark colorscheme.


	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.


	// Load the Internet Explorer 8 specific stylesheet.

	wp_style_add_data( 'eglobaleducation-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'eglobaleducation-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$eglobaleducation_l10n = array(
		'quote'          => eglobaleducation_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	// if ( has_nav_menu( 'top' ) ) {
	// 	wp_enqueue_script( 'eglobaleducation-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
	// 	$eglobaleducation_l10n['expand']         = __( 'Expand child menu', 'eglobaleducation' );
	// 	$eglobaleducation_l10n['collapse']       = __( 'Collapse child menu', 'eglobaleducation' );
	// 	$eglobaleducation_l10n['icon']           = eglobaleducation_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );
	// }


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'eglobaleducation_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function eglobaleducation_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'eglobaleducation_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function eglobaleducation_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'eglobaleducation_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function eglobaleducation_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'eglobaleducation_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function eglobaleducation_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'eglobaleducation_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Seventeen 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function eglobaleducation_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'eglobaleducation_widget_tag_cloud_args' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
//require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );



add_action( 'init', 'ege_course_init' );
/**
 * Register a course post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function ege_course_init() {
	$labels = array(
		'name'               => _x( 'Courses', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Course', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Courses', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Course', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'course', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Course', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Course', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Course', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Course', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Courses', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Courses', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Courses:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No courses found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No courses found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'course' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'course', $args );
		// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Educators', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'educator', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Educators', 'textdomain' ),
		'popular_items'              => __( 'Popular Educators', 'textdomain' ),
		'all_items'                  => __( 'All Educators', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit educator', 'textdomain' ),
		'update_item'                => __( 'Update educator', 'textdomain' ),
		'add_new_item'               => __( 'Add New educator', 'textdomain' ),
		'new_item_name'              => __( 'New educator Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Educators with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove Educators', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Educators', 'textdomain' ),
		'not_found'                  => __( 'No Educators found.', 'textdomain' ),
		'menu_name'                  => __( 'Educators', 'textdomain' ),
	);
	
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'educators' ),
	);


	register_taxonomy( 'educators', 'course', $args );



	$labels = array(
		'name'                       => _x( 'Subjects', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Subject', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search subjects', 'textdomain' ),
		'popular_items'              => __( 'Popular subjects', 'textdomain' ),
		'all_items'                  => __( 'All Subjects', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Subject', 'textdomain' ),
		'update_item'                => __( 'Update Subject', 'textdomain' ),
		'add_new_item'               => __( 'Add New Subject', 'textdomain' ),
		'new_item_name'              => __( 'New subject Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Subjects with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove subjects', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used subjects', 'textdomain' ),
		'not_found'                  => __( 'No subjects found.', 'textdomain' ),
		'menu_name'                  => __( 'subjects', 'textdomain' ),
	);
	
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'subject' ),
	);


	register_taxonomy( 'subject', 'course', $args );
	    $labels = array(
        'name'                       => _x( 'Study Levels', 'taxonomy general name', 'textdomain' ),
        'singular_name'              => _x( 'Study Level', 'taxonomy singular name', 'textdomain' ),
        'search_items'               => __( 'Search Study Levels', 'textdomain' ),
        'popular_items'              => __( 'Popular Study Levels', 'textdomain' ),
        'all_items'                  => __( 'All Study Levels', 'textdomain' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Study Level', 'textdomain' ),
        'update_item'                => __( 'Update Study Level', 'textdomain' ),
        'add_new_item'               => __( 'Add New Study Level', 'textdomain' ),
        'new_item_name'              => __( 'New Study Level Name', 'textdomain' ),
        'separate_items_with_commas' => __( 'Separate Study Levels with commas', 'textdomain' ),
        'add_or_remove_items'        => __( 'Add or remove Study Levels', 'textdomain' ),
        'choose_from_most_used'      => __( 'Choose from the most used Study Levels', 'textdomain' ),
        'not_found'                  => __( 'No Study Levels found.', 'textdomain' ),
        'menu_name'                  => __( 'Study Levels', 'textdomain' ),
    );
    
    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'study-level' ),
    );


    register_taxonomy( 'studylevel', 'course', $args );

        $labels = array(
        'name'                       => _x( 'Educator type', 'taxonomy general name', 'textdomain' ),
        'singular_name'              => _x( 'Educator type', 'taxonomy singular name', 'textdomain' ),
        'search_items'               => __( 'Search Educator type', 'textdomain' ),
        'popular_items'              => __( 'Popular Educator type', 'textdomain' ),
        'all_items'                  => __( 'All Educator type', 'textdomain' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Educator type', 'textdomain' ),
        'update_item'                => __( 'Update Educator type', 'textdomain' ),
        'add_new_item'               => __( 'Add New Educator type', 'textdomain' ),
        'new_item_name'              => __( 'New Educator type Name', 'textdomain' ),
        'separate_items_with_commas' => __( 'Separate Educator type with commas', 'textdomain' ),
        'add_or_remove_items'        => __( 'Add or remove Educator type', 'textdomain' ),
        'choose_from_most_used'      => __( 'Choose from the most used Educator type', 'textdomain' ),
        'not_found'                  => __( 'No Educator type found.', 'textdomain' ),
        'menu_name'                  => __( 'Educator type', 'textdomain' ),
    );
    
    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'educator-type' ),
    );


    register_taxonomy( 'educator-type', 'educator', $args );
}





//Theme Options Add

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    // acf_add_options_sub_page(array(
    //     'page_title' 	=> 'Theme Header Settings',
    //     'menu_title'	=> 'Header',
    //     'parent_slug'	=> 'theme-general-settings',
    // ));

    // acf_add_options_sub_page(array(
    //     'page_title' 	=> 'Theme Footer Settings',
    //     'menu_title'	=> 'Footer',
    //     'parent_slug'	=> 'theme-general-settings',
    // ));

}

add_image_size( 'gallery-size', 298, 205, true );
add_image_size( 'bannerimage-educators-list', 555, 168, true );
add_image_size( 'bannerimage-courses-list',360,195, true );
add_image_size( 'country-flag',23,16, true );

add_action( 'init', 'ege_university_init' );
/**
 * Register a educator post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function ege_university_init() {
	$labels = array(
		'name'               => _x( 'Educators', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'educator', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Educators', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'educator', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'educator', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New educator', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New educator', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit educator', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View educator', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Educators', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Educators', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Educators:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Educators found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Educators found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'educator' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
	register_post_type( 'educator', $args );


	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Countries', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Country', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search countries', 'textdomain' ),
		'popular_items'              => __( 'Popular countries', 'textdomain' ),
		'all_items'                  => __( 'All countries', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Country', 'textdomain' ),
		'update_item'                => __( 'Update Country', 'textdomain' ),
		'add_new_item'               => __( 'Add New Country', 'textdomain' ),
		'new_item_name'              => __( 'New Country Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate countries with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove countries', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used countries', 'textdomain' ),
		'not_found'                  => __( 'No countries found.', 'textdomain' ),
		'menu_name'                  => __( 'Countries', 'textdomain' ),
	);
	
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'Country' ),
	);


	register_taxonomy( 'Country', 'educator', $args );

}
// University Data Sections Fileds
function educators_data_sections_request() {
 
    // The $_REQUEST contains all the data sent via ajax
    if ( isset($_REQUEST) ) {
						$universityposts = get_posts(
						array(
						'post_type' => 'educator', // Post type
						'tax_query' => array(
						array(
						'taxonomy' => 'Country', // Taxonomy name
						'field' => 'slug',
						'terms' => $_REQUEST['categoryname']
						)
						),
						'posts_per_page' => 2,
						'orderby'       => 'menu_order',
						'order'         => 'ASC'
						)
						);	
    					if(!empty($universityposts)){
						//print_r($universityposts);
						foreach ($universityposts as $postvalue) {
							# code...
							$universityid    =  $postvalue->ID;
							$universitytitle = $postvalue->post_title;
							$universitycontent = $postvalue->post_content;
							$terms  = get_the_terms($universityid,'Country');
							$countryname = $terms[0]->name;
							$countview  = pvc_get_post_views($universityid);
							if(empty($countview)){
								$countview = 0;
							}
							//print_r($terms);	
							?>
										<div class="school-detail-block">
					<div class="school-image">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/lsc.png">
						<a href="#" title="like" class="like-ico">
							<i class="fa fa-heart"></i>
						</a>
						<div class="image-overlay">
							<div class="image-overlay-inner">
								<?php if(!empty($universitycontent)){ ?>
								<p><?php echo $universitycontent;  ?></p>
								<?php } ?>
								<span>there is a course in english</span>
							</div>
						</div>
					</div>
					<div class="school-details-wrap">
						<a href="<?php echo get_permalink($universityid); ?>"><h3><?php echo $universitytitle; ?></h3></a>
						<span class="school-flag">
							<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/school-flag.png">
							<p><?php echo $countryname; ?></p>
						</span>
						<div class="school-detail-bottom">
							<div class="rate-star">
								<span class="like-count">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
								</span>
								<a class="rate-like" href="javascript" title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
							</div>
							<div class="world-ranks">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/graph-ico.png">
								<p>World Ranking:124</p>
							</div>
							<div class="views-count">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png">
								<p>Views:<?php echo $countview; ?></p>
							</div>
						</div>
					</div>
					</div>
    <?php } }else{
    		echo "empty";
    } }
    // Always die in functions echoing ajax content
   wp_die();
}


// University Data Sections Filedsasas
/*
function educators_data_sections_request_two() {
 
    // The $_REQUEST contains all the data sent via ajax
    if ( isset($_REQUEST) ) {
						$universityposts = get_posts(
						array(
						'post_type' => 'educator', // Post type
						'tax_query' => array(
						array(
						'taxonomy' => 'Country', // Taxonomy name
						'field' => 'slug',
						'terms' => $_REQUEST['categoryname']
						)
						),
						'posts_per_page' => 2,
						'orderby'       => 'menu_order',
						'order'         => 'ASC'
						)
						);	
    					if(!empty($universityposts)){
						//print_r($universityposts);
						foreach ($universityposts as $postvalue) {
							# code...
							$universityid    =  $postvalue->ID;
							$universitytitle = $postvalue->post_title;
							$universitycontent = $postvalue->post_content;
							$terms  = get_the_terms($universityid,'Country');
							$countryname = $terms[0]->name;
							$countview  = pvc_get_post_views($universityid);
							if(empty($countview)){
								$countview = 0;
							}
							//print_r($terms);	
							?>
										<div class="school-detail-block">
					<div class="school-image">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/lsc.png">
						<a href="#" title="like" class="like-ico">
							<i class="fa fa-heart"></i>
						</a>
						<div class="image-overlay">
							<div class="image-overlay-inner">
								<?php if(!empty($universitycontent)){ ?>
								<p><?php echo $universitycontent;  ?></p>
								<?php } ?>
								<span>there is a course in english</span>
							</div>
						</div>
					</div>
					<div class="school-details-wrap">
						<a href="<?php echo get_permalink($universityid); ?>"><h3><?php echo $universitytitle; ?></h3></a>
						<span class="school-flag">
							<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/school-flag.png">
							<p><?php echo $countryname; ?></p>
						</span>
						<div class="school-detail-bottom">
							<div class="rate-star">
								<span class="like-count">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
								</span>
								<a class="rate-like" href="javascript" title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
							</div>
							<div class="world-ranks">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/graph-ico.png">
								<p>World Ranking:124</p>
							</div>
							<div class="views-count">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png">
								<p>Views:<?php echo $countview; ?></p>
							</div>
						</div>
					</div>
					</div>
    <?php } }else{
    		echo "empty";
    } }
    // Always die in functions echoing ajax content
   wp_die();
}
*/
 
add_action( 'wp_ajax_educators_data_sections', 'educators_data_sections_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_educators_data_sections', 'educators_data_sections_request' );


//define in function file
function custom_pagination($numpages = '', $pagerange = '', $paged='')  {

	if (empty($pagerange)) {
	$pagerange = 2;
	}

	if ($paged == '') {
	global $paged;
		if (empty($paged)) {
		$paged = 1;
		}
	}
	if ($numpages == '') {
	global $wp_query;
		$numpages = $wp_query->max_num_pages;
		if(!$numpages) {
		$numpages = 1;
		}
	}
	    
	$pagination_args = array(
	'base'            => get_pagenum_link(1) . '%_%',
	'format' => '?page=%#%',
	'total'           => $numpages,
	'current'         => $paged,
	'show_all'        => false,
	'end_size'        => 1,
	'mid_size'        => $pagerange,
	'prev_next'       => true,
	'prev_text'       => __('Previous'),
	'next_text'       => __('Next'),
	'type'            => 'list',
	'add_args'        => true,
	'add_fragment'    => '',
	'after_page_number' => '',
	'before_page_number' =>'',
	);
	$paginate_links = paginate_links_two($pagination_args);

	if ( $paginate_links ) {
	    echo $paginate_links;
	}
}


function paginate_links_two( $args = '' ) {
	global $wp_query, $wp_rewrite;

	// Setting up default values based on the current URL.
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$url_parts    = explode( '?', $pagenum_link );

	// Get max pages and current page out of the current query, if available.
	$total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
	$current = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;

	// Append the format placeholder to the base URL.
	$pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

	// URL base depends on permalink settings.
	$format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

	$defaults = array(
		'base'               => $pagenum_link, // http://example.com/all_posts.php%_% : %_% is replaced by format (below)
		'format'             => $format, // ?page=%#% : %#% is replaced by the page number
		'total'              => $total,
		'current'            => $current,
		'aria_current'       => 'page',
		'show_all'           => false,
		'prev_next'          => true,
		'prev_text'          => __( '&laquo; Previous' ),
		'next_text'          => __( 'Next &raquo;' ),
		'end_size'           => 1,
		'mid_size'           => 2,
		'type'               => 'plain',
		'add_args'           => array(), // array of query args to add
		'add_fragment'       => '',
		'before_page_number' => '',
		'after_page_number'  => '',
	);

	$args = wp_parse_args( $args, $defaults );

	if ( ! is_array( $args['add_args'] ) ) {
		$args['add_args'] = array();
	}

	// Merge additional query vars found in the original URL into 'add_args' array.
	if ( isset( $url_parts[1] ) ) {
		// Find the format argument.
		$format = explode( '?', str_replace( '%_%', $args['format'], $args['base'] ) );
		$format_query = isset( $format[1] ) ? $format[1] : '';
		wp_parse_str( $format_query, $format_args );

		// Find the query args of the requested URL.
		wp_parse_str( $url_parts[1], $url_query_args );

		// Remove the format argument from the array of query arguments, to avoid overwriting custom format.
		foreach ( $format_args as $format_arg => $format_arg_value ) {
			unset( $url_query_args[ $format_arg ] );
		}

		$args['add_args'] = array_merge( $args['add_args'], urlencode_deep( $url_query_args ) );
	}

	// Who knows what else people pass in $args
	$total = (int) $args['total'];
	if ( $total < 2 ) {
		return;
	}
	$current  = (int) $args['current'];
	$end_size = (int) $args['end_size']; // Out of bounds?  Make it the default.
	if ( $end_size < 1 ) {
		$end_size = 1;
	}
	$mid_size = (int) $args['mid_size'];
	if ( $mid_size < 0 ) {
		$mid_size = 2;
	}
	$add_args = $args['add_args'];
	$r = '';
	$page_links = array();
	$dots = false;

	if ( $args['prev_next'] && $current && 1 < $current ) :
		$link = str_replace( '%_%', 2 == $current ? '' : $args['format'], $args['base'] );
		$link = str_replace( '%#%', $current - 1, $link );
		if ( $add_args )
			$link = add_query_arg( $add_args, $link );
		$link .= $args['add_fragment'];

		/**
		 * Filters the paginated links for the given archive pages.
		 *
		 * @since 3.0.0
		 *
		 * @param string $link The paginated link URL.
		 */
		$page_links[] = '<a class="prev-link nav-links" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['prev_text'] . '</a>';
	endif;
	for ( $n = 1; $n <= $total; $n++ ) :
		if ( $n == $current ) :
			$page_links[] = "<span aria-current='" . esc_attr( $args['aria_current'] ) . "' class='page-numbers current'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</span>";
			$dots = true;
		else :
			if ( $args['show_all'] || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :
				$link = str_replace( '%_%', 1 == $n ? '' : $args['format'], $args['base'] );
				$link = str_replace( '%#%', $n, $link );
				if ( $add_args )
					$link = add_query_arg( $add_args, $link );
				$link .= $args['add_fragment'];

				/** This filter is documented in wp-includes/general-template.php */
				$page_links[] = "<a class='nav-links page-numbers' data-page='".number_format_i18n( $n )."'  href='javascript:void(0)'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a>";
				$dots = true;
			elseif ( $dots && ! $args['show_all'] ) :
				$page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';
				$dots = false;
			endif;
		endif;
	endfor;
	if ( $args['prev_next'] && $current && $current < $total ) :
		$link = str_replace( '%_%', $args['format'], $args['base'] );
		$link = str_replace( '%#%', $current + 1, $link );
		if ( $add_args )
			$link = add_query_arg( $add_args, $link );
		$link .= $args['add_fragment'];

		/** This filter is documented in wp-includes/general-template.php */
		$page_links[] = '<a class="nav-links next-link" href="javascript:void(0)">' . $args['next_text'] . '</a>';
	endif;
	switch ( $args['type'] ) {
		case 'array' :
			return $page_links;

		case 'list' :
			$r .= "<ul class='pagination-nav cf'>\n\t<li>";
			$r .= join("</li>\n\t<li>", $page_links);
			$r .= "</li>\n</ul>\n";
			break;

		default :
			$r = join("\n", $page_links);
			break;
	}
	return $r;
}


// Course Listing Page ajax Funtions.

add_action( 'wp_ajax_course_listing_sections', 'ege_course_listing_sections' );
add_action( 'wp_ajax_nopriv_course_listing_sections', 'ege_course_listing_sections' );

function ege_course_listing_sections(){
			//print_r($queried_object);
				session_start();
				$_SESSION['studylevel'] 	 = $_REQUEST['studylevel'];	
				$_SESSION['educatorid'] 	 = $_REQUEST['educatorname'];
				$_SESSION['termid']			 = $_REQUEST['termid'];	
			    $_SESSION['countrydropdown'] = $_REQUEST['countrydropdown'];	

	

			   $sortbyvalue =  $_REQUEST['sortbyvaluedropdown'];
			   if(!empty($sortbyvalue)){
				$metavalue = '';
				$ordervalueby   = 'date';
				$orderby  ='ASC';
			   	if($sortbyvalue == 'price'){
			   		$metavalue = 'fees_per_year';
			   		$ordervalueby   = 'meta_value_num';
			   		$orderby  ='ASC';
			   	}
			   	if($sortbyvalue == 'durations'){
					$metavalue = 'time_durations';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='ASC';
			   	}
			   	if($sortbyvalue == 'popularity'){
			   		$metavalue = 'post_views_counter';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='DESC';	
			   	}

			   	if($sortbyvalue == 'worldrank'){
			   		$metavalue = 'world_rank';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='DESC';	
			   	}
				if($sortbyvalue == 'scholarship'){
					$metavalue = 'scholarship_provide';
					$ordervalueby   = 'meta_value';
					$orderby  ='DESC';	
				}



			   }
	           if(!empty($_REQUEST['termid'])){
					$term_id = $_REQUEST['termid'];
					$term = get_term_by( 'id', $term_id, 'subject');
					//print_r($term);

					$termsname = $term->name;
					//$termdescription = $term->description;
					if(!empty($term)){
					$subject_array = array(
									'taxonomy' => 'subject',
									'field'    => 'slug',
									'terms' => $term->slug
									);
					}
				}
			   $paged = $_REQUEST['page'];
			   $studylevelid = $_REQUEST['studylevel'];
			   $educatorname = $_REQUEST['educatorname'];
			   $educator_array = array();
			   if(!empty($educatorname)){
				   	$educator_array = array(  
					'key' => 'select_university',
					'value' =>$educatorname,
					'compare' => "LIKE",
					);
				}
			   //echo $studylevelid;
			    $studylevel = get_term_by( 'id',$studylevelid, 'studylevel');
				if(!empty($studylevel)){
					$subject_array = array(
					'taxonomy' => 'studylevel',
					'field'    => 'slug',
					'terms' => $studylevel->slug
					);
				}

    //$studylevel = '';
				if(!empty($term) && !empty($studylevel)) {
				     $subject_array = array( // (array) - use taxonomy parameters (available with Version 3.1).
					'relation' => 'AND', 
					array(
					  'taxonomy' => 'subject', // (string) - Taxonomy.
					  'field' => 'slug', // (string) - Select taxonomy term by Possible 
					  'terms' => $term->slug // (int/string/array) - Taxonomy term(s).
			
					),
					array(
					  'taxonomy' => 'studylevel',
					  'field' => 'slug',
					  'terms' => $studylevel->slug,
					)
				 );

				}

				// Countrt wise Dropdown
			    $countrydropdown = trim($_REQUEST['countrydropdown']);
				if(!empty($countrydropdown)){

					$universitypostss = get_posts(
						array(
						'post_type' => 'educator', // Post type
						'tax_query' => array(
						array(
						'taxonomy' => 'Country', // Taxonomy name
						'field' => 'slug',
						'terms' => $countrydropdown
						)
						),
						'posts_per_page' => 2,
						'orderby'       => 'menu_order',
						'order'         => 'ASC'
						)
					);  
  			
					// echo "<pre>";
					// print_r($universitypostss);
					// echo "</pre>";
					foreach ($universitypostss as $educator) {
					# code...
					$educatorid[] =  $educator->ID;

					}
					if(empty($educator_array )){
						$educator_array = array(  
						'key' => 'select_university',
						'value' =>$educatorid,
						'compare' => "IN",
						);
					}
				}	
			   //$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

			   //echo $paged;

					// $course_detail = array(
					// 'post_type' => 'post',
					// 'post_status' => 'publish',
					// 'posts_per_page' =>1,
					// 'order' => 'ASC',
					// //'orderby' =>'date',
					// 'paged' => $paged
					// );
			
				$the_query = new WP_Query( array(
					'posts_per_page' => 10,
					'paged' => $paged,
					'post_type'		=>'course',
					'tax_query' => array($subject_array),
					'meta_query' => array($educator_array),
					'meta_key' => $metavalue,
					'order' => $orderby,
                    'orderby' => $ordervalueby,
		 
					)
				);

				// print_r( array(
				// 	'posts_per_page' => 10,
				// 	'order' => 'ASC',
				// 	//'orderby' =>'date',
				// 	'paged' => $paged,
				// 	'post_type'		=>'course',
				// 	'tax_query' => array($subject_array),
				// 	'meta_query' => array($educator_array),
		 
				// 	));

				$queried_object = get_queried_object();
			    $term_id = $queried_object->term_id;
				$term = get_term_by( 'id', $term_id, 'subject');
				//print_r($term);
				// $the_query = new WP_Query( array(
				// 'posts_per_page' =>4,
				// 'post_type'		=>'course',
				// 	 )	
				// ); 
				if($the_query -> have_posts()){
				?>
		            <div class="loading overlay-bgcolor" style="display:none">
            <div class="all-middleicon-loader">  <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect"></div></div>
				<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
				        <?php
				       	//echo get_the_id();
				       // echo "<br>";
						 $unvisityid 	 = get_field('select_university',get_the_id());
						 $universityname = get_the_title($unvisityid);
						 $terms  		 = get_the_terms($unvisityid,'Country');
						if(!empty($terms)){
						$countryname =  $termsname = $terms[0]->name;
						$termsid     = $terms[0]->term_id;
						$countryflag = ege_countryflagurl($termsid);
						}
						 //$countryname    =  $termsname = $terms[0]->name;
						 $feesperyear    = get_field('fees_per_year',get_the_id());
				 		 $time_durations = get_field('time_durations',get_the_id());
 				 		 $deadline_date  = get_field('deadline_date',get_the_id());
				 		 $language       = get_field('language',get_the_id());
 				 		 $world_rank     = get_field('world_rank',get_the_id());
                         $scholarshipprovide = get_field('scholarship_provide',get_the_id());

						 $countview      = pvc_get_post_views(get_the_id());
						 if(empty($countview)){
						 	 $countview = 0;
						 }
						 $studylevelname = '';
						 $studylevel   = wp_get_post_terms(get_the_id(),'studylevel');
						// echo $studylevel[0]['term_id'];
						 if(!empty($studylevel)){
						 $studylevelname = $studylevel[0]->name;
						 } 
					  // echo $universityname;
                    	?>


				                <div class="three-block">
                    <div class="white-bg ourcourse-block">
                        <div class="course-imgblock">
                            <span class="imgmain-box"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/course-block-img1.png" alt="" /></span>

                                     <div class="bachlabl-leftbox">
                            <?php if(!empty($studylevelname)){ ?>
                            <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt=""><p><?php echo $studylevelname; ?></p></span>
                            <?php } ?>
                            <?php if(!empty($scholarshipprovide)) { ?>
                             <span class="pre-bachelr-label pre-scholarship"><p><?php echo 'Scholarship'; ?></p></span>

                         <?php } ?>
                               </div>

                           <?php echo do_action('course_short_list_actions'); ?>

        					<?php /*	          
                            <span class="course-heart-icon">
                                <a href="javascript:void(0);" class="course-heart-click"><i class="fa fa-heart"></i></a>
                                <div class="addtoshort-btn"><a href="#">Add to shortlist</a></div>
                            </span>
                            */ ?>

                            <div class="timeline-flowbox">
                                <div class="rows cf">
                                     <?php if(!empty($time_durations)) { ?>
                                    <div class="three-block timelinebox">
                                        <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/duration-icons.png" alt="" /></span>
                                        <p>Duration<br><?php echo $time_durations; ?> Years</p>
                                    </div>
	                                <?php } ?>
                                    <?php if(!empty($deadline_date)) { ?>
                                    <div class="three-block timelinebox">
                                        <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/deadline-icon.png" alt="" /></span>
                                        <p>Deadline<br> <?php echo  $deadline_date; ?> </p>
                                    </div>
	                                <?php } ?>
                                    <?php if(!empty($language)) { ?>
                                    <div class="three-block timelinebox">
                                        <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/language-earth-icon.png" alt="" /></span>
                                        <p>Language<br> <?php echo $language; ?> </p>
                                    </div>
	                                <?php } ?>

                                </div>
                            </div>
                            <div class="inner-titwid-img">
                                <div class="min-imgbox"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/small-leftimg.jpg" width="65" /></div>
                                <div class="ourcourse-contbox cf">
                                    <h4><?php echo $universityname; ?></h4>
                                    <div class="fulcour-flatbox">
                                        <?php if(!empty($countryname)) { ?>

                                        <span class="small-flg-span">
                                            <?php if(!empty($countryflag)) {?>
                                            <img src="<?php echo $countryflag ?>" width="18" />
                                            <?php } ?>
                                            <p><?php echo  $countryname; ?></p>
                                        </span>
                                        <?php } ?>
                                        <?php  if(!empty($world_rank)){ ?>
                                        <span class="ratio-icon-box"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/ratioup-arrow.png" width="18" /><p><?php echo  $world_rank; ?></p></span>
	                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="our-courseblk-content">
                            <a href="<?php echo get_permalink(); ?> "><h3><?php echo get_the_title(); ?> </h3></a>
                            <div class="cf">
                                <div class="school-detail-bottom">
                                    <div class="rate-star">
                                        <span class="like-count">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a class="ratenum-like" href="javascript" title="like">
                                            <span class="rating-number-box">4</span>
                                        </a>
                                    </div>
                                    <div class="review-texts">
                                        <span>135 reviews</span>
                                    </div>
                                </div>
                            </div>
                            <div class="year-view-box">
                                <?php if(!empty($feesperyear)){?>
                                <span class="rate-leftbox">From <i>£<?php echo $feesperyear; ?> </i>per year</span>
                            <?php } ?>

                                <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png" />views: <i><?php echo $countview; ?></i></span>
                            </div>
                        </div>
                    </div>
                </div>	

				<?php

				endwhile;
				
				//$big = 999999999; // need an unlikely integer
				// echo paginate_links( array(
				// //'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
				// 'base' => add_query_arg('page','%#%'),
				// 'format' => '?page=%#%',
				// 'current' => max( 1, get_query_var('page') ),
				// 'total' => $the_query->max_num_pages
				// ) );
				}else{
					echo "No Courses Founds";
				}

				wp_reset_postdata(); ?>
				                <div class="fullwidth-block">
                <div class="page-linkox cf">
                        <?php
                        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                        /*
                        ?>
                        <li><a class="prev-link nav-links" href="#">Previous</a></li>
                        <li><a class="nav-links current" href="#">1</a></li>
                        <li><a class="nav-links" href="#">2</a></li>
                        <li><a class="nav-links" href="#">3</a></li>
                        <li><a class="nav-links" href="#">4</a></li>
                        <li><a class="nav-links next-link" href="#">Next</a></li>
                        <?php  */ ?>
                </div>
                </div>
                <?php

	wp_die();
}

add_action( 'wp_ajax_educators_listing_sections', 'ege_educators_listing_sections' );
add_action( 'wp_ajax_nopriv_educators_listing_sections', 'ege_educators_listing_sections' );

function ege_educators_listing_sections(){

			$_SESSION['educatorcountrydropdown']   	= $_REQUEST['countrydropdown'];
			$_SESSION['educatoreducatortypeid']   	= $_REQUEST['educatortypeid'];
			$_SESSION['educatortermid']  			= $_REQUEST['termid'];
			$_SESSION['educatoreducatortype']   	= $_REQUEST['educatortype'];


			if(!empty($_REQUEST['educatortypeid'])){
				 $educatorid     = $_REQUEST['educatortypeid'];
				 $educators_list_unique = array($educatorid);
			}
			if(!empty($_REQUEST['termid'])){
				 $educators_list = array(1);
				$term = get_term_by( 'id',$_REQUEST['termid'], 'subject');
				if(!empty($term)){
					$subject_array = array(
					    'taxonomy' => 'subject',
					    'field'    => 'slug',
					    'terms' => $term->slug
					    );
					}
					$universityposts = get_posts(
					    array(
					    'posts_per_page' => -1,
					    'order' => 'ASC',
					    //'orderby' =>'date',
					    'paged' => $paged,
					    'post_type'     =>'course',
					    'tax_query' => array($subject_array),
					    )
					);
					if(!empty($universityposts)){ 
					    $unvisityid = array();
					    foreach ($universityposts as $postvalue) {
					        # code...
					         $courseid     =  $postvalue->ID;
					         $universityid = get_field('select_university',$courseid);

					         //print_r($studylevel); 
					         if(!empty( $universityid)){
					           $unvisityid[]   = $universityid;
					         }
					         //echo '<br>';
					    //echo $universityname = get_the_title($unvisityid);
					    }
					}
					if(!empty($unvisityid)){  
					    $educators_list = (array_unique($unvisityid));
					}
			
			}

			$sortbyvalue =  $_REQUEST['sortbyvaluedropdown'];
			if(!empty($sortbyvalue)){
				$metavalue = '';
				$ordervalueby   = 'date';
				$orderby  ='ASC';
				if($sortbyvalue == 'price'){
					$metavalue = 'fees_per_year';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='ASC';
				}
				if($sortbyvalue == 'durations'){
					$metavalue = 'post_views_counter';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='ASC';
				}
				if($sortbyvalue == 'popularity'){
					$metavalue = 'post_views_counter';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='DESC';	
				}
			  if($sortbyvalue == 'worldrank'){
			   		$metavalue = 'world_rank';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='ASC';	
			   	}
			}
			$paged = $_REQUEST['page'];
			$countrydropdown = $_REQUEST['countrydropdown'];
			$educatortype   = $_REQUEST['educatortype'];
			$termslug 		 = $_REQUEST['countrydropdown'];
			$term 		     = get_term_by( 'slug',$termslug, 'Country');
			$termsname 	     = $term->name;
			$termsuid        = $term->term_id;
			$countryflag     = ege_countryflagurl($termsuid,'educator-list');
			$countrydropdownsarray = array(
			'taxonomy' => 'Country', // Taxonomy name
			'field' => 'slug',
			'terms' => $countrydropdown
			);

			if(!empty($countrydropdown) && !empty($educatortype)) {
				$countrydropdownsarray = array( // (array) - use taxonomy parameters (available with Version 3.1).
				'relation' => 'AND', // (string) - The logical relationship between each inner taxonomy array when there is more than one. Possible values are 'AND', 'OR'. Do not use with a single inner taxonomy array. Default value is 'AND'.
				array(
				'taxonomy' => 'Country', // (string) - Taxonomy.
				'field' => 'slug', // (string) - Select taxonomy term by Possible values are 'term_id', 'name', 'slug' or 'term_taxonomy_id'. Default value is 'term_id'.
				'terms' => $countrydropdown // (int/string/array) - Taxonomy term(s).
				//'include_children' => true, // (bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
				//'operator' => 'IN' // (string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS'. Default value is 'IN'.
				),
				array(
				'taxonomy' => 'educator-type',
				'field' => 'slug',
				'terms' => $educatortype,
				//'include_children' => false,
				//'operator' => 'NOT IN'
				)
				);

			}
				if(!empty($_REQUEST['educatortypeid'])){
						$the_query = new WP_Query( array(
					'posts_per_page' => 4,
					'order' => 'DESC',
					'post__in' => $educators_list_unique,
					'orderby' =>'date',
					'paged' => $paged,
					'post_type'     =>'educator',
					'order' => $orderby,
                    'orderby' => $ordervalueby,
				  )
				);
				}else{

						$the_query = new WP_Query( array(
					'posts_per_page' => 4,
					'order' => 'DESC',
					'post__in' => $educators_list,
					'orderby' =>'date',
					'paged' => $paged,
					'post_type'     =>'educator',
					'tax_query' => array($countrydropdownsarray
					),
					'meta_key' => $metavalue,
					'order' => $orderby,
                    'orderby' => $ordervalueby,
				  )
				);
				}	
	
		
			
				if(!empty($educatortype)){
							$educatortypeterm   = get_term_by( 'slug',$educatortype, 'educator-type');
							$educatorname 	    = $educatortypeterm->name;
							$educatormaintitle  = $educatorname;
				}else{
					$educatormaintitle = 'Educators';
			    }
                   if($the_query -> have_posts()){
                ?>
				<input type="hidden" class="total-records" value="<?php echo $the_query->max_num_pages; ?> Educators in <?php echo $termsname; ?>"> 
				<input type="hidden" class="main-title-mangement" value="<?php  echo $educatormaintitle.' of '.$termsname; ?>">                  
				<input type="hidden" class="main-title-img" value="<?php echo $countryflag; ?>">                  
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                            // print_r($the_query);   
                            $universityid    =  get_the_id();
                            $universitytitle = get_the_title();
                            $universitycontent = get_the_content();
                            $terms  = get_the_terms($universityid,'Country');
							if(!empty($terms)){
							$countryname =  $termsname = $terms[0]->name;
							$termsid     = $terms[0]->term_id;
							$countryflag = ege_countryflagurl($termsid,'educator-list');
							}
                            //$countryname =  $termsname = $terms[0]->name;
                            $world_rank     = get_field('world_rank',get_the_id());
                            $unvisitylogo = get_field('university_logo');
                            $countview  = pvc_get_post_views($universityid);                  
                            if(empty($countview)){
                              $countview = 0;  
                            }
                        ?>
					<div class="loading overlay-bgcolor" style="display:none">
						<div class="all-middleicon-loader">  
							<img width="150" src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect">
						</div>
					</div>

                    <div class="two-blocks educator-blocks">
                       <div class="school-detail-block aos-init aos-animate" data-aos="fade-left" data-aos-duration="600">
                        <div class="school-image">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/lsc.png">
							<?php 
								echo do_action('educator_short_list_actions')
							?>
                            <div class="image-overlay">
                                <div class="image-overlay-inner">
                                    <?php echo $universitycontent; ?>
                                    <span>there is a course in english</span>
                                </div>
                            </div>
                        </div>
                        <div class="school-details-wrap">
                            <a href="<?php echo get_permalink(); ?>"><h3><?php echo $universitytitle; ?></h3></a>
                            <?php if(!empty($countryname)){?>
                            <span class="school-flag">
                                <?php if(!empty($countryflag)) {?>
                                            <img src="<?php echo $countryflag ?>" width="25" />
                                            <?php } ?>

                                            <p><?php echo  $countryname; ?></p>
                            </span>
	                        <?php } ?>
                            <div class="school-detail-bottom">
                                <div class="rate-star">
                                    <span class="like-count">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a class="rate-like" href="javascript" title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
                                </div>
								<?php  if(!empty($world_rank )){  ?>
                                <div class="world-ranks">
						        <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/graph-ico.png">
									<p>World Ranking:<?php echo $world_rank;  ?></p>
                                </div>
								<?php } ?>
                                <div class="views-count">
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png">
                                    <p>Views:<?php echo $countview; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <?php
                    endwhile;
                    }else{
                    echo "No Educators Founds";
                    ?>
				<input type="hidden" class="total-records" value="<?php echo $the_query->max_num_pages; ?> Educators in <?php echo $termsname; ?>"> 
				<input type="hidden" class="main-title-mangement" value="<?php  echo $educatormaintitle.' of '.$termsname; ?>">  
                    <?php
                    }

                    wp_reset_postdata(); ?>
                        <div class="fullwidth-block">
                        <div class="page-linkox cf">
                        <?php

                        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                        ?>
                        </div>
                        </div>
             <?php
             
             wp_die();           

}

add_action( 'wp_ajax_educators_listing_header', 'ege_educators_listing_header' );
add_action( 'wp_ajax_nopriv_educators_listing_header', 'ege_educators_listing_header' );


function ege_educators_listing_header(){
$termslug 		 = $_REQUEST['countrydropdown'];
$term 		     = get_term_by( 'slug',$termslug, 'Country');
$termsname 	     = $term->name;
$termdescription = $term->description;

	?>		
					<?php if(!empty($termsname )){ ?>
	                <h3>Student life in <?php echo $termsname; ?></h3>	
		            <?php } ?>
		            <?php if(!empty($termdescription )){ 
                    	echo '<p>'.$termdescription.'</p>';
	                 } ?>

             <?php
             
             wp_die();       
}


add_action( 'wp_ajax_educator_listing_drodpown', 'ege_educator_listing_drodpown' );
add_action( 'wp_ajax_nopriv_educator_listing_drodpown', 'ege_educator_listing_drodpown' );

function ege_educator_listing_drodpown(){
	if(isset($_REQUEST['countrydropdown'])){
	session_start();
    //$_SESSION['countrydropdowneducator'] = $_REQUEST['countrydropdown'];	
	$universityposts = get_posts(
	array(
	'post_type' => 'educator', // Post type
	'tax_query' => array(
	array(
	'taxonomy' => 'Country', // Taxonomy name
	'field' => 'slug',
	'terms' => $_REQUEST['countrydropdown']
	)
	),
	'posts_per_page' => -1,
	'orderby'       => 'menu_order',
	'order'         => 'ASC'
	)
	);	

	 //$educators_list = array('184');
	?>                    <select id="educators-drodown" class="chosen-select chosen-serach-dropdown">

                           <option value="">Educator Name</option>
                           <?php
                           	if(!empty($universityposts)){
                                foreach ($universityposts as $postvalue) {
                                    # code...
                                    echo '<option value="'.$postvalue->ID.'">'.$postvalue->post_title.'</option>'
                                  
                                    ?>
                                    <?php

                                }
                        	}
                            ?> 
                        </select>

    <?php        
    }        
    wp_die();

}
add_action( 'wp_ajax_educator_listing_drodpown_two', 'ege_educator_listing_drodpown_two' );
add_action( 'wp_ajax_nopriv_educator_listing_drodpown_two', 'ege_educator_listing_drodpown_two' );

function ege_educator_listing_drodpown_two(){
	if(isset($_REQUEST['countrydropdown'])){
	session_start();
    //$_SESSION['countrydropdowneducator'] = $_REQUEST['countrydropdown'];	
	$universityposts = get_posts(
	array(
	'post_type' => 'educator', // Post type
	'tax_query' => array(
	array(
	'taxonomy' => 'Country', // Taxonomy name
	'field' => 'slug',
	'terms' => $_REQUEST['countrydropdown']
	)
	),
	'posts_per_page' => -1,
	'orderby'       => 'menu_order',
	'order'         => 'ASC'
	)
	);	

	 $educators_list = array('184');
	?>                    

               			 <option value=" ">Select Educator Name</option>
                           <?php
                           	if(!empty($universityposts)){
                                foreach ($universityposts as $postvalue) {
                                    # code...
                                    echo '<option value="'.$postvalue->ID.'">'.$postvalue->post_title.'</option>'
                                  
                                    ?>
                                    <?php

                                }
                        	}
                            ?> 

    <?php        
    }        
    wp_die();

}
function boot_session() {
  session_start();
}
add_action('wp_loaded','boot_session');


// Educator Add In Favorait List.

add_action( 'wp_ajax_educator_wish_list', 'ege_educator_wish_list_user_update' );
add_action( 'wp_ajax_nopriv_educator_wish_list', 'ege_educator_wish_list_user_update' );

function ege_educator_wish_list_user_update(){

	$userid 	= get_current_user_id();
	$educatorid = $_REQUEST['educatorid'];
	$wishdate 	= $_REQUEST['wishdata'];
	$meta_key   = 'educator_short_list';
	if(	$wishdate  == 'add'){
	    $educatorarray = get_user_meta($userid,$meta_key,true);
	    if(empty($educatorarray)){
		    $originalarray=array($educatorid);
		    $metavalue = maybe_serialize($originalarray);

		 }else{
		 	$originaleducatorarray=maybe_unserialize($educatorarray);
		 	//echo "adding".$educatorid;
		 	//$thereid ='';
		 	//$thereid = array_search($educatorid,$originaleducatorarray);
			array_push($originaleducatorarray,$educatorid);

		    $metavalue = maybe_serialize(array_unique($originaleducatorarray));

		 }
	    	update_user_meta($userid,$meta_key,$metavalue);
	}else{
	   $educatorarray = get_user_meta($userid,$meta_key,true);
	   $originaleducatorarray=maybe_unserialize($educatorarray);
 	   $thereid = array_search($educatorid,$originaleducatorarray);
	   unset($originaleducatorarray[$thereid]);
	   print_r($originaleducatorarray);
	   $metavalue = maybe_serialize(array_unique($originaleducatorarray));

		 if(!empty($originaleducatorarray)){
	    	$metavalue = maybe_serialize(array_unique($originaleducatorarray));
		}else{
			$metavalue = '';
		}
				 //	print_r($originaleducatorarray);

    	update_user_meta($userid,$meta_key,$metavalue);

	}

	// $wishdate
 //    array_push($a,"blue","saran","yellow");
 //    $value = "saran"; 
 //    echo $id = array_search($value,$a);
 //    unset($a[$id]);
 //   // print_r(maybe_serialize($a));
 //    $arrayseralize = maybe_serialize($a);
    //print_r(maybe_unserialize($arrayseralize));

	echo "Work";

	wp_die();

}



// Courses add in short list
add_action( 'wp_ajax_course_wish_list', 'ege_course_wish_list_list_user_update' );
add_action( 'wp_ajax_nopriv_course_wish_list', 'ege_course_wish_list_list_user_update');

function ege_course_wish_list_list_user_update(){
	$userid 	= get_current_user_id();
	$educatorid = $_REQUEST['courseid'];
	$wishdate 	= $_REQUEST['wishdata'];
	$meta_key   = 'course_short_list';
	echo $wishdate;
	if(	$wishdate  == 'add'){
	    $educatorarray = get_user_meta($userid,$meta_key,true);
	    print_r($educatorarray);
	    if(empty($educatorarray)){
		    $originalarray=array($educatorid);
		    $metavalue = maybe_serialize($originalarray);
		 }else{
		 	$originaleducatorarray=maybe_unserialize($educatorarray);
		 	//echo "adding".$educatorid;
		 	//$thereid ='';
		 	//$thereid = array_search($educatorid,$originaleducatorarray);
			array_push($originaleducatorarray,$educatorid);
		    $metavalue = maybe_serialize(array_unique($originaleducatorarray));
		 }
	    	update_user_meta($userid,$meta_key,$metavalue);
	}else{
	   $educatorarray = get_user_meta($userid,$meta_key,true);
	   $originaleducatorarray=maybe_unserialize($educatorarray);
 	   $thereid = array_search($educatorid,$originaleducatorarray);
	   unset($originaleducatorarray[$thereid]);
	   print_r($originaleducatorarray);
	   $metavalue = maybe_serialize(array_unique($originaleducatorarray));

		 if(!empty($originaleducatorarray)){
	    	$metavalue = maybe_serialize(array_unique($originaleducatorarray));
		}else{
			$metavalue = '';
		}
				 //	print_r($originaleducatorarray);

    	update_user_meta($userid,$meta_key,$metavalue);

	}
	//print_r(array_unique($originaleducatorarray));
	wp_die();

}




add_action('course_short_list_actions','ege_course_short_list_html');
function ege_course_short_list_html(){

    	$added 			= "add to shortlist";
	    if(is_user_logged_in()) {
		    $userid         = get_current_user_id();
		    $meta_key       = 'course_short_list';
		    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
		    $wishclass      = '';
		    $wishclassajax      = 'add';
		    //$wishsortlistclass   = 'fa-heart';
		    if(is_array($educatorarray)){
			    if(in_array(get_the_id(), $educatorarray)){
			    $wishclass = 'active';
			    //$wishsortlistclass = 'fa-heart-o';
			    $added = "shortlisted";  
			    $wishclassajax = 'remove';  
		    }
		}

    } 
	if(is_user_logged_in()) {
	    ?>
	<span class="course-heart-icon" data-wish="<?php echo  $wishclassajax; ?>" data-id="<?php echo get_the_id();?>">
	    <a href="javascript:void(0);"  class="course-heart-click"><i class="fa fa-heart <?php echo $wishclass;  ?> "></i></a>
	    <div class="addtoshort-btn"><a href="#"><?php echo $added; ?></a></div>
	</span>
	<?php } 
}


add_action('educator_short_list_actions','ege_educator_short_list_html');
function ege_educator_short_list_html(){
    	$added 			= "add to shortlist";
	    if(is_user_logged_in()) {
		    $userid         = get_current_user_id();
		    $meta_key       = 'educator_short_list';
		    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
		    $wishclass      = '';
		    $wishclassajax      = 'add';
		    //$wishsortlistclass   = 'fa-heart';
		    if(in_array(get_the_id(), $educatorarray)){
		    $wishclass = 'active';
		    $wishclassajax = 'remove';
		    //$wishsortlistclass = 'fa-heart-o';
		    $added = "shortlisted";    
	    }

    } 
	if(is_user_logged_in()) {
	    ?>
	<span class="educator-heart-icon" data-wish="<?php echo $wishclassajax;  ?>" data-id="<?php echo get_the_id();?>">
	    <a href="javascript:void(0);"  class="like-ico course-heart-o"><i class="fa fa-heart <?php echo $wishclass;  ?> "></i></a>
	    <div class="addtoshort-btn"><a href="#"><?php echo $added; ?></a></div>
	</span>
	<?php } 
}



function wp_star_rating( $args = array() ) {
	$defaults = array(
		'rating' => 0,
		'type'   => 'rating',
		'number' => 0,
		'echo'   => true,
	);
	$r = wp_parse_args( $args, $defaults );

	// Non-English decimal places when the $rating is coming from a string
	$rating = (float) str_replace( ',', '.', $r['rating'] );

	// Convert Percentage to star rating, 0..5 in .5 increments
	if ( 'percent' === $r['type'] ) {
		$rating = round( $rating / 10, 0 ) / 2;
	}

	// Calculate the number of each type of star needed
	$full_stars = floor( $rating );
	$half_stars = ceil( $rating - $full_stars );
	$empty_stars = 5 - $full_stars - $half_stars;

	if ( $r['number'] ) {
		/* translators: 1: The rating, 2: The number of ratings */
		$format = _n( '%1$s rating based on %2$s rating', '%1$s rating based on %2$s ratings', $r['number'] );
		$title = sprintf( $format, number_format_i18n( $rating, 1 ), number_format_i18n( $r['number'] ) );
	} else {
		/* translators: 1: The rating */
		$title = sprintf( __( '%s rating' ), number_format_i18n( $rating, 1 ) );
	}

	//$output = '<div class="star-rating">';
	$output .= '<span class=\'like-count\'>';
	$output .= str_repeat( '<i class=\'fa fa-star\' ></i>', $full_stars );
	$output .= str_repeat( '<i class=\'fa fa-star-half\' ></i>', $half_stars );
	$output .= str_repeat( '<i class=\'fa fa-star-o\'></i>', $empty_stars );
	$output .= '</span>';

	if ( $r['echo'] ) {
		echo $output;
	}

	return $output;
}


function wp_star_rating_avarage_float( $avarage ) {
	 $rating  = (float) str_replace( ',', '.', $avarage);
	 $output   = number_format_i18n( $rating, 1 );
	return $output;
}




function wp_star_rating_add( $args = array() ) {
	$defaults = array(
		'rating' => 0,
		'type'   => 'rating',
		'number' => 0,
		'echo'   => true,
	);
	$r = wp_parse_args( $args, $defaults );

	// Non-English decimal places when the $rating is coming from a string
	$rating = (float) str_replace( ',', '.', $r['rating'] );

	// Convert Percentage to star rating, 0..5 in .5 increments
	if ( 'percent' === $r['type'] ) {
		$rating = round( $rating / 10, 0 ) / 2;
	}

	// Calculate the number of each type of star needed
	$full_stars = floor( $rating );
	$half_stars = ceil( $rating - $full_stars );
	$empty_stars = 5 - $full_stars - $half_stars;

	if ( $r['number'] ) {
		/* translators: 1: The rating, 2: The number of ratings */
		$format = _n( '%1$s rating based on %2$s rating', '%1$s rating based on %2$s ratings', $r['number'] );
		$title = sprintf( $format, number_format_i18n( $rating, 1 ), number_format_i18n( $r['number'] ) );
	} else {
		/* translators: 1: The rating */
		$title = sprintf( __( '%s rating' ), number_format_i18n( $rating, 1 ) );
	}

	$output = '<div class="star-rating">';
	$output .= '<span class="screen-reader-text">' . $title . '</span>';
	$output .= str_repeat( '<div class="star star-full" aria-hidden="true"></div>', $full_stars );
	$output .= str_repeat( '<div class="star star-half" aria-hidden="true"></div>', $half_stars );
	$output .= str_repeat( '<div class="star star-empty" aria-hidden="true"></div>', $empty_stars );
	$output .= '</div>';

	if ( $r['echo'] ) {
		echo $output;
	}

	return $output;
}



/** Chart Functions for Review **/
function wp_star_rating_chart( $chatstar ) {
	$defaults = array(
		'rating' => $chatstar,
		'type'   => 'rating',
		'number' => 0,
		'echo'   => true,
	);
	$r = wp_parse_args( $argss, $defaults );

	// Non-English decimal places when the $rating is coming from a string
	$rating = (float) str_replace( ',', '.', $r['rating'] );

	// Convert Percentage to star rating, 0..5 in .5 increments
	if ( 'percent' === $r['type'] ) {
		$rating = round( $rating / 10, 0 ) / 2;
	}

	// Calculate the number of each type of star needed
	$full_stars = floor( $rating );
	$half_stars = ceil( $rating - $full_stars );
	$empty_stars = 5 - $full_stars - $half_stars;

	if ( $r['number'] ) {
		/* translators: 1: The rating, 2: The number of ratings */
		$format = _n( '%1$s rating based on %2$s rating', '%1$s rating based on %2$s ratings', $r['number'] );
		$title = sprintf( $format, number_format_i18n( $rating, 1 ), number_format_i18n( $r['number'] ) );
	} else {
		/* translators: 1: The rating */
		$title = sprintf( __( '%s rating' ), number_format_i18n( $rating, 1 ) );
	}

	//$output = '<div class="star-rating">';
	$output .= "<span class=\"like-count\">";
	$output .= str_repeat( "<i class=\'fa fa-star\' ></i>", $full_stars );
	$output .= str_repeat( "<i class=\'fa fa-star-half\' ></i>", $half_stars );
	$output .= str_repeat( "<i class=\'fa fa-star-o\'></i>", $empty_stars );
	$output .= "</span>";

	if ( $r['echo'] ) {
		echo $output;
	}

	return $output;
}


/**Get Country Flag Functions Hook.*/
function ege_countryflagurl($termid,$detail=null){

	$countryflag = get_field('add_icon', 'Country_'.$termid);
	if(!empty($countryflag)){
		//print_r($countryflag);
		if($detail == 'educator-list'){
	 		$countryflag  = $countryflag['sizes']['medium'];
	 	}else{
	 		$countryflag  = $countryflag['sizes']['country-flag'];
	 	}
	}else{
		$countryflag = '';
	}

	return $countryflag;
}








