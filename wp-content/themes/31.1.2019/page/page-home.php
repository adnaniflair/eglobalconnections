<?php /* Template Name: Home Page Template */ 
get_header(); 
?>

	<section class="banner">
		<div class="wrapper">
	    	<div class="banner-text">
                <h1 data-aos="fade-up" data-aos-duration="300" class="aos-init aos-animate">Study abroad</h1>
                <p data-aos="fade-up" data-aos-duration="500" class="aos-init">Lorem ipsum dolor sit amet, consectetur adipiscing elit quisque pharetra vel urna a imperdiet duis eu egestas metus eu viverra orci varius natoque penatibus et magnis dis parturient montes nascetur ridiculus mus</p>
	            </div>
				<div class="banner-course-filter aos-init aos-animate" data-aos="fade-up" data-aos-duration="700">
					<?php do_action('serach_filter_code'); ?>
				</div>
			</div>
		</div>
		<a href="#popular-subjects" class="click-down"><i class="fa fa-angle-double-down"></i></a>
	</section>
	<?php  do_action('popular_subject_html'); ?>
	<section class="recomended-school">
		<div class="wrapper">
			<h2>Recommended Schools and Universities</h2>
			<div class="bxslider university-slider">
					<?php  
					$cat_args = array(
					'orderby'       => 'term_id', 
					'order'         => 'ASC',
					'hide_empty'    => false, 
					);

					$universityterms = get_terms('Country',$cat_args);
					// echo "<pre>";
					// print_r($terms);
					// echo "</pre>";
					//echo get_field('add_image','Country_9');

					//$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type ) );
			
					foreach ($universityterms as  $value) {
						# code...
						$countryname = 	$value->name;
						$countryslug = 	$value->slug;
					 	$countrydescriptions = $value->description;
						$image = get_field('add_image',$value->taxonomy . '_' . $value->term_id);
						?>
						<a class="item university-block university-block-sections" data-name="<?php echo $countryslug;  ?>">
							<div class="block-image">
								<div class="image-overlay"></div>
								<img src="<?php echo $image; ?>">
							</div>
							<div class="university-detail">
								<div class="detail-inner">
									<h3><?php echo $countryname; ?></h3>
									<p><?php echo $countrydescriptions; ?></p>
								</div>
							</div>
						</a>
						<?php
					}
					?>
			</div>
			<div class="school-detail-slider">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>

			<?php
						
						$universityposts = get_posts(
						array(
						'post_type' => 'educator', // Post type
						'tax_query' => array(
						array(
						'taxonomy' => 'Country', // Taxonomy name
						'field' => 'slug',
						'terms' => 'south-africa'
						)
						),
						'posts_per_page' => 2,
						'orderby'       => 'menu_order',
						'order'         => 'ASC'
						)
						);	

						//print_r($universityposts);
						if(!empty($universityposts)){
						foreach ($universityposts as $postvalue) {
							# code...
							$universityid       =  $postvalue->ID;
                            $unvisitylogo       = get_field('university_logo',$universityid );
							$universitytitle    = $postvalue->post_title;
							$universitycontent  = $postvalue->post_content;
							$terms              = get_the_terms($universityid,'Country');
							$countryname        =  $termsname = $terms[0]->name;
							$termid             = $terms[0]->id;
							$countview          = pvc_get_post_views($universityid);
							$bannerimage        = get_the_post_thumbnail_url( get_the_id(),  'bannerimage-educators-list' );
                            if(empty($bannerimage)){
                              $bannerimages     =   get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                            }
                        $wordranktooltipcontent = get_field('world_rank_tooltip',$universityid );
							//print_r($terms);	
							?>
						<div class="school-detail-block  educator-blocks">
						 <div class="school-image">
							     <?php  if(!empty($unvisitylogo)) { ?>
	                                <span class="min-imgbox educatort-logo-image"><img src="<?php echo $unvisitylogo ?>" width="65" /></span>
	                               <?php }  ?>
						     <?php
	                                if(!empty($bannerimage)){
	                                    echo get_the_post_thumbnail($universityid,'bannerimage-educators-list');
	                                }else{
	                               ?>
	                                <img src="<?php echo $bannerimages; ?>">
	                                <?php
	                             } ?>
	                                <?php 
	                                    echo do_action('educator_short_list_actions')
	                                 ?>
							<div class="image-overlay">
								<div class="image-overlay-inner">
									<p><?php echo $universitycontent;  ?></p>
									<span style="display:none;">there is a course in english</span>
								</div>
							</div>
						</div>
					<div class="school-details-wrap">
						<a href="<?php echo get_permalink($universityid); ?>"><h3><?php echo $universitytitle; ?></h3></a>
						<span class="school-flag">
							<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/school-flag.png">
							<p><?php echo $countryname; ?></p>
						</span>
						<div class="school-detail-bottom">
                            <div class="rate-star">
                                <span class="like-count">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </span>
                                <a class="rate-like" href="javascript" title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
                                  <div class="rate-discr" ><p>Total  Rating 4 out of 5 - Good</p></div>
                            </div>
							<div class="world-ranks">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/graph-ico.png">
								<p>World Ranking:124</p>
								<?php if(!empty($wordranktooltipcontent)){ ?>
								  <div class="worldrank-desc" ><p><?php echo   $wordranktooltipcontent; ?></p></div>
								<?php } ?>
							</div>
							<div class="views-count">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png">
								<p>Views:<?php echo $countview ?></p>
							</div>
						</div>
					</div>
				</div>
			<?php
						}

					}

			?>
		</div>
	
	</div>
	
	</section>
<?php /*
    <section class="introduction">
        <div class="wrapper">
            <h2 data-aos="fade-right" data-aos-duration="600">Let us introduce ourselves</h2>
<div class="introdu-box left" data-aos="flip-right"
data-aos-easing="ease-in-cubic"
data-aos-duration="1200">
                 <div class="intro-block" >
                    <div class="intro-img-wrap">
                        <img src="images/intro-1.png">
                    </div>
                    <h4>Help you choose a course</h4>
                    <p>From being there to chat as you browse courses, to interviews with experts and real students, we'll guide you all the way to finding your perfect course.</p>
                </div>
            </div>
            <div class="introdu-box middlebox" data-aos="fade-up" data-aos-duration="1200">
                <div class="intro-block">
                    <div class="intro-img-wrap">
                        <img src="images/intro-2.png">
                    </div>
                    <h4>Provide materials</h4>
                    <p>From being there to chat as you browse courses, to interviews with experts and real students, we'll guide you all the way to finding your perfect course.</p>
                </div>
            </div>
            <div class="introdu-box right" data-aos="flip-left"
     data-aos-easing="ease-in-cubic"
     data-aos-duration="1200">
                <div class="intro-block">
                    <div class="intro-img-wrap">
                        <img src="images/intro-3.png">
                    </div>
                    <h4>Support after of the course</h4>
                    <p>From being there to chat as you browse courses, to interviews with experts and real students, we'll guide you all the way to finding your perfect course.</p>
                </div>
            </div>
        </div>
    </section>
 */ ?>
	<section class="introduction">
		<div class="wrapper">
			<?php
			$intromaintitle = get_field('introductions_main_tilte');
			if(!empty($intromaintitle)){ 
			?>
			<h2 data-aos="fade-right" data-aos-duration="600" class="aos-init aos-animate"><?php echo $intromaintitle; ?></h2>
			<?php
			}
			// check if the repeater field has rows of data
			if( have_rows('introductions_sections') ):
				// loop through the rows of data
				$count =1;
				while ( have_rows('introductions_sections') ) : the_row();
					//echo "string".$count;
					$intro_image 		=	get_sub_field('intro_image');
					$intro_title 		=	get_sub_field('intro_title');
					$intro_descriptions = 	get_sub_field('intro_descriptions');
					if($count == 1){
						$data_animate = 'data-aos="flip-right" data-aos-easing="ease-in-cubic" data-aos-duration="1200"';
						$animationclass = 'left';
					}
					if($count == 2){

						$data_animate = 'data-aos="fade-up" data-aos-duration="1200"';
						$animationclass = 'middlebox';
						
					}
					if($count == 3){
						$data_animate ='data-aos="flip-left" data-aos-easing="ease-in-cubic" data-aos-duration="1200"';
						$animationclass = 'right';
					}
 				?>
			   <div class="introdu-box <?php echo $animationclass; ?>" <?php echo $data_animate; ?>>
				<div class="intro-block">
					<?php if(!empty($intro_image)){ ?>
					<div class="intro-img-wrap">
						<img src="<?php echo $intro_image  ?>">
					</div>
					<?php } if(!empty($intro_title)){ ?>
					<h4><?php echo $intro_title;  ?></h4>
				    <?php } if(!empty($intro_descriptions)){ ?>
					<p><?php echo $intro_descriptions?></p>
					<?php } ?>
				</div>	
				</div>
				<?php
				$count++;
				// display a sub field value
				endwhile;

			else :
					echo '<h2> No Rows Founds </h2>';
			// no rows found
			endif;

			?>
		</div>
	</section><!-- introduction -->

    <section class="reviews">
        <div class="wrapper">
            <h2 data-aos="fade-right" data-aos-duration="600">reviews</h2>
            <div id="reviews-slider" data-aos="fade-left" data-aos-duration="600" class="testmonial-block owl-carousel owl-theme">
                <div class="item testmonial-block-innner" data-animate="bounceIn animated">
                    <div class="profile-wrap">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/review-profile.png">
                    </div>
                    <h4>Victoriya Markova</h4>
                    <div class="rate-star">
                        <span class="like-count">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-o"></i>
							</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<br> nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                </div>
                <div class="item testmonial-block-innner" data-animate="bounceIn animated">
                    <div class="profile-wrap">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/review-profile.png">
                    </div>
                    <h4>Victoriya Markova</h4>
                    <div class="rate-star">
                        <span class="like-count">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
							</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<br> nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                </div>
                <div class="item testmonial-block-innner" data-animate="bounceIn animated">
                    <div class="profile-wrap">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/review-profile.png">
                    </div>
                    <h4>Victoriya Markova</h4>
                    <div class="rate-star">
                        <span class="like-count">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
							</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<br> nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                </div>
                <div class="item testmonial-block-innner" data-animate="bounceIn animated">
                    <div class="profile-wrap">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/review-profile.png">
                    </div>
                    <h4>Victoriya Markova</h4>
                    <div class="rate-star">
                        <span class="like-count">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
								<i class="fa fa-star-o"></i>
							</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis<br> nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                </div>
            </div>
        </div>
    </section>
<?php //do_action('home-social-section'); ?>

<script async defer src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=403915283748078&autoLogAppEvents=1"></script>

<section class="feedback-feed-section" >
    <div class="wrapper cf">
        <div class="twoblocks">	
            <h2> Facebook Feed </h2>
            <div  class="fb-page" data-href="https://www.facebook.com/eGlobalConnections/" data-tabs="timeline" data-width="500" data-height="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/eGlobalConnections/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/eGlobalConnections/">eGlobal Connections</a></blockquote></div>
        </div>
        <div class="twoblocks">
            <h2> Twitter Feed </h2>
            <a class="twitter-timeline" data-width="500" data-height="520" data-theme="light" href="https://twitter.com/eGconnections?ref_src=twsrc%5Etfw">Tweets by eGconnections</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    </div>
</section>
<?php do_action('loginbox_html_code'); ?>    

<?php get_footer();
?>

<script type = 'text/javascript'>
jQuery(document).ready(function($) {
	$('.university-block-sections').click(function(event) {
		$categoryname = jQuery(this).data('name');	
		//alert($categoryname);
		ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
		    $.ajax({
		        url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
		        data: {
		            'action': 'educators_data_sections',
		            'categoryname' : $categoryname
		        },
		        success:function(data) {
		            // This outputs the result of the ajax request
		            //console.log(data);
		            if(data != 'empty'){
	        		jQuery('.school-detail-slider').show();	
		            jQuery('.school-detail-slider').html(data);
		        	}else{
		        		jQuery('.school-detail-slider').hide();
		        	}
		        },
		        error: function(errorThrown){
		            console.log(errorThrown);
		        }
		    }); 
	}); 
});
</script>