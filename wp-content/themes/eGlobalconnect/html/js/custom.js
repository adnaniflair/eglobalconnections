jQuery(document).ready(function($) {

	$('.sub-toggle').click(function(event) {
       	event.preventDefault(); 
       	$('.sub-toggle').not(this).siblings("ul").slideUp();
			$(this).siblings("ul").slideToggle();
			
		});
		$(".drp_value").click(function(){
			$('.sub-toggle').not(this).siblings("ul").slideUp();
		});	
		$('#popular-subjects.owl-carousel').owlCarousel({
		    loop:true,
		    items:3,
		    nav:true,
		    dots:false,
		    margin:0,
		    responsiveClass:true,
		    responsive:{
		        767:{
		            items:3
		        },
		       568:{
		            items:2
		        },
		        0:{
		            items:1
		        }
		    }
		})
		$('#reviews-slider.owl-carousel').owlCarousel({
		    loop:true,
		    items:1,
		    nav:true,
		    dots:true,
		    margin:0
		})
		var windowsize = $(window).width();

		    if (windowsize < 480) {
				$('.bxslider').bxSlider({
					auto: true,
					slideWidth:320,
					slideMargin: 30,
					speed:500,
					autoControls: true,
					stopAutoOnClick: true,
					pager: true,
					minSlides: 2,
					maxSlides: 1
				});
				
			} else {
				$('.bxslider').bxSlider({
					auto: true,
					slideWidth:265,
					slideMargin: 30,
					speed:500,
					autoControls: true,
					stopAutoOnClick: true,
					pager: true,
					minSlides: 2,
					maxSlides: 3
				});
			}
		 
		 $('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1500);
		        return false;
		      }
		    }
		  });
});