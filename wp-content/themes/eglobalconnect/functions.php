<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage eglobaleducation
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function eglobaleducation_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/eglobaleducation
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'eglobaleducation' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'eglobaleducation' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'eglobaleducation-featured-image', 2000, 1200, true );

	add_image_size( 'eglobaleducation-thumbnail-avatar', 100, 100, true );




	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'eglobaleducation' ),
		'social' => __( 'Social Links Menu', 'eglobaleducation' ),
		'abouts-us' => __( 'About Us Links Menu', 'eglobaleducation' ),
		'user-menu' => __( 'User menu', 'eglobaleducation' ),
		'Policies' => __( 'Policies Links Menu', 'eglobaleducation' ),
		'Contact-us' => __( 'Contact Us Links Menu', 'eglobaleducation' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', eglobaleducation_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'eglobaleducation' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'eglobaleducation' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'eglobaleducation' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'eglobaleducation' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'eglobaleducation' ),
				'items' => array(
					'link_yelp',
					'link_facecourse',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	/**
	 * Filters Twenty Seventeen array of starter content.
	 *
	 * @since Twenty Seventeen 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'eglobaleducation_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'eglobaleducation_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function eglobaleducation_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( eglobaleducation_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Twenty Seventeen content width of the theme.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'eglobaleducation_content_width', $content_width );
}
add_action( 'template_redirect', 'eglobaleducation_content_width', 0 );

/**
 * Register custom fonts.
 */
function eglobaleducation_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'eglobaleducation' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function eglobaleducation_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'eglobaleducation-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'eglobaleducation_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function eglobaleducation_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'eglobaleducation' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'eglobaleducation' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'eglobaleducation' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'eglobaleducation' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'eglobaleducation' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'eglobaleducation' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'eglobaleducation_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function eglobaleducation_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'eglobaleducation' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
//add_filter( 'excerpt_more', 'eglobaleducation_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function eglobaleducation_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'eglobaleducation_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function eglobaleducation_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'eglobaleducation_pingback_header' );

/**
 * Display custom color CSS.
 */
function eglobaleducation_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo eglobaleducation_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'eglobaleducation_colors_css_wrap' );


/**
 * Enqueue scripts and styles.
 */
function eglobaleducation_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'eglobaleducation-fonts', eglobaleducation_fonts_url(), array(), null );

	// Theme stylesheet.
	wp_enqueue_style( 'eglobaleducation-style', get_stylesheet_uri() );

	// Load the dark colorscheme.


	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.


	// Load the Internet Explorer 8 specific stylesheet.

	wp_style_add_data( 'eglobaleducation-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	//styles Defined
	wp_enqueue_style( 'eglobaleducation-1-css', get_theme_file_uri( '/assettwo/css/owl.carousel.css' ), array( 'eglobaleducation-style' ), '1.0' );
	wp_enqueue_style( 'eglobaleducation-2-css', get_theme_file_uri( '/assettwo/css/jquery.bxslider.css' ), array( 'eglobaleducation-style' ), '1.0' );
	wp_enqueue_style( 'eglobaleducation-3-css', get_theme_file_uri( '/assettwo/css/chosen.min.css' ), array( 'eglobaleducation-style' ), '1.0' );
	wp_enqueue_style( 'aos-css', get_theme_file_uri( '/assettwo/css/aos.css' ), array( 'eglobaleducation-style' ), '1.0' );
	wp_enqueue_style( 'font-awesome.css', get_theme_file_uri( '/assettwo/css/font-awesome.css' ), array( 'eglobaleducation-style' ), '1.0' );
	wp_enqueue_style( 'jquery.easy_slides.css', get_theme_file_uri( '/assettwo/css/jquery.easy_slides.css' ), array( 'eglobaleducation-style' ), '1.0' );
	wp_enqueue_style( 'style-add.css', get_theme_file_uri( '/assettwo/css/style.css' ), array( 'eglobaleducation-style' ), '1.0' );
	wp_enqueue_style( 'login-myaccount', get_theme_file_uri( '/assettwo/css/login-myaccount.css' ), array( 'eglobaleducation-style' ), '1.0' );
	wp_enqueue_style( 'login-myaccount.css', get_theme_file_uri( '/assettwo/css/responsive.css' ), array( 'eglobaleducation-style' ), '1.0' );


	wp_enqueue_script( 'bxslider-js', get_theme_file_uri( '/assettwo/js/jquery.bxslider.min.js' ), array(), '1.0', true );
	wp_enqueue_script( 'easy-slider-js', get_theme_file_uri( '/assettwo/js/jquery.easy_slides.js' ), array(), '1.0', true );
	wp_enqueue_script( 'choosen-min-js', get_theme_file_uri( '/assettwo/js/chosen.jquery.min.js' ), array(), '1.0', true );
	wp_enqueue_script( 'owl-carousel-js', get_theme_file_uri( '/assettwo/js/owl.carousel.js' ), array(), '1.0', true );
	wp_enqueue_script( 'aos-js', get_theme_file_uri( '/assettwo/js/aos.js' ), array(), '1.0', true );
	wp_enqueue_script( 'sticky-sidebar', get_theme_file_uri( '/assettwo/js/sticky-sidebar.js' ), array(), '1.0', true );
	wp_enqueue_script( 'custom-js', get_theme_file_uri( '/assettwo/js/custom.js' ), array(), '1.0', true );


	//wp_enqueue_script( 'eglobaleducation-skip-link-focus-fix', get_theme_file_uri( '/assettwo/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$eglobaleducation_l10n = array(
		'quote'          => eglobaleducation_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	// if ( has_nav_menu( 'top' ) ) {
	// 	wp_enqueue_script( 'eglobaleducation-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
	// 	$eglobaleducation_l10n['expand']         = __( 'Expand child menu', 'eglobaleducation' );
	// 	$eglobaleducation_l10n['collapse']       = __( 'Collapse child menu', 'eglobaleducation' );
	// 	$eglobaleducation_l10n['icon']           = eglobaleducation_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );
	// }


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'eglobaleducation_scripts' );


/**
 * Enqueue scripts and styles.
 */
/*
function eglobaleducation_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'eglobaleducation-fonts', eglobaleducation_fonts_url(), array(), null );

	// Theme stylesheet.
	wp_enqueue_style( 'eglobaleducation-style', get_stylesheet_uri() );

	// Load the dark colorscheme.


	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.


	// Load the Internet Explorer 8 specific stylesheet.

	wp_style_add_data( 'eglobaleducation-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'eglobaleducation-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$eglobaleducation_l10n = array(
		'quote'          => eglobaleducation_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	// if ( has_nav_menu( 'top' ) ) {
	// 	wp_enqueue_script( 'eglobaleducation-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
	// 	$eglobaleducation_l10n['expand']         = __( 'Expand child menu', 'eglobaleducation' );
	// 	$eglobaleducation_l10n['collapse']       = __( 'Collapse child menu', 'eglobaleducation' );
	// 	$eglobaleducation_l10n['icon']           = eglobaleducation_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );
	// }


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'eglobaleducation_scripts' );
*/
/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function eglobaleducation_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'eglobaleducation_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function eglobaleducation_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'eglobaleducation_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function eglobaleducation_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'eglobaleducation_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function eglobaleducation_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'eglobaleducation_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Seventeen 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function eglobaleducation_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'eglobaleducation_widget_tag_cloud_args' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
//require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );



add_action( 'init', 'ege_course_init' );
/**
 * Register a course post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function ege_course_init() {
	$labels = array(
		'name'               => _x( 'Courses', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Course', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Courses', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Course', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'course', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Course', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Course', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Course', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Course', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Courses', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Courses', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Courses:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No courses found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No courses found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'course' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'course', $args );
		// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Educators URL Selections For Courses', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'educator', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Educators', 'textdomain' ),
		'popular_items'              => __( 'Popular Educators', 'textdomain' ),
		'all_items'                  => __( 'All Educators', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit educator', 'textdomain' ),
		'update_item'                => __( 'Update educator', 'textdomain' ),
		'add_new_item'               => __( 'Add New educator', 'textdomain' ),
		'new_item_name'              => __( 'New educator Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Educators with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove Educators', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Educators', 'textdomain' ),
		'not_found'                  => __( 'No Educators found.', 'textdomain' ),
		'menu_name'                  => __( 'Educators', 'textdomain' ),
	);
	
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'educators' ),
	);


	register_taxonomy( 'educators', 'course', $args );



	$labels = array(
		'name'                       => _x( 'Subjects', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Subject', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search subjects', 'textdomain' ),
		'popular_items'              => __( 'Popular subjects', 'textdomain' ),
		'all_items'                  => __( 'All Subjects', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Subject', 'textdomain' ),
		'update_item'                => __( 'Update Subject', 'textdomain' ),
		'add_new_item'               => __( 'Add New Subject', 'textdomain' ),
		'new_item_name'              => __( 'New subject Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Subjects with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove subjects', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used subjects', 'textdomain' ),
		'not_found'                  => __( 'No subjects found.', 'textdomain' ),
		'menu_name'                  => __( 'subjects', 'textdomain' ),
	);
	
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'subject' ),
	);


	register_taxonomy( 'subject', 'course', $args );
	    $labels = array(
        'name'                       => _x( 'Study Levels', 'taxonomy general name', 'textdomain' ),
        'singular_name'              => _x( 'Study Level', 'taxonomy singular name', 'textdomain' ),
        'search_items'               => __( 'Search Study Levels', 'textdomain' ),
        'popular_items'              => __( 'Popular Study Levels', 'textdomain' ),
        'all_items'                  => __( 'All Study Levels', 'textdomain' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Study Level', 'textdomain' ),
        'update_item'                => __( 'Update Study Level', 'textdomain' ),
        'add_new_item'               => __( 'Add New Study Level', 'textdomain' ),
        'new_item_name'              => __( 'New Study Level Name', 'textdomain' ),
        'separate_items_with_commas' => __( 'Separate Study Levels with commas', 'textdomain' ),
        'add_or_remove_items'        => __( 'Add or remove Study Levels', 'textdomain' ),
        'choose_from_most_used'      => __( 'Choose from the most used Study Levels', 'textdomain' ),
        'not_found'                  => __( 'No Study Levels found.', 'textdomain' ),
        'menu_name'                  => __( 'Study Levels', 'textdomain' ),
    );
    
    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'study-level' ),
    );


    register_taxonomy( 'studylevel', 'course', $args );
//    register_taxonomy( 'studylevel', 'product', $args );

        $labels = array(
        'name'                       => _x( 'Educator type', 'taxonomy general name', 'textdomain' ),
        'singular_name'              => _x( 'Educator type', 'taxonomy singular name', 'textdomain' ),
        'search_items'               => __( 'Search Educator type', 'textdomain' ),
        'popular_items'              => __( 'Popular Educator type', 'textdomain' ),
        'all_items'                  => __( 'All Educator type', 'textdomain' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Educator type', 'textdomain' ),
        'update_item'                => __( 'Update Educator type', 'textdomain' ),
        'add_new_item'               => __( 'Add New Educator type', 'textdomain' ),
        'new_item_name'              => __( 'New Educator type Name', 'textdomain' ),
        'separate_items_with_commas' => __( 'Separate Educator type with commas', 'textdomain' ),
        'add_or_remove_items'        => __( 'Add or remove Educator type', 'textdomain' ),
        'choose_from_most_used'      => __( 'Choose from the most used Educator type', 'textdomain' ),
        'not_found'                  => __( 'No Educator type found.', 'textdomain' ),
        'menu_name'                  => __( 'Educator type', 'textdomain' ),
    );
    
    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'educator-type' ),
    );


    register_taxonomy( 'educator-type', 'educator', $args );
}





//Theme Options Add

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
   
}

add_image_size( 'gallery-size', 298, 205, true );
add_image_size( 'bannerimage-educators-list', 555, 168, true );
add_image_size( 'bannerimage-product-list',263,200, true );
add_image_size( 'bannerimage-courses-list',360,195, true );
add_image_size( 'country-flag',23,16, true );

add_action( 'init', 'ege_university_init' );
/**
 * Register a educator post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function ege_university_init() {
	$labels = array(
		'name'               => _x( 'Educators', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'educator', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Educators', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'educator', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'educator', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New educator', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New educator', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit educator', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View educator', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Educators', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Educators', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Educators:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Educators found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Educators found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'educator' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
	register_post_type( 'educator', $args );


	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Countries', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Country', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search countries', 'textdomain' ),
		'popular_items'              => __( 'Popular countries', 'textdomain' ),
		'all_items'                  => __( 'All countries', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Country', 'textdomain' ),
		'update_item'                => __( 'Update Country', 'textdomain' ),
		'add_new_item'               => __( 'Add New Country', 'textdomain' ),
		'new_item_name'              => __( 'New Country Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate countries with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove countries', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used countries', 'textdomain' ),
		'not_found'                  => __( 'No countries found.', 'textdomain' ),
		'menu_name'                  => __( 'Countries', 'textdomain' ),
	);
	
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'Country' ),
	);


	register_taxonomy( 'Country', 'educator', $args );

}
// University Data Sections Fileds
function educators_data_sections_request() {
		// The $_REQUEST contains all the data sent via ajax
		if ( isset($_REQUEST) ) {
				$countryslud = $_REQUEST['categoryname']; // Added by PK (22-04-2019)
				//$countryterm  = get_the_terms($countryid,'Country');
			    $school      = ege_count_educator_base_on_etype('school',$countryslud );
			    $university  = ege_count_educator_base_on_etype('university',$countryslud );
			    $college     = ege_count_educator_base_on_etype('college',$countryslud );
				$term 		 = get_term_by('slug',$countryslud, 'Country');
				$description = $term->description;
				?>
	            <div class="countr-ullibox">
	                <ul>
	                    <li>
	                        <span><?php echo $university; ?></span>
	                        <p>Universities</p>
	                    </li>
	                    <li>
	                        <span><?php echo $college; ?></span>
	                        <p>Colleges</p>
	                    </li>
	                    <li>
	                        <span><?php echo $school; ?></span>
	                        <p>High schools</p>
	                    </li>
	                </ul>
	            </div>
	            <div class="countr-detai-box">
	                <p><?php echo $description; ?></p>
	            </div>
				<?php
		}else{
				echo "<h3>No schools and universites founds for this country</h3>";
		} 
	     // Always die in functions echoing ajax content
	  	wp_die();
}
 
add_action( 'wp_ajax_educators_data_sections', 'educators_data_sections_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_educators_data_sections', 'educators_data_sections_request' );


//define in function file
function custom_pagination($numpages = '', $pagerange = '', $paged='')  {

	if (empty($pagerange)) {
	$pagerange = 2;
	}

	if ($paged == '') {
	global $paged;
		if (empty($paged)) {
		$paged = 1;
		}
	}
	if ($numpages == '') {
	global $wp_query;
		$numpages = $wp_query->max_num_pages;
		if(!$numpages) {
		$numpages = 1;
		}
	}
	    
	$pagination_args = array(
	'base'            => get_pagenum_link(1) . '%_%',
	'format' => '?page=%#%',
	'total'           => $numpages,
	'current'         => $paged,
	'show_all'        => false,
	'end_size'        => 1,
	'mid_size'        => $pagerange,
	'prev_next'       => true,
	'prev_text'       => __('Previous'),
	'next_text'       => __('Next'),
	'type'            => 'list',
	'add_args'        => true,
	'add_fragment'    => '',
	'after_page_number' => '',
	'before_page_number' =>'',
	);
	$paginate_links = paginate_links_two($pagination_args);

	if ( $paginate_links ) {
	    echo $paginate_links;
	}
}


function paginate_links_two( $args = '' ) {
	global $wp_query, $wp_rewrite;

	// Setting up default values based on the current URL.
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$url_parts    = explode( '?', $pagenum_link );

	// Get max pages and current page out of the current query, if available.
	$total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
	$current = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;

	// echo "string".$current;
	// $priviousnumber = $current -1;
	// $currentnumber = $current +1;
	// Append the format placeholder to the base URL.
	$pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

	// URL base depends on permalink settings.
	$format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

	$defaults = array(
		'base'               => $pagenum_link, // http://example.com/all_posts.php%_% : %_% is replaced by format (below)
		'format'             => $format, // ?page=%#% : %#% is replaced by the page number
		'total'              => $total,
		'current'            => $current,
		'aria_current'       => 'page',
		'show_all'           => false,
		'prev_next'          => true,
		'prev_text'          => __( '&laquo; Previous' ),
		'next_text'          => __( 'Next &raquo;' ),
		'end_size'           => 1,
		'mid_size'           => 2,
		'type'               => 'plain',
		'add_args'           => array(), // array of query args to add
		'add_fragment'       => '',
		'before_page_number' => '',
		'after_page_number'  => '',
	);

	$args = wp_parse_args( $args, $defaults );

	if ( ! is_array( $args['add_args'] ) ) {
		$args['add_args'] = array();
	}

	// Merge additional query vars found in the original URL into 'add_args' array.
	if ( isset( $url_parts[1] ) ) {
		// Find the format argument.
		$format = explode( '?', str_replace( '%_%', $args['format'], $args['base'] ) );
		$format_query = isset( $format[1] ) ? $format[1] : '';
		wp_parse_str( $format_query, $format_args );

		// Find the query args of the requested URL.
		wp_parse_str( $url_parts[1], $url_query_args );

		// Remove the format argument from the array of query arguments, to avoid overwriting custom format.
		foreach ( $format_args as $format_arg => $format_arg_value ) {
			unset( $url_query_args[ $format_arg ] );
		}

		$args['add_args'] = array_merge( $args['add_args'], urlencode_deep( $url_query_args ) );
	}

	// Who knows what else people pass in $args
	$total = (int) $args['total'];
	if ( $total < 2 ) {
		return;
	}
	$current  = (int) $args['current'];
	$priviousnumber = $current -1;
	$nextnumber = $current +1;
	$end_size = (int) $args['end_size']; // Out of bounds?  Make it the default.
	if ( $end_size < 1 ) {
		$end_size = 1;
	}
	$mid_size = (int) $args['mid_size'];
	if ( $mid_size < 0 ) {
		$mid_size = 2;
	}
	$add_args = $args['add_args'];
	$r = '';
	$page_links = array();
	$dots = false;

	if ( $args['prev_next'] && $current && 1 < $current ) :
		$link = str_replace( '%_%', 2 == $current ? '' : $args['format'], $args['base'] );
		$link = str_replace( '%#%', $current - 1, $link );
		if ( $add_args )
			$link = add_query_arg( $add_args, $link );
		$link .= $args['add_fragment'];

		/**
		 * Filters the paginated links for the given archive pages.
		 *
		 * @since 3.0.0
		 *
		 * @param string $link The paginated link URL.
		 */
		$page_links[] = '<a  data-page='.number_format_i18n($priviousnumber).' class="prev-link nav-links" href="javascript:void(0)">' . $args['prev_text'] . '</a>';
	endif;
	for ( $n = 1; $n <= $total; $n++ ) :
		if ( $n == $current ) :
			$page_links[] = "<span aria-current='" . esc_attr( $args['aria_current'] ) . "' class='page-numbers current'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</span>";
			$dots = true;
		else :
			if ( $args['show_all'] || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :
				$link = str_replace( '%_%', 1 == $n ? '' : $args['format'], $args['base'] );
				$link = str_replace( '%#%', $n, $link );
				if ( $add_args )
					$link = add_query_arg( $add_args, $link );
				$link .= $args['add_fragment'];

				/** This filter is documented in wp-includes/general-template.php */
				$page_links[] = "<a class='nav-links page-numbers' data-page='".number_format_i18n( $n )."'  href='javascript:void(0)'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a>";
				$dots = true;
			elseif ( $dots && ! $args['show_all'] ) :
				$page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';
				$dots = false;
			endif;
		endif;
	endfor;
	if ( $args['prev_next'] && $current && $current < $total ) :
		$link = str_replace( '%_%', $args['format'], $args['base'] );
		$link = str_replace( '%#%', $current + 1, $link );
		if ( $add_args )
			$link = add_query_arg( $add_args, $link );
		$link .= $args['add_fragment'];

		/** This filter is documented in wp-includes/general-template.php */
		$page_links[] = '<a  data-page='.number_format_i18n($nextnumber).' class="nav-links next-link" href="javascript:void(0)">' . $args['next_text'] . '</a>';
	endif;
	switch ( $args['type'] ) {
		case 'array' :
			return $page_links;

		case 'list' :
			$r .= "<ul class='pagination-nav cf'>\n\t<li>";
			$r .= join("</li>\n\t<li>", $page_links);
			$r .= "</li>\n</ul>\n";
			break;

		default :
			$r = join("\n", $page_links);
			break;
	}
	return $r;
}


// Course Listing Page ajax Funtions.

add_action( 'wp_ajax_course_listing_sections', 'ege_course_listing_sections' );
add_action( 'wp_ajax_nopriv_course_listing_sections', 'ege_course_listing_sections' );

function ege_course_listing_sections(){
			//print_r($queried_object);
				// session_start();
				// $_SESSION['studylevel'] 	 = $_REQUEST['studylevel'];	
				// $_SESSION['educatorid'] 	 = $_REQUEST['educatorname'];
				// $_SESSION['termid']			 = $_REQUEST['termid'];	
			 //    $_SESSION['countrydropdown'] = $_REQUEST['countrydropdown'];	

			   $sortbyvalue =  $_REQUEST['sortbyvaluedropdown'];
			   if(!empty($sortbyvalue)){
				$metavalue = '';
				$ordervalueby   = 'date';
				$orderby  ='ASC';
			   	if($sortbyvalue == 'price'){
			   		$metavalue = 'fees_per_year';
			   		$ordervalueby   = 'meta_value_num';
			   		$orderby  ='ASC';
			   	}
			   	if($sortbyvalue == 'durations'){
					$metavalue = 'listing_durations';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='ASC';
			   	}
			   	if($sortbyvalue == 'popularity'){
			   		$metavalue = 'post_views_counter';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='DESC';	
			   	}

			   	if($sortbyvalue == 'worldrank'){
			   		$metavalue = 'world_rank';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='DESC';	
			   	}
				if($sortbyvalue == 'scholarship'){
					$metavalue = 'scholarship_provide';
					$ordervalueby   = 'meta_value';
					$orderby  ='DESC';	
				}

			   }
	           if(!empty(trim($_REQUEST['termid']))){
					$term_id = $_REQUEST['termid'];
					$term = get_term_by( 'id', $term_id, 'subject');
					//print_r($term);
					$termsname = $term->name;
					//$termdescription = $term->description;
					if(!empty($term)){
					$subject_array = array(
									'taxonomy' => 'subject',
									'field'    => 'slug',
									'terms' => $term->slug
									);
					}
				}
			   $paged        = trim($_REQUEST['page']);
			   $studylevelid = trim($_REQUEST['studylevel']);
			   $educatorname = trim($_REQUEST['educatorname']);
			   $educator_array = array();
			   if(!empty($educatorname)){
				   	$educator_array = array(  
					'key' => 'select_university',
					'value' =>$educatorname,
					'compare' => "LIKE",
					);
				}
			   //echo $studylevelid;
			    $studylevel = get_term_by( 'id',$studylevelid, 'studylevel');
				if(!empty($studylevel)){
					$subject_array = array(
					'taxonomy' => 'studylevel',
					'field'    => 'slug',
					'terms' => $studylevel->slug
					);
				}

    			//$studylevel = '';
				if(!empty($term) && !empty($studylevel)) {
				     $subject_array = array( // (array) - use taxonomy parameters (available with Version 3.1).
					'relation' => 'AND', 
					array(
					  'taxonomy' => 'subject', // (string) - Taxonomy.
					  'field' => 'slug', // (string) - Select taxonomy term by Possible 
					  'terms' => $term->slug // (int/string/array) - Taxonomy term(s).
			
					),
					array(
					  'taxonomy' => 'studylevel',
					  'field' => 'slug',
					  'terms' => $studylevel->slug,
					)
				 );

				}

				// Countrt wise Dropdown
			    $countrydropdown = trim($_REQUEST['countrydropdown']);
				if(!empty($countrydropdown)){

					$universitypostss = get_posts(
						array(
						'post_type' => 'educator', // Post type
						'tax_query' => array(
						array(
						'taxonomy' => 'Country', // Taxonomy name
						'field' => 'slug',
						'terms' => $countrydropdown
						)
						),
						'posts_per_page' => -1,
						'orderby'       => 'menu_order',
						'order'         => 'ASC'
						)
					);  
  			
					// echo "<pre>";
					// print_r($universitypostss);
					// echo "</pre>";
					foreach ($universitypostss as $educator) {
					# code...
					$educatorid[] =  $educator->ID;

					}
					if(empty($educator_array )){
						$educator_array = array(  
						'key' => 'select_university',
						'value' =>$educatorid,
						'compare' => "IN",
						);
					}
				}	
			   //$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

			   //echo $paged;

					// $course_detail = array(
					// 'post_type' => 'post',
					// 'post_status' => 'publish',
					// 'posts_per_page' =>1,
					// 'order' => 'ASC',
					// //'orderby' =>'date',
					// 'paged' => $paged
					// );
			
				$the_query = new WP_Query( array(
					'posts_per_page' => 12,
					'paged' => $paged,
					'post_type'		=>'course',
					'tax_query' => array($subject_array),
					'meta_query' => array($educator_array),
					'meta_key' => $metavalue,
					'order' => $orderby,
                    'orderby' => $ordervalueby,
		 
					)
				);

				$queried_object = get_queried_object();
			    $term_id = $queried_object->term_id;
				$term = get_term_by( 'id', $term_id, 'subject');
				//print_r($term);
				// $the_query = new WP_Query( array(
				// 'posts_per_page' =>4,
				// 'post_type'		=>'course',
				// 	 )	
				// ); 
				if($the_query -> have_posts()){ ?>
			
		     <div class="loading overlay-bgcolor" style="display:none">
            <div class="all-middleicon-loader">  <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect"></div></div>
				<?php 
				$count = 0;
				while ($the_query -> have_posts()) : $the_query -> the_post();
				    $count++;
                    do_action('courses_html_actions',$count);
                    if($count >= 3){
                       $count = 0;
                    }
				endwhile;
				
				}else{
					echo "No Courses Founds";
				}

				wp_reset_postdata(); ?>
				 <div class="fullwidth-block">
			                <div class="page-linkox cf">
			                        <?php
			                        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
			                        ?>
			                </div>
                </div>
                <?php

	wp_die();
}

add_action( 'wp_ajax_educators_listing_sections', 'ege_educators_listing_sections' );
add_action( 'wp_ajax_nopriv_educators_listing_sections', 'ege_educators_listing_sections' );

function ege_educators_listing_sections(){

			// $_SESSION['educatorcountrydropdown']   	= $_REQUEST['countrydropdown'];
			// $_SESSION['educatoreducatortypeid']   	= $_REQUEST['educatortypeid'];
			// $_SESSION['educatortermid']  			= $_REQUEST['termid'];
			// $_SESSION['educatoreducatortype']   	= $_REQUEST['educatortype'];


			if(!empty($_REQUEST['educatortypeid'])){
				 $educatorid     = $_REQUEST['educatortypeid'];
				 $educators_list_unique = array($educatorid);
			}
			if(!empty($_REQUEST['termid'])){
				 $educators_list = array(1);
				$term = get_term_by( 'id',$_REQUEST['termid'], 'subject');
				if(!empty($term)){
					$subject_array = array(
					    'taxonomy' => 'subject',
					    'field'    => 'slug',
					    'terms' => $term->slug
					    );
					}
					$universityposts = get_posts(
					    array(
					    'posts_per_page' => -1,
					    'order' => 'ASC',
					    //'orderby' =>'date',
					    'paged' => $paged,
					    'post_type'     =>'course',
					    'tax_query' => array($subject_array),
					    )
					);
					if(!empty($universityposts)){ 
					    $unvisityid = array();
					    foreach ($universityposts as $postvalue) {
					        # code...
					         $courseid     =  $postvalue->ID;
					         $universityid = get_field('select_university',$courseid);

					         //print_r($studylevel); 
					         if(!empty( $universityid)){
					           $unvisityid[]   = $universityid;
					         }
					         //echo '<br>';
					    //echo $universityname = get_the_title($unvisityid);
					    }
					}
					if(!empty($unvisityid)){  
					    $educators_list = (array_unique($unvisityid));
					}
			
			}

			$sortbyvalue =  $_REQUEST['sortbyvaluedropdown'];
			if(!empty($sortbyvalue)){
				$metavalue = '';
				$ordervalueby   = 'date';
				$orderby  ='ASC';
				if($sortbyvalue == 'price'){
					$metavalue = 'fees_per_year';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='ASC';
				}
				// if($sortbyvalue == 'durations'){
				// 	$metavalue = 'listing_durations';
				// 	$ordervalueby   = 'meta_value_num';
				// 	$orderby  ='ASC';
				// }
				if($sortbyvalue == 'popularity'){
					$metavalue = 'post_views_counter';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='DESC';	
				}
			  if($sortbyvalue == 'worldrank'){
			   		$metavalue = 'world_rank';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='ASC';	
			   	}
			}
			$paged = $_REQUEST['page'];
			$countrydropdown = $_REQUEST['countrydropdown'];
			$educatortype    = $_REQUEST['educatortype'];
			$termslug 		 = $_REQUEST['countrydropdown'];
			$term 		     = get_term_by( 'slug',$termslug, 'Country');
			$termsname 	     = $term->name;
			$termsuid        = $term->term_id;
			$countryflag     = ege_countryflagurl($termsuid,'educator-list');

			if(!empty($countrydropdown)){
				$countrydropdownsarray = array(
				'taxonomy' => 'Country', // Taxonomy name
				'field' => 'slug',
				'terms' => $countrydropdown
				);
			}
			if(!empty($educatortype)){
					$countrydropdownsarray = array( // (array) - use taxonomy parameters (
					array(
					'taxonomy' => 'educator-type',
					'field' => 'slug',
					'terms' => $educatortype,
					//'include_children' => false,
					//'operator' => 'NOT IN'
					)
					);
			}
			if(!empty($countrydropdown) && !empty($educatortype)) {
				$countrydropdownsarray = array( // (array) - use taxonomy parameters (available with Version 3.1).
				'relation' => 'AND', // (string) - The logical relationship between each inner taxonomy array when there is more than one. Possible values are 'AND', 'OR'. Do not use with a single inner taxonomy array. Default value is 'AND'.
				array(
				'taxonomy' => 'Country', // (string) - Taxonomy.
				'field' => 'slug', // (string) - Select taxonomy term by Possible values are 'term_id', 'name', 'slug' or 'term_taxonomy_id'. Default value is 'term_id'.
				'terms' => $countrydropdown // (int/string/array) - Taxonomy term(s).
				//'include_children' => true, // (bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
				//'operator' => 'IN' // (string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS'. Default value is 'IN'.
				),
				array(
				'taxonomy' => 'educator-type',
				'field' => 'slug',
				'terms' => $educatortype,
				//'include_children' => false,
				//'operator' => 'NOT IN'
				)
				);

			}
				if(!empty($_REQUEST['educatortypeid'])){
						$the_query = new WP_Query( array(
					'posts_per_page' => 8,
					'order' => 'DESC',
					'post__in' => $educators_list_unique,
					'orderby' =>'date',
					'paged' => $paged,
					'post_type'     =>'educator',
					'order' => $orderby,
                    'orderby' => $ordervalueby,
				  )
				);
				}else{

						$the_query = new WP_Query( array(
					'posts_per_page' => 8,
					'order' => 'DESC',
					'post__in' => $educators_list,
					'orderby' =>'date',
					'paged' => $paged,
					'post_type'     =>'educator',
					'tax_query' => array($countrydropdownsarray
					),
					'meta_key' => $metavalue,
					'order' => $orderby,
                    'orderby' => $ordervalueby,
				  )
				);
				}	
	
		
			
				if(!empty($educatortype)){
							$educatortypeterm   = get_term_by( 'slug',$educatortype, 'educator-type');
							$educatorname 	    = $educatortypeterm->name;
							$educatormaintitle  = $educatorname;
				}else{
					$educatormaintitle = 'Educators';
			    }
                   if($the_query -> have_posts()){
                ?>
				<input type="hidden" class="total-records" value="<?php echo $the_query->max_num_pages; ?> Educators in <?php echo $termsname; ?>"> 
				<input type="hidden" class="main-title-mangement" value="<?php  echo $educatormaintitle.' of '.$termsname; ?>">                  
				<input type="hidden" class="main-title-img" value="<?php echo $countryflag; ?>">                  
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                            // print_r($the_query);   
                		do_action('educator_html_actions');
                    endwhile;
                    }else{
                    echo "No Educators Founds";
                    ?>
				<input type="hidden" class="total-records" value="<?php echo $the_query->max_num_pages; ?> Educators in <?php echo $termsname; ?>"> 
				<input type="hidden" class="main-title-mangement" value="<?php  echo $educatormaintitle.' of '.$termsname; ?>">  
                    <?php
                    }

                    wp_reset_postdata(); ?>
                        <div class="fullwidth-block">
                        <div class="page-linkox cf">
                        <?php

                        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                        ?>
                        </div>
                        </div>
             <?php
             
             wp_die();           

}

add_action( 'wp_ajax_educators_listing_header', 'ege_educators_listing_header' );
add_action( 'wp_ajax_nopriv_educators_listing_header', 'ege_educators_listing_header' );


function ege_educators_listing_header(){
$termslug 		 = $_REQUEST['countrydropdown'];
$term 		     = get_term_by( 'slug',$termslug, 'Country');
$termsname 	     = $term->name;
$termdescription = $term->description;

	?>		
					<?php if(!empty($termsname )){ ?>
	                <h3>Student life in <?php echo $termsname; ?></h3>	
		            <?php } ?>
		            <?php if(!empty($termdescription )){ 
                    	echo '<p>'.$termdescription.'</p>';
	                 } ?>

             <?php
             
             wp_die();       
}

/** Subject Page country Wise Eductor Come **/

add_action( 'wp_ajax_educator_listing_drodpown', 'ege_educator_listing_drodpown' );
add_action( 'wp_ajax_nopriv_educator_listing_drodpown', 'ege_educator_listing_drodpown' );

function ege_educator_listing_drodpown(){
	if(isset($_REQUEST['countrydropdown'])){
	session_start();
    //$_SESSION['countrydropdowneducator'] = $_REQUEST['countrydropdown'];	
	$universityposts = get_posts(
	array(
	'post_type' => 'educator', // Post type
	'tax_query' => array(
	array(
	'taxonomy' => 'Country', // Taxonomy name
	'field' => 'slug',
	'terms' => $_REQUEST['countrydropdown']
	)
	),
	'posts_per_page' => -1,
	'orderby'       => 'menu_order',
	'order'         => 'ASC'
	)
	);	

	 //$educators_list = array('184');
	?>         
                           <option value=" ">Institution name</option>
                           <?php
                           	if(!empty($universityposts)){
                                foreach ($universityposts as $postvalue) {
                                    # code...
                                    $posttitle = str_replace("'",'&apos;', $postvalue->post_title);
                                    $posttitle = str_replace("’",'&apos;', $postvalue->post_title);
                                    echo '<option value="'.$postvalue->ID.'">'.$posttitle.'</option>'
                                  
                                    ?>
                                    <?php

                                }
                        	}
                            ?> 

    <?php        
    }        
    wp_die();

}
add_action( 'wp_ajax_educator_listing_drodpown_two', 'ege_educator_listing_drodpown_two' );
add_action( 'wp_ajax_nopriv_educator_listing_drodpown_two', 'ege_educator_listing_drodpown_two' );

function ege_educator_listing_drodpown_two(){
	if(isset($_REQUEST['countrydropdown'])){
	session_start();
    //$_SESSION['countrydropdowneducator'] = $_REQUEST['countrydropdown'];	
	$universityposts = get_posts(
	array(
	'post_type' => 'educator', // Post type
	'tax_query' => array(
	array(
	'taxonomy' => 'Country', // Taxonomy name
	'field' => 'slug',
	'terms' => $_REQUEST['countrydropdown']
	)
	),
	'posts_per_page' => -1,
	'orderby'       => 'menu_order',
	'order'         => 'ASC'
	)
	);	

	 $educators_list = array('184');

	?>               

               			 <option value=" ">Institution name</option>
                           <?php
                           	if(!empty($universityposts)){
                                foreach ($universityposts as $postvalue) {
                                	$posttitle = str_replace("'",'&apos;', $postvalue->post_title);
                                	$posttitle = str_replace("’",'&apos;', $postvalue->post_title);
                                    # code...
                                    echo '<option value="'.$postvalue->ID.'">'.$posttitle.'</option>'
                                  
                                    ?>
                                    <?php

                                }
                        	}
                            ?> 

    <?php        
    }        
    wp_die();

}
// when sessison not work that time code below work.
// function boot_session() {
//  // session_start();
// }
// add_action('wp_loaded','boot_session');



// Star Rating Html Hooks

function wp_star_rating( $args = array() ) {
	$defaults = array(
		'rating' => 0,
		'type'   => 'rating',
		'number' => 0,
		'echo'   => true,
	);
	$r = wp_parse_args( $args, $defaults );

	// Non-English decimal places when the $rating is coming from a string
	$rating = (float) str_replace( ',', '.', $r['rating'] );

	// Convert Percentage to star rating, 0..5 in .5 increments
	if ( 'percent' === $r['type'] ) {
		$rating = round( $rating / 10, 0 ) / 2;
	}

	// Calculate the number of each type of star needed
	$full_stars = floor( $rating );
	$half_stars = ceil( $rating - $full_stars );
	$empty_stars = 5 - $full_stars - $half_stars;
	$output = '';
	if ( $r['number'] ) {
		/* translators: 1: The rating, 2: The number of ratings */
		$format = _n( '%1$s rating based on %2$s rating', '%1$s rating based on %2$s ratings', $r['number'] );
		$title = sprintf( $format, number_format_i18n( $rating, 1 ), number_format_i18n( $r['number'] ) );
	} else {
		/* translators: 1: The rating */
		$title = sprintf( __( '%s rating' ), number_format_i18n( $rating, 1 ) );
	}

	//$output = '<div class="star-rating">';
	$output .= '<span class=\'like-count\'>';
	$output .= str_repeat( '<i class=\'fa fa-star\' ></i>', $full_stars );
	$output .= str_repeat( '<i class=\'fa fa-star-half\' ></i>', $half_stars );
	$output .= str_repeat( '<i class=\'fa fa-star-o\'></i>', $empty_stars );
	$output .= '</span>';

	if ( $r['echo'] ) {
		echo $output;
	}

	return $output;
}

// Star Rating avarage Float code.
function wp_star_rating_avarage_float( $avarage ) {
	 $rating  = (float) str_replace( ',', '.', $avarage);
	 $output   = number_format_i18n( $rating, 1 );
	return $output;
}

// Rationd adding functionality.
function wp_star_rating_add( $args = array() ) {
	$defaults = array(
		'rating' => 0,
		'type'   => 'rating',
		'number' => 0,
		'echo'   => true,
	);
	$r = wp_parse_args( $args, $defaults );

	// Non-English decimal places when the $rating is coming from a string
	$rating = (float) str_replace( ',', '.', $r['rating'] );

	// Convert Percentage to star rating, 0..5 in .5 increments
	if ( 'percent' === $r['type'] ) {
		$rating = round( $rating / 10, 0 ) / 2;
	}

	// Calculate the number of each type of star needed
	$full_stars = floor( $rating );
	$half_stars = ceil( $rating - $full_stars );
	$empty_stars = 5 - $full_stars - $half_stars;

	if ( $r['number'] ) {
		/* translators: 1: The rating, 2: The number of ratings */
		$format = _n( '%1$s rating based on %2$s rating', '%1$s rating based on %2$s ratings', $r['number'] );
		$title = sprintf( $format, number_format_i18n( $rating, 1 ), number_format_i18n( $r['number'] ) );
	} else {
		/* translators: 1: The rating */
		$title = sprintf( __( '%s rating' ), number_format_i18n( $rating, 1 ) );
	}

	$output = '<div class="star-rating">';
	$output .= '<span class="screen-reader-text">' . $title . '</span>';
	$output .= str_repeat( '<div class="star star-full" aria-hidden="true"></div>', $full_stars );
	$output .= str_repeat( '<div class="star star-half" aria-hidden="true"></div>', $half_stars );
	$output .= str_repeat( '<div class="star star-empty" aria-hidden="true"></div>', $empty_stars );
	$output .= '</div>';

	if ( $r['echo'] ) {
		echo $output;
	}

	return $output;
}

/** Chart Functions for Review **/
function wp_star_rating_chart( $chatstar ) {
	$defaults = array(
		'rating' => $chatstar,
		'type'   => 'rating',
		'number' => 0,
		'echo'   => true,
	);
	$r = wp_parse_args( $argss, $defaults );

	// Non-English decimal places when the $rating is coming from a string
	$rating = (float) str_replace( ',', '.', $r['rating'] );

	// Convert Percentage to star rating, 0..5 in .5 increments
	if ( 'percent' === $r['type'] ) {
		$rating = round( $rating / 10, 0 ) / 2;
	}

	// Calculate the number of each type of star needed
	$full_stars = floor( $rating );
	$half_stars = ceil( $rating - $full_stars );
	$empty_stars = 5 - $full_stars - $half_stars;

	if ( $r['number'] ) {
		/* translators: 1: The rating, 2: The number of ratings */
		$format = _n( '%1$s rating based on %2$s rating', '%1$s rating based on %2$s ratings', $r['number'] );
		$title = sprintf( $format, number_format_i18n( $rating, 1 ), number_format_i18n( $r['number'] ) );
	} else {
		/* translators: 1: The rating */
		$title = sprintf( __( '%s rating' ), number_format_i18n( $rating, 1 ) );
	}

	//$output = '<div class="star-rating">';
	$output .= "<span class=\"like-count\">";
	$output .= str_repeat( "<i class=\'fa fa-star\' ></i>", $full_stars );
	$output .= str_repeat( "<i class=\'fa fa-star-half\' ></i>", $half_stars );
	$output .= str_repeat( "<i class=\'fa fa-star-o\'></i>", $empty_stars );
	$output .= "</span>";

	if ( $r['echo'] ) {
		echo $output;
	}

	return $output;
}


/**Get Country Flag Functions Hook.*/
function ege_countryflagurl($termid,$detail=null){

	$countryflag = get_field('add_icon', 'Country_'.$termid);
	if(!empty($countryflag)){
		//print_r($countryflag);
		if($detail == 'educator-list'){
	 		$countryflag  = $countryflag['sizes']['medium'];
	 	}else{
	 		$countryflag  = $countryflag['sizes']['country-flag'];
	 	}
	}else{
		$countryflag = '';
	}

	return $countryflag;
}


// Create  Product Custom Post Type


add_action( 'init', 'codex_Product_init',10 );
/**
 * Register a Product post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_Product_init() {
    $labels = array(
        'name'               => _x( 'Books', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Book', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Books', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Book', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Book', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Book', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Book', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Book', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Book', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Book', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Book', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Books:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Book found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Book found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'product' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
    );

    register_post_type( 'Product', $args );


    	$labels = array(
		'name'                       => _x( 'Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Categories', 'textdomain' ),
		'popular_items'              => __( 'Popular Categories', 'textdomain' ),
		'all_items'                  => __( 'All Categories', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Category', 'textdomain' ),
		'update_item'                => __( 'Update Category', 'textdomain' ),
		'add_new_item'               => __( 'Add New Category', 'textdomain' ),
		'new_item_name'              => __( 'New Category Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Categories with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove Categories', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Categories', 'textdomain' ),
		'not_found'                  => __( 'No Categories found.', 'textdomain' ),
		'menu_name'                  => __( 'Categories', 'textdomain' ),
	);
	
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'product-categories' ),
	);


	register_taxonomy( 'product-categories', 'product', $args );

		$labels = array(
		'name'                       => _x( 'Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Categories', 'textdomain' ),
		'popular_items'              => __( 'Popular Categories', 'textdomain' ),
		'all_items'                  => __( 'All Categories', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Category', 'textdomain' ),
		'update_item'                => __( 'Update Category', 'textdomain' ),
		'add_new_item'               => __( 'Add New Category', 'textdomain' ),
		'new_item_name'              => __( 'New Category Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Categories with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove Categories', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Categories', 'textdomain' ),
		'not_found'                  => __( 'No Categories found.', 'textdomain' ),
		'menu_name'                  => __( 'Categories', 'textdomain' ),
	);
	
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'product-categories' ),
	);


	register_taxonomy( 'product-categories', 'product', $args );

	    $labels = array(
        'name'                       => _x( 'Study Levels', 'taxonomy general name', 'textdomain' ),
        'singular_name'              => _x( 'Study Level', 'taxonomy singular name', 'textdomain' ),
        'search_items'               => __( 'Search Study Levels', 'textdomain' ),
        'popular_items'              => __( 'Popular Study Levels', 'textdomain' ),
        'all_items'                  => __( 'All Study Levels', 'textdomain' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Study Level', 'textdomain' ),
        'update_item'                => __( 'Update Study Level', 'textdomain' ),
        'add_new_item'               => __( 'Add New Study Level', 'textdomain' ),
        'new_item_name'              => __( 'New Study Level Name', 'textdomain' ),
        'separate_items_with_commas' => __( 'Separate Study Levels with commas', 'textdomain' ),
        'add_or_remove_items'        => __( 'Add or remove Study Levels', 'textdomain' ),
        'choose_from_most_used'      => __( 'Choose from the most used Study Levels', 'textdomain' ),
        'not_found'                  => __( 'No Study Levels found.', 'textdomain' ),
        'menu_name'                  => __( 'Study Levels', 'textdomain' ),
    );
    
    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'study-level-product' ),
    );

    register_taxonomy( 'study-level-product', 'product', $args );

}


// Product List Html Added

add_action( 'wp_ajax_product_listing_sections', 'ege_product_listing_drodpown' );
add_action( 'wp_ajax_nopriv_product_listing_sections', 'ege_product_listing_drodpown' );
function ege_product_listing_drodpown(){
				$paged = $_REQUEST['page'];
				$sortbyvalue =  $_REQUEST['sortbyvaluedropdown'];
				if(!empty($sortbyvalue)){
					$metavalue = '';
					$ordervalueby   = 'date';
					$orderby  ='ASC';
					if($sortbyvalue == 'price'){
					$metavalue = 'product_price';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='ASC';
					}
					if($sortbyvalue == 'popularity'){
					$metavalue = 'post_views_counter';
					$ordervalueby   = 'meta_value_num';
					$orderby  ='DESC';	
					}
				}

				$the_query = new WP_Query( array(
					'posts_per_page' => 10,
					'order' => 'DESC',
					'orderby' =>'date',
					'paged' => $paged,
					'post_type'     =>'product',
					'meta_key' => $metavalue,
					'order' => $orderby,
                    'orderby' => $ordervalueby,
					)
				);
                // echo "</pre>";
                if($the_query -> have_posts()){
                ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
					$productid       = get_the_id();
                    $producttitle    = get_the_title();
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'full');
                   // $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'bannerimage-product-list');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                    $countview       = pvc_get_post_views($productid);
                    $bestseller      = get_field('best_seller',$productid);

                    $productcontent  = get_the_excerpt();
                    $terms           = get_the_terms($productid,'product-categories');
                    if(!empty($terms)){
                    $categoryname    = $termsname = $terms[0]->name;
                    $termsid         = $terms[0]->term_id;
                    // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    }
                    $productprice    = get_field('product_price',get_the_id());
                    //Rating Code

                    //Review Sections  Code
                    global $wpdb;
                    $table_name = $wpdb->prefix . "review";
                    $totalproductreviws = 0;
                    $args = '';
                    $avarage =0;
                    $totalaverage =0;
                    $productview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE 
                        status = 1 AND `product_id` = ".$productid );
                    if(!empty($productview)){
                        $totalproductreviws =  count($productview);
                        $totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE  status = 1 AND `product_id` =".$productid);
                        $avarage                    = $totalrating->avarage;
                        $totalaverage               = wp_star_rating_avarage_float($avarage);
                    }
                      $args = array(
                       'rating' => $avarage,
                       'type' => 'rating',
                       'number' => 5,
                    );
                    ?>
                    <div class="three-block fourblock pagepro-block">
                        <div class="white-bg ourcourse-block">
                            <div class="course-imgblock">
                                <span class="imgmain-box"><img src="<?php echo $bannerimages; ?>" alt=""></span>
                                <?php if( !empty($bestseller)){ ?>
                                <div class="bachlabl-leftbox">
                                    <span class="pre-bachelr-label">
                                          <p>Bestseller</p>
                                    </span>
                                </div> 
                                <?php } ?> 

                                <div class="inner-titwid-img">
								  <?php /*
                                   <div class="ourcourse-contbox cf" >
                                       <h4><?php echo $producttitle;?></h4>
                                        <?php if(!empty($productcontent)){ ?>
                                        <p><?php echo substr($productcontent,'0','172').'..'; ?> </p>
                                        <?php } ?>
                                    </div>
									*/ ?>
                                </div>
                            </div>
                            <?php  echo do_action('product_short_list_actions'); ?>

                            <div class="our-courseblk-content">
                                <a href="<?php echo get_permalink(); ?>"><h3><?php echo $producttitle;?> </h3></a>
                                <?php if(!empty($categoryname)) { ?>
                                    <div class="ourcor-smlcont-box"><?php echo $categoryname; ?> 
                                    </div>
                                <?php } ?>
                                <div class="cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                            <a class="ratenum-like" href="javascript:void(0)" title="like">
                                                <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                            </a>
                                        </div>
                                        <?php if(!empty($totalproductreviws)){ ?>
                                        <div class="review-texts">
                                            <span><?php echo $totalproductreviws; ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="year-view-box">
                                    <?php if(!empty($productprice)) { ?>
                                    <span class="rate-leftbox">From <i>£<?php echo $productprice; ?> </i></span>
                                    <?php } ?>

                                    <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assettwo/images/view-ico.png">views: <i><?php echo $countview; ?></i></span>
                                </div>
                            </div>
                        </div>
                </div>
                <?php  endwhile; ?>
                <?php }else{
                    echo "No Products Founds";
                    }
		?>
	       <div class="fullwidth-block">
                <div class="page-linkox cf">
                    <?php

                if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                ?>
                </div>
            </div>
            <?php

			wp_die();
}







add_action('wp_head','ede_payemnt_process_insert');

function  ede_payemnt_process_insert(){
	if(is_user_logged_in()){
		if(isset($_REQUEST['item_number']))	{	
		    //print_r($_REQUEST);

		    $userid = get_current_user_id();
		    $payer_email = 'email';
		    $Product_id = $_REQUEST['item_number'];
		    //$receiver_id = $_REQUEST['receiver_id'];
		    $payment_status = $_REQUEST['product_type'];
		    $payer_id = $_REQUEST['payer_id'];
		    $receiver_id = $_REQUEST['receiver_id'];
		    $payer_email = $_REQUEST['payer_email'];
		    $txn_id = $_REQUEST['txn_id'];
		    $payment_status = $_REQUEST['payment_status'];
		    $product_type   = 'product';
			$downloadbookurls = get_field('product_pdf',$Product_id);
			if(!empty($downloadbookurls)){
				$status = 1;
			}else{
				$status = 0;
			}
		    $payment_status = $_REQUEST['payment_status'];
		    $payment_status = $_REQUEST['payment_status'];
		    $userid = get_current_user_id();
		    //$userid = get_current_user_id();
			$datecreated 		= date('Y-m-d H:i:s');
			$txtidthere = checktxtidthere($txn_id);
			if(empty($txtidthere)){
			global $wpdb;
			$table = $wpdb->prefix.'order_history';
			$data = array('userid'=>$userid,'payer_email'=>$payer_email,'product_id' =>$Product_id, 'product_type' =>$product_type,'status'=>$status,'payment_status'=>$payment_status,'receiver_id'=>$receiver_id,'create_datetime'=>$datecreated ,'update_datetime'=>$datecreated,'txn_id'=>$txn_id);
			 $format = array('%d','%s','%d','%s','%d','%s','%s','%s','%s','%s');
			  $insert_id =$wpdb->insert($table,$data,$format);

				$subject = "Thank You For Purchased Book";
				$message = "your order is successfully processed";
				ob_start();
			?>
			<table border="0" cellpadding="10" cellspacing="0" style=
			"background-color: #FFFFFF" width="100%">
			<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" class=
			"content" style="background-color: #FFFFFF; border-collapse:collapse; width:100%; max-width:600px">
			  <tr>
			    <td id="templateContainerHeader" valign="top" mc:edit="welcomeEdit-01" style="font-size:14px; padding-top:2.429em; padding-bottom:.929em">
			      <p style="text-align:center;margin:0;padding:0; color:#545454; display:block; font-family:Helvetica; font-size:16px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal;"><img  src="http://www.eglobaleducation.co.uk/wp-content/themes/eglobalconnect/assettwo/images/logo.png"
			      style="display:inline-block;height:50%;width:30%;"></p>
			    </td>
			  </tr>
			  <tr>
			    <td align="center" valign="top">
			      <table border="0" cellpadding="0" cellspacing="0" class=
			      "brdBottomPadd-two" id="templateContainer" width="100%" style="border-collapse:collapse; border-top:1px solid #e2e2e2;border-left:1px solid #e2e2e2; border-right:1px solid #e2e2e2;border-radius:4px 4px 0 0; background-clip:padding-box; border-spacing:0; border-bottom:1px solid #f0f0f0">
			        <tr>
			          <td class="bodyContent" valign="top" mc:edit="welcomeEdit-02" style="color:#505050; font-family:Helvetica; font-size:14px; line-height:150%; padding-top:3.143em; padding-right:3.5em; padding-left:3.5em; text-align:left; padding-bottom:.857em;">
			            <h4 style="color:#4E4E4E; display:block; font-family:Helvetica; font-size:16px; line-height:1.154em; font-style:normal; font-weight:400; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0; text-align:left;"><b>Dear <?php echo $user_displayname; ?>,</h4> 

			            <p style="color:#2e2e2e; display:block; font-family:Helvetica; font-size:14px;line-height:1.385em; font-style:normal; font-weight:300; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;
			text-align:left"><?php echo $message;  ?></p><p style="color:#2e2e2e; display:block; font-family:Helvetica; font-size:14px;line-height:1.385em; font-style:normal; font-weight:300; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;
			text-align:left">
						
			       <a class="blue-btn" download="download" href=
			            "<?php echo $downloadbookurls  ?>">Click here</a> to Download Book.</p>
			                  <h4 style="color:#4E4E4E; display:block; font-family:Helvetica; font-size:16px; line-height:1.154em; font-style:normal; font-weight:400; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0; text-align:left;"><b>Thank you!</h4> 

			          </td>
			        </tr>
			      </table>
			    </td>
			  </tr>
			<tr>
			    <td align="center" class="unSubContent" id="bodyCellFooter" valign=
			    "top" style="margin:0;padding:0;width:100%!important;padding-top:10px;padding-bottom:15px">
			      <table border="0" cellpadding="0" cellspacing="0" id=
			      "templateContainerFooter" width="100%" style="border-collapse:collapse;">
			        <tr>
			          <td valign="top" width="100%" mc:edit="welcomeEdit-11">
			            <h6 style="text-align:center;margin-top: 9px; color:#a1a1a1; display:block; font-family:Helvetica; font-size:12px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal; margin-right:0; margin-bottom:0; margin-left:0;">Eglobal Education</h6>
			                <h6 style="text-align:center; color:#a1a1a1; display:block; font-family:Helvetica; font-size:12px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal; margin-right:0; margin-top:0; margin-bottom:0; margin-left:0;">121 King Street ,London 3000 United Kingdom</h6>
			          </td>
			        </tr>
			      </table>
			    </td>
			  </tr>
			</table>
			</td>
			</tr>
			</table>
			<?php
			$html = ob_get_contents();
			ob_end_clean();
				$headers = array('Content-Type: text/html; charset=UTF-8');
				wp_mail($to, $subject, $html, $headers);
			}
			//$wpdb->show_error();
			 //$wpdb->last_error;
			// print_r($wpdb);
				// if($wpdb->last_error !== '') {
				//     $wpdb->print_error();
				// }
			}
	}

}

// add_action( 'publish_book', 'edf_course_published_notification', 10, 2 );

// function edf_course_published_notification(){



// }


// Post Ajax list code Here

// Product List Html Added

add_action( 'wp_ajax_posts_listing_sections', 'ege_posts_listing_drodpown' );
add_action( 'wp_ajax_nopriv_posts_listing_sections', 'ege_posts_listing_drodpown' );
function ege_posts_listing_drodpown(){
				$paged = $_REQUEST['page'];
		
				$metavalue = '';
				$ordervalueby   = 'date';
				$orderby  ='ASC';
				$the_query = new WP_Query( array(
					'posts_per_page' => 5,
					'order' => 'DESC',
					'orderby' =>'date',
					'paged' => $paged,
					'post_type'     =>'post',
					)
				);
                // echo "</pre>";
                    if($the_query -> have_posts()){
                    ?>
                    <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    $productid       = get_the_id();
                    $producttitle    = get_the_title();
					//bannerimage-product-list;
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'full');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                    $productcontent = get_the_content();
                    $productcontent = preg_replace("/<img[^>]+\>/i", "", $productcontent);  
                    $productcontent = apply_filters('the_content', $productcontent);
                    $productcontent = str_replace(']]>', ']]>', $productcontent);
                  
                    // $terms           = get_the_terms($productid,'product-categories');
                    // if(!empty($terms)){
                    // $categoryname    =  $termsname = $terms[0]->name;
                    // $termsid         = $terms[0]->term_id;
                    // // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    // }

                    //Review Sections  Code
                    ?>
                    <div class="blog-mainbox cf">
                        <div class="left-blog-imgbox equal-height" style="background-image:url(<?php echo $bannerimages; ?>);"></div>
                        <div class="right-blog-box equal-height">
                            <div class="table">
                                <div class="table-cell">
                                    <div class="ritblog-content">
                                        <div class="blog-datebox">05/06/2017</div>
                                        <h4><?php echo get_the_title(); ?> </h4>
										<?php echo substr(strip_tags(trim($productcontent)),'0','325').'...'; ?>
                                        <div class="blog-viewmor-box">
                                            <a href="<?php echo get_permalink(); ?>">read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php  endwhile; ?>
                    <?php }else{
                    echo "No Post Founds";
                    }?>
	       <div class="fullwidth-block">
                <div class="page-linkox cf">
                    <?php

                if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                ?>
                </div>
            </div>
            <?php

			wp_die();
}

/**
When Update Course code That Time Create Durations Fileds.
**/
function on_all_status_transitions($post_id ) {
	    $post_type = get_post_type($post_id);

	    if($post_type == 'course'){
			update_post_meta( $post_id, 'listing_durations',0); 
	        $month 		= get_field('durations_month',$post_id);
		    $time_durations = get_field('time_durations',$post_id);
			if(!empty($time_durations) || !empty($month)){
				if(!empty($month)){
					$time_durationstotal = $month;
				}
				if(!empty($time_durations) && $time_durations !=0 ){
					$time_durationstotal = $time_durations * 12;
				}
				if(!empty($month) && !empty($time_durations)){
					$time_durationstotal = $month + $time_durationstotal;
				}
				update_post_meta( $post_id, 'listing_durations',$time_durationstotal); 
			}
		}
       // wp_die();
    //}
}
add_action(  'save_post',  'on_all_status_transitions', 10, 3 );




/** Home Social Sections Html **/
add_action('home-social-section','ege_home_social_html',10);

function ege_home_social_html(){
	?>
    <section class="home-social-section">
    	<div class="wrapper cf">
            <div class="socal-titbox">
                <h2>Social Media</h2>
            </div>
    	    <div class="home-sicalbox">
    	        <ul>
    	            <li class="twiter-soc"><a href="https://twitter.com/eGconnections"><i class="fa fa-twitter"></i> Follow us on Twitter</a></li>
    	            <li class="facebook-soc"><a href="https://www.facebook.com/eGlobalConnections/"><i class="fa fa-facebook"></i> Like us on Facebook</a></li>
    	            <li class="insta-soc"><a  href="https://www.instagram.com/eglobalconnnections/?hl=en"><i class="fa fa-instagram"></i> Follow us on Instagram</a></li>
    	        </ul>
    	    </div>
    	</div>
 
    </section>
<?php
}

/** Advance Serach Filter Code **/
add_action('serach_filter_code','ege_search_filter_html');

function ege_search_filter_html(){ 
	$find_your_perfect_course_title 	  = get_field('find_your_perfect_course_title');
	$find_your_perfect_Institutions_title = get_field('find_your_perfect_Institutions_title');
	?>
	<div class="course-edcutor-radio input-name">
		<form>
		<span data-id="1" class="select-adserch-filter-course from-span-class select-adserch-filter">
		<input checked-id="course-filter" class="radio-one-course checkslider-id" type="radio" name="courses"  checked  ><label for="checkslider-id">Courses</label></span> 
		<span data-id="2" class="select-adserch-filter-institute from-span-class select-adserch-filter"><input checked-id='institution-filter' class="radio-two-institue checkslider-id2"  type="radio" name="courses"><label for="checkslider-id2">Institutions</label></span> 
		</form>
	</div>
	<form  class="sub-toggle-course" method="post" action="<?php echo site_url(); ?>/subject/">
				<h1><?php echo $find_your_perfect_course_title;  ?></h1>
		
				<div class="banner-course-filter-wrap">
					<div class="course-filter">
						<?php 
						$country_args = array(
						'orderby'       => 'name', 
						'order'         => 'ASC',
						'parent'        => 0,
						'hide_empty'    => true, 

						);

						$countrylist = get_terms('Country',$country_args);

						?>
                        <select name='country-id' class="custom-select chosen-serach-dropdown chosen-select">
                            <option value="">Country</option>
                        	<?php
							foreach ($countrylist as $country ) {
							# code...
								echo '<option value="'.$country->slug.'">'.$country->name.'</option>';
							}	
							?>
                        </select>



						<!--<a href="#" class="sub-toggle" id="sub3">Study level</a>
						<ul class="dropdown-content">
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te koop">Country</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Huur">Study level</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Apart">Subject area</a></li>
						</ul>-->
				<!-- 		<a href="#" class="sub-toggle" id="sub3">Country</a>
						<ul class="dropdown-content">
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te koop">Country</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Huur">Study level</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Apart">Subject area</a></li>
						</ul> -->
					</div>
					<div class="course-filter">
						<?php
						$study_args = array(
						'orderby'       => 'name', 
						'order'         => 'ASC',
						'parent'        => 0,
						'hide_empty'    => true, 

						);

						$studylevllist = get_terms('studylevel',$study_args);

						?>
                        <select name='studylevel-id' class="custom-select chosen-serach-dropdown chosen-select">
                            <option value="">Study level</option>
                        	<?php
							foreach ($studylevllist as $studylevel ) {
							# code...
								echo '<option value="'.$studylevel->term_id.'">'.$studylevel->name.'</option>';
							}	
							?>
                        </select>
                  
						<!--<a href="#" class="sub-toggle" id="sub3">Study level</a>
						<ul class="dropdown-content">
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te koop">Country</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Huur">Study level</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Apart">Subject area</a></li>
						</ul>-->
					</div>
					<div class="course-filter">
						<!--<a href="#" class="sub-toggle" id="sub3">Subject area</a>
						<ul class="dropdown-content">
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te koop">Country</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Huur">Study level</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Apart">Subject area</a></li>
						</ul>-->
					   <?php
						$subject_args = array(
						'orderby'       => 'name', 
						'order'         => 'ASC',
						'parent'        => 0,
						'hide_empty'    => true, 

						);

						$subjectlist = get_terms('subject',$subject_args);

						?>
                        <select name='subject-id' class="custom-select chosen-serach-dropdown  custom-select-two">
                            <option value=" ">Subject Area</option>
                        	<?php
							foreach ($subjectlist as $subject ) {
							# code...
								echo '<option value="'.$subject->term_id.'">'.$subject->name.'</option>';
							}	
							?>
                        </select>
				
					</div>

					<div class="course-filter search-btn">
						<button class="Search" value="Search" title="Search">Search</button>
					</div>
        </div>
				</form>
		<form style="display:none" class="sub-toggle-institution" method="post" action="<?php echo site_url(); ?>/educators/">
				<?php if(!empty($find_your_perfect_Institutions_title)){ ?>
				<h1><?php echo $find_your_perfect_Institutions_title;  ?></h1>
				<?php } ?>
		
				<div class="banner-course-filter-wrap">
					<div class="course-filter">
						<?php 
						$country_args = array(
						'orderby'       => 'name', 
						'order'         => 'ASC',
						'parent'        => 0,
						'hide_empty'    => true, 

						);

						$countrylist = get_terms('Country',$country_args);


						?>
                        <select name='country-id' class="chosen-select">
                            <option value="">Country</option>
                        	<?php
							foreach ($countrylist as $country ) {
							# code...
								echo '<option value="'.$country->slug.'">'.$country->name.'</option>';
							}	
							?>
                        </select>



						<!--<a href="#" class="sub-toggle" id="sub3">Study level</a>
						<ul class="dropdown-content">
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te koop">Country</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Huur">Study level</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Apart">Subject area</a></li>
						</ul>-->
				<!-- 		<a href="#" class="sub-toggle" id="sub3">Country</a>
						<ul class="dropdown-content">
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te koop">Country</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Huur">Study level</a></li>
							<li><a href="#" title="" class="drp_value" data-id="sub4" value="Te Apart">Subject area</a></li>
						</ul> -->
					</div>
					<div class="course-filter">
						<?php
						$study_args = array(
						'orderby'       => 'name', 
						'order'         => 'ASC',
						'parent'        => 0,
						'hide_empty'    => true, 

						);

						
                        $educator_typelist = get_terms('educator-type',$educator_type_args);
                        if(!empty( $educator_typelist)){
						?>
                        <select name='educatoridtype-id' class="chosen-select">
                            <option value="">Institution Type</option>
                        	<?php
							foreach ($educator_typelist as $educator_type  ) {
							# code...
							   $posttitle = str_replace("'",'&apos;',$educator_type->name);
								echo '<option value="'.$educator_type->slug.'" '.$selected.'  >'.$posttitle.'</option>';
							}	
							?>
                        </select>
                    <?php } ?>
					</div>
					<div class="course-filter">
					   <?php
						$subject_args = array(
						'orderby'       => 'name', 
						'order'         => 'ASC',
						'parent'        => 0,
						'hide_empty'    => true, 

						);

						$subjectlist = get_terms('subject',$subject_args);

						?>
                          <select name='subject-id' class="custom-select chosen-serach-dropdown  custom-select-two">
                            <option value=" ">Subject Area</option>
                        	<?php
							foreach ($subjectlist as $subject ) {
							# code...
								echo '<option value="'.$subject->term_id.'">'.$subject->name.'</option>';
							}	
							?>
                        </select>
				
					</div>
					<div class="course-filter search-btn">
						<button class="Search" value="Search" title="Search">Search</button>
					</div>
        </div>
				</form>
<?php
}


/** Populr Subject Html Code **/

add_action('popular_subject_html','ege_popular_subject_html');

function ege_popular_subject_html(){
	$popular_subjects = get_field('popular_subjects');

	?>
	<section id="popular-subjects" class="popular-subjects">
		<div class="wrapper">
		<h2 data-aos="fade-right" data-aos-duration="600" class="aos-init aos-animate"><?php echo $popular_subjects; ?></h2>
			<?php
				$cat_args = array(
					'orderby'       => 'term_id', 
					'order'         => 'ASC',
					'parent'        => 0,
					'hide_empty'    => true, 
					);

					
					//echo "CHidren";
					$subjectterms = get_terms('subject',$cat_args);
					// echo "<pre>";
					// print_r($subjectterms);
					// echo "</pre>";	

					?>
			<div id="popular-subjects" class="owl-carousel owl-theme">
					<?php
					$count = 0;
					if(!empty($subjectterms)){
						foreach ($subjectterms as $key => $value) {
						$count++;	
						# code...

						$subjectname = $value->name;
						$subjectid   = $value->term_id;
						$childerenterm  = get_term_children( $subjectid, 'subject' );

					$subjecticonimage    =  get_field('subject_icon','subject_'.$subjectid);
					if(empty($subjecticonimage)){
					$subjecticonimage   = get_stylesheet_directory_uri() .'/assettwo/images/agriculture-icon.png';
					}

						$subjectimage =  get_field('subject_image','subject_'.$subjectid);
							$data_animate = '';
							if($count == 1){
								$data_animate = 'data-aos="fade-right" data-aos-duration="600"';
							}
							if($count == 2){
								$data_animate = 'data-aos="fade-up" data-aos-duration="800"';
							}
							if($count == 3){
								$data_animate = 'data-aos="fade-left" data-aos-duration="1100"';
							}
							if(!empty($childerenterm  )){
							?>

							<div class="item subject-block aos-init aos-animate"  <?php echo $data_animate; ?>>
								<div class="subject-block-inner">
									<div class="sub-image">
										<?php if(!empty($subjectimage)){ ?>
										<img src="<?php echo $subjectimage; ?>">
										<?php }else{ ?>
										<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/agriculture.png">
										<?php } ?>
										<div class="image-text">
											<img src="<?php echo $subjecticonimage; ?>">
											<p><?php echo $subjectname; ?></p>
										</div>
									</div>
									<div class="sub-bottom">
										<ul>
											<?php	
										
												// echo "<pre>";
												// print_r($childerenterm);
												// echo "</pre>";
												if(!empty($childerenterm )){
													$counts=0;
													foreach ($childerenterm as $value) {
														$counts++;
														if($counts == 5){
															break;
														}
														$subjectcourse =0;
														$term = get_term_by( 'id', $value, 'subject');
														if(!empty($term->slug))	{
															$subjectcourse = get_posts( array(
															'posts_per_page' => 5,
															'post_type'		=>'course',
															'tax_query' => array(
															array(
															'taxonomy' => 'subject',
															'field'    => 'slug',
															'terms' => $term->slug
															)
															) ));
														}
														//echo  count($myposts);
													# code...
														$term = get_term_by( 'id', $value, 'subject');

														echo '<li><a title="'.$term->name.'" href="'.get_term_link($term->term_id,'subject').'"><h4>'.$term->name.'</h4></a>';
														if(!empty($subjectcourse)){
															echo '<p>'.count($subjectcourse).' '.'Courses</p>';
														}else{
															echo '<p>'.count($subjectcourse).' '.'Courses</p>';
														}
														echo "</li>";

													}
												}
											?>
										</ul>
										<a class="view-job" href="<?php echo get_term_link($subjectid,'subject'); ?>" title="view all">view all</a>
									</div>
								</div>
							</div>
							<?php

						}
					}
					?>
			</div>
		<?php } ?>
			<div class="all-subject-link">
				<a href="<?php echo site_url('all-subject-listings');  ?>" title="view all subjects">view all subjects</a>
			</div>
		</div>
	</section>
<?php
}

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

function egobal_get_the_excerpt($post_id) {
  global $post;  
  $save_post = $post;
  $post = get_post($post_id);
  $output = get_the_excerpt($post_id);
  $post = $save_post;
  return $output;
}

/**
* Eductor  Listing Page Html Actions. 
**/
add_action('educator_html_actions','egobal_eductor_list_html_functions');
function  egobal_eductor_list_html_functions(){
							$avarage ='';
							$universityid       = get_the_id();
                            $unvisitylogo       = get_field('university_logo',$universityid );
                            $universitytitle    = get_the_title();
                            $universitycontent  = get_the_excerpt();
                            $terms              = get_the_terms($universityid,'Country');
                            if(!empty($terms)){
		                            $countryname =  $termsname = $terms[0]->name;
		                            $termsid     = $terms[0]->term_id;
		                            $countryflag = ege_countryflagurl($termsid,'educator-list');
                            }
                           // $countryname        =  $termsname = $terms[0]->name;
                            $unvisitylogo       = get_field('university_logo');
                            $world_rank         = get_field('world_rank',get_the_id());
                            $countview          = pvc_get_post_views($universityid);
                            $bannerimage        = get_the_post_thumbnail_url( get_the_id(),  'bannerimage-educators-list' );
                            if(empty($bannerimage)){
                              $bannerimages  =get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                            }
                            if(empty($countview)){
                              $countview = 0;  
                            }
                            $wordranktooltipcontent = get_field('world_rank_tooltip');
							//$coursereviewid = get_the_id();
							$totalaverage = 0.0;
							global $wpdb;
							$table_name = $wpdb->prefix . "review";
							$productview = $wpdb->get_results( "SELECT * FROM 	$table_name  WHERE `status` = 1  AND `educators_id` = ".$universityid );
							// echo '<pre>';
							// print_r($productview);
							// echo  '</pre>';
							if(!empty($productview)){
							$totalproductreviws =  count($productview);
							$totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE `status` = 1  AND `educators_id` = ".$universityid );
							//var_dump($totalrating);
							
							$avarage                    = $totalrating->avarage;
							$totalaverage               = wp_star_rating_avarage_float($avarage);

							}
							$labelmessag  = egobal_get_the_review_label($totalaverage);
							$args = array(
							'rating' => $avarage,
							'type' => 'rating',
							'number' => 5,
							);
                        ?>
                    <div class="loading overlay-bgcolor" style="display:none">
                        <div class="all-middleicon-loader"> <img width="150" src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect"></div>
                    </div>   
                    <div class="two-blocks educator-blocks">
                        <div class="school-detail-block aos-init aos-animate" data-aos="fade-left" data-aos-duration="600">
                            <div class="school-image">
                                <?php  if(!empty($unvisitylogo)) { ?>
                                <span class="min-imgbox educatort-logo-image"><img src="<?php echo $unvisitylogo ?>" width="65" /></span>
                               <?php }  ?>
                                <?php
                                if(!empty($bannerimage)){
                                    echo get_the_post_thumbnail($universityid,'bannerimage-educators-list');
                                }else{
                               ?>
                                <img src="<?php echo $bannerimages; ?>">
                                <?php
                             } ?>
                                <?php 
                                    echo do_action('educator_short_list_actions')
                                 ?>
                    
                                <div class="image-overlay">
                                    <div class="image-overlay-inner">
                                        <?php echo $universitycontent; ?>
                                        <?php /* <span>there is a course in english</span> */ ?>
                                    </div>
                                </div>
                            </div>
                            <div class="school-details-wrap">
                                <a title="<?php echo $universitytitle; ?>" href="<?php echo get_permalink(); ?> "><h3><?php
                           		 $wordlenght =  strlen($universitytitle);
                           		 $doted = '';
                           		 if($wordlenght >= 100){
                           		 	$doted =  "...";
                           		 }
                             	echo substr($universitytitle,0,100).$doted;  ?> </h3></a>

                            <?php if(!empty($countryname)){?>
                            <span class="school-flag">
                                <?php if(!empty($countryflag)) {?>
                                            <img src="<?php echo $countryflag ?>" width="25" />
                                            <?php } ?>
                                            <p><?php echo  $countryname; ?></p>
                            </span>
                            <?php } ?>
                                <div class="school-detail-bottom">
                                	                 	<div class="rate-star" style="display:none">
                                    <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                    <?php if(!empty($totalaverage)){?>
                                    <a class="ratenum-like" href="javascript:void(0)" title="like">
                                        <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                    </a>
                                    <?php } ?>
                                     <div class="rate-discr" ><p>Total Rating 4 out of 5 - Good</p></div>
                                	</div>
                                    <div class="rate-star">
	                                    <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                        <span class="like-count" style="display:none">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a class="rate-like" href="javascript" title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
                                  <div class="rate-discr" ><p><?php echo $labelmessag; ?></p></div>
                                    </div>

                                    <div class="world-ranks">
                                        <?php  if(!empty($world_rank )){  ?>
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/graph-ico.png">
                                        <p>World Ranking:
                                            <?php echo $world_rank;  ?>
                                        </p>
                                        <?php } ?>
                                        <?php if(!empty($wordranktooltipcontent)){ ?>
                                          <div class="worldrank-desc" ><p><?php echo   $wordranktooltipcontent; ?></p></div>
                                       <?php } ?>
                                    </div>
                                    <div class="views-count">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png">
                                        <p>Views:
                                            <?php echo $countview; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
}

/**
* Course  Listing Page Html Actions. 
**/
add_action('courses_html_actions','egobal_courses_list_html_functions');
function  egobal_courses_list_html_functions($count=null){
				       	//echo get_the_id();
				       // echo "<br>";
                        $unvisityid 	 = get_field('select_university',get_the_id());
						$universityname  = get_the_title($unvisityid);
						$terms  		 = get_the_terms($unvisityid,'Country');
						$countryname     = $termsname = $terms[0]->name;

                        if(!empty($terms)){
                            $countryname = $termsname = $terms[0]->name;
                            $termsid     = $terms[0]->term_id;
                            $countryflag = ege_countryflagurl($termsid);
                        }

						 $feesperyear    = get_field('fees_per_year',get_the_id());
                         $time_durations_year = get_field('time_durations',get_the_id());
				 		 $time_durations_months  = get_field('durations_month',get_the_id());
                         $displaydurationssvalue = '';
                         if(!empty($time_durations_months)){
                            $displaydurationssvalue = $time_durations_months.' '.'Months';
                         }
                         if(!empty($time_durations_year)){
                            $displaydurationssvalue = $time_durations_year.' '.'Years';
                         }
                         if(!empty($time_durations_year) && !empty($time_durations_months)){
                            $displaydurationssvalue = $time_durations_year .'.'.$time_durations_months.' '.'Years';
                         }

 				 		 $deadline_date       = get_field('deadline_date',get_the_id());
                         $scholarshipprovide  = get_field('scholarship_provide',get_the_id());
                         $unvisitylogo        = get_field('university_logo',$unvisityid );
				 		 $language            = get_field('language',get_the_id());
 				 		 $world_rank          = get_field('world_rank',get_the_id());
						 $countview           = pvc_get_post_views(get_the_id());
                         $bannerimages        = get_the_post_thumbnail_url(get_the_id(),'bannerimage-courses-list');
                        if(empty($bannerimages)){
                        $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/course-block-img1.png';
                        }
                         $studylevelname = '';
                         $studylevel     = wp_get_post_terms(get_the_id(),'studylevel');
                        // echo $studylevel[0]['term_id'];
                        if(!empty($studylevel)){
                            $studylevelname = $studylevel[0]->name;
                        } 
					  // echo $universityname;
                        $world_ranking_tool_tip_content = get_field('world_rank_tooltip');
                    $totalproductreviws = 0;
                    $args = '';
                    $avarage =0;
                    global $wpdb;
                    $table_name = $wpdb->prefix . "review";
                    $coursereviewid = get_the_id();
                    $totalaverage = 0.0;
                    $productview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE  `status` = 1 AND `course_id` = ".$coursereviewid );
                    // echo '<pre>';
                    // print_r($productview);
                    // echo  '</pre>';
                    if(!empty($productview)){
                        $totalproductreviws =  count($productview);
                        $totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE `course_id` = ".$coursereviewid );
                        $avarage                    = $totalrating->avarage;
                        $totalaverage               = wp_star_rating_avarage_float($avarage);
                    }
					$labelmessag  = egobal_get_the_review_label($totalaverage);
                    $args = array(
                       'rating' => $avarage,
                       'type' => 'rating',
                       'number' => 5,
                    );
                    // echo '<pre>';
                    // print_r($productview);
                    // echo "</pre>";
                    $durations = 600;
                    if($count == 1){
                    	$durations = 400;
                    }
                    if($count == 2){
                    $durations = 800;	
                    }
                     if($count == 3){
                    	$durations = 1200;
                    }
                    	?>

				    <div class="three-block">
                    <div class="white-bg ourcourse-block aos-init aos-animate" data-aos="fade-left" data-aos-duration="<?php echo $durations ?>" >
                        <div class="course-imgblock">
                            <span class="imgmain-box"><img src="<?php echo $bannerimages; ?>" alt="" /></span>
                           <div class="bachlabl-leftbox">
                            <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt=""><p><?php echo $studylevelname; ?></p></span>
                            <?php if(!empty($scholarshipprovide)) { ?>
                             <span class="pre-bachelr-label pre-scholarship"><p><?php echo 'Scholarship'; ?></p></span>

                         <?php } ?>
                               </div>
                           <?php echo do_action('course_short_list_actions'); ?>
                            <?php /*   
                            <span class="course-heart-icon">  
                                <a href="javascript:void(0);" class="course-heart-click"><i class="fa fa-heart active"></i></a>
                                <div class="addtoshort-btn"><a href="#">Add to shortlist</a></div>
                            </span>
                            */ ?>

                            <div class="timeline-flowbox">
                                <div class="rows cf">
                                    <?php if(!empty($displaydurationssvalue)) { ?>
                                    <div class="three-block timelinebox">
                                        <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/duration-icons.png" alt="" /></span>

                                        <p>Duration<br><?php echo $displaydurationssvalue ?></p>
                                    </div>
	                                <?php } ?>
                                    <?php if(!empty($deadline_date)) { ?>
                                    <div class="three-block timelinebox">
                                        <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/deadline-icon.png" alt="" /></span>
                                        <p>Deadline<br> <?php echo  $deadline_date; ?> </p>
                                    </div>
	                                <?php } ?>
                                    <?php if(!empty($language)) { ?>
                                    <div class="three-block timelinebox">
                                        <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/language-earth-icon.png" alt="" /></span>
                                        <p>Language<br> <?php echo $language; ?> </p>
                                    </div>
	                                <?php } ?>
                                </div>
                            </div>
                            <div class="inner-titwid-img">
                                <?php if(!empty($unvisitylogo)) { ?>
                                <div class="min-imgbox"><img src="<?php echo $unvisitylogo ?>" width="65" /></div>
                               <?php } ?>
                                <div class="ourcourse-contbox cf">
                                    <h4><?php echo $universityname; ?></h4>
                                    <div class="fulcour-flatbox">
                                        <?php if(!empty($countryname)) { ?>

                                        <span class="small-flg-span">
                                            <?php if(!empty($countryflag)) {?>
                                            <img src="<?php echo $countryflag ?>" width="18" />
                                            <?php } ?>

                                            <p><?php echo  $countryname; ?></p>
                                        </span>
                                        <?php } ?>
                                        <?php  if(!empty($world_rank)){ ?>
                                           <div class="worldrank-box"> 
                                            <span class="ratio-icon-box"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/ratioup-arrow.png" width="18" /><p><?php echo  $world_rank; ?></p>
                                            </span>
                                             <?php if(!empty($world_ranking_tool_tip_content)){ ?>     <div class="worldrank-desc" ><p><?php echo $world_ranking_tool_tip_content ?></p></div>
                                            <?php } ?>
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="our-courseblk-content">
                            <a title="<?php echo get_the_title(); ?>" href="<?php echo get_permalink(); ?> "><h3><?php
                           		 $wordlenght =  strlen(get_the_title());
                           		 $doted = '';
                           		 if($wordlenght >= 62){
                           		 	$doted =  "...";
                           		 }
                             	echo substr(get_the_title(),0,62).$doted;  ?> </h3></a>
                            <div class="cf">
                                <div class="school-detail-bottom">
                                	<div class="rate-star" style="display:none">
                                    <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                    <?php if(!empty($totalaverage)){?>
                                    <a class="ratenum-like" href="javascript:void(0)" title="like">
                                        <span class="rating-number-box"><?php echo $totalaverage; ?></span>
                                    </a>
                                    <?php } ?>
                                     <div class="rate-discr" ><p>Total Rating 4 out of 5 - Good</p></div>
                                	</div>
                                    <div class="rate-star" >
                                      <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                        <a class="ratenum-like" href="javascript" title="like">
                                            <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                        </a>
                                   <div class="rate-discr" ><p><?php echo $labelmessag; ?></p></div>
                                    </div>
                                    <div class="review-texts">
                                        <span><?php echo $totalproductreviws; ?> reviews</span>
                                    </div>
                                </div>
                            </div>
                            <div class="year-view-box">
                                <?php if(!empty($feesperyear)){?>
                                <span class="rate-leftbox">From <i>£<?php echo $feesperyear; ?> </i>per year</span>
                            <?php } ?>

                                <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png" />views: <i><?php echo $countview; ?></i></span>
                            </div>
                        </div>
                    </div>
                </div>	
				<?php
}


/**
* Login Box Page Html Actions. 
**/
add_action('loginbox_html_code','ege_login_box_popup');
function  ege_login_box_popup(){
	if(!is_user_logged_in()){
	?>
     <div id="loginboxhtml" class="enquiry-form-popup myprofile-informations">
         <div class="maxwidth-box" style="max-width:450px">
             <div class="padding whitecolor">
                 <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                 <h1 style="font-size:20px">  Please sign in to shortlist your item</h1>
                    <a  class="common-btn" href="<?php echo site_url().'/login-register/'; ?>">Login </a>
                    <a  href="javascript:void(0)"  class="nothanks-btn clspopup-box"><span class="clspopup no-thanks-btn">No, Thanks!</span></a>
             </div>    
      </div>
    </div> 
    <?php
	}
}

/**
* Login Box Page Html Actions Two. 
**/
add_action('loginbox_html_code_two','ege_login_box_popup_two');
function  ege_login_box_popup_two(){
	if(!is_user_logged_in()){
	?>
     <div id="loginboxhtmltwo" class="enquiry-form-popup myprofile-informations">
         <div class="maxwidth-box" style="max-width:450px">
             <div class="padding whitecolor">
                 <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                 <?php if(is_single()){ ?>
                 	                 <h1 style="font-size:20px">  Please sign in  for add Review</h1>
  
                 <?php }else{ ?>	
                 	                 <h1 style="font-size:20px">  Please sign in to shortlist your item</h1>
  
                 <?php } ?>	

                    <a  class="common-btn" href="<?php echo site_url().'/login-register/'; ?>">Login </a>
                    <a  href="javascript:void(0)"  class="nothanks-btn clspopup-box"><span class="clspopup no-thanks-btn">No, Thanks!</span></a>
             </div>    
      </div>
    </div> 
    <?php
	}
}
// add_action( 'insta_add_data', 'wpdocs_my_save_post', 10, 2);

// function wpdocs_my_save_post( $post_ID ) {
//    // do stuff here

// 	echo "string".$post_ID;
// }

//add the needed scripts and styles
//Define Label Name for reveiw.
add_action('review_label_message','egobal_get_the_review_label',1,10);
function egobal_get_the_review_label($totalaverage) {
		 $totalaverage = round($totalaverage); 

		$ratestarmessage = 'No Review';
		if($totalaverage == 1.0 || $totalaverage == 0.5 ){
		$ratestarmessage  =   'Terrible';
		}
		if($totalaverage == 2.0 || $totalaverage == 1.5 ){
		$ratestarmessage  =   'Poor';
		}
		if($totalaverage == 3.0 || $totalaverage == 2.5){
		$ratestarmessage  =   'Average';
		}
		if($totalaverage == 4.0 || $totalaverage == 3.5 ){
		$ratestarmessage  =   'Good';
		}
		if($totalaverage == 5.0 || $totalaverage == 4.5 ){
		$ratestarmessage  =   'Excellent';
		}
		$labelmessag =  'Total  Rating '.$totalaverage.' out of 5 
		-'.$ratestarmessage;
	
	  	return $labelmessag;
}

function egobal_get_the_review_label_two($totalaverage) {
		$ratestarmessage = 'No Review';
		 $totalaverage = round($totalaverage); 

		if($totalaverage == 1.0 || $totalaverage == 0.5 ){
		$ratestarmessage  =   'Terrible';
		}
		if($totalaverage == 2.0 || $totalaverage == 1.5 ){
		$ratestarmessage  =   'Poor';
		}
		if($totalaverage == 3.0 || $totalaverage == 2.5){
		$ratestarmessage  =   'Average';
		}
		if($totalaverage == 4.0 || $totalaverage == 3.5 ){
		$ratestarmessage  =   'Very Good';
		}
		if($totalaverage == 5.0 || $totalaverage == 4.5 ){
		$ratestarmessage  =   'Excellent';
		}
	  	return $ratestarmessage;
}

function egobal_get_the_review_label_color($totalaverage) {
		$ratestarmessage = 'one-rate';
		 $totalaverage = round($totalaverage); 
		if($totalaverage >= 1.0 && $totalaverage == 0.5 ){
		$ratestarmessage  =   'one-rate';
		}
		if($totalaverage == 2.0 || $totalaverage == 1.5 ){
		$ratestarmessage  =   'two-rate';
		}
		if($totalaverage == 3.0 || $totalaverage == 2.5){
		$ratestarmessage  =   'theree-rate';
		}
		if($totalaverage == 4.0 || $totalaverage == 3.5 ){
		$ratestarmessage  =   'four-rate';
		}
		if($totalaverage == 5.0 || $totalaverage == 4.5 ){
		$ratestarmessage  =   'five-rate';
		}
	  	return $ratestarmessage;
}

/*
	Disable Default Dashboard Widgets
	@ https://digwp.com/2014/02/disable-default-dashboard-widgets/
*/
function disable_default_dashboard_widgets() {
	global $wp_meta_boxes;
	// wp..
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	// bbpress
	unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);
	// yoast seo
	unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);
	// gravity forms
	unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);
}
add_action('wp_dashboard_setup', 'disable_default_dashboard_widgets', 999);
/**
Count Eductor school,university and collage wise.
**/
function  ege_count_educator_base_on_etype($educatortslug,$country){
				//$educatortype = 'school';
				$educatortype = $educatortslug;
				// $educatortype = 'university';
				$countrydropdown = $country;
				if(!empty($countrydropdown) && !empty($educatortype)) {
				$countrydropdownsarray = array( // (array) - use taxonomy parameters (available with Version 3.1).
				'relation' => 'AND', // (string) - The logical relationship between each inner taxonomy array when there is more than one. Possible values are 'AND', 'OR'. Do not use with a single inner taxonomy array. Default value is 'AND'.
				array(
				'taxonomy' => 'Country', // (string) - Taxonomy.
				'field' => 'slug', // (string) - Select taxonomy term by Possible values are 'term_id', 'name', 'slug' or 'term_taxonomy_id'. Default value is 'term_id'.
				'terms' => $countrydropdown // (int/string/array) - Taxonomy term(s).
				//'include_children' => true, // (bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
				//'operator' => 'IN' // (string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS'. Default value is 'IN'.
				),
				array(
				'taxonomy' => 'educator-type',
				'field' => 'slug',
				'terms' => $educatortype,
				//'include_children' => false,
				//'operator' => 'NOT IN'
				)
				);

				}

				// $countrydropdownsarray = array( // (array) - use taxonomy parameters (
				//     array(
				//     'taxonomy' => 'educator-type',
				//     'field' => 'slug',
				//     'terms' => $educatortype,
				//     //'include_children' => false,
				//     //'operator' => 'NOT IN'
				//     )
				// );
				$the_query = new WP_Query( array(
				'posts_per_page' => 4,
				'order' => 'DESC',
				'post__in' => $educators_list,
				'orderby' =>'date',
				'paged' => $paged,
				'post_type'     =>'educator',
				'tax_query' => array($countrydropdownsarray
				),
				));
				return $the_query->found_posts;
	}

add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );

function wti_loginout_menu_link( $items, $args ) {
   if ($args->theme_location == 'user-menu') {
      if (is_user_logged_in()) {
         $items .= '<li class="right"><a href="'. wp_logout_url('login-register') .'">'. __("Log Out") .'</a></li>';
      } else {
         $items .= '<li class="right"><a href="'. wp_login_url(get_permalink()) .'">'. __("Log In") .'</a></li>';
      }
   }
   return $items;
}

// Register Custom Post Type
function create_team_members() {

	$labels = array(
		'name'                  => _x( 'Team Members', 'Post Type General Name', 'cw-custom-post-types' ),
		'singular_name'         => _x( 'Team Member', 'Post Type Singular Name', 'cw-custom-post-types' ),
		'menu_name'             => __( 'Team Members', 'cw-custom-post-types' ),
		'name_admin_bar'        => __( 'Team Members', 'cw-custom-post-types' ),
		'archives'              => __( 'Team Member Archives', 'cw-custom-post-types' ),
		'parent_item_colon'     => __( 'Parent Team Member:', 'cw-custom-post-types' ),
		'all_items'             => __( 'All Team Members', 'cw-custom-post-types' ),
		'add_new_item'          => __( 'Add New Team Member', 'cw-custom-post-types' ),
		'add_new'               => __( 'Add New', 'cw-custom-post-types' ),
		'new_item'              => __( 'New Team Member', 'cw-custom-post-types' ),
		'edit_item'             => __( 'Edit Team Member', 'cw-custom-post-types' ),
		'update_item'           => __( 'Update Team Member', 'cw-custom-post-types' ),
		'view_item'             => __( 'View Team Member', 'cw-custom-post-types' ),
		'search_items'          => __( 'Search Team Member', 'cw-custom-post-types' ),
		'not_found'             => __( 'Not found', 'cw-custom-post-types' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cw-custom-post-types' ),
		'featured_image'        => __( 'Featured Image (example : 140*140)', 'cw-custom-post-types' ),
		'set_featured_image'    => __( 'Set featured image', 'cw-custom-post-types' ),
		'remove_featured_image' => __( 'Remove featured image', 'cw-custom-post-types' ),
		'use_featured_image'    => __( 'Use as featured image', 'cw-custom-post-types' ),
		'insert_into_item'      => __( 'Insert into Team Member', 'cw-custom-post-types' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Team Member', 'cw-custom-post-types' ),
		'items_list'            => __( 'Team Members list', 'cw-custom-post-types' ),
		'items_list_navigation' => __( 'Team Members list navigation', 'cw-custom-post-types' ),
		'filter_items_list'     => __( 'Filter Team Members list', 'cw-custom-post-types' ),
	);
	$args = array(
		'label'                 => __( 'Team Member', 'cw-custom-post-types' ),
		'description'           => __( 'Chalk and Ward Team Members', 'cw-custom-post-types' ),
		'labels'                => $labels,
		'supports'              => array( 'title', ),
		'taxonomies'            => array( 'teamcategory' ),
		'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
		'publicly_queryable' => true,  // you should be able to query it
		'show_ui' => true,  // you should be able to edit it in wp-admin
		'exclude_from_search' => true,  // you should exclude it from search results
		'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
		'has_archive' => false,  // it shouldn't have archive page
		'rewrite' => false,  // it shouldn't have rewrite rules
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'can_export'            => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail'),
	);
	register_post_type( 'team-members', $args );

}
add_action( 'init', 'create_team_members', 0 );

function ege_posttype_admin_css() {
    global $post_type;
    $post_types = array(
            'team-members'
          );
    if(in_array($post_type, $post_types))
    echo '<style >#post-preview, #view-post-btn{display: none;}
			span.view {
			display: none;
			}
		 </style>';
}
add_action( 'admin_head-post-new.php', 'ege_posttype_admin_css' );
add_action( 'admin_head-post.php', 'ege_posttype_admin_css' );



/**
* Login Box Page Html Actions. 
**/
add_action('loginbox_html_code_product','ege_login_product_box_popup');
function  ege_login_product_box_popup(){
	if(!is_user_logged_in()){
	?>
     <div id="loginboxhtml" class="enquiry-form-popup myprofile-informations">
         <div class="maxwidth-box" style="max-width:450px">
             <div class="padding whitecolor">
                 <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                 <h1 style="font-size:20px">  Please sign in for Purchase this item</h1>
                    <a  class="common-btn" href="<?php echo site_url().'/login-register/'; ?>">Login </a>
                    <a  href="javascript:void(0)"  class="nothanks-btn clspopup-box"><span class="clspopup no-thanks-btn">No, Thanks!</span></a>
             </div>    
      </div>
    </div> 
    <?php
	}
}

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */

// function my_login_redirect( $redirect_to, $request, $user ) {
//     //is there a user to check?

//             // redirect them to another URL, in this case, the homepage 
//     $redirect_to =  home_url();

//     return $redirect_to;
// }

// add_filter( 'login_redirect', 'my_login_redirect',5, 3 );


 //apply_filters('wplc_start_button_custom_attributes_filter', "", $wplc_settings);

 //add_filter('wplc_start_button_custom_attributes_filter',$wplc_settings)
// add_filter( 'wplc_activate_default_settings_array', 'ege_wp_callback',10 );
//  function ege_wp_callback( $wplc_default_settings_array ) {
//     // Maybe modify $example in some way.
//      $wplc_default_settings_array['wplc_pro_sst1'] = 'Start the Chat.';

//     return $wplc_default_settings_array;


// }

