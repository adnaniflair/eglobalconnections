
<?php
get_header(); 

$educatorviews = pvc_get_post_views(get_the_id());
update_post_meta(get_the_id(),'post_views_counter',$educatorviews);

$universityid = get_the_id();
$terms  = get_the_terms($universityid,'Country');
$countryname =  '';
$countryflag = '';
if(!empty($terms)){
    $countryname =  $termsname = $terms[0]->name;
    $termsid     = $terms[0]->term_id;
    $countryflag = ege_countryflagurl($termsid);
}

//$taxonomy . '_' . $term_id;
$unvisitylogo = get_field('university_logo');


$unvisitybannerimages = get_field('banner_image');
if(empty($unvisitybannerimages)){
    $unvisitybannerimages =	site_url().'/wp-content/uploads/2018/12/inner-banner.jpg';
}

// Educator Serach Dropdown 
$educatorid = array(get_the_id());
$educator_array = array(  
'key' => 'select_university',
'value' =>$educatorid,
'compare' => "IN",
);

//Rating 
$average = 2;
$detailpageid = get_field('profile_details_page','option');

$why_de_montfort = get_field('why_de_montfort');
$entry_requirements = get_field('entry_requirements');
$fees_and_funding = get_field('fees_and_funding');
$facilities = get_field('facilities');
$campus_culture = get_field('campus_culture');
$location = get_field('location');
wp_reset_postdata(); 

if(is_user_logged_in()) {
    $userid         = get_current_user_id();
    $meta_key       = 'educator_short_list';
    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
    $wishclass      = 'add';
    $wishsortlistclass   = 'fa-heart';
    if(is_array($educatorarray)){
        if(in_array(get_the_id(), $educatorarray)){
            $wishclass = 'remove';
            $wishsortlistclass = 'fa-heart-o';
        }
    }
}else{
    $userid         = get_current_user_id();
    $meta_key       = 'educator_short_list';
    $educatorarray = maybe_unserialize(get_educator_wislist($ipaddress));
    $educatorarray = maybe_unserialize($educatorarray[0]);    
    $wishclass      = 'add';
    $wishsortlistclass   = 'fa-heart';
    if(is_array($educatorarray)){
        if(in_array(get_the_id(), $educatorarray)){
            $wishclass = 'remove';
            $wishsortlistclass = 'fa-heart-o';
        }
    }

}

//Review Sections  For Educator
global $wpdb;
$table_name = $wpdb->prefix . "review";

$avarage = 0;
$totalaverage =0;
$totalcoursereviws =0;
$educatoreid = get_the_id();
//SELECT * FROM `ege_review` WHERE `course_id` = 15
$coursereview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE 
    `status` = 1  AND `educators_id` = ".$educatoreid  );


// echo "<pre>";
// print_r($cosrsereview);
// echo "</pre>";
if(!empty($coursereview)){
$totalcoursereviws =  count($coursereview);
$totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE  `status` = 1  AND `educators_id` = ".$educatoreid."  ");
$teching_quality = $wpdb->get_row("SELECT AVG(`teching_quality`) as avarage FROM $table_name WHERE  `status` = 1  AND `educators_id` = ".$educatoreid." ");
$school_facilites = $wpdb->get_row("SELECT AVG(`school_facilites`) as avarage FROM $table_name WHERE   `status` = 1  AND `educators_id` = ".$educatoreid."  ");
$locations = $wpdb->get_row("SELECT AVG(`locations`) as avarage FROM $table_name WHERE 
     `status` = 1  AND `educators_id` = ".$educatoreid."  ");
$career_assistance = $wpdb->get_row("SELECT AVG(`career_assistance`) as avarage FROM $table_name WHERE  `status` = 1  AND `educators_id` = ".$educatoreid." ");
$value_of_money = $wpdb->get_row("SELECT AVG(`value_of_money`) as avarage FROM $table_name WHERE  `status` = 1  AND `educators_id` = ".$educatoreid." ");
//echo $avarage     = number_format((float)$totalrating->avarage, 2, '.', '');


$avarage = $totalrating->avarage;
$teching_quality_avrage     = wp_star_rating_avarage_float($teching_quality->avarage);
$school_facilites_avrage    = wp_star_rating_avarage_float($school_facilites->avarage);
$locations_avrage           = wp_star_rating_avarage_float($locations->avarage);
$career_assistance_avrage   = wp_star_rating_avarage_float($career_assistance->avarage);
$value_of_money_avrage      = wp_star_rating_avarage_float($value_of_money->avarage);
$totalaverage               = wp_star_rating_avarage_float($avarage);
}
$args = array(
   'rating' => $avarage,
   'type' => 'rating',
   'number' => 5,
); 


// Images sections array video.

$images = get_field('gallery_images');
$size = 'gallery-size'; // (thumbnail, medium, large, full or custom size)

$gallery_title = get_field('gallery_title');
$gallery_descriptions = get_field('gallery_descriptions');


$reviews_title = get_field('ranking_title');
$reviews_descriptions = get_field('ranking_descriptions');
$eductaortitlehead = get_the_title();
$wordlenght =  strlen($eductaortitlehead);
  $classname = '';
  $classname2='';
 if($wordlenght >= 60){
    $classname =  'full-course-width';
    $classname2 =  'full-course-width2';
 }

?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/fancybox/source/jquery.fancybox.pack.js"></script>
    <section class="inner-bannerbox topgreen-border" style="background-image: url(<?php echo $unvisitybannerimages; ?>)">
        <div class="wrapper">

            <div class="inner-baner-cont <?php echo $classname; ?>" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">
                    <ul class="cf">
                        <li><a href="<?php ?>"><i class="fa fa-home"></i></a></li>
                        <li><a href="<?php echo site_url().'/educators/'; ?>">Educators</a></li>
                        <li><label> <?php echo get_the_title(); ?></label></li>
                    </ul>
                </div>
                <div class="innerbanner-block cf">

                	<?php if(!empty($unvisitylogo)){ ?>
                    <div class="inrbanner-leftimg">
                        <img src="<?php echo $unvisitylogo; ?>" alt="" />
                    </div>
	                <?php } ?>
                    <div class="inrbaner-rightcont <?php echo $classname; ?>">
                        <div class="baner-titbox">
                            <h3><?php echo get_the_title(); ?></h3>
                            <?php if(is_user_logged_in()) { ?>
                              <span data-wish="<?php echo $wishclass; ?>" class="educator-wish-list"><i class="fa <?php echo $wishsortlistclass; ?>"></i></span>
                            <?php }else{ ?>
                             <span data-wish="<?php echo $wishclass; ?>" class="educator-wish-list"><i class="fa <?php echo $wishsortlistclass; ?>"></i></span>
                             <?php } ?>   
                        </div>
                        <div class="banner-country-flag">
                            <?php if(!empty($countryflag)) { ?>
                            <span><img src="<?php echo $countryflag; ?>"></span>
                            <?php  } ?>
                            <?php if(!empty($countryname)) { ?>
                            <span class="dottd-btmborder"><?php echo $countryname; ?></span>
                            <?php } ?>
                        </div>
                        <div class="school-detail-bottom">
                            <div class="rate-star">
                                <?php /*
                                <?php if(!empty($average) ){
                                    $totalavaragereaming = 5;
                                  
                                 ?>
                                <span class="like-count">
                                    <?php for($i=1; $i<=$average; $i++ ){ ?>
                                    <i class="fa fa-star"></i>
                                    <?php } ?>
                                    <?php for($i=$average; $i<$totalavaragereaming; $i++ ){ ?>
                                    <i class="fa fa-star-o"></i>
                                    <?php  } ?>
                                </span>
                                <?php  } ?>
                                */ ?>
                                <?php wp_star_rating($args); ?>
                                <a class="rate-like" href="javascript:void(0)"  title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
                            </div>
                            <div class="review-texts">
                                <span><?php echo $totalcoursereviws; ?> reviews</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="innerpage-menu-box greenbgcolor">
        <div class="wrapper cf">
            <div class="sticky-imgtit titimgsticky  <?php echo $classname;  ?>">
                <?php if(!empty($unvisitylogo)){ ?>
                <div class="stikimg-small">
                    <img src="<?php echo $unvisitylogo; ?>" alt="" width="60" />
                </div>
                <?php } ?>

                <div class="sticky-tibox <?php echo $classname;  ?>">
                   <div class="table">
                       <div class="table-cell">
                            <h4><?php echo get_the_title(); ?></h4>
                       </div>
                    </div>
                </div>
            </div>
            <div class="enquiry-rightbox"><a href="javascript:void(0);" class="btn enquiry-btn openpopup1">Enquire now</a></div>
        
            <div id="eductors-popup" class="enquiry-form-popup">
                 <div class="maxwidth-box">
                     <div class="padding whitecolor  only-enqu-box">
                         <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                         <?php echo do_shortcode('[contact-form-7 id="129" title="Contact form 1"]'); ?>
                         
                     </div>    
                 </div>
            </div>
            <div class="iner-left-menubox" data-aos="fade-right" data-aos-duration="1200">
               <a href="javascript:void(0);" id="innermenu-click" class="innermenu-linkbox">
                   <span></span>
                   <span></span>
                   <span></span>
                </a>
                <ul class="cf in-responsive">
                    <li><a href="#go-aboutid">about</a></li>
                    <li><a href="#go-courseid">Courses</a></li>
                    <?php if(!empty($coursereview)) { ?>
                    <li><a href="#go-rankid">Ranking</a></li>
                <?php } ?>
                    <li><a href="#go-reviewid">Reviews</a></li>
                    <?php if(!empty($images )){ ?>
                    <li><a href="#go-imgid">Images</a></li>
                <?php } 
                  if( have_rows('gallery_videos') ){?>
                    <li><a href="#go-videoid">Videos</a></li>
                   <?php } ?> 

                </ul>
            </div>
        </div>
    </section>

    <section class="about-top-contebox" id="go-aboutid">
        <div class="wrapper cf">
            <div class="title-contentbox" data-aos="fade-right" data-aos-duration="600">
                    <h1>About</h1>
                    <?php 
                    echo  wpautop(get_the_content()); 
                    //'<p>'.substr(get_the_content(),0,550);
                    ?>
                    <?php /* if(!empty(get_the_content())) {?>
                    <a  href="<?php echo get_permalink($detailpageid);  ?>?educatoid=<?php echo $universityid;  ?>" class="arrow-links">read more</a></p>
                    <?php }  */ ?>
            </div>
            <div class="blockpad-box">
                <div class="blocks-rows cf">
                    <?php if(!empty($why_de_montfort)){ ?>
                    <div class="aboutblock-box" data-aos="fade-right" data-aos-duration="600">
                        <div class="about-icons"> <a  target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=0"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/montfort.png" width="53"></span></a></div>
                        <a target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=0"><p><strong>Why this uni?</strong></p></a>
                    </div>
                    <?php } ?>
                    <?php if(!empty($facilities)){ ?>
                    <div class="aboutblock-box" data-aos="fade-left" data-aos-duration="600">
                        <div class="about-icons"><a  target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=1"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/facilities-icon.png" width="47"></span></a></div>
                       <a target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=1"> <p><strong>Facilities  </strong></p></a>
                    </div>
                    <?php } ?>

                    <?php if(!empty($campus_culture)){ ?>
                    <div class="aboutblock-box" data-aos="fade-left" data-aos-duration="600">
                        <div class="about-icons"> <a  target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=2"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/campus-culcture.png" width="58"></span></a></div>
                        <a target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=2"><p><strong>Campus Culture    </strong></p></a>
                    </div>
                    <?php } ?>
                    <?php if(!empty($entry_requirements)){ ?>
                    <div class="aboutblock-box" data-aos="fade-right" data-aos-duration="600">
                        <div class="about-icons"><a  target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=3"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/requirement-icon.png" width="60"></span></a></div>
                        <a target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=3"><p><strong>Entry & Credit </strong></p></a>
                    </div>
                    <?php } ?>
                    <?php if(!empty($fees_and_funding)){ ?>
                    <div class="aboutblock-box" data-aos="fade-right" data-aos-duration="600">
                        <div class="about-icons"><a  target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=4"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/fees-and-funding.png" width="48"></span></a></div>
                        <a target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=4"><p><strong>Fees & Funding </strong></p></a>
                    </div>
                    <?php } ?>
       
                    <?php if(!empty($location)){ ?>
                    <div class="aboutblock-box" data-aos="fade-left" data-aos-duration="600">
                        <div class="about-icons"><a  target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=5"><span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/location-icon.png" width="50"></span></a></div>
                        <a target="_blank" href="<?php echo get_permalink($detailpageid); ?>?educatoid=<?php echo $universityid;  ?>&menuid=5"><p><strong>location   </strong></p></a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>

    <section class="course-section greybg" id="go-courseid">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2>Courses</h2>
                <p>This course designed for those who need flexible scheduling and learning<br/> in an accelerated format</p>
            </div>


            <div class="dropdownmain-box cf" data-aos="fade-left" data-aos-duration="1200" style="display:none">

                <div class="drop-labelbox"><label>Search by</label></div>
                <div class="dropdown-blocks">
                    <?php
                    $studylevelid = '';
                    // print_r( array(
                    // 'posts_per_page' => 10,
                    // 'order' => 'DESC',
                    // 'orderby' =>'date',
                    // 'paged' => $paged,
                    // 'post_type'     =>'course',
                    // 'tax_query' => array($subject_array),
                    // 'meta_query' => array($educator_array),
                    
                    // ));
                    $universityposts = get_posts(
                       array(
                            'posts_per_page' =>2,
                            'order' => 'ASC',
                            //'orderby' =>'date',
                            'paged' => $paged,
                            'post_type'     =>'course',
                            'meta_query' => array($educator_array), 
                           )
                        ); 
                        if(!empty($universityposts)){ 
                            $countryids = array();
                            $studylevelid = array();
                            $unvisityid = array();

                            foreach ($universityposts as $postvalue) {
                            # code...
                             $courseid     =  $postvalue->ID;
                             $universityid = get_field('select_university',$courseid);
                             $studylevel   = wp_get_post_terms($courseid,'studylevel');

                              //print_r($studylevel);  
                             $universityid = get_field('select_university',$courseid);
                             $country   = wp_get_post_terms($universityid,'Country');

                                                      
                            // echo $studylevel[0]['term_id'];
                         
                            if(!empty($studylevel[0])){
                                $studylevelid[] = $studylevel[0]->term_id;
                            }  
                            // country dropdown
                             //$countryids = array();   
                            if(!empty($country[0])){
                                $countryids[] = $country[0]->term_id;
                            } 
                             //print_r($studylevel); 
                             if(!empty( $universityid)){
                               $unvisityid[]   = $universityid;
                             }
                             //echo '<br>';
                            //echo $universityname = get_the_title($unvisityid);
                            }
                        }
                          if(!empty($unvisityid)){  
                            $educators_list = (array_unique($unvisityid));
                          }
                         // print_r($studylevelid);
                         if(!empty($studylevelid)){  
                            $studylevel_list = (array_unique($studylevelid));
                          }
                          if(!empty($countryids)){
                            $coutrylist    = (array_unique($countryids));
                          }
                     // print_r($studylevel_list);
                 
                      //  echo "<pre>"; 
                      //  print_r($educators_list); 
                      // echo "</pre>"; 
                        //print_r($universityposts);
                    ?>
         
                    <select id="country-dropdown" class="chosen-select chosen-serach-dropdown">
                        <option value="">Country</option>
                        <?php
                        if(!empty($coutrylist)){
                            foreach ($coutrylist as $key) {
                                # code...
                                 $term = get_term_by( 'id', $key, 'Country');
                                 echo '<option value="'.$term->slug.'">'.$term->name.'</option>'
                                 ?>
                                <?php

                                }
                            }
                        ?> 
                    </select>
                </div>
                <div class="dropdown-blocks">
                    <select id="educators-drodown" class="chosen-select chosen-serach-dropdown">
                            <option value="">Educator Name</option>
                           <?php
                                foreach ($educators_list as $key) {
                                    # code...
                                    echo '<option value="'.$key.'">'.get_the_title($key).'</option>'
                                  
                                    ?>
                                    <?php

                                }
                            ?> 
                    </select>
                </div>
   
                <div class="dropdown-blocks">
                    <select  id="studylevel-dropdown" class="chosen-select chosen-serach-dropdown">
                        <option value="">Study Level</option>
                         <?php
                            foreach ($studylevel_list as $key) {
                                # code...
                                $term = get_term_by( 'id', $key, 'studylevel');
                                echo '<option value="'.$key.'">'.$term->name.'</option>'
                              
                                ?>
                                <?php

                            }
                        ?> 
                    </select>
                </div>
                <div class="dropdown-blocks">
                    <select id="sort-by-dropdown" class="chosen-select chosen-serach-dropdown">
                        <option value="date">Sort By</option>
                        <option value="price">Price</option>
                        <option value="popularity">Popularity</option>
                        <option value="durations">Durations</option>
                    </select>
                </div>
            </div>

            <div class="rows cf" data-aos="fade-right" id="course-listing-data" data-aos-duration="1200">
              
            <div class="loading overlay-bgcolor" style="display:none">
            <div class="all-middleicon-loader">  <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect"></div></div>
               <?php
                $queried_object = get_queried_object();
                $term_id = $queried_object->term_id;
                $term = get_term_by( 'id', $term_id, 'subject');
                $queried_object = get_queried_object();
                $term_id = $queried_object->term_id;
                $term = get_term_by( 'id', $term_id, 'subject');
            
                //print_r($term);
                $the_query = new WP_Query( array(
                'posts_per_page' =>12,
                'post_type'     =>'course',
                'meta_query' => array($educator_array),

                     )  

                ); 

                if($the_query -> have_posts()){
                ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    do_action('courses_html_actions'); 
                endwhile;
                }else{
                    echo "No Courses Founds";
                }

                wp_reset_postdata(); ?>
                <div class="fullwidth-block">
                <div class="page-linkox cf">
                        <?php
                        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                         ?>
                </div>
                </div>
      
            </div>

            <?php /*
            <div class="page-linkox cf">
                <ul class="pagination-nav cf">
                    <li><a class="prev-link nav-links" href="#">Previous</a></li>
                    <li><a class="nav-links current" href="#">1</a></li>
                    <li><a class="nav-links" href="#">2</a></li>
                    <li><a class="nav-links" href="#">3</a></li>
                    <li><a class="nav-links" href="#">4</a></li>
                    <li><a class="nav-links next-link" href="#">Next</a></li>
                </ul>
            </div>
            */ ?>
        </div>
    </section>
<?php if(!empty($coursereview)) { ?>
    <section class="course-section ranking-section" id="go-rankid">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">

                <?php if($reviews_title) { ?>
                <h2><?php echo $reviews_title; ?></h2>
                <?php }else{ ?>
                    <?php echo '<h2>Ranking</h2>'; ?>
                 <?php } ?>   
                <?php if(!empty($reviews_descriptions)){ ?>
                    <p><?php echo $reviews_descriptions; ?></p>
                <?php }else{ ?>
                   <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br>
interdum auctor velit in laoreet tincidunt elit.</p>
                 <?php }?>  
            </div>

            <div class="our-ranking-box cf" data-aos="fade-left" data-aos-duration="1200">
                <div class="ourrank-leftbox">
                    <h3>Average student ranking</h3>
                    <div class="cf">
                        <div class="school-detail-bottom all-reviebox cf">
                            <div class="rate-star">
                                <?php wp_star_rating($args); ?>

                                <a class="ratenum-like" href="javascript:void(0)" title="like">
                                    <span class="rating-number-box"><?php echo $totalaverage; ?></span>
                                </a>
                            </div>
                            <div class="review-texts">
                                <span><?php echo $totalcoursereviws; ?> reviews</span>
                            </div>
                        </div>
                    </div>
                    <div class="cf rankcout-box">
                        <div class="leftrevie-box">
                            <span><?php echo $totalcoursereviws; ?></span>
                            <p>reviews</p>
                        </div>
                          <?php                             
                            $world_rank         = get_field('world_rank',get_the_id());
                            if(!empty($world_rank)){
                            ?>
                        <div class="leftcourserevie-box">
                          
                            <span><?php echo $world_rank; ?></span>
                            <p>World Rank</p>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="rows cf rankicon-boxs" style="display:none">
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/campus-culcture.png" width="38" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-uparow.png" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-earlyicon.png" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-usericon.png" alt="">
                        </div>

                    </div>
                </div>
                <div id="chartContainer" class="ourrank-rightbox" ></div>

                <div class="ourrank-rightbox" style="display:none;">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/graph-main-img.png" alt="" />
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<?php if(!empty($coursereview)) { ?>
    <section class="our-review-section" id="go-reviewid">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2>Reviews</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br/>interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="ourreiv-blockbox" data-aos="fade-left" data-aos-duration="1200">
                 <?php  foreach ($coursereview as $value) {
                    # code...
                   $userid          = $value->userid;
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                  // $user            = get_user_by( 'email',$useremail);
                   //print_r($user);
                   $firstname       =  $user_info->first_name;
                   $lastname        =  $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $descriptions    = $value->descriptions;
                   $id              = $value->id;
                   $overal_rating   = $value->overal_rating;
                  // echo get_avatar( $userid ); 

                   ?>
                <div class="reviwebox-block renew-review-block cf">
                    <div class="left-riveimg-box equal-height">
                        <span class="rie-imgicons"><?php echo get_avatar( $userid ,52); ?></span>                        
                    </div>
                    <div class="right-rivecontbox equal-height">
                        <div class="table" >
                            <div class="table-cell">
                               <div class="revrat-box cf">                                    
                                    <h4><?php echo $userlogin; ?></h4>
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($overal_rating) ){
                                            $totalavaragereaming = 5;
                                            ?>
                                            <span class="like-count">
                                            <?php for($i=1; $i<=$overal_rating; $i++ ){ ?>
                                            <i class="fa fa-star"></i>
                                            <?php } ?>
                                            <?php for($i=$overal_rating; $i<$totalavaragereaming; $i++ ){ ?>
                                            <i class="fa fa-star-o"></i>
                                            <?php  } ?>
                                            </span>
                                            <?php  } ?>
                                        </div>
                                    </div>                                    
                                </div>
                                <p><?php echo substr($descriptions,'0','172').'...'; ?><a href="<?php echo site_url().'/review-details/?id='.$id  ?>" class="arrow-links">read more</a></p>
                            </div>
                        </div>
                        <a href="#" style="display:none;" class="helpfull-btn">Helpful</a>
                    </div>
                </div>
                   <?php
                } ?>
                <div class="wpcf7s form-design-comon">
                <form method="post" action="<?php echo site_url().'/review'; ?>">
                    <input type="hidden" name="educatorid" value="<?php echo get_the_id(); ?>">
                    <?php   if(is_user_logged_in()) { ?>
                    <div class="Submit-btn">
                        <input type="submit"  name="Add review" value="Add Review">
                    </div>
                    <?php }else{ ?>
                  <div class="Submit-btn">
                        <input type="button" data-wish="nologin" data-id="0"
                        class="course-heart-icon-two " name="Add review" value="Add Review">
                    </div>
                    <?php } ?>
                </form>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
     <?php
 
            
             if( $images ):
            ?>
    <section class="course-section gallery-section greybg" id="go-imgid">
        <div class="wrapper cf">
            <div class="top-title-contbox aos-init" data-aos="fade-right" data-aos-duration="1200">
                <?php if(!empty($gallery_title)){ ?>
                <h2><?php echo $gallery_title; ?></h2>
                <?php } ?>
                <?php if(!empty($gallery_descriptions)){ ?>
                <p><?php echo $gallery_descriptions; ?></p>
                <?php } ?>
            </div>
       
            <div class="university-sliderbox cf" data-aos="fade-left" data-aos-duration="1200">
                <div class="gallery-sld university-slider owl-carousel">


					<?php foreach( $images as $image ): ?>
					<div class="block-image">

					<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
					</div>
					<?php endforeach; ?>
	
    
                </div>
            </div>
        </div>
    </section>
    <?php
    endif; 
    // check if the repeater field has rows of data
    if( have_rows('gallery_videos') ):
        $video_title = get_field('video_title');
        $video_descriptions = get_field('video_descriptions');
        ?>

    <section class="course-section videogallery-section" id="go-videoid">
        <div class="wrapper cf">
            <div class="top-title-contbox aos-init" data-aos="fade-right" data-aos-duration="1200">
                <?php if(!empty($video_title)) { ?>
                <h2><?php echo $video_title;  ?></h2>
                <?php } ?>
                <?php if(!empty($video_descriptions )) {?>
                <p><?php echo $video_descriptions;  ?></p>
            <?php } ?>
            </div>
            <div class="videogally-box">
                <div class="slider slider_circle_10">
				<?php
				// check if the repeater field has rows of data

				// loop through the rows of data
				while ( have_rows('gallery_videos') ) : the_row();
					?>
				  <div>
					<iframe width="619" height="313" src="<?php echo get_sub_field('add_video_url'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>

				<?php

				endwhile;
				// no rows found
                    ?>
                  <div class="next_button"><i class="fa fa-angle-right"></i></div>  
                  <div class="prev_button"><i class="fa fa-angle-left"></i></div>  
                </div>
            </div> 
        </div>
    </section>

    <?php
            endif;

                ?>
<?php 
do_action('loginbox_html_code'); 
do_action('loginbox_html_code_two'); 
?>    
<?php get_footer(); ?>
    <?php
        if($teching_quality_avrage != 0){
             $teching_quality_avrage_percentage = ( $teching_quality_avrage / 5 ) * 100;
         }
         if($school_facilites_avrage != 0){
             $school_facilites_percentage = ( $school_facilites_avrage / 5 ) * 100;
         }
         if($locations_avrage != 0){
             $locations_avrage_percentage = ( $locations_avrage / 5 ) * 100;
         }
         if($career_assistance_avrage != 0){
             $career_assistance_avrage_percentage = ( $career_assistance_avrage / 5 ) * 100;
         } 
        if($value_of_money_avrage != 0){
            $value_of_money_avrage_percentage = ( $value_of_money_avrage / 5 ) * 100;
        }

    ?>
    <?php
    $args = array(
   'rating' => 4.5,
   'type' => 'rating',
   'number' => 5,
);
?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/dist/canvasjs.min.js"></script>
    <script>
    window.onload = function () {
     CanvasJS.addColorSet("greenShades",
                [//colorSet Array

                "#9eedf0",
                "#b0c2d9",
                "#ffcbbb",
                "#7ecd97",
                "#dd9acf"                
                ]);
    var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: false,
    colorSet: "greenShades",
    theme: "light1", // "light1", "light2", "dark1", "dark2"
toolTip:{
   contentFormatter: function ( e ) {
               return "<div class='main-chart-id'><span>" + e.entries[0].dataPoint.label +"</span><p>"+e.entries[0].dataPoint.y +"%</p><div class='ad  ada'>" +  e.entries[0].dataPoint.html + "</div>";  
   }  
 },
    axisX:{
        labelFontSize: 15,
        labelFontColor: "black",
        gridColor: "orange",

        //labelFontWeight: "bold"

      },
    axisY: {
    suffix: "%",
    labelFontSize: 15,
    labelFontColor: "black",
    scaleBreaks: {
    customBreaks: [{
    startValue: 100,
    endValue: 100
    }]
    },
    includeZero: false
    },
    data: [{
    type: "column",
    fontSize: 25,
    yValueFormatString: "#,##0\"%\"",
  
    // toolTipContent : '<div class="main-chart-id">{label}: {y} <div class="main-chart"> <span class="like-count-graph">Average rate = </span><b>{xa}</b></div> {html}</div>',

    dataPoints: [
    { label: "Teaching quality", y:<?php echo $teching_quality_avrage_percentage; ?>,xa:<?php echo $teching_quality_avrage; ?>,html:'<?php  wp_star_rating_chart($teching_quality_avrage); ?>' },
    { label: "School Facilities", y: <?php echo $school_facilites_percentage; ?>,xa:<?php echo $school_facilites_avrage; ?>,html:'<?php  wp_star_rating_chart($school_facilites_avrage); ?>'},
    { label: "Location", y: <?php echo $locations_avrage_percentage; ?>,xa:<?php echo $locations_avrage; ?>,html:'<?php  wp_star_rating_chart($locations_avrage); ?>'},
    { label: "Carrer assistance", y: <?php echo $career_assistance_avrage_percentage; ?>,xa:<?php echo $career_assistance_avrage; ?>,html:'<?php  wp_star_rating_chart($career_assistance_avrage); ?>'},
    { label: "Value for money", y: <?php echo $value_of_money_avrage_percentage; ?> ,xa:<?php echo $value_of_money_avrage; ?>,html:'<?php  wp_star_rating_chart($value_of_money_avrage); ?>'},

    ]
    }],

    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1    
    });
    chart.render();


    }
    </script>
<script type = 'text/javascript'>
 jQuery(document).ready(function($) {
    jQuery('.educator-name').val('<?php echo get_the_title(); ?>');

    jQuery("#educators-drodown").live("change", function(){

          $educatorname    = jQuery('#educators-drodown').val();
            if($educatorname != ''){
                jQuery('#country-dropdown').find('option:first').attr('selected', 'selected');
                jQuery('#country_dropdown_chosen').children('a').children('span').text('Country');
            }
     }); 
     /* 
    jQuery('.course-heart-icon').on('click',function (event) {

//    jQuery(".course-heart-icon").on("click",function(){
        $thisclass = jQuery(this); 
        $wishclass = jQuery($thisclass).data('wish');
        $courseid = jQuery($thisclass).data('id');

                $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                'action': 'course_wish_list',
                'courseid' : $courseid,
                'wishdata'   : $wishclass,
                },
                success:function(data) {
                // This outputs the result of the ajax request
                //alert(data);
                if($wishclass == 'add'){
                    //jQuery($thisclass).children().removeClass('fa-heart');
                    //jQuery($thisclass).children().addClass('fa-heart-o');
                   // jQuery(this).children('.addtoshort-btn').show();
                    jQuery($thisclass).children('.course-heart-click').children().addClass('active')
                    jQuery($thisclass).data('wish','remove');
                   // jQuery(this).children('.addtoshort-btn').html('<a href="#">add to shortlist</a>');
                }else{
                    jQuery($thisclass).children('.course-heart-click').children().removeClass('active')
                   // jQuery($thisclass).children().addClass('fa-heart');
                    jQuery($thisclass).children().removeClass('fa-heart-o');
                    jQuery($thisclass).data('wish','add');
                    //jQuery(this).children('.addtoshort-btn').html('<a href="#">removed shortlist</a>');
                   // jQuery(this).children('.addtoshort-btn').show();

                }

                },
                error: function(errorThrown){
                console.log(errorThrown);
                }
                //setTimeout(function(){ jQuery($thisclass).children('.addtoshort-btn').hide(); }, 3000);

            });     
    });  
    */ 
    jQuery(".educator-wish-list").live("click", function(){
                //alert('Add To Wish');
                ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
                $thisclass = jQuery(this); 
                $wishclass = jQuery(this).data('wish');
               // alert($wishclass);
          

                $educatorid = <?php echo $universityid; ?>;
                $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                'action': 'educator_wish_list',
                'educatorid' : $educatorid,
                'wishdata'   : $wishclass,
                },
                success:function(data) {
                // This outputs the result of the ajax request
               // alert(data);
                if($wishclass == 'add'){

                    jQuery($thisclass).children().removeClass('fa-heart');
                    jQuery($thisclass).children().addClass('fa-heart-o');
                    jQuery($thisclass).data('wish','remove');
                }else{

                    jQuery($thisclass).children().addClass('fa-heart');
                    jQuery($thisclass).children().removeClass('fa-heart-o');
                    jQuery($thisclass).data('wish','add');
                }


                },
                error: function(errorThrown){
                console.log(errorThrown);
                }

                });        
    
     }); 
    jQuery("#country-dropdown").live("change", function(){
        $countrydropdown    = jQuery('#country-dropdown').val();
        if($countrydropdown != ''){
        jQuery('#educators-drodown').find('option:first').attr('selected', 'selected');
        jQuery('#educators_drodown_chosen').children('a').children('span').text('Educator Name');
        }
    });     
    jQuery(".chosen-serach-dropdown").live("change", function(){
            jQuery('.loading').show();
            jQuery('.page-template-taxonomy-subject-php').addClass('loader-body');
            jQuery('.tax-subject').addClass('loader-body');

            $educatorname    = jQuery('#educators-drodown').val();
            $studylevel      = jQuery('#studylevel-dropdown').val();
            $countrydropdown = jQuery('#country-dropdown').val();
            $sortbyvalue     = jQuery('#sort-by-dropdown').val();

            $study = jQuery('.studylevel-id').val();
            if($study !=''){
                $studylevel     =   $study;
            }
            $coutryid = jQuery('.country-id-value').val();
            if($coutryid !=''){
                $countrydropdown    =  $coutryid;
            }
            //alert($educatorname);
            ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $pagenumber =1;
            $termid = jQuery('.term-id-value').val();
            $.ajax({
            url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
            method:'Post',
            data: {
                'action': 'course_listing_sections',
                'page' : $pagenumber,
                'termid' : $termid,
                'studylevel': $studylevel,
                'educatorname':$educatorname,
                'countrydropdown':$countrydropdown,
                'sortbyvaluedropdown':$sortbyvalue,
            },
            success:function(data) {
                // This outputs the result of the ajax request
                jQuery('#course-listing-data').html(data);
                jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
                jQuery('.tax-subject').removeClass('loader-body');

                
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        }); 
    }); 
    jQuery(".nav-links").live("click", function(){
        jQuery('.loading').show();
        jQuery('.page-template-taxonomy-subject-php').addClass('loader-body');
        jQuery('.tax-subject').addClass('loader-body');

        $pagenumber = jQuery(this).data('page');
        $termid     = jQuery('.term-id-value').val();
        $educatorname    = '<?php echo get_the_id(); ?>';
        $studylevel      = jQuery('#studylevel-dropdown').val();
        $countrydropdown = jQuery('#country-dropdown').val();
        $sortbyvalue     = jQuery('#sort-by-dropdown').val();
        $study = jQuery('.studylevel-id').val();
        if($study !=''){
        $studylevel     =   $study;
        }
        $coutryid = jQuery('.country-id-value').val();
        if($coutryid !=''){
        $countrydropdown    =  $coutryid;
        }
       // alert($pagenumber);  
        //alert($categoryname);
        ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                    'action': 'course_listing_sections',
                    'page' : $pagenumber,
                    'termid' : $termid,
                    'studylevel': $studylevel,
                    'educatorname':$educatorname,
                    'countrydropdown':$countrydropdown,
                    'sortbyvaluedropdown':$sortbyvalue,
                },
                success:function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#course-listing-data').html(data);
                    jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
                    jQuery('.tax-subject').removeClass('loader-body');
                
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
    }); 
});
</script>