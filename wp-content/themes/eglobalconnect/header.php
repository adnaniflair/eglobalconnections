<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<title>
	<?php if(is_front_page() || is_home()){
	    echo get_bloginfo('name');
	} else{
		wp_title('|',true,'right'); ?> <?php //bloginfo('name'); 	
	}?>
	</title>    
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/favicon.ico ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600" rel="stylesheet">
		<?php wp_head(); ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137915772-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-137915772-1');
	</script>
</head>
<body <?php body_class(); ?>>
	<div class="overflow-hiden" style="overflow: hidden;">  
	<header>
		<div class="wrapper">
			<div class="logo-wrap">
				<a href="<?php echo home_url(); ?>" title="eGlobalconnect">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/logo.png" alt="eGlobalconnect">
				</a>
			</div>
			<nav class="menu">
					<?php
					wp_nav_menu( array(
					'theme_location' => 'top',
					'container'=>false,
					'menu_class'=>''
					) );
					?>
				<?php /*
				<ul>

					<li>
						<a href="#" title="Universities">Universities</a>
					</li>
					<li>
						<a href="#" title="educators">Educators</a>
					</li>
					<li>
						<a href="#" title="resources">Resources</a>
					</li>
					<li>
						<a href="#" title="contact">Contact</a>
					</li>
					<a href="#" class="serach-icon" title="search">
						<i class="fa fa-search"></i>
					</a>
				</ul> */ ?>
			</nav>
			<div class="header-right">
				<?php //echo do_shortcode('[gtranslate]'); ?>
				<?php if(is_user_logged_in()){ 
					$current_user = get_userdata(get_current_user_id());
					$user_firstname   =  $current_user->user_firstname; 
					?>
				<span class="profil-popupbox">
                    <a href="<?php echo site_url(); ?>/my-profiles/" class="profile">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/user.png">Hi, <?php echo $user_firstname; ?></a>
                    <ul class=user-popupbox>
                        <li><a href="#">test</a></li>
                        <li><a href="#">test</a></li>
                        <li><a href="#">test</a></li>
                    </ul>

                    <?php
					wp_nav_menu( array(
					'theme_location' => 'user-menu',
					'container'=>false,
					'menu_class'=>'user-popupbox'
					) );
					?>
				</span>
				<div class="lan-dropdown" style="display:none;">
					<a class="lan-toggle" href="<?php echo wp_logout_url('login-register'); ?>">Logout</a>
				</div>


				<?php }else{ ?>
					<a href="<?php echo site_url(); ?>/login-register/" class="profile">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/user.png">Login
				</a>
				<?php } 
				  if(!is_user_logged_in()){ 
				?>	
				<span class="wishlist-menubox">
				    <a href="javascript:void(0);" class="heat-iconbox"><i class="fa fa-heart"></i></a>
				    <?php

					$ipaddress     = getRealIpAddr();
					$educatorscount = $coursecount = $bookcount = 0;

					//edcator count list.
					$educatorarray = maybe_unserialize(get_educator_wislist($ipaddress));
					$educatorarray = maybe_unserialize($educatorarray[0]);
					if(!empty($educatorarray)){
						$educatorscount = count($educatorarray);
					}

					//course count list.
					$coursearray = maybe_unserialize(get_course_wislist($ipaddress));
					$coursearray = maybe_unserialize($coursearray[0]);
					if(!empty($coursearray)){
						$coursecount = count($coursearray);
					}

					//book count list
					$bookarray = maybe_unserialize(get_book_wislist($ipaddress));
					$bookarray = maybe_unserialize($bookarray[0]);
					if(!empty($bookarray)){
						$bookcount = count($bookarray);
					}
				    ?>
				    <ul class="wishlit-dropdown">
				        <li><a href="<?php echo site_url(); ?>/course-shortlist/"><i class="fa fa-graduation-cap"></i> Courses <i class="smlic"><?php echo $coursecount; ?></i></a></li>
				        <li><a href="<?php echo site_url(); ?>/educators-shortlist/"><i class="fa fa-university"></i> Institutions <i class="smlic"><?php echo $educatorscount; ?></i></a></li>
				        <li><a href="<?php echo site_url(); ?>/book-shortlist/"><i class="fa fa-book"></i> Books <i class="smlic"><?php echo $bookcount; ?></i></a></li>
				    </ul>
				    <?php echo __('Shortlist','eglobalconnect'); ?>
				</span>
				<?php } ?>
			</div>

		</div>

	</header>
<script type="text/javascript">
var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>