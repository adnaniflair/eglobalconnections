<?php

get_header(); 
$courseviews = pvc_get_post_views(get_the_id());
update_post_meta(get_the_id(),'post_views_counter',$courseviews);


$unvisityid 	    = get_field('select_university',get_the_id());
$universityname     = get_the_title($unvisityid);

$terms  		    = get_the_terms($unvisityid,'Country');
$unvisitylogo       = get_field('university_logo',$unvisityid);
//$countryname    = $termsname = $terms[0]->name;

$countryname =  '';
$countryflag = '';
if(!empty($terms)){
    $countryname =  $termsname = $terms[0]->name;
    $termsid     = $terms[0]->term_id;
    $countryflag = ege_countryflagurl($termsid);
}


wp_reset_postdata(); 
//Tab Sections Code
$corsebannerimages = get_field('banner_image');
if(empty($corsebannerimages)){
$corsebannerimages = site_url().'/wp-content/uploads/2018/12/inner-banner.jpg';
}

$course_outline_title = get_field('course_outline_title');
$course_outline_sort_descriptions = get_field('course_outline_sort_descriptions');
$course_descriptions = get_field('course_descriptions');

$learning_teaching_title = get_field('learning_teaching_title');
$learning_teaching_short_descriptions = get_field('learning_teaching_short_descriptions');
$learning_teaching_descriptions = get_field('learning_teaching_descriptions');

$career_possiblities_tiltle = get_field('career_possiblities_tiltle');
$career_possiblities_short_descriptions = get_field('career_possiblities_short_descriptions');
$career_possiblities_descriptions = get_field('career_possiblities_descriptions');

$entry_credit_tiltle = get_field('entry_credit_tiltle');
$entry_credit_short_descriptions = get_field('entry_credit_short_descriptions');

$reviews_title = get_field('reviews_title');
$reviews_descriptions = get_field('reviews_descriptions');

$video_title = get_field('video_title');
$video_descriptions = get_field('video_descriptions');
$gallery_videos = get_field('gallery_videos');


//course details page changes
$feesperyear    = get_field('fees_per_year',get_the_id());

//Review Sections 
global $wpdb;
$table_name = $wpdb->prefix . "review";
$courseidreviwid = get_the_id();
$avarage = 0;
$totalaverage =0;
$totalcoursereviws =0;
//SELECT * FROM `ege_review` WHERE `course_id` = 15
$coursereview = $wpdb->get_results( "SELECT * FROM  $table_name WHERE `status` = 1  and `course_id` = $courseidreviwid  ORDER BY `create_datetime` DESC");
if(!empty($coursereview)){
$totalcoursereviws =  count($coursereview);
$totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE `status` = 1  and `course_id` =".$courseidreviwid);
$teching_quality = $wpdb->get_row("SELECT AVG(`teching_quality`) as avarage FROM $table_name WHERE `course_id` =".$courseidreviwid);
$school_facilites = $wpdb->get_row("SELECT AVG(`school_facilites`) as avarage FROM $table_name WHERE `course_id` = ".$courseidreviwid);
$locations = $wpdb->get_row("SELECT AVG(`locations`) as avarage FROM $table_name WHERE `course_id` = ".$courseidreviwid);
$career_assistance = $wpdb->get_row("SELECT AVG(`career_assistance`) as avarage FROM $table_name WHERE `course_id` =".$courseidreviwid);
$value_of_money = $wpdb->get_row("SELECT AVG(`value_of_money`) as avarage FROM $table_name WHERE `course_id` =".$courseidreviwid);
//echo $avarage     = number_format((float)$totalrating->avarage, 2, '.', '');
// echo "<pre>";
// print_r($coursereview);
// echo "</pre>";

$avarage = $totalrating->avarage;
$teching_quality_avrage     = wp_star_rating_avarage_float($teching_quality->avarage);
$school_facilites_avrage    = wp_star_rating_avarage_float($school_facilites->avarage);
$locations_avrage           = wp_star_rating_avarage_float($locations->avarage);
$career_assistance_avrage   = wp_star_rating_avarage_float($career_assistance->avarage);
$value_of_money_avrage      = wp_star_rating_avarage_float($value_of_money->avarage);
$totalaverage               = wp_star_rating_avarage_float($avarage);
}
$args = array(
   'rating' => $avarage,
   'type' => 'rating',
   'number' => 5,
);

//$ratings = round( $avarage / 10, 0 ) / 2;
//$average = 3;

if(is_user_logged_in()) {
    $userid         = get_current_user_id();
    $meta_key       = 'course_short_list';
    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
    $wishclass      = 'add';
    $wishsortlistclass   = 'fa-heart';
    if(is_array($educatorarray)){
        if(in_array(get_the_id(), $educatorarray)){
            $wishclass = 'remove';
            $wishsortlistclass = 'fa-heart-o';
        }
    }
}else{
        $educatorarray = maybe_unserialize(get_course_wislist($ipaddress));
        $educatorarray = maybe_unserialize($educatorarray[0]);
        $wishclass      = 'add';
        $wishsortlistclass   = 'fa-heart';
        if(is_array($educatorarray)){
            if(in_array(get_the_id(), $educatorarray)){
                 $wishclass = 'remove';
                 $wishsortlistclass = 'fa-heart-o';
            }
        }
}

// Video Url Code Get Code.
$get_video_urls     = get_field('course_video_url',get_the_id());
$coursetitlehead = get_the_title();
$wordlenght =  strlen($coursetitlehead);
 $classname = '';
  $classname2='';
 if($wordlenght >= 57){
    $classname =  'full-course-width';
    $classname2 =  'full-course-width-two';
 }
?>

    <section class="inner-bannerbox topgreen-border" style="background-image: url(<?php echo $corsebannerimages; ?>)">
        <div class="wrapper">
            <div class="inner-baner-cont  <?php echo $classname; ?>" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">
                    <ul class="cf ">
                        <li><a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a></li>
                        <li><a href="<?php echo get_permalink($unvisityid);  ?>"><?php echo $universityname; ?></a></li>
                       <?php /*
                        <li><a href="#">University</a></li>
                        <li><a href="#">UK</a></li>
                        */ ?>
                        <li><label> <?php echo get_the_title(); ?></label></li>

                    </ul>
                </div>
                <div class="innerbanner-block cf">
                    <?php if(!empty($unvisitylogo)){ ?>
                    <div class="inrbanner-leftimg">
                        <img src="<?php echo $unvisitylogo; ?>" alt="" />
                    </div>
                    <?php } ?>
                    <div class="inrbaner-rightcont <?php echo $classname; ?> ">
                        <div class="baner-titbox ">
                            <h3><?php echo get_the_title(); ?></h3>
                            <?php if(is_user_logged_in()) { ?>
                            <span data-wish="<?php echo $wishclass; ?>" class="educator-wish-list"><i class="fa <?php echo $wishsortlistclass; ?>"></i></span>
                            <?php }else{ ?>
                        <span data-wish="<?php echo $wishclass; ?>" class="educator-wish-list"><i class="fa <?php echo $wishsortlistclass; ?>"></i></span>
                        <?php } ?>
                            <?php /*
                            <span data-wish="add" class="educator-wish-list red-heart" ><i class="fa fa-heart-o"></i></span>
                            */ ?>
                        </div>
                        <div class="banner-country-flag">
                           <a title="<?php echo get_the_title($unvisityid); ?>" href="<?php echo get_permalink($unvisityid); ?>"> <span>at <?php echo $universityname; ?></span></a>
                            <div class="course-country-banner">
                                 <?php if(!empty($countryflag)) {?>   
                                 <span><img src="<?php echo  $countryflag; ?>"></span>
                                <?php } ?>
                                <span class="dottd-btmborder"><?php echo $countryname; ?></span>
                            </div>
                        </div>
                        <div class="school-detail-bottom">
                            <div class="rate-star">
                                <?php wp_star_rating($args); ?>
                                <a class="ratenum-like" href="javascript:void(0)"  title="like">
                                    <span class="white-brd rating-number-box"><?php echo $totalaverage;  ?></span>
                                </a>
                            </div>
                            <div class="review-texts">
                                <span class="noborder-bold"><?php echo $totalcoursereviws; ?> reviews</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="innerpage-menu-box greenbgcolor">
        <div class="wrapper cf">
            <div class="sticky-imgtit titimgsticky <?php echo $classname; ?>">
                <?php if(!empty($unvisitylogo)) { ?>
                <div class="stikimg-small">
                  <img src="<?php echo $unvisitylogo ?>" alt="" width="60"/>
                </div>
                <?php } ?>
                <div class="sticky-tibox">
                   <div class="table">
                       <div class="table-cell">
                            <h4><?php echo get_the_title(); 
                   

                            ?></h4>
                       </div>
                    </div>
                </div>
            </div>
            <div class="enquiry-rightbox">
                <a style="display:none"  href="#" class="btn booknow-btn">Book now</a>
                <a href="javascript:void(0);" class="btn enquiry-btn">Enquire now</a>
            </div>

            <div class="enquiry-form-popup course-form-popup">
                 <div class="maxwidth-box">
                     <div class="padding whitecolor only-enqu-box"> 
                         <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                         <?php echo do_shortcode('[contact-form-7 id="129" title="Contact form 1"]'); ?>
                     </div>    
                 </div>
            </div>
            <div class="iner-left-menubox" data-aos="fade-right" data-aos-duration="1200">
                <a href="javascript:void(0);" id="innermenu-click" class="innermenu-linkbox">
                   <span></span>
                   <span></span>
                   <span></span>
                </a>
                <ul class="cf in-responsive">
                    <li><a href="#go-aboutid">Course Summary</a></li>
                    <?php if(!empty($course_outline_title) && !empty($course_descriptions)){?>
                    <li><a href="#go-courseid">Course Outline</a></li>
                    <?php } 
                    if(!empty($learning_teaching_title) && !empty($learning_teaching_descriptions)){?>
                    <li><a href="#go-learning">Learning & Teaching</a></li>
                    <?php } 
                    if(!empty($career_possiblities_tiltle) && !empty($career_possiblities_descriptions)){?>
                    <li><a href="#go-career">Career Possiblities</a></li>
                    <?php } ?>
                    <li><a href="#go-rankid">Entry & Credit</a></li>
                    <li><a href="#go-reviewid">Reviews</a></li>
                </ul>
            </div>
        </div>
    </section>
    <section class="main-leftrigt-secbox cf">
    <div class="wrapper cf">
    
    <div class="right-30par-box" id="side">
       <div class="theiaStickySidebar">
            <div class="box_detail">

                <?php if(!empty($get_video_urls)){ 
                    ?>
                <div class="video-iframbox">
                    <iframe width="100%" height="200" src="<?php echo trim($get_video_urls) ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <?php } ?>
                <div class="price"  style="display:none"> <?php if(!empty($feesperyear)){?>
                    <span class="rate-leftbox"> <i>£<?php echo $feesperyear; ?> </i></span><span class="original_price"><b>Per Year</b></span>
                    <?php } ?>
                </div>
                <a href="javascript:void(0)" style="display:none;" class="btn_1 full-width">Book Now</a>
      
                <a style="display:none;"  href="javascript:void(0)" class="btn_1 full-width outline"><i class="fa fa-heart"></i>Add to wishlist</a>
                <div id="list_feat">
                    <?php /*
                    <h3>What's includes</h3>
                    <ul>
                        <li><i class="fa fa-mobile"></i>Mobile support</li>
                        <li><i class="fa fa-archive"></i>Lesson archive</li>
                        <li><i class="fa fa-mobile"></i>Mobile support</li>
                        <li><i class="fa fa-comments-o"></i>Tutor chat</li>
                        <li><i class="fa fa-file-text"></i>Course certificate</li>
                    </ul>
                    */ 
                    // KEY Informations Mangements code.
                    $feesperyear            = get_field('fees_per_year',get_the_id());
                    $language               = get_field('language',get_the_id());
                    $time_durations_year    = get_field('time_durations',get_the_id());
                    $time_durations_months  = get_field('durations_month',get_the_id());
                    $displaydurationssvalue = '';
                    if(!empty($time_durations_months)){
                    $displaydurationssvalue = $time_durations_months.' '.'Months';
                    }
                    if(!empty($time_durations_year)){
                    $displaydurationssvalue = $time_durations_year.' '.'Years';
                    }
                    if(!empty($time_durations_year) && !empty($time_durations_months)){
                    $displaydurationssvalue = $time_durations_year .'.'.$time_durations_months.' '.'Years';
                    }

                    $deadline_date              = get_field('deadline_date',get_the_id());
                    $course_qualifications      = get_field('course_qualifications',get_the_id());
                    $tuition_fee_international  = get_field('tuition_fee_international',get_the_id());
                    $study_mode                 = get_field('study_mode',get_the_id());
                    $start_date                 = get_field('start_date',get_the_id());
                    $scholarshipprovide         = get_field('scholarship_provide',get_the_id());
                    $studylevelname             = '';
                    $studylevel                 = wp_get_post_terms(get_the_id(),'studylevel');
                    // echo $studylevel[0]['term_id'];
                    if(!empty($studylevel)){
                     $studylevelname = $studylevel[0]->name;
                    } 
                    ?>
                    <h3>Key Information</h3>
                    <ul>
                        <?php if(!empty( $studylevelname)){?>
                        <li><span>Study Level: </span><p><?php echo $studylevelname; ?></p></li>
                        <?php } ?>
                        <?php if(!empty( $displaydurationssvalue)){?>
                        <li><span>Duration: </span><p><?php echo $displaydurationssvalue;    ?></p></li>
                        <?php } ?>
                        <?php if(!empty( $study_mode)){?>
                        <li><span>Study Mode: </span><p><?php echo $study_mode; ?></p></li>
                        <?php } ?>
                        <?php if(!empty( $deadline_date)){

                                $deadline_date = date_create($deadline_date);
                                $deadline_date = date_format($deadline_date,"d F Y");
                            ?>
                        <li><span>Application Deadline: </span><p><?php echo $deadline_date; ?></p></li>
                        <?php } ?>
                              <?php if(!empty( $start_date)){

                                $start_date = date_create($start_date);
                                $start_date = date_format($start_date,"d F Y");
                            ?>
                        <li><span>Start Date: </span><p><?php echo $start_date; ?></p></li>
                        <?php } ?>
                        <?php /* if(!empty( $language)){?>
                        <li><span>Language: </span><p><?php echo  $language;    ?></p></li>
                        <?php } */ ?>
                        <?php if(!empty( $feesperyear)){?>
                        <li class="tutions-li-tooltip"><span>Tuition Fees EU/EEA: </span><p>£<?php echo $feesperyear;  ?> Per Year</p>
                         <div class="tut-tooltip"> <p>These fees apply to students from the EU/EEA</p></div> 
                        </li>
                        <?php } ?>
                        <?php if(!empty( $course_qualifications)){?>
                        <li><span>Qualification: </span><p><?php echo $course_qualifications;  ?></p></li>
                        <?php } ?>
                        <?php if(!empty( $tuition_fee_international)){?>
                            <li class="tutions-li-tooltip"><span>Tuition Fees International : </span><p> £<?php echo $tuition_fee_international;  ?></p>
                               <div class="tut-tooltip"> <p>These fees apply to students from outside the EU/EEA</p></div> 
                            </li>
                        <?php } ?>

                            </li>
                    </ul>   
                </div>
            </div>
            
            <div class="sidesti-headinbox">
                <h3>institution</h3>
            </div>
            <div class="single-cour-imgblock">
                <?php
                $bannerimage       = get_the_post_thumbnail_url($unvisityid,  'bannerimage-educators-list' );
                if(empty($bannerimage)){
                    $bannerimage  = get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                }
                //$unvisitylogo = get_field('university_logo',$unvisityid);
                $universitycontent  = egobal_get_the_excerpt($unvisityid);


                ?>
                <div class="cours-sing-blockimg" style="background-image:url('<?php echo $bannerimage ?>');">
                    <?php if(!empty($unvisitylogo)) { ?>
                    <span><img src="<?php echo $unvisitylogo; ?>" alt="" width="60"></span>
                    <?php } ?>
                </div>
                <div class="cours-cont-block course-details-uni-link">
                    <a   title="<?php echo get_the_title($unvisityid); ?>" href="<?php echo get_permalink($unvisityid); ?>"><h4><?php echo get_the_title($unvisityid); ?></h4></a>
                    <p><?php echo $universitycontent; ?></p>
                    <div class="text-right">
                        <a href="<?php echo get_permalink($unvisityid); ?>" class="sinlink-blk">read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="left-70sidebox">
    <section class="about-top-contebox" id="go-aboutid">
        <div class="wrapper cf">
            <div class="title-contentbox" data-aos="fade-right" data-aos-duration="600">
                <h1> Course Summary </h1>
                <?php
                    //$NumberFour = 4.1;
                    //$total = 5;
                ?>
                <?php echo wpautop(get_the_content()); ?>
                <div class="hidecont-box">
                    <p>
                    If you are changing direction after starting in another career area, you will possess valuable work experience and professional awareness. Many employers actively seek candidates with these benefits. Our network of contacts, a dedicated employability service and a reputation in the legal profession mean we also have an outstanding track-record of finding students legal employment.Many employers favour GDL students in an increasingly competitive legal job market. Through studying another degree subject, you will have gained many transferable skills. By taking the conversion route into law, you are showing motivation and determination by choosing law at a later stage than some others. This looks good to employers.
                </p>
                <p>
                    If you are changing direction after starting in another career area, you will possess valuable work experience and professional awareness. Many employers actively seek candidates with these benefits. Our network of contacts, a dedicated employability service and a reputation in the legal profession mean we also have an outstanding track-record of finding students legal employment.Many employers favour GDL students in an increasingly competitive legal job market. Through studying another degree subject, you will have gained many transferable skills. By taking the conversion route into law, you are showing motivation and determination by choosing law at a later stage than some others. This looks good to employers.
                </p>    
                </div>
                <div class="seemore-btnbox" style="display:none;">
                    <a href="javascript:void(0);" class="seemore-btn" id="loadmore"><span class="plusicon"><i class="fa fa-plus-circle"></i>see more</span><span class="minus-icon"><i class="fa fa-minus-circle"></i>less more</span></a>
                </div>
            </div>
        </div>
    </section>
    <?php if(!empty($course_descriptions)) {?>

    <section class="course-section greybg" id="go-courseid">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2><?php echo ucwords(strtolower($course_outline_title)); ?></h2>
                <p><?php echo $course_outline_sort_descriptions; ?></p>
            </div>
            <div class="content-mainbox" data-aos="fade-left" data-aos-duration="1200">
                <?php echo $course_descriptions; ?>
            </div>
        </div>
    </section>
    <?php } ?>
    <?php if(!empty($learning_teaching_descriptions)) {?>
    <section class="course-section " id="go-learning">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2><?php echo ucwords(strtolower($learning_teaching_title)); ?></h2>
                <p><?php echo $learning_teaching_short_descriptions; ?></p>
            </div>
            <div class="content-mainbox" data-aos="fade-left" data-aos-duration="1200">
                <?php echo $learning_teaching_descriptions; ?>
            </div>
        </div>
    </section>
    <?php } ?>
    <?php if(!empty($career_possiblities_descriptions)) {?>
    <section class="course-section greybg" id="go-career">
    <div class="wrapper cf">
        <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
            <h2><?php echo ucwords(strtolower($career_possiblities_tiltle)); ?></h2>
            <p><?php echo $career_possiblities_short_descriptions; ?></p>
        </div>
        <div class="content-mainbox" data-aos="fade-left" data-aos-duration="1200">
            <?php echo $career_possiblities_descriptions; ?>
        </div>
    </div>
    </section>
    <?php } ?>
    

    <section class="course-section  ranking-section" id="go-rankid"  >
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2><?php echo ucwords(strtolower($entry_credit_tiltle)); ?></h2>
                <p><?php echo $entry_credit_short_descriptions; ?></p>
            </div>
            <div class="our-ranking-box cf" data-aos="fade-left" data-aos-duration="1200"  style="display:none;">
                <div class="ourrank-leftbox">
                    <h3>Average student ranking</h3>
                    <div class="cf">
                        <div class="school-detail-bottom all-reviebox cf">
                            <div class="rate-star">
                                <?php wp_star_rating($args); ?>
                                <a class="ratenum-like" href="javascript:void(0)" title="like">
                                    <span class="rating-number-box"><?php echo $totalaverage; ?></span>
                                </a>
                            </div>
                            <div class="review-texts">
                                <span><?php echo $totalcoursereviws; ?> reviews</span>
                            </div>
                        </div>
                    </div>
                    <div class="cf rankcout-box">
                        <div class="leftrevie-box">
                            <span><?php echo $totalcoursereviws; ?></span>
                            <p>reviews</p>
                        </div>
                        <div class="leftcourserevie-box">
                            <span>137</span>
                            <p>educator reviews</p>
                        </div>
                    </div>
                    <div class="rows cf rankicon-boxs" >
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/campus-culcture.png" width="38" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-uparow.png" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-earlyicon.png" alt="">
                        </div>
                        <div class="four-blocks ranking-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/rank-usericon.png" alt="">
                        </div>

                    </div>
                </div>


                <div id="chartContainer" class="ourrank-rightbox"  style="display:none"></div>
            </div>
        </div>
    </section>
    <?php 
               $categories = get_the_terms(get_the_id(),'subject' );
                if(!empty($categories)){
                $realtedcategories = array();
                foreach ($categories as $value) {
                # code...
                $realtedcategories[] =  $value->slug;
                }
                $subject_array = array(
                'taxonomy' => 'subject',
                'field'    => 'slug',
                'terms' => $realtedcategories
                );

                $the_query = new WP_Query( array(
                'posts_per_page' => 8,
                'order' => 'DESC',
                'orderby' =>'date',
                'post_type'     =>'course',
                'tax_query' => array($subject_array),
                'post__not_in' => array(get_the_id())
                )
                ); 
               
        if($the_query -> have_posts()){
      ?>          
    <section class="inner-blocks-slider">
        <div class="remove-extrapading"><h2>Related Courses</h2></div>
        <div class="wrapper cf">
            <div class="school-detail-slider page-product-mainbox">
                <div id="course-sliderbox" class="rows cf owl-carousel course-sliderbox innercou-slidebox">
                            <?php
                            $count = 0;
                            ?>
                            <?php while ($the_query -> have_posts()) : $the_query -> the_post() ;
                                do_action('courses_html_actions');
                            endwhile;
                            ?>
                </div>
            </div>
        </div>
    </section>
    <?php }  }?>
    <section class="our-review-section removesome-spacebox" id="go-reviewid">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <?php if(empty($reviews_title)){?> 
                 <h2><?php echo ucwords(strtolower($reviews_title));?></h2>
                 <?php }else{ ?>
                 <h2>Reviews</h2>
                  <?php } ?>  
                <!--<p><?php // echo $reviews_descriptions;  ?></p>-->
            </div>
            <div class="ourreiv-blockbox" data-aos="fade-left" data-aos-duration="1200">
                 <?php  foreach ($coursereview as $value) {
                    # code...
                   $userid          = $value->userid;
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                  // $user            = get_user_by( 'email',$useremail);
                   //print_r($user);
                   $firstname       =  $user_info->first_name;
                   $lastname        =  $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $descriptions    = $value->descriptions;
                   $overal_rating   = $value->overal_rating;
                   $id              = $value->id;
                  // echo get_avatar( $userid ); 

                   ?>
                <div class="reviwebox-block renew-review-block cf">
                    <div class="left-riveimg-box equal-height">
                        <span class="rie-imgicons"><?php echo get_avatar( $userid ,52); ?></span>                        
                    </div>
                    <div class="right-rivecontbox equal-height">
                        <div class="table" >
                            <div class="table-cell">
                               <div class="revrat-box cf">                                    
                                    <h4><?php echo $userlogin; ?></h4>
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($overal_rating) ){
                                            $totalavaragereaming = 5;
                                            ?>
                                            <span class="like-count">
                                            <?php for($i=1; $i<=$overal_rating; $i++ ){ ?>
                                            <i class="fa fa-star"></i>
                                            <?php } ?>
                                            <?php for($i=$overal_rating; $i<$totalavaragereaming; $i++ ){ ?>
                                            <i class="fa fa-star-o"></i>
                                            <?php  } ?>
                                            </span>
                                            <?php  } ?>
                                        </div>
                                    </div>                                    
                                </div>
                                <p><?php echo substr($descriptions,'0','172').'...'; ?><a href="<?php echo site_url().'/review-details/?id='.$id  ?>" class="arrow-links">read more</a></p>
                            </div>
                        </div>
                        <a href="#" style="display:none;" class="helpfull-btn">Helpful</a>
                    </div>
                </div>


                   <?php
                } ?>
  
            <div class="wpcf7s form-design-comon cf">
                <form method="post" action="<?php echo site_url().'/review'; ?>">
                     <input type="hidden" name="educatorid" value="<?php echo $unvisityid; ?>">
                    <input type="hidden" name="courseid" value="<?php echo get_the_id(); ?>">
                    <?php   if(is_user_logged_in()) { ?>
                    <div class="Submit-btn">
                        <input type="submit"  name="Add review" value="Add Review">
                    </div>
                    <?php }else{ ?>
                  <div class="Submit-btn">
                        <input type="button" data-wish="nologin" data-id="0" class=" course-heart-icon-two " name="Add review" value="Add Review">
                    </div>
                    <?php } ?>
                </form>
                </div>

            </div>
        </div>
    </section>
<?php //} ?>

    <?php
    // check if the repeater field has rows of data
    if( have_rows('gallery_videos') ):
        $video_title = get_field('video_title');
        $video_descriptions = get_field('video_descriptions');
        ?>
    <section style="display:none"  class="course-section videogallery-section" id="go-videoid">
        <div class="wrapper cf">
            <div class="top-title-contbox aos-init" data-aos="fade-right" data-aos-duration="1200">
                <?php if(!empty($video_title)) { ?>
                <h2><?php echo ucwords(strtolower($video_title));  ?></h2>
                <?php } ?>
                <?php if(!empty($video_descriptions )) {?>
                <p><?php echo $video_descriptions;  ?></p>
            <?php } ?>
            </div>
            <div class="videogally-box">
                <div class="slider slider_circle_10">
                <?php
                // check if the repeater field has rows of data

                // loop through the rows of data
                while ( have_rows('gallery_videos') ) : the_row();
                    ?>
                  <div>
                    <iframe width="619" height="313" src="<?php echo get_sub_field('add_video_url'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>

                <?php

                endwhile;


                // no rows found
                    ?>
                  <div class="next_button"><i class="fa fa-angle-right"></i></div>  
                  <div class="prev_button"><i class="fa fa-angle-left"></i></div>  
                </div>
            </div> 
        </div>
    </section>

    <?php
    endif;
//SELECT `teching_quality` / sum(`teching_quality`) *100 as percenage FROM `ege_review`

    ?>
    </div>
</div>
 </section>
    
    <section class="course-section cour-provbox" style="display:none;">
    <div class="wrapper cf">
        <div class="top-title-contbox aos-init" data-aos="fade-right" data-aos-duration="1200">
            <h2>About course provider</h2>
        </div>
        <div data-aos="fade-left" data-aos-duration="600" class="course-prov-slider owl-carousel">
            <div class="cour-blockbox">
                <div class="course-imgbox"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/course-provider-img1.jpg" alt="" /></div>
                <div class="course-detailbox">
                    <h4>The University of LAW</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                    <p>

                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <div class="view-profi-linkbox">
                        <a href="#" class="viewpori-btn">View profile <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="cour-blockbox">
                <div class="course-imgbox"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/course-provider-img1.jpg" alt="" /></div>
                <div class="course-detailbox">
                    <h4>The University of LAW</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                    <p>

                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <div class="view-profi-linkbox">
                        <a href="#" class="viewpori-btn">View profile <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="cour-blockbox">
                <div class="course-imgbox"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/course-provider-img1.jpg" alt="" /></div>
                <div class="course-detailbox">
                    <h4>The University of LAW</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                    <p>

                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <div class="view-profi-linkbox">
                        <a href="#" class="viewpori-btn">View profile <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?php 
do_action('loginbox_html_code'); 
do_action('loginbox_html_code_two'); 
?>    

    <?php
        if($teching_quality_avrage != 0){
             $teching_quality_avrage_percentage = ( $teching_quality_avrage / 5 ) * 100;
         }
         if($school_facilites_avrage != 0){
             $school_facilites_percentage = ( $school_facilites_avrage / 5 ) * 100;
         }
         if($locations_avrage != 0){
             $locations_avrage_percentage = ( $locations_avrage / 5 ) * 100;
         }
         if($career_assistance_avrage != 0){
             $career_assistance_avrage_percentage = ( $career_assistance_avrage / 5 ) * 100;
         } 
        if($value_of_money_avrage != 0){
            $value_of_money_avrage_percentage = ( $value_of_money_avrage / 5 ) * 100;
        }

    ?>
    <?php
    $args = array(
   'rating' => 3.5,
   'type' => 'rating',
   'number' => 5,
);
?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/dist/canvasjs.min.js"></script>
    <script>
    jQuery(document).ready(function($) {
        jQuery('.course-title-name').val('<?php echo get_the_title(); ?>');
        jQuery('.course-name').children().val('<?php echo get_the_title(); ?>');
        jQuery('.course-name').attr('readonly','readonly');
        ///jQuery('.course-name').val('<?php echo $universityname; ?>');
        jQuery('.course-title-name').attr('readonly','readonly');
        jQuery(".educator-wish-list").live("click", function(){
                //alert('Add To Wish');
                ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
                $thisclass = jQuery(this); 
                $wishclass = jQuery(this).data('wish');
               // alert($wishclass);
          

                $educatorid = <?php echo get_the_id(); ?>;
                $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                'action': 'course_wish_list',
                'courseid' : $educatorid,
                'wishdata'   : $wishclass,
                },
                success:function(data) {
                // This outputs the result of the ajax request
               // alert(data);
                if($wishclass == 'add'){

                    jQuery($thisclass).children().removeClass('fa-heart');
                    jQuery($thisclass).children().addClass('fa-heart-o');
                    jQuery($thisclass).data('wish','remove');
                }else{

                    jQuery($thisclass).children().addClass('fa-heart');
                    jQuery($thisclass).children().removeClass('fa-heart-o');
                    jQuery($thisclass).data('wish','add');

                }


                },
                error: function(errorThrown){
                console.log(errorThrown);
                }

                });        
    
     }); 

    });

   
    </script>
    
    <script>

jQuery(document).ready(function() {
			jQuery('#side')
			.theiaStickySidebar({
				additionalMarginTop: 100
			});
		});

</script>


<?php get_footer();
 