<?php

/* Template Name: Parnter Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}

$help_you_choose_a_course = get_field('help_you_choose_a_course');
$support_after_of_the_course = get_field('support_after_of_the_course');
$provide_materials = get_field('provide_materials');

$compny_title         = get_field('compny_title');
$company_descriptions = get_field('company_descriptions');
$company_main_logo  = get_field('company_main_logo');
$company_right_logo_one = get_field('company_right_logo_one');
$company_right_logo_two = get_field('company_right_logo_two');

?>
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox team-banner-tit">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                        <p>Please select an appointment slot and we will contact you shortly to confirm!</p>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>

<section class="blog-details-page main-servic-sect">
    <div id="primary" class="wrapper cf">
        <div class="">
            <div class="sterling-low-section">
                <div class="sterling-topbox cf">
                    <div class="leftster-box">
                        <?php if(!empty($company_main_logo)){ ?>
                        <span><img src="<?php echo $company_main_logo; ?>" alt="" width="187" /></span>
                        <?php }?>
                         <?php if(!empty($compny_title)){ ?>
                        <h3><?php $compny_title;   ?></h3>
                        <?php } ?>
                    </div>
                    <div class="ritster-box">
                        <?php if(!empty($company_right_logo_one)){ ?>
                        <span class="smllogos"><img src="<?php echo $company_right_logo_one; ?>" width="170" alt="" /></span>
                        <?php } ?>
                        <?php if(!empty($company_right_logo_two)){ ?>
                        <span class="smllogos"><img src="<?php echo $company_right_logo_two; ?>" alt="" width="50"/></span>
                        <?php } ?>
                    </div>
                </div>
                <div class="sterling-descbox">
                        <?php echo $company_descriptions; ?>
                </div>
            </div>
            
            <div class="our-partner-mainsection">
                <?php
                // check if the repeater field has rows of data
                if( have_rows('company_parntens') ):

                // loop through the rows of data
                 $count =0;
                while ( have_rows('company_parntens') ) : the_row();

                    $pa_name              =   get_sub_field('name');
                    $image              =   get_sub_field('image');
                    $designations       =   get_sub_field('designations');
                    $descriptions       =   get_sub_field('descriptions');
                    $book_appoiment_link =   get_sub_field('book_appoiment_link');
                    $pname              =   get_sub_field('sub_field_name');
                    $count++;
                    ?>
                <div class="partner-block cf">
                    <div class="partner-imgbox equal-height" style="background-image:url(<?php echo  $image; ?>)"></div>
                    <div class="equal-height partner-rit-block">
                        <div class="partern-detail-box">
                            <div class="partner-name-numbox cf">
                                <div class="patner-leftname">
                                    <h3><?php echo $pa_name;  ?></h3>
                                    <span><?php echo  $designations;  ?> </span>
                                </div>
                                <div class="partner-numbox"><?php echo str_pad($count, 2, "0", STR_PAD_LEFT); ?></div>
                            </div>
                            <div class="partner-des-textbox">
                                <?php echo $descriptions; ?>
                            </div>
                            <div class="book-appin-box">
                                <a href="<?php echo   $book_appoiment_link; ?>" class="book-appiont-btn">book appointment</a>
                            </div>
                        </div>
                    </div>
                </div>

                    <?php

                endwhile;

                else :

                // no rows found

                endif;

                ?>
                <?php /* ?>
                <div class="partner-block cf">
                    <div class="partner-imgbox equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/RuslanKosarenko.jpg)"></div>
                    <div class="equal-height partner-rit-block">
                        <div class="partern-detail-box">
                            <div class="partner-name-numbox cf">
                                <div class="patner-leftname">
                                    <h3>Ruslan Kosarenko</h3>
                                    <span>Managing Partner </span>
                                </div>
                                <div class="partner-numbox">01</div>
                            </div>
                            <div class="partner-des-textbox">
                                <p>Ruslan Kosarenko is a principal partner of Sterling Law.  Ruslan has 11 years of experience in immigration law practice with particular expertise in complex human rights cases, including family reunion and asylum cases at both initial application and appeal levels. He is accredited at the highest Level 3 with the Office of Immigration Services Commissioner (OISC).
                                His practice areas include all types of UK visa applications, naturalisation and EU law applications, and full-scope corporate immigration support. Ruslan has extensive experience in the Points-Based System (PBS), including sponsored migrants, highly-skilled migrants, entrepreneurs and investors.</p>
                            </div>
                            <div class="book-appin-box">
                                <a href="#" class="book-appiont-btn">book appointment</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="partner-block cf">
                    <div class="partner-imgbox equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/AliyaRimshelis.jpg)"></div>
                    <div class="equal-height partner-rit-block">
                        <div class="partern-detail-box">
                            <div class="partner-name-numbox cf">
                                <div class="patner-leftname">
                                    <h3>Aliya Rimshelis </h3>
                                    <span>Immigration Lawyer</span>
                                </div>
                                <div class="partner-numbox">02</div>
                            </div>
                            <div class="partner-des-textbox">
                                <p>Aliya Rimshelis is a Level 1 OISC accredited immigration adviser with experience in UK immigration law. She has a proven track record of successfully managing projects, meeting deadlines and producing positive results. Aliya specialises in UK immigration law and EU law in particular she has an experience in further leave to remain applications and entry clearance applications such as UK family migration application s (e.g. spouse visas, child of a settled parent), EEA applications, point based system applications (e.g. Tier 1 (Start-Up and Innovator), Tier 1(Investor), Tier 2), settlement, indefinite leave to remain and naturalisation applications. Aliya is also assisting senior caseworkers with complex cases and appeal matters.</p>
                            </div>
                            <div class="book-appin-box">
                                <a href="#" class="book-appiont-btn">book appointment</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="partner-block cf">
                    <div class="partner-imgbox equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/KatsiarynaPazniak.jpg)"></div>
                    <div class="equal-height partner-rit-block">
                        <div class="partern-detail-box">
                            <div class="partner-name-numbox cf">
                                <div class="patner-leftname">
                                    <h3>Katsiaryna Pazniak</h3>
                                    <span>Legal Advisor </span>
                                </div>
                                <div class="partner-numbox">03</div>
                            </div>
                            <div class="partner-des-textbox">
                                <p>Katsiaryna Pazniak is an intellectual property legal adviser with a wide experience in business and IP strategy planning. Katsiaryna’s main focus is the intellectual property services for startups, entrepreneurs, business and personal needs. She specialises in trademark registration, brand protection, patents, copyright, design law and prevention of IP risks and unfair competition. Her focus includes data protection advice and compliance, GDPR regulation, misleading advertising control, special IT and AI regulations and online reputation management. Katsiaryna specialises in Tier 4 (Student) and Tier 5 (Internship) applications.</p>
                            </div>
                            <div class="book-appin-box">
                                <a href="#" class="book-appiont-btn">book appointment</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="partner-block cf">
                    <div class="partner-imgbox equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/InnaSemeniuk.jpg)"></div>
                    <div class="equal-height partner-rit-block">
                        <div class="partern-detail-box">
                            <div class="partner-name-numbox cf">
                                <div class="patner-leftname">
                                    <h3>Inna Semeniuk</h3>
                                    <span>Trainee Solicitor</span>
                                </div>
                                <div class="partner-numbox">04</div>
                            </div>
                            <div class="partner-des-textbox">
                                <p>Inna Semeniuk has a special interest in Immigration Law in particular in cases raising human rights issues. Inna has completed the Oxford Blockchain Programme at Oxford University’s Saïd Business School which consolidates relevant information on blockchain for business leaders and innovators by showcasing best use cases, value propositions, and implementation strategies in the blockchain industry.</p>
                            </div>
                            <div class="book-appin-box">
                                <a href="#" class="book-appiont-btn">book appointment</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="partner-block cf">
                    <div class="partner-imgbox equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/MichaelIatsukha.jpg)"></div>
                    <div class="equal-height partner-rit-block">
                        <div class="partern-detail-box">
                            <div class="partner-name-numbox cf">
                                <div class="patner-leftname">
                                    <h3>Michael Iatsukha</h3>
                                    <span>Trainee Solicitor</span>
                                </div>
                                <div class="partner-numbox">05</div>
                            </div>
                            <div class="partner-des-textbox">
                                <p>Michael specializes in Commercial and Corporate law. He has vast experience in IT, IP and International Law. Part of his job is focused on researching the various possibilities and use cases of applying blockchain technology in real life areas and the arising legal challenges along the way. Michael is proactively engaged in research and pursuit of new business and legal opportunities in uncommon areas of law, such as cryptocurrency. Michael helps us navigate through the UK’s legal and regulatory framework and coordinating with the regulatory authorities.</p>
                            </div>
                            <div class="book-appin-box">
                                <a href="#" class="book-appiont-btn">book appointment</a>
                            </div>
                        </div>
                    </div>
                </div>

               <?php */ ?>
            </div>
            
        </div>      
    </div><!-- #primary -->
</section><!-- .wrap -->



<?php get_footer();?>

