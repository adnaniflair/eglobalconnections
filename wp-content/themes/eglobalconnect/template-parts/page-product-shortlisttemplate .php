<?php

/* Template Name:  Page Product Shortlist Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
// if(!is_user_logged_in()){
//     wp_redirect( site_url('login-register'));
//     exit;
// }
get_header(); 
wp_reset_postdata();
?>
<section class="inner-bannerbox all-subjectbox">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
    <?php
    $userid         = get_current_user_id();
    $meta_key       = 'product_short_list';
    //$educators_list_unique = array($educatorid);
    
    if(is_user_logged_in()){
    // wp_redirect( site_url('login-register'));
    // exit;

    $educatorarrys    = maybe_unserialize(get_user_meta($userid,$meta_key,true));
    }else{
        $ipaddress     = getRealIpAddr();
        $educatorarrys = maybe_unserialize(get_book_wislist($ipaddress));
        $educatorarrys = maybe_unserialize($educatorarrys[0]);
    }
    if(!empty($educatorarrys)){
    ?>
<section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
             <div class="booking-mainbox">
                <div class="rows cf">
                <?php
           
                $the_query = new WP_Query( array(
                    'posts_per_page' => -1,
                    'order' => 'DESC',
                    'orderby' =>'date',
                    'post_type'     =>'product',
                    'post__in' => $educatorarrys,
                    
                    )
                );
                    // echo "</pre>";
                if($the_query -> have_posts()){
                ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    $productid       = get_the_id();
                    $producttitle    = get_the_title();
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'bannerimage-product-list');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                    $countview       = pvc_get_post_views($productid);
                    $bestseller      = get_field('best_seller',$productid);

                    $productcontent  = get_the_excerpt();
                    $terms           = get_the_terms($productid,'product-categories');
                    if(!empty($terms)){
                    $categoryname    = $termsname = $terms[0]->name;
                    $termsid         = $terms[0]->term_id;
                    // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    }
                    $productprice    = get_field('product_price',get_the_id());
                    //Rating Code

                    //Review Sections  Code
                    global $wpdb;
                    $table_name = $wpdb->prefix . "review";
                    $totalproductreviws = 0;
                    $args = '';
                    $avarage =0;
                    $totalaverage =0;
                    $productview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE 
                        status = 1 AND `product_id` = ".$productid );
                    if(!empty($productview)){
                        $totalproductreviws =  count($productview);
                        $totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE  status = 1 AND `product_id` =".$productid);
                        $avarage                    = $totalrating->avarage;
                        $totalaverage               = wp_star_rating_avarage_float($avarage);
                    }
                      $args = array(
                       'rating' => $avarage,
                       'type' => 'rating',
                       'number' => 5,
                    );
                    ?>
                    <div class="three-block fourblock pagepro-block">
                        <div class="white-bg ourcourse-block">
                            <div class="course-imgblock">
                                <span class="imgmain-box"><img src="<?php echo $bannerimages; ?>" alt=""></span>
                                <?php if( !empty($bestseller)){ ?>
                                <div class="bachlabl-leftbox">
                                    <span class="pre-bachelr-label">
                                          <p>Bestseller</p>
                                    </span>
                                </div> 
                                <?php } ?> 

                                <div class="inner-titwid-img">
                                   <div class="ourcourse-contbox cf">
                                       <h4><?php echo $producttitle;?></h4>
                                        <?php if(!empty($productcontent)){ ?>
                                        <p><?php echo substr($productcontent,'0','172').'..'; ?> </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php  echo do_action('product_short_list_actions'); ?>

                            <div class="our-courseblk-content">
                                <a href="<?php echo get_permalink(); ?>"><h3><?php echo $producttitle;?> </h3></a>
                                <?php if(!empty($categoryname)) { ?>
                                    <div class="ourcor-smlcont-box"><?php echo $categoryname; ?> 
                                    </div>
                                <?php } ?>
                                <div class="cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                            <a class="ratenum-like" href="javascript:void(0)" title="like">
                                                <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                            </a>
                                        </div>
                                        <?php if(!empty($totalproductreviws)){ ?>
                                        <div class="review-texts">
                                            <span><?php echo $totalproductreviws; ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="year-view-box">
                                    <?php if(!empty($productprice)) { ?>
                                    <span class="rate-leftbox">From <i>£<?php echo $productprice; ?> </i></span>
                                    <?php } ?>

                                    <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assettwo/images/view-ico.png">views: <i><?php echo $countview; ?></i></span>
                                </div>
                            </div>
                        </div>
                </div>
                <?php  endwhile; ?>
                <?php }else{
                    echo "No Products Founds";
                    }
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </section>
<?php }else{ ?>
    <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
                <h2> shortlist not Founds</h2>
            </div>
    </section>
  <?php } ?>  
<?php get_footer();

