<?php

/* Template Name: My Profile Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

if(!is_user_logged_in()){
    wp_redirect( site_url('login-register'));
    exit;
}

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}
?>
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="blog-details-page">
    <div id="primary" class="wrapper cf">
        <div class="inner-detailbox">
            <?php
            if(is_user_logged_in()){ 
                $current_user = wp_get_current_user();
                /**
                 * @example Safe usage:
                 * $current_user = wp_get_current_user();
                 * if ( ! $current_user->exists() ) {
                 *     return;
                 * }
                 */
                  $user_login       =   $current_user->user_login; 
                  $user_email       =   $current_user->user_email; 
                  $user_firstname   =  $current_user->user_firstname; 
                  $user_lastname    =  $current_user->user_lastname;
                  $user_displayname =  $current_user->display_name; 
            }
            ?>
            <div  class="my-profiles-sections">
                    <h2>MY Personal Informations </h2>
                     <div class="enquiry-rightbox"><a href="javascript:void(0);" class="btn enquiry-btn openpersonalinfo">Edit</a></div>
                    <div id="personal-informtions" class="enquiry-form-popup">
                         <div class="maxwidth-box">
                             <div class="padding whitecolor">
                                 <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                                    <form  method="post" id="personal-informtions-form">
                                        First name:<br>
                                        <input type="hidden" name="action" value="update_personal_informations" />
                                        <input type="text" name="firstname" value="<?php echo  $user_firstname; ?>">
                                        <br>
                                        Last name:<br>
                                        <input type="text" name="lastname" value="<?php echo  $user_lastname; ?>" >
                                        <br>
                                        <br>
                                             Email:<br>
                                        <input readonly="readonly" disabled type="text" name="email"  value="<?php echo $user_email;   ?>">
                                        <br>
                                             Phone:<br>
                                        <input type="text" name="phone" >
                                        <br>
                                             Gender:<br>
                                        <input type="text" name="gener" >
                                        <br>
                                             Address:<br>
                                        <input type="text" name="address" >
                                        <br>
                                             Zip code:<br>
                                        <input type="text" name="zipcode" >
                                        <br>
                                            Town/city:<br>
                                        <input type="text" name="town-city" >
                                        <br>
                                            Country:<br>
                                        <input type="text" name="country" >
                                        <br>
                                        <input  class="submit-personal-infomations"  type="button" value="Submit">
                                    </form> 
                             </div>    
                         </div>
                    </div>
                    <table class="my-personal-informations">
                        <tr>
                            <td><span><?php echo __('First Name','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_firstname)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add First name','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_firstname; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                         <tr>
                            <td><span><?php echo __('Last Name','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_lastname)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Last name','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_lastname; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                        <tr>
                            <td><span><?php echo __('Date Of Birth','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_dateofbirth)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Date of Birth','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_dateofbirth; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                        <tr>
                            <td><span><?php echo __('Email','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_email)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Email','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_email; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                        <tr>
                            <td><span><?php echo __('Phone','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_phone)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Phone Number','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_phone; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                         <tr>
                            <td><span><?php echo __('Gender','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_gender)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Gender','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_firstname; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                                    <tr>
                            <td><span><?php echo __('Address','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_address)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Address','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_address; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>


                        <tr>
                            <td><span><?php echo __('Zip code','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_zipcode)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Gender','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_zipcode; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>

                        <tr>
                            <td><span><?php echo __('Town/City','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_town_or_city)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Town/City','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_town_or_city; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                        <tr>
                            <td><span><?php echo __('Country','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_country)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Country','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_country; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                    </table>
                    <h2> social link </h2>
                    <table class="social-link">
                        <tr>
                            <td><span><?php echo __('Facebook','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_country)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Facebook Link','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_country; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                        <tr>
                            <td><span><?php echo __('Twitter','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_country)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Twitter','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_country; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                        <tr>
                            <td><span><?php echo __('Linkedin','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_country)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Linkedin','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_country; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                        <tr>
                            <td><span><?php echo __('Google +','eglobaleducation'); ?></span></td>
                            <td>
                                <?php if(empty($user_country)){ ?>
                                <span class="add-informations-name"><?php echo __('+ Add Google Link','eglobaleducation'); ?></span>
                                <?php }else{ ?>
                                <span class="informations-name"><?php echo $user_country; ?></span>
                                 <?php } ?>   
                            </td>
                        </tr>
                    </table>
            </div>   
            <?php
            ?>
        </div>      
    </div><!-- #primary -->
</section><!-- .wrap -->

<?php get_footer();?>

<script type="text/javascript">
jQuery(document).ready(function($) {
    $(".submit-personal-infomations").on( "click", function(){
        var editpersonalfrm = $('#personal-informtions-form').serialize();

        ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: editpersonalfrm,
                success:function(data) {
                    // This outputs the result of the ajax request
                    alert(data);
                
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
     });   

    jQuery(".enquiry-rightbox .openpersonalinfo").on( "click", function(){
        jQuery("#personal-informtions").fadeIn();
        jQuery("body").addClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });
    jQuery(".close-popbox .clspopup").on( "click", function(){
        jQuery("#personal-informtions").fadeOut();
        jQuery("body").removeClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });
});
</script>

