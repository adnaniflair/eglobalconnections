<?php

/* Template Name: Teams Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}

$help_you_choose_a_course = get_field('help_you_choose_a_course');
$support_after_of_the_course = get_field('support_after_of_the_course');
$provide_materials = get_field('provide_materials');


?>
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox team-banner-tit">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a></li>
                    <li><a href="JavaScript:Void(0);"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                        <p>Please select an appointment slot and we will contact you shortly to confirm!</p>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="blog-details-page main-servic-sect">
    <div id="primary" class="wrapper cf">
        <div class="team-main-box">
            <div class="row cf">
                <?php
                // $arr = get_educator_wislist('192.168.1.152');
                // echo "<pre>";
                // print_r(maybe_unserialize($arr[0]));
                // echo "</pre>";
                $the_query = new WP_Query( array(
                'posts_per_page' => 10,
                'order'          => 'DESC',
                'orderby'        =>'date',
                'post_type'      =>'team-members',
                )
                );
                // echo "</pre>";
                if($the_query -> have_posts()){
                ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                $productid       = get_the_id();
                $producttitle    = get_the_title();
                $bannerimages    = get_the_post_thumbnail_url( get_the_id());
                if(empty($bannerimages)){
                      $bannerimages = get_stylesheet_directory_uri().'/assettwo/images/team-img1.png';
                }
                $designations      = get_field('designations',$productid);
                $facebook_link     = get_field('facebook_link',$productid);
                $linkdin_link      = get_field('linkdin_link',$productid);
                $instagram_link    = get_field('instagram_link',$productid);
                $content    = get_the_content($productid);
                ?>
                <div class="four-block">
                    <div class="teams-blocks equal-height">
                        <div class="team-grey-img equal-height">
                            <div class="team-imgbox"><img src="<?php echo $bannerimages; ?>" alt=""></div> 
                            <h4><?php echo $producttitle; ?></h4>
                            <?php if(!empty( $designations)){ ?>
                            <span><?php echo $designations; ?></span>
                            <?php } ?>                            
                        </div>
                        <div class="team-socia-contetbox">
                            <div class="team-soci-box">
                                <ul>
                                    <?php if(!empty($facebook_link)){ ?>
                                    <li><a target="_blank" href="<?php echo $facebook_link; ?>"><i class="fa fa-facebook-f"></i></a></li>
                                    <?php } ?>
                                    <?php if(!empty($linkdin_link)) { ?>
                                    <li><a target="_blank" href="<?php echo $linkdin_link; ?>"><i class="fa fa-instagram"></i></a></li>
                                    <?php } ?>
                                    <?php if(!empty($instagram_link)) { ?>
                                    <li><a target="_blank" href="<?php echo $instagram_link; ?>"><i class="fa fa-linkedin-in"></i></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="team-pbox">
                              <?php echo  wpautop($content); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php  endwhile; ?>
                <?php }else{
                echo "No Products Founds";
                }
                wp_reset_postdata(); ?>
                <?php /*
             
                <div class="four-block">
                    <div class="teams-blocks equal-height">
                        <div class="team-grey-img equal-height">
                            <div class="team-imgbox"><img src="<?php echo get_stylesheet_directory_uri().'/assettwo/images/team-img2.png'; ?>" alt=""></div> 
                            <h4>Janice Alvarez</h4>                            
                        </div>
                        <div class="team-socia-contetbox">
                            <div class="team-soci-box">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                            <div class="team-pbox">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="four-block">
                    <div class="teams-blocks equal-height">
                        <div class="team-grey-img equal-height">
                            <div class="team-imgbox"><img src="<?php echo get_stylesheet_directory_uri().'/assettwo/images/team-img1.png'; ?>" alt=""></div> 
                            <h4>Janice Alvarez</h4>
                            <span>CEO</span>                            
                        </div>
                        <div class="team-socia-contetbox">
                            <div class="team-soci-box">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                            <div class="team-pbox">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="four-block ">
                    <div class="teams-blocks equal-height">
                        <div class="team-grey-img equal-height">
                            <div class="team-imgbox"><img src="<?php echo get_stylesheet_directory_uri().'/assettwo/images/team-img2.png'; ?>" alt=""></div> 
                            <h4>Janice Alvarez</h4>                            
                        </div>
                        <div class="team-socia-contetbox">
                            <div class="team-soci-box">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                            <div class="team-pbox">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="four-block">
                    <div class="teams-blocks equal-height">
                        <div class="team-grey-img equal-height">
                            <div class="team-imgbox"><img src="<?php echo get_stylesheet_directory_uri().'/assettwo/images/team-img1.png'; ?>" alt=""></div> 
                            <h4>Janice Alvarez</h4>
                            <span>CEO</span>                            
                        </div>
                        <div class="team-socia-contetbox">
                            <div class="team-soci-box">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                            <div class="team-pbox">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="four-block ">
                    <div class="teams-blocks equal-height">
                        <div class="team-grey-img equal-height">
                            <div class="team-imgbox"><img src="<?php echo get_stylesheet_directory_uri().'/assettwo/images/team-img2.png'; ?>" alt=""></div> 
                            <h4>Janice Alvarez</h4>                            
                        </div>
                        <div class="team-socia-contetbox">
                            <div class="team-soci-box">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                            <div class="team-pbox">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the </p>
                            </div>
                        </div>
                    </div>
                </div> 
                */ ?>
            </div>
        </div>
    </div><!-- #primary -->
</section><!-- .wrap -->



<?php get_footer();?>

