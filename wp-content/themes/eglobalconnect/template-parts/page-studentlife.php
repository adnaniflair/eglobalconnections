<?php

/* Template Name:  Page Student Life Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
wp_reset_postdata();
?>
<section class="inner-bannerbox all-subjectbox">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="blog-details-page">
    <div id="primary" class="wrapper cf">
        <div class="inner-detailbox student-mainbox">
             <div class="rows cf">
                 <div class="two-blocks two-half">
                     <div class="student-blocks">
                         <span class="cf">
                             <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/student-lifebigimg.jpg" alt="">
                         </span>
                         <div class="stud-contbox">
                             <h4>loren ipsome changes loren ipsome changes</h4>
                             <p>lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes lorem ipsome changes </p>
                         </div>
                     </div>
                 </div>
                 <div class="two-blocks extrawidth75">
                     <div class="student-blocks hallbox cf">
                         <div class="stude-bgimg equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/student-lifesmallimg.jpg)">                             
                         </div>
                         <div class="stud-contbox equal-height">
                             <h4>loren ipsome changes loren ipsome changes</h4>
                             <p>lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes ipsome changeslorem ipsome changes  lorem ipsome changes</p>
                         </div>
                     </div>
                     <div class="student-blocks hallbox cf">
                         <div class="stude-bgimg equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/student-lifesmallimg.jpg)">                             
                         </div>
                         <div class="stud-contbox equal-height">
                             <h4>loren ipsome changes loren ipsome changes</h4>
                             <p>lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes ipsome changeslorem ipsome changes  lorem ipsome changes</p>
                         </div>
                     </div>
                 </div>
             </div> 
             <div class="rows cf">
                 <div class="two-blocks two-half">
                     <div class="student-blocks">
                         <span class="cf">
                             <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/student-lifebigimg.jpg" alt="">
                         </span>
                         <div class="stud-contbox">
                             <h4>loren ipsome changes loren ipsome changes</h4>
                             <p>lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes lorem ipsome changes </p>
                         </div>
                     </div>
                 </div>
                 <div class="two-blocks extrawidth75">
                     <div class="student-blocks hallbox cf">
                         <div class="stude-bgimg equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/student-lifesmallimg.jpg)">                             
                         </div>
                         <div class="stud-contbox equal-height">
                             <h4>loren ipsome changes loren ipsome changes</h4>
                             <p>lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes ipsome changeslorem ipsome changes  lorem ipsome changes</p>
                         </div>
                     </div>
                     <div class="student-blocks hallbox cf">
                         <div class="stude-bgimg equal-height" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/student-lifesmallimg.jpg)">                             
                         </div>
                         <div class="stud-contbox equal-height">
                             <h4>loren ipsome changes loren ipsome changes</h4>
                             <p>lorem ipsome changes lorem ipsome changes lorem ipsome changes lorem ipsome changeslorem ipsome changeslorem ipsome changeslorem ipsome changes  lorem ipsome changes ipsome changeslorem ipsome changes  lorem ipsome changes</p>
                         </div>
                     </div>
                 </div>
             </div> 
        </div>      
    </div><!-- #primary -->
</section><!-- .wrap -->

<?php get_footer();

