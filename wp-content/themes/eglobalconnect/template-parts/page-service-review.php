<?php

/* Template Name: My Services Review Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */


if(!is_user_logged_in()){
    wp_redirect( site_url('login-register'));
    exit;
}
$userid  = get_current_user_id();
$isservice = get_my_services_review($userid);
if(!empty($isservice)){
    wp_redirect( site_url('login-register'));
    exit;
}
// if(isset($_GET['reviewid'])){
//     $productid = $_GET['reviewid'];
//     if ( get_post_type($productid) != 'product' ) {
//         wp_redirect( site_url('login-register'));
//         exit;
//     }
// }else{
//         wp_redirect( site_url('login-register'));
//         exit;
    
// }
//$checkproductidarray = checkproductidarray();

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}
?>
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-details-page">
    <div class="loading overlay-bgcolor" style="display:none">
        <div class="all-middleicon-loader">  <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect"></div>
    </div>
    <div id="primary" class="wrapper cf">
        <div class="inner-detailbox no-white-bg cf">
           <div class="right-30" id="review-side">
               <div class="theiaStickySidebar">
                   <h2>Reviews should</h2>
                    <ul class="right-data">
                       <li>Be honest and truthful</li>
                       <li>Provide useful information</li>
                    </ul>
                          <h2>Reviews should not</h2>
                    <ul class="wrong-data">
                       <li>Contain vulgar language or comments</li>
                       <li>Contain derogatory language or comments</li>
                       <li>Contain specific names of people</li>
                       <li>Contain any contact information, web or email addresses</li>
                       <li>Contain excessive slang or text speak</li>
                    </ul>

                    <div class="topreview-con-box">
                       <h2>Why review?</h2>
                       <p>Writing a student review is a great way of letting others know what it is really like studying a course or what an institution is really like.</p>
                       <p>Sometimes, the course description, website or prospectus doesn't give future students everything they would like to know before joining a course.</p>
                       <p>So this is a chance for you to tell other people thinking of doing your course or studying where you studied exactly what it's like before they sign up.</p>
                   </div>
               </div>
           </div>
           <div class="review-70par-box left">
              <div class="topreview-con-box">
                   <h2>submit your review</h2>
                   <p>Writing a student review is a great way of letting others know what it is really like studying a course or what an institution is really like.</p>               
               </div>
            <?php
            // echo "<pre>";
            // print_r($checkcourseidarray);
            // echo "</pre>";
            if(is_user_logged_in()){ 
                $current_user = wp_get_current_user();
                /**
                 * @example Safe usage:
                 * $current_user = wp_get_current_user();
                 * if ( ! $current_user->exists() ) {
                 *     return;
                 * }
                 */
                  $user_login       =  $current_user->user_login; 
                  $user_email       =  $current_user->user_email; 
                  $user_firstname   =  $current_user->user_firstname; 
                  $user_lastname    =  $current_user->user_lastname;
                  $user_displayname =  $current_user->display_name; 
            }
            ?>
            <section class="wpcf7 form-design-comon paddingbox"  >
             <?php   $displayadd = 'none';
              /* if(in_array($productid,$checkproductidarray)){  
                 $displayadd = 'show';
                 $formdisplay = 'none';
              }else{
                 $displayadd = 'none';
                  $formdisplay = 'show';
              } */?>              
                 <div style="display:<?php echo $displayadd ?>" class="heigh-light produc-review-on" >  
                
                        <h2>
                        You review has been submitted and it will take 24 hours to be activated, kindly contact administrator on <br>
                        <a  href="mailto:admin@eglobaleducation.com">admin@eglobaleducation.com</a>if in-case any queries</h2>
                 </div>
               <div style="display:<?php echo $formdisplay; ?>" class="prouct-reveiwform-hide wrapper maxwidth-form cf">
                    <form  method="post" id="review-informtions-form" 
                    enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $productid; ?>" name="productid" id="productid" class="dataidthere">
                        <div class="input-name right-gorating">
                            <label> Select Overal Rating: </label> 
                            <div class="rate-star">
                                <span class="like-count overal-rating-js">
                                    <i data-rate='1' class="overal-rating fa fa-star"></i>
                                    <i data-rate='2' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='3' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='4' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='5' class="overal-rating fa fa-star-o"></i>
                                </span>
                                <div style="display:none" class="overal-rating-label">Good</div>
                            </div>
                        
                            </div>
                        <div class="input-name">
                            <label>Add review title: </label>    
                            <input  class="form-control review-title-form" type="text" name="review-title" value="<?php echo $user_phone; ?>" >
                            <input type="hidden" name="action" value="update_submit-reveiew-sections">
                            <span class="error-review review-title-error" style="display:none">This filed is required</span>
                         </div>
                        <div class="input-name">
                            <label>Write your opinion: </label> 
                            <textarea class="form-control user-opinion" id="review-opinion"  name="your-opinion"><?php echo $user_address; ?></textarea>
                        </div>

                          <div class="Submit-btn">
                              <input  class="submit-review-infomations"  type="button" value="Submit">
                        </div>
                    </form> 
                </div>
        </section>
    
            <?php
            ?>
            </div>
        </div>

  <?php 
        $recentreveiw = get_my_recent_services_review();
        if(!empty($recentreveiw)){ ?>
        <h2 style="text-align: center;">Recent Reviews</h2>
        <div class="rate-revmain-box cf">
            <div class="row cf">
               <?php 
                 foreach ($recentreveiw as $value) {
                    # code...
                   $userid          = $value['userid'];
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                  // $user            = get_user_by( 'email',$useremail);
                   //print_r($user);
                   $firstname       = $user_info->first_name;
                   $lastname        = $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $descriptions    = $value['descriptions'];
                   $title           = $value['Title'];
                   $id              = $value->id;
                   $overal_rating   = $value['overal_rating'];;
                  // echo get_avatar( $userid ); 

                   ?>
                  <div class="owl-item">
                    <div class="testmonial-block-innner">
                        <div class="profile-wrap">
                              <?php echo get_avatar( $userid ,84); ?>
                         </div>
                          <a href="<?php echo site_url().'/publicprofile/?userid='.$userid ?>"><h4><?php echo $userlogin; ?></h4></a>
                            <div class="rate-star">
                            <?php if(!empty($overal_rating) ){
                            $totalavaragereaming = 5;
                            ?>
                            <span class="like-count">
                            <?php for($i=1; $i<=$overal_rating; $i++ ){ ?>
                            <i class="fa fa-star"></i>
                            <?php } ?>
                            <?php for($i=$overal_rating; $i<$totalavaragereaming; $i++ ){ ?>
                            <i class="fa fa-star-o"></i>
                            <?php  } ?>
                            </span>
                            <?php  } ?>
                            </div>
                        <h4><?php echo $title; ?></h4>
                       <p><?php echo $descriptions; ?></p>
                  </div>
              </div>
          <?php } } ?>
          
    </div><!-- #primary -->
</section><!-- .wrap -->

<?php get_footer();?>

<script type="text/javascript">
jQuery(document).ready(function($) {

  function overalratingjs($thisclass,$mainclass){
        var rateclass = jQuery($thisclass).data( "rate" );
        var inactive  = jQuery($thisclass).hasClass( "fa-star-o" );
       // alert(rateclass);
       // var mainclass = $mainclass;
        if(inactive == true){
            //alert('work');
            for (i = 0; i <rateclass; i++) {
                jQuery($mainclass).children().eq(i).removeClass('fa-star-o')
                jQuery($mainclass).children().eq(i).addClass('fa-star')
            }
        }else{
            if(rateclass == 1){
                    jQuery(this).removeClass( "fa-star" );
                    jQuery(this).addClass( "fa-star-o" );
            }
            // jQuery(this).removeClass( "fa-star" );
            // jQuery(this).addClass( "fa-star-o" );
           // alert('ada');
            for (i = 5; i >=rateclass; i--) {
               // alert(i);
                 var ig= i;
                jQuery($mainclass).children().eq(ig).removeClass('fa-star')
                jQuery($mainclass).children().eq(ig).addClass('fa-star-o')
            }
        }
  }  

    //jQuery('.overal-rating-js').children('.fa-star').length 
  jQuery(".overal-rating").on( "hover", function(){
        var rateclass = jQuery(this).data( "rate" );
        var inactive  = jQuery(this).hasClass( "fa-star-o" );
        var mainclass = '.overal-rating-js';
        if(inactive == true){
            //alert('work');
            for (i = 0; i <rateclass; i++) {
                jQuery(mainclass).children().eq(i).removeClass('fa-star-o')
                jQuery(mainclass).children().eq(i).addClass('fa-star')
            }
        }else{
            // if(rateclass == 1){
            //     jQuery(this).removeClass( "fa-star" );
            // }
            // jQuery(this).removeClass( "fa-star" );
            // jQuery(this).addClass( "fa-star-o" );
           // alert('ada');
            for (i = 5; i >=rateclass; i--) {
                var ig= i;
                jQuery('.overal-rating-js').children().eq(ig).removeClass('fa-star')
                jQuery('.overal-rating-js').children().eq(ig).addClass('fa-star-o')
            }
        }
      });  

    $(".submit-review-infomations").on( "click", function(){
            //var editpersonalfrm = $('#personal-informtions-form').serialize();
            //var editpersonalfrm = $('#review-informtions-form').serialize();
            jQuery('.error-review').hide();
            var reviewtitle = jQuery.trim(jQuery('.review-title-form').val());
            if(reviewtitle  == ''){
                jQuery('.review-title-error').show();
                //return false;
            }
            if(reviewtitle == ''){
                return false;
            }
            var productid   = 0;
            var useropnion  = jQuery('.user-opinion').val();
            var overalrate  = jQuery('.overal-rating-js').children('i.overal-rating.fa.fa-star').length;
            jQuery('.page-template-page-product-review').addClass('loader-body');
            jQuery('.loading').show();

            $.ajax({
                    url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                    type: 'post',
                    data: {
                        'action': 'add_service_reveiw',
                        'productid' : productid,
                        'useropnion': useropnion,
                        'overalrate': overalrate,
                        'reviewtitle':reviewtitle
                    },
                    success:function(data) {
                        // This outputs the result of the ajax request
                        console.log(data);
                        if(data != 'error'){
                            jQuery('.page-template-page-product-review').removeClass('loader-body');
                            jQuery('.loading').hide();
                            jQuery('.produc-review-on').show();
                            jQuery('.prouct-reveiwform-hide').hide();
                            //window.location.href="<?php //echo site_url(); ?>/review-details/?id="+data;
                        }
                    },
                    error: function(errorThrown){
                        console.log(errorThrown);
                    }
            }); 
     });   

    $(".submit-personal-infomations").on( "click", function(){
        var editpersonalfrm = $('#review-informtions-form').serialize();
    
        //ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
        var abc ="ada";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: editpersonalfrm,
                success:function(data) {
                    // This outputs the result of the ajax request
                    console.log(data);
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
     });   




});
</script>