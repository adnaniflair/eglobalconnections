<?php

/* Template Name: Contact Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}
?>

<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="cpntact-details-page cpcontack-sec">
    <div id="primary" class="wrapper cf">
        <?php
        // check if the repeater field has rows of data
        if( have_rows('address_sections') ):
        // loop through the rows of data
        while ( have_rows('address_sections') ) : the_row();
            ?>
            <div class="inner-detailbox contact-sectionbox cf">
                <div class="left-contctbox equal-height aos-init" data-aos="fade-right" data-aos-duration="600">
                    <div class="table"><div class="table-cell">
                        <div class="cont-leftconte-box">
                            <div class="toptit-box">
                                <h4><?php echo get_sub_field('locations_name'); ?></h4>
                                <p><?php echo get_sub_field('address'); ?></p>
                            </div>
                            <div class="kontak-twoblk cf">
                                <div class="kontackblok-box">
                                     <span><?php echo get_sub_field('management_tiltle'); ?></span>
                                     <ul>
                                         <li><a href="tel:<?php echo get_sub_field('number_one'); ?>"><i class="fa fa-phone"></i><?php echo get_sub_field('number_one'); ?></a></li>
                                         <li><a href="tel:<?php echo get_sub_field('number_two'); ?>"><i class="fa fa-phone"></i> <?php echo get_sub_field('number_two'); ?></a></li>
                                         <li><a href="mailto:<?php echo get_sub_field('m_email'); ?>"><i class="fa fa-envelope"></i><?php echo get_sub_field('m_email'); ?></a></li>
                                     </ul>
                                </div>
                                <div class="kontackblok-box">
                                     <span><?php echo get_sub_field('support_title'); ?></span>
                                            <ul>
                                         <li><a href="tel:<?php echo get_sub_field('support_number_one'); ?>"><i class="fa fa-phone"></i><?php echo get_sub_field('support_number_one'); ?></a></li>
                                         <li><a href="tel:<?php echo get_sub_field('support_number_two'); ?>"><i class="fa fa-phone"></i> <?php echo get_sub_field('support_number_two'); ?></a></li>
                                         <li><a href="mailto:<?php echo get_sub_field('support_email'); ?>"><i class="fa fa-envelope"></i><?php echo get_sub_field('support_email'); ?></a></li>
                                     </ul>
                                </div>
                            </div>
                        </div>
                    </div></div>
                </div>
                <?php $bannerimage = get_sub_field('banner_image'); ?>
                <div class="rit-contbox-img equal-height aos-init" data-aos="fade-left" data-aos-duration="600" style="background-image:url('<?php echo $bannerimage; ?>')"></div>
         </div> 
            <?php

        endwhile;

        else :

        // no rows found

        endif;

        ?>         
         <div class="conact-formmap-section">
            <div class="leftcont-formbox"><?php the_content(); ?></div>
            <div class="rightcont-mapbox">
                 <div class="contact-map" style="width:100%;height:455px">
                    <!-- The element that will contain our Google Map. This is used in both the Javascript and CSS above. -->
                    <div id="map" style="width:100%;height:455px"></div>
                </div>          
            </div>
             
         </div>
          
         
    </div><!-- #primary -->

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSnC9SGy4vCyltOYSS1ZJcpc309d2dWmg&language=<?php echo $lang_temp; ?>"></script>        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 14,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(51.521730,0.017210), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [
                                {
                                    "featureType": "landscape",
                                    "stylers": [
                                        {
                                            "hue": "#FFBB00"
                                        },
                                        {
                                            "saturation": 43.400000000000006
                                        },
                                        {
                                            "lightness": 37.599999999999994
                                        },
                                        {
                                            "gamma": 1
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "stylers": [
                                        {
                                            "hue": "#FFC200"
                                        },
                                        {
                                            "saturation": -61.8
                                        },
                                        {
                                            "lightness": 45.599999999999994
                                        },
                                        {
                                            "gamma": 1
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "stylers": [
                                        {
                                            "hue": "#FF0300"
                                        },
                                        {
                                            "saturation": -100
                                        },
                                        {
                                            "lightness": 51.19999999999999
                                        },
                                        {
                                            "gamma": 1
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "stylers": [
                                        {
                                            "hue": "#FF0300"
                                        },
                                        {
                                            "saturation": -100
                                        },
                                        {
                                            "lightness": 52
                                        },
                                        {
                                            "gamma": 1
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "stylers": [
                                        {
                                            "hue": "#0078FF"
                                        },
                                        {
                                            "saturation": -13.200000000000003
                                        },
                                        {
                                            "lightness": 2.4000000000000057
                                        },
                                        {
                                            "gamma": 1
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "stylers": [
                                        {
                                            "hue": "#00FF6A"
                                        },
                                        {
                                            "saturation": -1.0989010989011234
                                        },
                                        {
                                            "lightness": 11.200000000000017
                                        },
                                        {
                                            "gamma": 1
                                        }
                                    ]
                                }
                            ]

                                            };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(51.521730, 0.017210),
                    map: map,
                    title: 'Snazzy!'
                    // icon: "<?php //echo get_stylesheet_directory_uri(); ?>/assetstwo/images/map-icon.png"
                });
            }
        </script>

   
</section><!-- .wrap -->


<?php get_footer();?>

