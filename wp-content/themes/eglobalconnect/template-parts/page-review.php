<?php

/* Template Name: My Review Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

if(!is_user_logged_in()){
    wp_redirect( site_url('login-register'));
    exit;
}

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}
?>
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-details-page">
    <div class="loading overlay-bgcolor" style="display:none">
        <div class="all-middleicon-loader">  <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect"></div>
    </div>
    <div id="primary" class="wrapper cf">
        
        <div class="inner-detailbox no-white-bg cf">
        <div class="right-30" id="review-side">
           <div class="theiaStickySidebar">
            <h2>Reviews should</h2>
            <ul class="right-data">
               <li>Be honest and truthful</li>
               <li>Provide useful information</li>
            </ul>
                  <h2>Reviews should not</h2>
            <ul class="wrong-data">
               <li>Contain vulgar language or comments</li>
               <li>Contain derogatory language or comments</li>
               <li>Contain specific names of people</li>
               <li>Contain any contact information, web or email addresses</li>
               <li>Contain excessive slang or text speak</li>
            </ul>
            
            <div class="topreview-con-box">
               <h2>Why review?</h2>
               <p>Writing a student review is a great way of letting others know what it is really like studying a course or what an institution is really like.</p>
               <p>Sometimes, the course description, website or prospectus doesn't give future students everything they would like to know before joining a course.</p>
               <p>So this is a chance for you to tell other people thinking of doing your course or studying where you studied exactly what it's like before they sign up.</p>
           </div>
            </div>
        </div>
        <div class="review-70par-box left">
           <div class="topreview-con-box">
               <h2>submit your review</h2>
               <p>Writing a student review is a great way of letting others know what it is really like studying a course or what an institution is really like.</p>               
           </div>
            <?php
            $checkcourseidarray = checkcourseidarray();
            // echo "<pre>";
            // print_r($checkcourseidarray);
            // echo "</pre>";
            if(is_user_logged_in()){ 
                $current_user = wp_get_current_user();
                /**
                 * @example Safe usage:
                 * $current_user = wp_get_current_user();
                 * if ( ! $current_user->exists() ) {
                 *     return;
                 * }
                 */
                  $user_login       =  $current_user->user_login; 
                  $user_email       =  $current_user->user_email; 
                  $user_firstname   =  $current_user->user_firstname; 
                  $user_lastname    =  $current_user->user_lastname;
                  $user_displayname =  $current_user->display_name; 
            }
            ?>
            <section class="wpcf7 form-design-comon paddingbox"  >
               <div class="wrapper maxwidth-form cf">
                    <form  method="post" id="review-informtions-form" 
                    enctype="multipart/form-data">
                        <div class="input-name">
                            <label>Institution: </label>
                            <?php
                            $universityposts = get_posts(
                            array(
                            'post_type' => 'educator', // Post type
                            'posts_per_page' => -1,
                            'orderby'       => 'menu_order',
                            'order'         => 'ASC'
                            )
                            );
                            // echo "<pre>";
                            // print_r($_REQUEST);
                            // echo "</pre>";
                            ?>
                            <?php if(!empty($_REQUEST['educatorid']) && !empty($_REQUEST['courseid'])){ 
                            $educatorid =  $_REQUEST['educatorid'];
                            $courseid =  $_REQUEST['courseid'];
                            ?>
                            <input  readonly="readonly" id='text' name="educator-nametext" value="<?php echo get_the_title($educatorid); ?>" class="form-control" >
                            <input  id="educators-drodown" type="hidden" value="<?php echo $educatorid; ?>" name="educator-id" class="form-control" >
                            <?php }else{
                                $data = ''; 
                                if(isset($_REQUEST['educatorid'])){
                                  $educatorid =  $_REQUEST['educatorid'];
                                  $data = 'disabled=disabled';
                                }  
                            ?>
                            <select <?php echo $data; ?> name="educator-name" id="educators-drodown" class="form-control chosen-serach-dropdown profile-educator-select-dropdown">

                                    <option value=" ">Select Educator</option>
                                    <?php
                                     $selected ='';
                                    if(!empty($universityposts)){
                                        foreach ($universityposts as $postvalue) {
                                            $universitysessisonid = '';
                                            // if(!empty($_SESSION['educatorid'])){
                                            //    echo  $universitysessisonid = $_SESSION['educatorid'];
                                            // }

                                           
                                            if(isset($_REQUEST['educatorid'])){
                                              $educatorid =  $_REQUEST['educatorid'];
                                            }
                                              $selected ='';
                                             if($educatorid == $postvalue->ID){
                                                $selected = 'selected= selected';
                                             }   
                                            if($universitysessisonid == $postvalue->ID){
                                                 $selected = 'selected= selected';
                                            }
                                            # code...
                                            echo '<option value="'.$postvalue->ID.'" '.$selected.'>'.$postvalue->post_title.'</option>'
                                          
                                            ?>
                                            <?php

                                        }
                                    }
                                    ?> 
                      
                                 </select>
                             <?php } ?>
                               <span class="error-review educator-review-error" style="display:none">This filed is required</span>
                         </div> 
                        <div class="input-name">
                            <label>Course: </label>
                                    <?php
                            $universityposts = get_posts(
                            array(
                            'post_type' => 'course', // Post type
                            'posts_per_page' => -1,
                            'orderby'       => 'menu_order',
                            'order'         => 'ASC'
                            )
                            );
                            ?>
                            <?php if(!empty($_REQUEST['educatorid']) && !empty($_REQUEST['courseid'])){ 
                            $educatorid =  $_REQUEST['educatorid'];
                            $courseid   =  $_REQUEST['courseid'];
                            ?>
                            <input readonly="readonly" type='hidden' name="educator-nametext" value="<?php echo get_the_title($courseid); ?>" class="form-control" >
                            <input readonly="readonly" type='text' name="educator-nametext" value="<?php echo get_the_title($courseid); ?>" class="form-control" >
                            <input type="hidden" id="course-drodown" value="<?php echo $courseid; ?>" name="educator-id" class="form-control" >
                            <?php }else{ ?>
                            <select name="course-name" id="course-drodown" class="form-control profile-course-select-dropdown  chosen-serach-dropdown">
                                    <option value=" ">Select Course</option>
                                    <?php
                                     $selected ='';
                                    if(!empty($universityposts)){
                                        foreach ($universityposts as $postvalue) {
                                            $universitysessisonid = '';
                                            // if(!empty($_SESSION['educatorid'])){
                                            //    echo  $universitysessisonid = $_SESSION['educatorid'];
                                            // }

                                            if($universitysessisonid == $postvalue->ID){
                                                 $selected = 'selected= selected';
                                            }
                                            # code...
                                            echo '<option value="'.$postvalue->ID.'" '.$selected.'>'.$postvalue->post_title.'</option>'
                                          
                                            ?>
                                            <?php

                                        }
                                    }
                                    ?> 
                                 </select>
                             <?php } ?><span class="error-review course-review-error" style="display:none">This filed is required</span>
                         </div>   
                        <div class="input-name">
                            <label>Title: </label>    
                            <input  class="form-control review-title-form" type="text" name="review-title" value="<?php echo $user_phone; ?>" >
                            <input type="hidden" name="action" value="update_submit-reveiew-sections">
                            <span class="error-review review-title-error" style="display:none">This filed is required</span>
                         </div>
                        <div class="input-name">
                            <label>Share your story: </label> 
                            <textarea class="form-control user-opinion" id="review-opinion"  name="your-opinion"><?php echo $user_address; ?></textarea>
                        </div>
                        <div class="input-name right-gorating">
                            <label> Overal Rating: </label> 
                            <div class="rate-star">
                                <span class="like-count overal-rating-js">
                                    <i data-rate='1' class="overal-rating fa fa-star"></i>
                                    <i data-rate='2' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='3' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='4' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='5' class="overal-rating fa fa-star-o"></i>
                                </span>
                                <div style="display:none" class="overal-rating-label">Good</div>
                            </div>
                        
                        </div>
                       <div class="input-name cf">
                            <label> Select Rating: </label> 
                            <div class="sub-input-name righ-ratingmainbox">
                               <div class="cf">
                                    <label> Teaching Quality: </label> 
                                    <div class="rate-star">
                                        <span class="like-count overal-teach-quality-js">
                                            <i data-rate='1' class="overal-tq-rating fa fa-star"></i>
                                            <i data-rate='2' class="overal-tq-rating fa fa-star-o"></i>
                                            <i data-rate='3' class="overal-tq-rating fa fa-star-o"></i>
                                            <i data-rate='4' class="overal-tq-rating fa fa-star-o"></i>
                                            <i data-rate='5' class="overal-tq-rating fa fa-star-o"></i>
                                        </span>
                                       <div  style="display:none" class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                                <div class="cf">
                                    <label> School Facilties: </label> 
                                    <div class="rate-star">
                                        <span class="like-count overal-school-facilties-js">
                                            <i data-rate='1' class="overal-sf-rating fa fa-star"></i>
                                            <i data-rate='2' class="overal-sf-rating fa fa-star-o"></i>
                                            <i data-rate='3' class="overal-sf-rating fa fa-star-o"></i>
                                            <i data-rate='4' class="overal-sf-rating fa fa-star-o"></i>
                                            <i data-rate='5' class="overal-sf-rating fa fa-star-o"></i>
                                        </span>
                                        <div style="display:none" class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                                <div class="cf">
                                    <label> Locations: </label> 
                                    <div class="rate-star">
                                            <span class="like-count overal-locations-js">
                                                <i data-rate='1' class="overal-ol-rating fa fa-star"></i>
                                                <i data-rate='2' class="overal-ol-rating fa fa-star-o"></i>
                                                <i data-rate='3' class="overal-ol-rating fa fa-star-o"></i>
                                                <i data-rate='4' class="overal-ol-rating fa fa-star-o"></i>
                                                <i data-rate='5' class="overal-ol-rating fa fa-star-o"></i>
                                            </span>
                                            <div style="display:none" class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                                <div class="cf">
                                    <label> Value For Money : </label> 
                                    <div class="rate-star">
                                            <span class="like-count overal-value-money-js">
                                                <i data-rate='1' class="overal-vm-rating fa fa-star"></i>
                                                <i data-rate='2' class="overal-vm-rating fa fa-star-o"></i>
                                                <i data-rate='3' class="overal-vm-rating fa fa-star-o"></i>
                                                <i data-rate='4' class="overal-vm-rating fa fa-star-o"></i>
                                                <i data-rate='5' class="overal-vm-rating fa fa-star-o"></i>
                                            </span>
                                                  <div style="display:none" class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                                <div class="cf">
                                    <label> Career Assistance: </label> 
                                    <div class="rate-star">
                                            <span class="like-count overal-career-js">
                                                <i data-rate='1' class="overal-ca-rating fa fa-star"></i>
                                                <i data-rate='2' class="overal-ca-rating fa fa-star-o"></i>
                                                <i data-rate='3' class="overal-ca-rating fa fa-star-o"></i>
                                                <i data-rate='4' class="overal-ca-rating fa fa-star-o"></i>
                                                <i data-rate='5' class="overal-ca-rating fa fa-star-o"></i>
                                            </span>
                                            <div style="display:none" class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-name">
                            <label>Photo:</label>
                            <input class="form-control" type="file" id="review_images" name="files[]" multiple="multiple"  accept=".png, .jpg, .jpeg"/>
                         </div>
                          <div class="input-name">
                            <label>Add Video:</label>
                            <input class="form-control" type="file" id="review_video" name="review-video[]" accept=".mp4" multiple="multiple" >
                          </div>
                          <div class="Submit-btn">
                              <input  class="submit-review-infomations"  type="button" value="Submit">
                        </div>
                    </form> 
                </div>
        </section>
        </div> 
        
    
            <?php
            ?>
        </div>      
    </div><!-- #primary -->
</section><!-- .wrap -->

<?php get_footer();?>

<script type="text/javascript">
jQuery(document).ready(function($) {
    onloadfunctions();
  function onloadfunctions(){
    <?php    if(isset($_REQUEST['educatorid'])){ ?>
    $educatorsdrodown    =  jQuery.trim(jQuery('.profile-educator-select-dropdown').val());
    //alert($educatorsdrodown);
    $.ajax({
    url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
    method:'Post',
    data: {
    'action': 'course_listing_drodpown',
    'educatordropdown':$educatorsdrodown
    },
    success:function(data) {
    // This outputs the result of the ajax request
    jQuery('.profile-course-select-dropdown').html(data);

    },
    error: function(errorThrown){
     console.log(errorThrown);
    }

    });
   <?php } ?>
  }  
  function overalratingjs($thisclass,$mainclass){
        var rateclass = jQuery($thisclass).data( "rate" );
        var inactive  = jQuery($thisclass).hasClass( "fa-star-o" );
       // alert(rateclass);
       // var mainclass = $mainclass;
        if(inactive == true){
            //alert('work');
            for (i = 0; i <rateclass; i++) {
                jQuery($mainclass).children().eq(i).removeClass('fa-star-o')
                jQuery($mainclass).children().eq(i).addClass('fa-star')
            }
        }else{
            if(rateclass == 1){
                    jQuery(this).removeClass( "fa-star" );
                    jQuery(this).addClass( "fa-star-o" );
            }
            // jQuery(this).removeClass( "fa-star" );
            // jQuery(this).addClass( "fa-star-o" );
           // alert('ada');
            for (i = 5; i >=rateclass; i--) {
               // alert(i);
                 var ig= i;
                jQuery($mainclass).children().eq(ig).removeClass('fa-star')
                jQuery($mainclass).children().eq(ig).addClass('fa-star-o')
            }
        }
  }  

    //jQuery('.overal-rating-js').children('.fa-star').length 
  jQuery(".overal-rating").on( "hover", function(){
        var rateclass = jQuery(this).data( "rate" );
        var inactive  = jQuery(this).hasClass( "fa-star-o" );
        var mainclass = '.overal-rating-js';
        if(inactive == true){
            //alert('work');
            for (i = 0; i <rateclass; i++) {
                jQuery(mainclass).children().eq(i).removeClass('fa-star-o')
                jQuery(mainclass).children().eq(i).addClass('fa-star')
            }
        }else{
            // if(rateclass == 1){
            //     jQuery(this).removeClass( "fa-star" );
            // }
            // jQuery(this).removeClass( "fa-star" );
            // jQuery(this).addClass( "fa-star-o" );
           // alert('ada');
            for (i = 5; i >=rateclass; i--) {
                var ig= i;
                jQuery('.overal-rating-js').children().eq(ig).removeClass('fa-star')
                jQuery('.overal-rating-js').children().eq(ig).addClass('fa-star-o')
            }
        }
      });  
        // jQuery(".overal-rating").on( "click", function(){
        //     var mainclass = '.overal-rating-js';
        //     $thisclass    = jQuery(this);
        //     overalratingjs($thisclass,mainclass);
        // }); 
      jQuery(".overal-tq-rating").on( "hover", function(){
           var mainclass = '.overal-teach-quality-js';
            $thisclass    = jQuery(this);
            overalratingjs($thisclass,mainclass);
        //alert(rateclass);
    });
    jQuery(".overal-sf-rating").on( "hover", function(){
        var mainclass = '.overal-school-facilties-js';
        $thisclass    = jQuery(this);
        overalratingjs($thisclass,mainclass);
    //alert(rateclass);
    });
    jQuery(".overal-ol-rating").on( "hover", function(){
        var mainclass = '.overal-locations-js';
        $thisclass    = jQuery(this);
        overalratingjs($thisclass,mainclass);
    //alert(rateclass);
    });
    jQuery(".overal-ca-rating").on( "hover", function(){
        var mainclass = '.overal-career-js';
        $thisclass    = jQuery(this);
        overalratingjs($thisclass,mainclass);
    //alert(rateclass);
    });
    jQuery(".overal-vm-rating").on( "hover", function(){
        var mainclass = '.overal-value-money-js';
        $thisclass    = jQuery(this);
        overalratingjs($thisclass,mainclass);
    //alert(rateclass);
    });  

    $(".submit-review-infomations").on( "click", function(){
            //var editpersonalfrm = $('#personal-informtions-form').serialize();
            //var editpersonalfrm = $('#review-informtions-form').serialize();
            jQuery('.error-review').hide();
            var reviewtitle = jQuery.trim(jQuery('.review-title-form').val());
            if(reviewtitle  == ''){
                jQuery('.review-title-error').show();
                //return false;
            }
            var educatorid  = jQuery.trim(jQuery('#educators-drodown').val());
           // alert(educatorid);
            if(educatorid  == ''){
                jQuery('.educator-review-error').show();
            }
            var courseid    = jQuery.trim(jQuery('#course-drodown').val());
            if(courseid  == ''){
                jQuery('.course-review-error').show();
            }
            //alert(courseid);
            if(reviewtitle == '' || educatorid=='' || courseid == '' ){
                return false;
            }
            var courseid    = jQuery('#course-drodown').val();
            var useropnion  = jQuery('.user-opinion').val();
            var overalrate  = jQuery('.overal-rating-js').children('i.overal-rating.fa.fa-star').length;
            var tqrate     = jQuery('.overal-teach-quality-js').children('i.overal-tq-rating.fa.fa-star').length;
            var sfrate     = jQuery('.overal-school-facilties-js').children('i.overal-sf-rating.fa.fa-star').length;
            var lqrate     = jQuery('.overal-locations-js').children('i.overal-ol-rating.fa.fa-star').length;
            var vmoney     = jQuery('.overal-value-money-js').children('i.overal-vm-rating.fa.fa-star').length;        
            var ovcareer   = jQuery('.overal-career-js').children('i.overal-ca-rating.fa.fa-star').length;
            var form_data = new FormData();
            form_data.append('action', 'update_submit-reveiew-sections');
            var fileList = jQuery('#review_images').get(0).files;
            var files_data1 = [];
            var count = fileList.length;
            for( var x=0;x<fileList.length;x++ ){
                form_data.append( 'gallery_image_'+x,fileList.item(x) );
            }
            var fileListvideo = jQuery('#review_video').get(0).files;
            var files_data_video = [];
            var countvideo = fileListvideo.length;
            for( var x=0;x<fileListvideo.length;x++ ){
                form_data.append( 'gallery_video_'+x,fileListvideo.item(x) );
            }
            form_data.append('educatorid', educatorid);
            form_data.append('courseid', courseid);
            form_data.append('reviewtitle',reviewtitle);
            form_data.append('useropnion', useropnion);
            form_data.append('overalrate', overalrate);
            form_data.append('tqrate', tqrate);
            form_data.append('sfrate', sfrate);
            form_data.append('lqrate', lqrate);
            form_data.append('vmoney', vmoney);
            form_data.append('ovcareer', ovcareer);
            form_data.append('totalimages', count);
            form_data.append('totalvideo', countvideo);
            console.log(form_data);
            jQuery('.page-template-page-review').addClass('loader-body');
            jQuery('.tax-subject').addClass('loader-body');
            jQuery('.loading').show();
            //var abc ="ada";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                type: 'post',
                contentType: false,
                processData: false,
                data: form_data,
                success:function(data) {
                    // This outputs the result of the ajax request
                    console.log(data);
                    if(data != 'error'){
                        jQuery('.page-template-page-review').removeClass('loader-body');
                        jQuery('.tax-subject').removeClass('loader-body');
                        jQuery('.loading').hide();
                        window.location.href="<?php echo site_url(); ?>/review-details/?id="+data;
                    }
                
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
     });   

    $(".submit-personal-infomations").on( "click", function(){
        var editpersonalfrm = $('#review-informtions-form').serialize();
    
        //ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
        var abc ="ada";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: editpersonalfrm,
                success:function(data) {
                    // This outputs the result of the ajax request
                    console.log(data);
                
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
     });   

       jQuery(".profile-educator-select-dropdown").live("change", function(){
            $educatorsdrodown    =  jQuery.trim(jQuery('.profile-educator-select-dropdown').val());
            //alert($educatorsdrodown);
            if($educatorsdrodown !=''){
                jQuery('.profile-course-select-dropdown').removeAttr('disabled')
            }else{
                jQuery('.profile-course-select-dropdown').attr('disabled','disabled');
                jQuery('.profile-course-select-dropdown').html('');
                return false;
            }
            ajaxurl             =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                'action': 'course_listing_drodpown',
                'educatordropdown':$educatorsdrodown
            },
            success:function(data) {
            // This outputs the result of the ajax request
                    jQuery('.profile-course-select-dropdown').html(data);

            },
            error: function(errorThrown){
                    console.log(errorThrown);
            }
        });
    });
    
    jQuery(document).ready(function() {
			jQuery('#review-side')
			.theiaStickySidebar({
				additionalMarginTop: 100
			});
		});


});
</script>