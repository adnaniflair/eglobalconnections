<?php /* Template Name: Public Profile Page Template */ 

if(!isset($_REQUEST['userid'])){
    wp_redirect( site_url('404'));
    exit;
}

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}
//User Manament Details :
if(!is_user_logged_in() || is_user_logged_in()){ 
    $current_user = get_userdata($_REQUEST['userid']);
    /**
     * @example Safe usage:
     * $current_user = wp_get_current_user();
     * if ( ! $current_user->exists() ) {
     *     return;
     * }
     */
      // echo "<pre>";
      // print_r($current_user);
      // echo "</pre>";

      $user_id          =  $current_user->ID; 
      $user_login       =  $current_user->user_login; 
      $user_email       =  $current_user->user_email; 
      $user_firstname   =  $current_user->user_firstname; 
      $user_lastname    =  $current_user->user_lastname;
      $user_displayname =  $current_user->display_name; 
      
      $user_gender      =  get_user_meta( $user_id, 'gender',true);
      $user_town_or_city=  get_user_meta( $user_id, 'town_city',true);
      $user_address     =  get_user_meta( $user_id, 'address',true);
      $user_phone       =  get_user_meta( $user_id, 'phone',true);
      $user_zipcode     =  get_user_meta( $user_id,'zipcode',true);
      $user_country     =  get_user_meta( $user_id,'country',true);
      $user_dateofbirth =  get_user_meta( $user_id,'dateofbirth',true);
      if(!empty($user_dateofbirth)){
         $user_dateofbirth = date("d-m-Y", strtotime($user_dateofbirth));
      }
    // $metas = array( 
    //     'facebookurls'  => $facebookurls, 
    //     'twitterurls'   => $twitterurls ,
    //     'linkidurl'     => $linkidurl ,
    //     'googleurls'    => $googleurls ,
    // );

    $user_facebookurl       =  get_user_meta( $user_id, 'facebookurls',true);
    $use_twitterurl         =  get_user_meta( $user_id, 'twitterurls',true);
    $use_linkdinurl         =  get_user_meta( $user_id, 'linkidinurls',true);
    $use_googleurl          =  get_user_meta( $user_id, 'googleurls',true);
   
    //print_r($user_displayname);
    //get_user_meta( $user_id, $key, $value );

    //     'first_name' => $firstname, 
    // 'last_name'  => $lastname ,
    // 'town_city'  => $towncity ,
    // 'gender'     => $gener,
    // 'address'     => $address,
    // 'zipcode'     => $zipcode,
    // 'country'     => $country,
}
global $wpdb;
$userid = $_REQUEST['userid'];
$curpreveducations       = $wpdb->get_results( "SELECT * FROM `ege_prv_cur_educ_users`  WHERE `futurestatus` = 0 and `userid` = '$userid' ORDER BY `ege_prv_cur_educ_users`.`syear` DESC " );

$futpreveducations       = $wpdb->get_results( "SELECT * FROM `ege_prv_cur_educ_users`  WHERE `futurestatus` = 1 and `userid` = '$userid' ORDER BY `ege_prv_cur_educ_users`.`syear` DESC" );
?>
    <section class="inner-bannerbox topgreen-border profile-diff-bg">
        <div class="wrapper">
            <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">
                    <ul class="cf">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">Profile</a></li>
                        <li><label><?php echo $user_firstname.' '.$user_lastname; ?></label></li>
                    </ul>
                </div>
                <div class="innerbanner-block profi-topbox cf">
                    <div class="inrbanner-leftimg">
                        <?php echo get_avatar($userid ,100); ?>
                    </div>
                    <div class="inrbaner-rightcont">
                        <div class="baner-titbox">
                            <h3><?php echo $user_firstname.' '.$user_lastname; ?></h3>
                        </div>
                        <div  style="display:none" class="profile-ratingmain-box" style="display:none">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/profile-ratingbox.png" alt="" />
                        </div>
                        <div class="country-locationbox">
                            <ul>
                                <li>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/country-flag-icon.png" alt="" />
                                    <?php if(!empty($user_town_or_city)){ ?>
                                    <span><?php echo ucfirst($user_town_or_city).','.$user_country; ?></span>
                                    <?php } ?>
                                </li>
                                <li>
                                   <?php
                                    if(!empty($curpreveducations)){
                                      
                                    foreach ($curpreveducations as $key) { 
                                        $educatorid     =  $key->eductorid;
                                        $courseid       =  $key->courseid; 
                                        $coursetitle    =  $key->course_name; 
                                        $educatortitle  =  $key->educator_name; 
                                        $presentstatus  = $key->presentstatus;
                                       
                                        if(empty($educatortitle)){
                                        $educatortitle  = get_the_title($educatorid);
                                        }
                                        if(empty($coursetitle)){
                                        $coursetitle    = get_the_title($courseid);
                                        }
                                        if($presentstatus){
                                            $hlable = 'Current';
                                        }else{
                                            $hlable ='Previous';
                                        }
                                        ?>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/current-icon.png" alt="" />    
                                    <span><?php echo $hlable;  ?>: <?php echo $educatortitle.'('.$coursetitle.')'; ?></span>

                                    <?php } ?>
                                <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="innerpage-menu-box greenbgcolor">
        <div class="wrapper cf">
            
            <div class="enquiry-rightbox">
          
                <span class="enqu-lang-dropdown" style="display:none">
                    <select class="chosen-select">
                        <option>Another Language</option>
                        <option>Another Language1</option>
                        <option>Another Language2</option>
                    </select>
                </span>
            </div>
            <div class="sticky-imgtit">
                <div class="stikimg-small imgroundedbox">
                        <?php echo get_avatar($userid ,60); ?>
                </div>
                <div class="sticky-tibox">
                   <div class="table">
                       <div class="table-cell">
                            <h4>Hello!,<?php echo $user_firstname; ?>
                       </div>
                    </div>
                </div>
            </div>
            <div class="iner-left-menubox" data-aos="fade-right" data-aos-duration="1200">
                <a href="javascript:void(0);" id="innermenu-click" class="innermenu-linkbox">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <ul class="cf in-responsive">
                    <li><a href="#profile-id">Personal Information</a></li>
                    <li><a href="#education-id">education</a></li>
                    <li><a href="#review-id">Reviews</a></li>

                </ul>
            </div>
        </div>
    </section>

    <section class="profile-main-box" id="profile-id">
        <div class="wrapper cf">
            <div class="rows cf"  data-aos="fade-left" data-aos-duration="1500">
                <div class="two-blocks personal-box">
                    <div class="paddingbox shadowbox equal-height">
                        <div class="protile-tit-withbtn">
                            <h3>Personal Information</h3>
                            <div style="display:none" class="enquiry-rightbox"><a href="javascript:void(0);" class="btn enquiry-btn openpersonalinfo">Edit</a></div>
                        </div>
                <!-- Example Html For Profile Page -->
                 <div class="wpcf7 form-design-comon" style="display:none">
                    <form  method="post" id="social-informtions-form-two">
                       <input type="hidden" name="action" value="update_socialmedia_informations" />
                        <?php if(!empty($user_facebookurl)){?>
                        <div class="input-name">
                            <label>Face Book Url: </label>    
                            <input type="text"  class="form-control"  name="facebook-urls" value="<?php echo $user_facebookurl; ?>" >
                         </div>   
                         <?php } ?>
                         <?php if(!empty($twitterurls)){?>
                        <div class="input-name">
                            <label>Twitter Url : </label>    
                            <input type="text" name="twitter-urls" value="<?php echo $twitterurls; ?>"  class="form-control">      
                        </div>
                        <?php } ?>
                         <?php if(!empty($user_linkedinurl)){?>
                        <div class="input-name">
                            <label>Linkedin Url: </label>    
                            <input type="text"  class="form-control" name="linkiedin-url" value="<?php echo $user_linkedinurl; ?>" >
                         </div>
                         <?php } ?>
                       <?php if(!empty($user_googleurl)){?>
                        <div class="input-name">
                            <label>Google url: </label>    
                            <input type="text"  class="form-control" name="google-urls" value="<?php echo $user_googleurl; ?>" >
                        </div> 
                        <?php } ?>   
                        <div class="Submit-btn">
                          <input  class="submit-socialmedia-infomations"  type="button" value="Submit">
                        </div>
                    </form> 
                   </div> 
                        <div class="personal-infobox">
                            <div class="pers-blkbox cf">
                                <label>First name :</label>
                                <span><?php echo $user_firstname; ?></span>
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Last name :</label>
                                <span><?php echo $user_lastname; ?></span>
                            </div>
                            <?php if(!empty( $user_dateofbirth )){ ?>
                            <div class="pers-blkbox cf">
                                <label>Date of Birth : </label>
                                <span><?php echo $user_dateofbirth;  ?></span>
                            </div>
                             <?php } ?>
                         <?php if(!empty($user_gender)){ ?>
                            <div class="pers-blkbox cf">
                                <label>Gender :</label>
                                <span><?php echo $user_gender; ?></span>
                            </div>
                        <?php } ?>
                         <?php if(!empty($user_email)){ ?>
                            <div class="pers-blkbox cf">
                                <label>E-mail :</label>
                                <span><?php echo $user_email;   ?></span>
                            </div>
                        <?php } ?>
                     <?php if(!empty($user_phone)){ ?>
                            <div class="pers-blkbox cf">
                                <label>Phone :</label>
                                <span><?php echo $user_phone; ?></span>
                            </div>
                    <?php } ?>
                            <?php if(!empty($user_address)){ ?>
                            <div class="pers-blkbox cf">
                                <label>Address :</label>
                                <span><?php echo $user_address; ?></span>
                            </div>
                            <?php } ?>
                         <?php if(!empty($user_zipcode)){ ?>
                            <div class="pers-blkbox cf">
                                    <label>ZIP code :</label>
                                    <span><?php echo $user_zipcode; ?></span>
                            </div>
                        <?php } ?> 
                            <?php if(!empty($user_town_or_city)){ ?>
                            <div class="pers-blkbox cf">
                                <label>Town/City :</label>
                                <span><?php echo $user_town_or_city; ?></span>
                            </div>
                           <?php } ?>   
                          <?php if(!empty($user_country)){ ?>
                            <div class="pers-blkbox cf">
                                <label>Country :</label>
                                 <span class="informations-name"><?php echo $user_country; ?></span>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="two-blocks personal-box">
                    <div class="paddingbox shadowbox equal-height">
                        <div class="protile-tit-withbtn">
                            <h3>Social links</h3>
                        </div>
                        <div class="personal-infobox personal-social-link">
                            <div class="pers-blkbox cf">
                                <label><i class="fa fa-facebook"></i>Facebook :</label>
                                <?php if(empty($user_facebookurl)) {?>
                                <?php }else{ ?>
                                <span><a href="<?php echo $user_facebookurl; ?>"><?php echo $user_facebookurl; ?></a></span>
                                <?php } ?>
                            </div>
                            <div class="pers-blkbox cf">
                                <?php if(empty($use_twitterurl)) {?>
                                <?php }else{ ?>
                                <label><i class="fa fa-twitter"></i>Twitter :</label>
                                <span><a href="<?php echo $use_twitterurl; ?>"><?php echo $use_twitterurl; ?></a></span>
                                <?php } ?>
                            </div>
                            <div class="pers-blkbox cf">
                                <label><i class="fa fa-linkedin"></i>Linkedin :</label>
                                <?php if(empty($use_linkdinurl)) {?>
                                <?php }else{ ?>
                                <span><a href="<?php echo $use_linkdinurl; ?>"><?php echo $use_linkdinurl; ?></a></span>
                                <?php } ?>
                            </div>
                            <div class="pers-blkbox cf">
                                <label><i class="fa fa-google-plus"></i>Google+ :</label>
                                <?php if(empty($use_googleurl)) {?>
                                <?php }else{ ?>
                                <span><a href="<?php echo $use_googleurl; ?>"><?php echo $use_googleurl; ?></a></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="course-section education-blok-section border-bottom" id="education-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox">
                <h2>Education</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="content-mainbox">
                <div class="education-blocks cf">
                    <div class="educat-titbox">
                        <h3>Previous and Current Education</h3>
                    </div>
                   <?php
                    if(!empty($curpreveducations)){
                        foreach ($curpreveducations as $key) {
                                 $formid         =  $key->id;
                                $educatorid     =  $key->eductorid;
                                $courseid       =  $key->courseid;
                                $educatoridother = '';
                                if($educatorid == 1){
                                    $educatoridother = $key->educator_name;
                                }
                                $courseididother = '';
                                if($courseid == 1){
                                    $courseididother = $key->course_name;
                                }
                                $Descriptions   =  $key->Descriptions;
                                $smonth         =  $key->smonth;
                                $syear          =  $key->syear;
                                $emonth         =  $key->emonth;
                                $eyear          =  $key->eyear;
                                $bannerimage    =  get_the_post_thumbnail_url($educatorid,'bannerimage-educators-list');
                                if(empty($bannerimage)){
                                 $bannerimage  = get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                                }
                                $presentstatus  =  $key->presentstatus;
                                if($presentstatus){
                                    $pndetails =  $syear.'- present';
                                }else{
                                    $pndetails =  $syear.'-'.$eyear;
                                }
                                //$eyear          =  $key->futurestatus;
                            # code...
                                $educator_title = get_the_title($educatorid);
                                if(!empty($educatoridother)){
                                    $educator_title = $educatoridother;
                                }
                                $course_title = get_the_title($educatorid);
                                if(!empty($educatoridother)){
                                    $course_title = $courseididother;
                                }
                                //$eyear          =  $key->futurestatus;
                            # code...
                            ?>

                        <div id="currentloop<?php echo $formid; ?>" class="education-blockbox cf">
                                <div class="educat-leftimg"><img src="<?php echo $bannerimage; ?>" alt=""></div>
                                <div class="educat-rightcont">
                                    <div class="educat-tit">
                                        <h4><?php echo get_the_title($educatorid); ?></h4><span>(<?php echo get_the_title($courseid); ?>)</span>
                                    </div>
                                    <p> <?php echo  $Descriptions;  ?></p>
                                    <div class="educat-twobutton">
                                        <a href="#" class="present-btn"><?php echo $pndetails; ?></a>
                                    </div>
                                </div>
                            </div>

                            <?php
 
                        }

                    }

                    ?>
                 

                </div>
            </div>
        </div>
    </section>

    <section class="course-section education-blok-section border-bottom">
        <div class="wrapper cf"  data-aos="fade-left" data-aos-duration="1500">
            <div class="content-mainbox">
                <div class="education-blocks cf">
                    <div class="educat-titbox">
                        <h3>Future Education</h3>
                    </div>
                                  <?php
                    if(!empty($futpreveducations )){
                        foreach ($futpreveducations   as $key) {
                                $formid         =  $key->id;
                                $educatorid     =  $key->eductorid;
                                $courseid       =  $key->courseid;
                                $Descriptions   =  $key->Descriptions;
                                $smonth         =  $key->smonth;
                                $syear          =  $key->syear;
                                $emonth         =  $key->emonth;
                                $futurestatus   =  $key->futurestatus;
                                $eyear          =  $key->eyear;
                                $bannerimage    =  get_the_post_thumbnail_url($educatorid,'bannerimage-educators-list');
                                if(empty($bannerimage)){
                                 $bannerimage  = get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                                }
                                $futurestatus  =  $key->futurestatus;
                                if($presentstatus){
                                    $pndetails =  $syear.'- present';
                                }else{
                                    $pndetails =  $syear;
                                }
                                if($notdecidedstatus){
                                    $pndetails = 'Not Decided Yet';
                                }
                                //$eyear          =  $key->futurestatus;
                            # code...
                            ?>

                        <div id="currentloop<?php echo $formid; ?>" class="education-blockbox cf">
                                <div class="educat-leftimg"><img src="<?php echo $bannerimage; ?>" alt=""></div>
                                <div class="educat-rightcont">
                                    <div class="educat-tit">
                                        <h4><?php echo get_the_title($educatorid); ?></h4><span>(<?php echo get_the_title($courseid); ?>)</span>
                                    </div>
                                    <p> <?php echo  $Descriptions;  ?></p>
                                    <div class="educat-twobutton">
                                        <a href="#" class="present-btn"><?php echo $pndetails; ?></a>
                                    </div>
                                </div>
                            </div>

                            <?php
 
                        }

                    }

                    ?>
                </div>
            </div>
        </div>
    </section>

    <?php
    $activeid = 1;
    $myreveiw = get_my_reveiw($userid,$activeid);
    if(!empty($myreveiw)){ ?>
    <section class="course-section whitebg" id="review-id">
        <div class="wrapper cf"  data-aos="fade-left" data-aos-duration="1500">
            <div class="top-title-contbox">

                <h2>My reviews</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
                    <?php
      
                        foreach ($myreveiw as $value) {
                            # code...
                             $id              =  $value['id'];
                             $educatorid      =  $value['educators_id'];
                             $courseid        =  $value['course_id'];
                             $datetetime      =  $value['create_datetime'];
                             $create_datetime = date("d F Y", strtotime($datetetime)); 

                             $title          = $value['Title'];
                             $overalrating   = $value['overal_rating'];
                                $overalratingarg = array(
                                'rating' =>  $overalrating,
                                'type' => 'rating',
                                'number' => 5,
                                ); 

                            $bannerimages        = get_the_post_thumbnail_url($courseid ,'bannerimage-courses-list');
                            if(empty($bannerimages)){
                                $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/course-block-img1.png';
                            } 
                            ?>

                    <div class="three-block">
                        <div class="booking-blok">
                            <div class="booking-imgblk cf" style="display:none">
                                <img src="<?php echo $bannerimages; ?>" alt="">
                                <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt="">
                                    <p>Course Review</p>
                                </span>
                            </div>
                            <div class="booking-desbox equal-height">
                                <h3><?php echo get_the_title( $educatorid )  ?></h3>
                                <div class="small-headingbox"><?php echo get_the_title( $courseid )  ?></div>
                                <div class="bookreview-box cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star cf">
                                             <?php wp_star_rating($overalratingarg); ?>
                                            <label class="<?php echo egobal_get_the_review_label_color($overalrating) ?> "><?php  echo egobal_get_the_review_label_two($overalrating); ?></label>
                                        </div>
                                        <div class="review-texts fullblock">
                                            <span>Reviewed  <?php echo $create_datetime; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="policy-box bold-text">
                                    <span>"<?php echo $title; ?>"</span>
                                </div>
                                <div class="boking-btnlink review-btnlinks">
                                    <div class="reviewlinks"><a href="<?php echo site_url().'/review-details/?id='.$id .'#go-aboutid' ?>">About</a></div>
                                    <div class="ratinglinks"><a href="<?php echo site_url().'/review-details/?id='.$id .'#go-rating' ?>">Rating</a></div>
                                    <div class="photoslinks"><a href="<?php echo site_url().'/review-details/?id='.$id .'#go-imgid' ?>">Photos</a></div>
                                    <div class="videwoslinks"><a href="<?php echo site_url().'/review-details/?id='.$id .'#go-videoid' ?>">Video</a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                            <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php } 
    get_footer(); ?>

