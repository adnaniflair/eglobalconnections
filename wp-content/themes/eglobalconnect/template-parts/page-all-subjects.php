<?php
/* Template Name: All Subjects Template */

get_header(); 
wp_reset_postdata();
?>
<section class="inner-bannerbox all-subjectbox">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="blog-details-page">
    <div id="primary" class="wrapper cf">
        <div class="inner-detailbox">
                <?php the_content(); ?>
        </div>      
    </div><!-- #primary -->
</section><!-- .wrap -->
<section class="all-subject-list" style="display:none" >
        <?php
        $cat_args = array(
        'orderby'       => 'term_id', 
        'order'         => 'ASC',
        'parent'        => 0,
        'hide_empty'    => true,
        'child_of' => 0, 
        );
        //echo "CHidren";
        $subjectterms   = get_terms('subject',$cat_args);
        // echo "<pre>";
        // print_r($subjectterms);
        // echo "</pre>";
        //$count = 0;
        if(!empty($subjectterms)){
            foreach ($subjectterms as $key => $value) {
                //$count++;   
                # code...
               $subjectname = $value->name;
               $subjectid       = $value->term_id;
               
                // $term_id = get_queried_object_id(); 
                // $taxonomy_name = 'subject';

               $description     = $value->description;
               $subjectimage    =  get_field('subject_image','subject_'.$subjectid);
               $childerenterm  = get_term_children( $subjectid, 'subject' );
                // echo "<pre>";
                // print_r($childerenterm);
                // echo "</pre>";
               ?>
                <h2><?php echo $subjectname; ?></h2>
                <div class="description">
                   <p> <?php echo $description; ?></p>
                </div>
                <?php

                if(!empty($subjectimage)){ ?>
                <img src="<?php echo $subjectimage; ?>">
                <?php }else{ ?>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/agriculture.png">
                <a class="view-job" href="<?php echo get_term_link($subjectid,'subject'); ?>" title="view all">view all</a>
                <?php } 

                if(!empty($childerenterm )){
                    ?>
                    <div class="childerenterm">
                    <?php
                    foreach ($childerenterm as $value) {
                        # code...
                        $term = get_term_by( 'id', $value, 'subject');
                        $sub_subjectid = $term ->term_id;
                        $termname =  $term->name;
                        ?>
                      <a class="view-job" href="<?php echo get_term_link($sub_subjectid,'subject'); ?>" title="view all"><?php echo $termname;  ?></a>

                        <?php
                    }
                    ?>
                    </div>
                    <?php
                }

            }
        }   
        // echo "<pre>";
        // print_r($subjectterms);
        // echo "</pre>";   
        ?>
</section>

<section class="all-subject-section">
    <div class="wrapper cf">
        <?php
         if(!empty($subjectterms)){
            foreach ($subjectterms as $key => $value) {
                //$count++;   
                # code...
               $subjectname         = $value->name;
               $subjectid           = $value->term_id;
               $description         = $value->description;
               $subjectimage        =  get_field('subject_image','subject_'.$subjectid);
               $subjecticonimage    =  get_field('subject_icon','subject_'.$subjectid);
               if(empty($subjecticonimage)){
                $subjecticonimage   = get_stylesheet_directory_uri() .'/assettwo/images/agriculture-icon.png';
               }
               if(empty($subjectimage)){
                $subjectimage       = get_stylesheet_directory_uri().'/assettwo/images/all-subjects-img1.jpg';
               }

                $term_children = get_terms(
                'subject',
                array(
                'parent' =>$subjectid,
                )
                );
              //  print_r($term_children);
      

               ?>
            <div class="overhid-box cf">
                       <div class="all-subje-blocks cf">
                       <div class="zooming-imgbox" style="background-image:url(<?php echo $subjectimage; ?>" ></div>
            <div class="table">
                <div class="table-cell">
                    <div class="allsub-midle-contbox cf">
                        <div class="allsub-left-content">
                            <div class="all-toptit cf">
                                <span><img src="<?php echo $subjecticonimage; ?>" alt=""></span>
                                     <h4 data-aos="fade-left" data-aos-duration="600" class="aos-init aos-animate"><?php echo $subjectname;  ?></h4>
                            </div>
                            <div class="allsub-contbox">
                                      <p data-aos="fade-left" data-aos-duration="600" class="aos-init aos-animate"><?php echo substr($description,'0','210').'..'; ?> </p>
                            </div>
                            <div class="allsub-viewmor-link">
                                  <a href="<?php echo get_term_link($subjectid,'subject'); ?>" title="view all">View More</a>
                            </div>
                            
                            
                        </div>
                        <div class="allsubjet-rigt-links">
                            <ul data-aos="fade-left" data-aos-duration="600" class="aos-init aos-animate">
                            <?php
                                $childerenterm  = get_term_children( $subjectid, 'subject' );
                
                                if(!empty($term_children )){
                                ?>
                              
                                <?php
                                foreach ($term_children as $key => $value) {
                                # code...

                                    $term_id  =  $value->term_id;
                                    $termname =  $value->name;
                                    $termlink = get_term_link($term_id,'subject'); ?>
                                        <li> <a  href="<?php echo $termlink; ?>" title="<?php echo $termname;  ?>"><?php echo $termname;  ?></a></li>
                                        <?php
                                    }
                                    /*
                                    foreach ($childerenterm as $value) {
                                    # code...
                                    $term = get_term_by( 'id', $value, 'subject');
                                    $sub_subjectid = $term ->term_id;
                                    $termname =  $term->name;
                                    ?>
                                     <li> <a  href="<?php echo get_term_link($sub_subjectid,'subject'); ?>" title="<?php echo $termname;  ?>"><?php echo $termname;  ?></a></li>

                                    <?php
                                    } */
                                    ?>
                                    <?php
                                }
                            ?>
                            </ul>
    
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        </div>
        
               <?php
              } 
           }   

        ?>
        <div class="overhid-box cf" style="display:none;">
            <div  class="all-subje-blocks cf" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/all-subjects-img1.jpg)">
                <div class="table">
                    <div class="table-cell">
                        <div class="allsub-midle-contbox cf">
                            <div class="allsub-left-content">
                                <div class="all-toptit cf">
                                    <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/agriculture-icon.png" alt=""></span>
                                    <h4 data-aos="fade-left" data-aos-duration="600" class="aos-init aos-animate">Agriculture and Veterinary</h4>
                                </div>
                                <div class="allsub-contbox">
                                    <p data-aos="fade-left" data-aos-duration="600" class="aos-init aos-animate">Loren ipsome changes Loren ipsome changes Loren ipsome changes Loren ipsome changes Loren ipsome changes Loren ipsome changes Loren ipsome changes Loren ipsome changes </p>
                                </div>
                                <div class="allsub-viewmor-link">
                                    <a href="#">View More</a>
                                </div>


                            </div>
                            <div class="allsubjet-rigt-links">
                                <ul data-aos="fade-left" data-aos-duration="600" class="aos-init aos-animate">
                                    <li><a href="#">General Engineering and Technology</a></li>
                                    <li><a href="#">General Engineering and Technology</a></li>
                                    <li><a href="#">General Engineering and Technology</a></li>
                                    <li><a href="#">General Engineering and Technology</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>
                                    <li><a href="#">Subcategories</a></li>

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

</section>

<?php get_footer(); ?>