<?php

/* Template Name: My Review Page Details Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

//Review Sections  For Educator
global $wpdb;
$projectid = $_REQUEST['id'];
$statusdata = $_REQUEST['review'];
if($statusdata == 'publish'){
        $current_user = wp_get_current_user();
        $status      = 0;
        $table_name  = $wpdb->prefix . "review";
        $where       = "`id` = '" . $projectid . "'";
        $update_data = "status='" . $status . "'";
        $sql_update  = "UPDATE " . $table_name . " SET " . $update_data . "  WHERE " . $where . " ";  
        $current_user = wp_get_current_user();
       if ( $wpdb->query( $sql_update ) ) {
        $coursereview       = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE `id` = '$projectid' " );
        $title              = $coursereview[0]->Title;
       // echo "Record Updated";
        $admin_email    = get_option('admin_email');
        $subject        = "New Review Submitted By ".$current_user->user_firstname. $current_user->user_lastname;
        //esc_html( $current_user->user_firstname ) );
        $reviewlinkdin   = site_url().'/review-details/?id='.$projectid;
        $message.= '<!DOCTYPE html>
        <html>
            <head>
                <style>
                    * { box-sizing:border-box; }
                </style>
            </head>
        <body bgcolor="#FFFFFF">';
        $message.='Hello Administrator,<br><br>
        This is to notify that one new review has been submitted recently by '.$current_user->user_firstname.' '.$current_user->user_lastname.' '.$title.'<br>;  
        Please click here to check that review and review.';
        // $message.= 'The new review has submitted by a customer name '.$current_user->user_firstname.' '.$current_user->user_lastname.'  on eGlobal Education platform';
        //  $message .= '<br>';
        // $message.= 'Please click here to check the added review and do accept or reject accordingly';
        $message.='<br><br>';
        $message.= " <a  href='".$reviewlinkdin."'> Review Link</a>"; 
        $message .= '<br><br>Thank You';
        $message .= '<br><br>';
        $message .= '<span class="HOEnZb adL"><font color="#888888"><br>
        <br>
        This e-mail was sent from eglobaleducation (<a href="'.site_url().'" rel="noreferrer" target="_blank" >'.site_url().'<wbr>onnect</a>)<br>
        <br>
        </font></span></body></html>';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail($admin_email,$subject,$message, $headers);
    }
}


$table_name = $wpdb->prefix ."review";
$avarage = 0;
$totalaverage =0;
$totalcoursereviws =0;

//SELECT * FROM `ege_review` WHERE `course_id` = 15
$coursereview       = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE `id` = '$projectid' " );
$course_id          = $coursereview[0]->course_id;
$educatorid         = $coursereview[0]->educators_id;
$product_id         = $coursereview[0]->product_id;
$reviewtype         = $coursereview[0]->review_type;
$overal_rating      = $coursereview[0]->overal_rating;
$title              = $coursereview[0]->Title;
$descriptions       = $coursereview[0]->descriptions;
$teching_quality    = $coursereview[0]->teching_quality;
$school_facilites   = $coursereview[0]->school_facilites;
$locations          = $coursereview[0]->locations;
$career_assistance  = $coursereview[0]->career_assistance;
$value_of_money     = $coursereview[0]->value_of_money;
$reveiwpublishstatus= $coursereview[0]->status;
$useridstatus       = $coursereview[0]->userid;
$images             = maybe_unserialize($coursereview[0]->images);
$videos             = maybe_unserialize($coursereview[0]->videos);
$userid             = get_current_user_id();
$useridstatus       = $useridstatus;


if(empty($coursereview)){
   wp_redirect( site_url('404'));
    exit();
}

if($statusdata == 'delete'){
    if($useridstatus == $userid){
        $status      = 0;
        $wpdb->query(
                      'DELETE  FROM '.$wpdb->prefix.'review
                       WHERE id = "'.$projectid.'"'
        );
        wp_redirect('my-profiles');
    }
}

global $wpdb;
$projectid = $_REQUEST['id'];
if($useridstatus != $userid  ){
    if($reveiwpublishstatus !=1){
        if(current_user_can('administrator')){
            // do nothing.
        }else{
                   wp_redirect( site_url('404'));
                exit;
        }
    }
}
get_header(); 


// $educatorviews = pvc_get_post_views(get_the_id());
// update_post_meta(get_the_id(),'post_views_counter',$educatorviews);
$universityid = 184;
$terms  = get_the_terms($universityid,'Country');
$countryname =  '';
$countryflag = '';
if(!empty($terms)){
    $countryname =  $termsname = $terms[0]->name;
    $termsid     = $terms[0]->term_id;
    $countryflag = ege_countryflagurl($termsid);
}

//$taxonomy . '_' . $term_id;
$unvisitylogo = get_field('university_logo');
$unvisitybannerimages = get_field('banner_image');
if(empty($unvisitybannerimages)){
    $unvisitybannerimages = site_url().'/wp-content/uploads/2018/12/inner-banner.jpg';
}

// Educator Serach Dropdown 
// $educatorid = array(get_the_id());
// $educator_array = array(  
// 'key' => 'select_university',
// 'value' =>$educatorid,
// 'compare' => "IN",
// );

//Rating 
$average = 2;
$detailpageid = get_field('profile_details_page','option');
$why_de_montfort = get_field('why_de_montfort');
$entry_requirements = get_field('entry_requirements');
$fees_and_funding = get_field('fees_and_funding');
$facilities = get_field('facilities');
$campus_culture = get_field('campus_culture');
$location = get_field('location');
//wp_reset_postdata(); 
if(is_user_logged_in()) {
    $userid         = get_current_user_id();
    $meta_key       = 'educator_short_list';
    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
    $wishclass      = 'add';
    $wishsortlistclass   = 'fa-heart';
    if(is_array($educatorarray)){
        if(in_array(get_the_id(), $educatorarray)){
            $wishclass = 'remove';
            $wishsortlistclass = 'fa-heart-o';
        }
    }
}

// echo "<pre>";
// print_r($coursereview);
// echo "</pre>";
//$overal_rating =5;
$args = array(
   'rating' => $overal_rating,
   'type' => 'rating',
   'number' => 5,
);  
$args1 = array(
   'rating' => $teching_quality,
   'type' => 'rating',
   'number' => 5,
); 
$args2 = array(
   'rating' => $school_facilites,
   'type' => 'rating',
   'number' => 5,
); 
$args3 = array(
   'rating' => $locations,
   'type' => 'rating',
   'number' => 5,
); 
$args4 = array(
   'rating' => $value_of_money,
   'type' => 'rating',
   'number' => 5,
); 
$args5 = array(
   'rating' => $career_assistance,
   'type' => 'rating',
   'number' => 5,
);       
//wp_reset_postdata();
?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/fancybox/source/jquery.fancybox.pack.js"></script>
    <section class="inner-bannerbox topgreen-border" style="background-image: url(<?php echo $unvisitybannerimages; ?>)">
        <div class="wrapper">

            <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">
                    <ul class="cf">
                        <li><a href="<?php ?>"><i class="fa fa-home"></i></a></li>
                        <li><label> <?php echo get_the_title($course_id).' Review'; ?></label></li>
                    </ul>
                </div>
                <div class="innerbanner-block cf">
                    <?php if(!empty($unvisitylogo)){ ?>
                    <div class="inrbanner-leftimg">
                        <img src="<?php echo $unvisitylogo; ?>" alt="" />
                    </div>
                    <?php } ?>
                    <div class="inrbaner-rightcont">
                        <div class="baner-titbox">
                            <h3><?php echo get_the_title($educatorid); ?></h3>
                        </div>
                             <span class="course-review-name"><?php echo get_the_title($course_id); ?></span>
                       
                        <div class="school-detail-bottom">
                            <div class="rate-star">
                                <?php /*
                                <?php if(!empty($average) ){
                                    $totalavaragereaming = 5;
                                  
                                 ?>
                                <span class="like-count">
                                    <?php for($i=1; $i<=$average; $i++ ){ ?>
                                    <i class="fa fa-star"></i>
                                    <?php } ?>
                                    <?php for($i=$average; $i<$totalavaragereaming; $i++ ){ ?>
                                    <i class="fa fa-star-o"></i>
                                    <?php  } ?>
                                </span>
                                <?php  } ?>
                                */ ?>
                                <?php wp_star_rating($args); ?>

                            </div>
                            <div class="review-texts" style="display:none">
                                <span><?php echo $totalcoursereviws; ?> reviews</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="innerpage-menu-box greenbgcolor">
        <div class="wrapper cf">
            <div class="sticky-imgtit titimgsticky">
                <?php if(!empty($unvisitylogo)){ ?>
                <div class="stikimg-small">
                    <img src="<?php echo $unvisitylogo; ?>" alt="" width="60" />
                </div>
                <?php } ?>

                <div class="sticky-tibox">
                   <div class="table">
                       <div class="table-cell">
                            <h4><?php echo get_the_title(); ?></h4>
                       </div>
                    </div>
                </div>
            </div>
      
        
            <div id="eductors-popup" class="enquiry-form-popup">
                 <div class="maxwidth-box">
                     <div class="padding whitecolor">
                         <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                         <?php echo do_shortcode('[contact-form-7 id="129" title="Contact form 1"]'); ?>
                         
                     </div>    
                 </div>
            </div>
            <div class="iner-left-menubox" data-aos="fade-right" data-aos-duration="1200">
               <a href="javascript:void(0);" id="innermenu-click" class="innermenu-linkbox">
                   <span></span>
                   <span></span>
                   <span></span>
                </a>
                <ul class="cf in-responsive">
                    <li><a href="#go-aboutid" class="menu-scroll-class">about</a></li>
                    <li><a href="#go-rating">Rating</a></li>
                    <?php if(!empty($images)){ ?>
                    <li><a href="#go-imgid">Images</a></li>
                    <?php } ?>
                    <?php if(!empty($videos)){ ?>
                    <li><a href="#go-videoid">Videos</a></li>
                    <?php } ?>

                </ul>
            </div>
            <?php if($userid == $useridstatus ){ ?>
                    <div class="enquiry-rightbox">
                    <?php if($reveiwpublishstatus != 1){ ?>    
                    <?php if($reveiwpublishstatus == 0){?>
                        <a href="javascript:void(0)" class="btn enquiry-btn">Pending</a>
                     <?php }else{ ?>
                           <a href="<?php echo site_url().'/review-details/?id='.$projectid.'&review=publish' ?>" class="btn publish-class enquiry-btn">Publish</a>
                     <?php } ?>  
                     <?php } ?> 
                    <a href="<?php echo site_url().'/review-details/?id='.$projectid.'&review=delete' ?>" class="btn enquiry-btn delete-class">Delete</a>
                    </div>
             <?php } ?>       
        </div>

    </section>

    <section class="about-top-contebox menu-scroll-about"  id="go-aboutid">
        <div class="wrapper cf">
            <div class="title-contentbox" data-aos="fade-right" data-aos-duration="600">
                    <?php if($userid == $useridstatus ){ ?>
                    <?php if($reveiwpublishstatus == 0){?>
                      <div class="heigh-light">  
                        <h2>
                        You review has been submitted and it will take 24 hours to be activated, kindly contact administrator on <a href="mailto:admin@eglobaleducation.com">admin@eglobaleducation.com</a>if in-case any queries</h2>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <h1><?php echo $title; ?></h1>
                    <?php 
                     echo  wpautop($descriptions);
                    ?>

                    <?php
                     /* if(!empty(get_the_content())) {?>
                    <a  href="<?php echo get_permalink($detailpageid);  ?>?educatoid=<?php echo $universityid;  ?>" class="arrow-links">read more</a></p>
                    <?php }  */ ?>
            </div>
        </div>
    </section>

    <section class="about-top-contebox" id="go-rating">
        <div class="wrapper cf">
            <div class="title-contentbox" data-aos="fade-right" data-aos-duration="600">
                    <h1><?php echo 'Rating'; ?></h1>
                    <div class="sub-input-name righ-ratingmainbox inline-ratingbox cf">
                          <div class="row">
                           <div class="cf divide-box">
                                <label> Teaching Quality: </label> 
                                <div class="rate-star">
                                  <?php wp_star_rating($args1); ?>
                                   <div class="<?php echo egobal_get_the_review_label_color($teching_quality) ?> overal-rating-labe"><?php 
                                    echo egobal_get_the_review_label_two($teching_quality)
                                   ?></div>
                                </div>
                            </div>
                            <div class="cf divide-box">
                                <label> School Facilties: </label> 
                                <div class="rate-star">
                                    <?php wp_star_rating($args2); ?>
                                    <div class="<?php echo egobal_get_the_review_label_color($school_facilites) ?> overal-rating-labe"><?php 
                                     echo egobal_get_the_review_label_two($school_facilites)
                                   ?></div>
                                </div>
                            </div>
                            <div class="cf divide-box">
                                <label> Locations: </label> 
                                <div class="rate-star">
                                        <?php wp_star_rating($args3); ?>
                                        <div class="<?php echo egobal_get_the_review_label_color($locations) ?> overal-rating-labe"><?php 
                                    echo egobal_get_the_review_label_two($locations)
                                   ?></div>
                                </div>
                            </div>
                            <div class="cf divide-box">
                                <label> Value for Money : </label> 
                                <div class="rate-star">
                                        <?php wp_star_rating($args4); ?>
                                        <div class="<?php echo egobal_get_the_review_label_color($value_of_money) ?> overal-rating-labe"><?php 
                                    echo egobal_get_the_review_label_two($value_of_money)
                                   ?></div>
                                </div>
                            </div>
                            <div class="cf divide-box">
                                <label> Career assistance: </label> 
                                <div class="rate-star">
                                        <?php wp_star_rating($args5); ?>
                                        <div class="<?php echo egobal_get_the_review_label_color($value_of_money) ?>  overal-rating-labe"><?php 
                                    echo egobal_get_the_review_label_two($career_assistance)
                                   ?></div>
                                </div>
                            </div>
                            </div>
                    </div>        
                    <?php 
                     //echo  wpautop($descriptions);
                    // echo "<pre>"; 
                    // print_r($coursereview);
                    // echo "</pre>";
                    // echo "<pre>"; 
                    // print_r($images);
                    // echo "</pre>";
                    // echo "<pre>"; 
                    // print_r($video);
                    // echo "</pre>";
                   // echo wp_get_attachment_url( 716 );
                    //'<p>'.substr(get_the_content(),0,550);
                    ?>
                    <?php /* if(!empty(get_the_content())) {?>
                    <a  href="<?php echo get_permalink($detailpageid);  ?>?educatoid=<?php echo $universityid;  ?>" class="arrow-links">read more</a></p>
                    <?php }  */ ?>
            </div>
        </div>
    </section>
    <?php
    $size = 'gallery-size'; 
    // $images = get_field('gallery_images');
    // $size = 'gallery-size'; // (thumbnail, medium, large, full or custom size)

    // $gallery_title = get_field('gallery_title');
    // $gallery_descriptions = get_field('gallery_descriptions');

     if(!empty($images )){
    ?>
    <section class="course-section gallery-section greybg" id="go-imgid">
        <div class="wrapper cf">
            <div class="top-title-contbox aos-init" data-aos="fade-right" data-aos-duration="1200">
                <h2>Image Gallery</h2>
            </div>
            <div class="university-sliderbox cf" data-aos="fade-left" data-aos-duration="1200">
                <div class="gallery-sldtwo university-slider owl-carousel revie-sldslier">


                    <?php foreach( $images as $image ): ?>
                    <div class="block-image">
                    <?php echo wp_get_attachment_image($image, $size ); ?>
                    </div>
                    <?php endforeach; ?>
    
    
                </div>
            </div>
        </div>
    </section>
    <?php
    }
    $size = 'gallery-size'; 
    // $images = get_field('gallery_images');
    // $size = 'gallery-size'; // (thumbnail, medium, large, full or custom size)

    // $gallery_title = get_field('gallery_title');
    // $gallery_descriptions = get_field('gallery_descriptions');

     if(!empty($videos )){
    ?>
    <section class="course-section gallery-section greybg" id="go-videoid">
        <div class="wrapper cf">
            <div class="top-title-contbox aos-init" data-aos="fade-right" data-aos-duration="1200">
                <h2>Videos Gallery</h2>
            </div>
            <div class="university-sliderbox cf" data-aos="fade-left" data-aos-duration="1200">
                <div  class="gallery-sldtwo university-slider owl-carousel revie-gallvideo">


                    <?php foreach( $videos as $video ): ?>
                    <div class="block-image">
                            <video width="298" height="205" controls>
                            <source src="<?php echo wp_get_attachment_url($video); ?>" type="video/mp4">
                            Your browser does not support the video tag.
                            </video>
                    </div>
                    <?php endforeach; ?>
    
    
                </div>
            </div>
        </div>
    </section>
    <?php
    }

    // check if the repeater field has rows of data
    if( have_rows('gallery_videos') ):
        $video_title = get_field('video_title');
        $video_descriptions = get_field('video_descriptions');
        ?>

    <section class="course-section videogallery-section" id="go-videoid">
        <div class="wrapper cf">
            <div class="top-title-contbox aos-init" data-aos="fade-right" data-aos-duration="1200">
                <?php if(!empty($video_title)) { ?>
                <h2><?php echo $video_title;  ?></h2>
                <?php } ?>
                <?php if(!empty($video_descriptions )) {?>
                <p><?php echo $video_descriptions;  ?></p>
            <?php } ?>
            </div>
            <div class="videogally-box">
                <div class="slider slider_circle_10">
                <?php
                // check if the repeater field has rows of data

                // loop through the rows of data
                while ( have_rows('gallery_videos') ) : the_row();
                    ?>
                  <div>
                    <iframe width="619" height="313" src="<?php echo get_sub_field('add_video_url'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>

                <?php

                endwhile;
                // no rows found
                    ?>
                  <div class="next_button"><i class="fa fa-angle-right"></i></div>  
                  <div class="prev_button"><i class="fa fa-angle-left"></i></div>  
                </div>
            </div> 
        </div>
    </section>
    <?php
    endif;
    ?>
<?php get_footer(); ?>