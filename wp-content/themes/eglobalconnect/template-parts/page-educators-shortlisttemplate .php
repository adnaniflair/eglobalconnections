<?php

/* Template Name: Page educators Shortlist Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
if(!is_user_logged_in()){
    // wp_redirect( site_url('login-register'));
    // exit;
}
get_header(); 
wp_reset_postdata();
?>
<section class="inner-bannerbox all-subjectbox">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
    <?php
    $userid         = get_current_user_id();
    $meta_key       = 'educator_short_list';
    //$educators_list_unique = array($educatorid);
    if(is_user_logged_in()){
    // wp_redirect( site_url('login-register'));
    // exit;

        $educatorarry    = maybe_unserialize(get_user_meta($userid,$meta_key,true));

    }else{
        $ipaddress     = getRealIpAddr();
        $educatorarry = maybe_unserialize(get_educator_wislist($ipaddress));
        $educatorarry = maybe_unserialize($educatorarry[0]);
    }
    if(!empty($educatorarry)){
    ?>
    <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox" style="display:none;">
                <h2>Favorite Institutions</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
                <?php
          
                $the_query = new WP_Query( array(
                    'posts_per_page' =>-1,
                    'order' => 'DESC',
                    'orderby' =>'date',
                    'post_type'     =>'educator',
                    'post__in' => $educatorarry,
                    
                    )
                );
                while ($the_query -> have_posts()) : $the_query -> the_post(); 
                        // print_r($the_query);
                        do_action('educator_html_actions');
                endwhile;
                ?>

                </div>
            </div>
        </div>
    </section>
<?php }else{ ?>

    <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
                <h2> shortlist not Founds</h2>
            </div>
    </section>
 <?php } ?>   
<?php get_footer();

