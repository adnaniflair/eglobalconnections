<?php
/* Template Name: Country List Template */

get_header(); 
//session_start();
$term_id ='';
global $post;
$post_slug              = $post->post_name;
$countrydropdown        = $post_slug;
$term           = get_term_by( 'slug',$countrydropdown, 'Country');
$termsname      = $term->name;
$termsuid       = $term->term_id;
$countryflag    = ege_countryflagurl($termsuid,'educator-list');
//$countrydropdown        = '';
$educatortypeid         = $_REQUEST['etype'];
$educator_dropdown_id   = '';
// if(empty($educatortypeid)){
//     if(!empty($_SESSION['educatorcountrydropdown'])){
//         $countrydropdown = $_SESSION['educatorcountrydropdown'];
//         if(!empty($_SESSION['educatortermid'])){
//             $term_id = $_SESSION['educatortermid'];
//         }

//         if(!empty($_SESSION['educatoreducatortype'])){
//             $educatortypeid = $_SESSION['educatoreducatortype'];
//         }

//         if(!empty($_SESSION['educatoreducatortypeid'])){
//             $educator_dropdown_id = $_SESSION['educatoreducatortypeid'];
//         }
//     }
// }
$term           = get_term_by( 'slug',$countrydropdown, 'Country');
$termsname      = $term->name;
$termsuid       = $term->term_id;
$countryflag    = ege_countryflagurl($termsuid,'educator-list');

$termdescription = $term->description;
$paged = 1;
if(!empty($_REQUEST['etype'])){
    $educatortype    = $_REQUEST['etype'];
}
if(!empty($countrydropdown)){
    $countrydropdownsarray = array(
    'taxonomy' => 'Country', // Taxonomy name
    'field' => 'slug',
    'terms' => $countrydropdown
    );
}

//Study Level Type Done
if(!empty($educatortypeid)){
        $countrydropdownsarray = array( // (array) - use taxonomy parameters (available with Version 3.1).
        array(
        'taxonomy' => 'educator-type',
        'field' => 'slug',
        'terms' => $educatortypeid,
        //'include_children' => false,
        //'operator' => 'NOT IN'
        )
        );
}

if(!empty($countrydropdown) && !empty($educatortypeid)) {
$countrydropdownsarray = array( // (array) - use taxonomy parameters (available with Version 3.1).
'relation' => 'AND', // (string) - The logical relationship between each inner taxonomy array when there is more than one. Possible values are 'AND', 'OR'. Do not use with a single inner taxonomy array. Default value is 'AND'.
array(
'taxonomy' => 'Country', // (string) - Taxonomy.
'field' => 'slug', // (string) - Select taxonomy term by Possible values are 'term_id', 'name', 'slug' or 'term_taxonomy_id'. Default value is 'term_id'.
'terms' => $countrydropdown // (int/string/array) - Taxonomy term(s).
//'include_children' => true, // (bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
//'operator' => 'IN' // (string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS'. Default value is 'IN'.
),
array(
'taxonomy' => 'educator-type',
'field' => 'slug',
'terms' => $educatortypeid,
//'include_children' => false,
//'operator' => 'NOT IN'
)
);

}


if(!empty($educatortypeid)){
            $educatortypeterm   = get_term_by( 'slug',$educatortypeid, 'educator-type');
            $educatorname       = $educatortypeterm->name;
            $educatormaintitle  = $educatorname;
}else{
    $educatormaintitle = get_the_title();
}

//'page_id' => 119, 
//'post__in' =>array(119, 184, 79),

//dad
$term = get_term_by( 'id',$term_id, 'subject');
if(!empty($term)){
$subject_array = array(
    'taxonomy' => 'subject',
    'field'    => 'slug',
    'terms' => $term->slug
    );

$universityposts = get_posts(
    array(
    'posts_per_page' => -1,
    'order' => 'ASC',
    //'orderby' =>'date',
    'paged' => $paged,
    'post_type'     =>'course',
    'tax_query' => array($subject_array),
    )
);
if(!empty($universityposts)){ 
    $unvisityid = array();
    foreach ($universityposts as $postvalue) {
        # code...
         $courseid     =  $postvalue->ID;
         $universityid = get_field('select_university',$courseid);

         //print_r($studylevel); 
         if(!empty( $universityid)){
           $unvisityid[]   = $universityid;
         }
         //echo '<br>';
    //echo $universityname = get_the_title($unvisityid);
    }
}

if(!empty($unvisityid)){  
    $educators_list = (array_unique($unvisityid));
}
}
if($sortbyvalue == 'popularity'){
    $metavalue = 'post_views_counter';
    $ordervalueby   = 'meta_value_num';
    $orderby  ='DESC';  
}
// 'tax_query' => array($countrydropdownsarray),
//    'tax_query' => array($countrydropdownsarray),
//'post__in' => $educators_list,
if(!empty($educator_dropdown_id)){
    $educators_list = array($educator_dropdown_id);
    $the_query = new WP_Query( array(
    'posts_per_page' => 8,
    'order' => 'DESC',
    'post__in' => $educators_list,
    'orderby' =>'date',
    'paged' => $paged,
    'post_type'     =>'educator',
    'order' => $orderby,
    'orderby' => $ordervalueby,
       ) 
    );
}else{
    $the_query = new WP_Query( array(
    'posts_per_page' => 8,
    'order' => 'DESC',
    'post__in' => $educators_list,
    'orderby' =>'date',
    'paged' => $paged,
    'post_type'     =>'educator',
    'tax_query' => array($countrydropdownsarray),
    'order' => $orderby,
    'orderby' => $ordervalueby,
   
    )
    );
}

$banner_image    = get_field('banner_image',get_the_id());
$header_title    = get_field('header_title',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}

//Advertisment Sections
$ad_title               = get_field('ad_title',get_the_id());
$descriptions           = get_field('descriptions',get_the_id());
$download_pdf           = get_field('download_pdf',get_the_id());
$advertisement_image    = get_field('advertisement_image',get_the_id());
if(empty($advertisement_image)){
    $advertisement_image = get_stylesheet_directory_uri() .'/assettwo/images/united-kingdom-img.png';
}


//popular title and descriptions
$popular_title                          = get_field('popular_title',get_the_id());
$popular_country_descriptions           = get_field('popular_country_descriptions',get_the_id());
?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/js/select2.min.js"></script>
<link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/select2.min.css" rel="stylesheet" />
<section class="inner-bannerbox green-bgcover topgreen-border" style="background-image: url(<?php echo $banner_image;  ?>">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript:void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <span class="banerpage-icon" >
                        <?php  if(!empty($countryflag)) { ?>
                            <img src="<?php echo $countryflag; ?>" alt="">
                        <?php } ?>
                        </span>
                        <?php if($educatormaintitle == 'University'){
                                $educatormaintitle = 'Universitie';
                             }
                            ?>
                        <h2 class="educator-main-title">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
                    <div class="pagebaner-allconte cf" style="display:none" >
                        <p>
                            <?php echo $the_query->found_posts; ?>
                            <?php /*
                            <?php echo $educatormaintitle.'s'; ?> in
                            <?php echo $termsname; ?>
                            */ ?>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="business-sectionbox">
    <div class="wrapper cf">
        <div class="minusmartin-top cf">
            <div style="width:100%" class="leftbusinessbox whitebg equal-height" id="country-name-html">
                    <h3><?php echo $header_title ?> </h3>
                    <?php 
                    wp_reset_postdata();
                    the_content(); 
                    ?>
            </div>
            <div style="display:none" class="rightbusiness-box cf greybg equal-height" >
                <div class="rightleft-textbox">
                    <h4><?php echo $ad_title;  ?></h4>
                    <p><?php echo $descriptions;  ?></p>
                    <a download="download" href="<?php echo $download_pdf; ?>" class="download-links">Download</a>
                </div>
                <div class="rightrit-imgbox">
                    <img src="<?php echo $advertisement_image; ?>" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>

<section class="course-section greybg remove30padingtop">

    <div class="wrapper cf">
           <div class="top-title-contbox">
                <?php if(empty($popular_title )){ ?>
                <h2>Popular Institutions</h2>
                <?php }else{ 
                    echo '<h2>'.$popular_title.'</h2>';
                } ?>    
                <?php if(empty($popular_country_descriptions)){ ?>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
                <?php }else {
                    echo $popular_country_descriptions;    
                } ?>
            </div>
        <?php


        // $universityposts = get_posts(
        //     array(
        //     'post_type' => 'educator', // Post type
        //     // 'tax_query' => array(
        //     // array(
        //     // 'taxonomy' => 'Country', // Taxonomy name
        //     // 'field' => 'slug',
        //     // 'terms' => 'south-africa'
        //     // )
        //     // ),
        //     'posts_per_page' => -1,
        //     'orderby'       => 'menu_order',
        //     'order'         => 'ASC'
        //     )
        // );
        // echo "<pre>";
        // print_r($universityposts);
        // echo "</pre>";
        ?>
        <div class="cf">
            <div class="school-detail-slider">
                <div class="rows cf" id="educators-listing-data">
                
                    <?php  
                    // echo "<pre>";
                    // print_r($the_query );
                    // echo "</pre>";
                    if($the_query -> have_posts()){
                ?>
                    <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                            // print_r($the_query);   
                      
                        do_action('educator_html_actions');
                    
                    endwhile;
                    }else{
                    echo "No Educators Founds";
                    }
                    wp_reset_postdata(); ?>
                    <div class="fullwidth-block" style="display:none">
                        <div class="page-linkox cf">
                            <?php

                        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php 
 get_footer();
?>

<script type='text/javascript'>
    jQuery(document).ready(function($) {

        jQuery("#select-country-name").live("change", function(){
        $countrydropdown    = jQuery('#select-country-name').val();
        ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";

        // if($countrydropdown != ''){
        // jQuery('#educators-drodown').find('option:first').attr('selected', 'selected');
        // jQuery('#educators_drodown_chosen').children('a').children('span').text('Educator Name');
        // }

        $.ajax({
        url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
        method:'Post',
        data: {
        'action': 'educator_listing_drodpown_two',
        'countrydropdown':$countrydropdown
        },
        success:function(data) {
        // This outputs the result of the ajax request
        jQuery('#selected-educator-id').html(data);
        // jQuery('.chosen-select').chosen({
        // width: "100%"
        // });

        // alert(data);
        // jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
        //jQuery('.tax-subject').removeClass('loader-body');

        },
        error: function(errorThrown){
        console.log(errorThrown);
        }
        });
        }); 

        //Jqury_ajax_call();
        $(".custom-select").select2({
            tags: "true",
            placeholder: "Select an option",
            allowClear: true,
            width: '100%',
            createTag: function(params) {
                var term = $.trim(params.term);

                if (term === '') {
                    return null;
                }

                return {
                    id: term,
                    text: term,
                    value: true // add additional parameters
                }
            }
        });

        jQuery(".selecte-sortby").live("click", function() {
        jQuery(".sortby-dropdown-ul").slideToggle("100", function() {
        // Animation complete
        });
        // jQuery('.sort-by-dropdown').fadeIn('slow');
        //  jQuery('.sortby-dropdown-ul').hide();
        });

        jQuery(".selecte-serachby").live("click", function() {
            jQuery(".serach-by-dropdown").slideToggle("100", function() {
                // Animation complete
            });
            // jQuery('.sort-by-dropdown').fadeIn('slow');
            jQuery('.sort-by-dropdown').hide();


        });

    jQuery(".sortby-dropdown").live("click", function() {
        jQuery('.sortby-dropdown').removeClass('active');
        jQuery(this).addClass('active');
        Jqury_ajax_call();
    });
    
    function Jqury_ajax_call(){
            jQuery('.loading').show();
            jQuery('.page-template-page-educator').addClass('loader-body');
            //jQuery('.tax-subject').addClass('loader-body');

            $pagenumber = 1;
            $termid = '';
            $educatortype = jQuery('#select-educator-type').val();
            $studylevel = '';
            $countrydropdown = jQuery('#select-country-name').val();
            $sortdropdown    = jQuery('ul.sortby-dropdown-ul').find('li.active').children().data('value');
            if($sortdropdown == 'undefined'){
                $sortdropdown = 'date';
            }           
            $sortbyvalue     = $sortdropdown;
            $termid = jQuery('#subject-id-dropdown').val();
            $educatortypeid = jQuery.trim(jQuery('#selected-educator-id').val());

            ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method: 'Post',
                data: {
                    'action': 'educators_listing_sections',
                    'page': $pagenumber,
                    'termid': $termid,
                    'studylevel': $studylevel,
                    'educatortype': $educatortype,
                    'educatortypeid': $educatortypeid,
                    'countrydropdown': $countrydropdown,
                    'sortbyvaluedropdown': $sortbyvalue,
                },
                success: function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#educators-listing-data').html(data);
                    //jQuery('#country-name-html').html('TEXT IS COMing');
                    /*
                    $.ajax({
                        url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                        method: 'Post',
                        data: {
                            'action': 'educators_listing_header',
                            'page': $pagenumber,
                            'countrydropdown': $countrydropdown,
                        },
                        success: function(data) {
                            // This outputs the result of the ajax request
                            //jQuery('#educators-listing-data').html(data);
                            jQuery('#country-name-html').html(data).fadeIn(500);
                            $totalrecords = jQuery('.total-records').val();
                            if ($totalrecords != 'undefined') {
                            jQuery('.pagebaner-allconte').html('<p>' + jQuery('.total-records').val() + '<p>');
                            }
                            $maintitle = jQuery('.main-title-mangement').val();
                            $mainimg = jQuery('.main-title-img').val();
                            if ($maintitle != 'undefined') {
                                jQuery('.educator-main-title').html($maintitle);
                                jQuery('.banerpage-icon').children().attr('src',$mainimg);
                                if($mainimg != ''){
                                jQuery('.banerpage-icon').html('<img  src="'+$mainimg+'" alt="'+$maintitle+'">');
                                }else{
                                    jQuery('.banerpage-icon').html('');
                                }                            
                            }
                            jQuery('.loading').hide();
                            jQuery('.page-template-page-educator').removeClass('loader-body');


                        },
                        error: function(errorThrown) {
                            console.log(errorThrown);
                        }
                    }); */

                    jQuery('.loading').hide();
                    jQuery('.page-template-page-educator').removeClass('loader-body');
                    equalheight('.school-details-wrap');
                    equalheight('.school-details-wrap h3');
                    // equal height testimonials block
   
                    // jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
                    //jQuery('.tax-subject').removeClass('loader-body');

                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
    }
        // jQuery("#educators-drodown").live("change", function(){

        //       $educatorname    = jQuery('#educators-drodown').val();
        //         if($educatorname != ''){
        //             jQuery('#country-dropdown').find('option:first').attr('selected', 'selected');
        //             jQuery('#country_dropdown_chosen').children('a').children('span').text('Country');
        //         }
        //  }); 
        // jQuery("#country-dropdown").live("change", function(){
        //     $countrydropdown    = jQuery('#country-dropdown').val();
        //     if($countrydropdown != ''){
        //     jQuery('#educators-drodown').find('option:first').attr('selected', 'selected');
        //     jQuery('#educators_drodown_chosen').children('a').children('span').text('Educator Name');
        //     }
        // });      
        jQuery(".chosen-serach-dropdown").live("change", function() {
                   Jqury_ajax_call(); 
        });
        jQuery(".nav-links").live("click", function() {
            jQuery('.loading').show();
            jQuery('.page-template-page-educator').addClass('loader-body');

            $pagenumber = jQuery(this).data('page');
            $termid = jQuery('.term-id-value').val();
            $educatorname = '';
            $studylevel = '';
            $countrydropdown = jQuery('#select-country-name').val();
            $sortbyvalue = '';

            ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method: 'Post',
                data: {
                    'action': 'educators_listing_sections',
                    'page': $pagenumber,
                    'termid': $termid,
                    'studylevel': $studylevel,
                    'educatorname': $educatorname,
                    'countrydropdown': $countrydropdown,
                    'sortbyvaluedropdown': $sortbyvalue,
                },
                success: function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#educators-listing-data').html(data);
                    //jQuery('.loading').hide();
                    jQuery('.loading').hide();
                    jQuery('.page-template-page-educator').removeClass('loader-body');
                    equalheight('.school-details-wrap');
                    equalheight('.school-details-wrap h3');

                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
    });

</script>
