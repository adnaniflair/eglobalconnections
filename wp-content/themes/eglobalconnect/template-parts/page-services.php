<?php

/* Template Name: Services Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}

$help_you_choose_a_course = get_field('help_you_choose_a_course');
$support_after_of_the_course = get_field('support_after_of_the_course');
$provide_materials = get_field('provide_materials');


?>
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="blog-details-page main-servic-sect">
    <div id="primary" class="wrapper cf">
        <div class="inner-detailbox">
            <div class="allservic-blocks right-img cf ">
                <div class="left-serv-contbox equal-height aos-init" data-aos="fade-left" data-aos-duration="700">
                   <div class="serv-add-content">
                        <?php echo $help_you_choose_a_course; ?>
                    </div>                   
                    <div class="serv-cont-btnbox">
                        <a href="<?php echo site_url('/contact') ?>">Contact us</a>
                    </div>
                </div>
                <div class="service-ritimgbox equal-height aos-init" data-aos="fade-right" data-aos-duration="700" style="background-image:url('<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/help-image.jpg')"></div>
            </div>
            <div class="allservic-blocks left-img cf">
                <div class="service-ritimgbox equal-height" data-aos="fade-left" data-aos-duration="700" style="background-image:url('<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/material-img.jpg')"></div>
                <div class="left-serv-contbox equal-height" data-aos="fade-right" data-aos-duration="700">
                   <div class="serv-add-content">
                        <?php echo $provide_materials; ?>
                    </div>                   
                    <div class="serv-cont-btnbox">
                        <a href="<?php echo site_url('/contact') ?>">Contact us</a>
                    </div>
                </div>
            </div>
            <div class="allservic-blocks right-img cf">
                <div class="left-serv-contbox equal-height" data-aos="fade-left" data-aos-duration="700" >
                   <div class="serv-add-content">
                            <?php echo $support_after_of_the_course;  ?>
                    </div>                   
                    <div class="serv-cont-btnbox">
                        <a href="<?php echo site_url('/contact') ?>">Contact us</a>
                    </div>
                </div>
                <div class="service-ritimgbox equal-height" data-aos="fade-right" data-aos-duration="700" style="background-image:url('<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/supporter-img.jpg')"></div>
            </div>
        </div>      
    </div><!-- #primary -->
</section><!-- .wrap -->



<?php get_footer();?>

