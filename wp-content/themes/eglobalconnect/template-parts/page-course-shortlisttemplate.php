<?php

/* Template Name:  Page Courses Shortlist Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
// if(!is_user_logged_in()){
//     wp_redirect( site_url('login-register'));
//     exit;
// }
get_header(); 
wp_reset_postdata();
?>
<section class="inner-bannerbox all-subjectbox">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
    <?php
    $userid         = get_current_user_id();
    $meta_key       = 'course_short_list';
    //$educators_list_unique = array($educatorid);
    if(is_user_logged_in()){
    // wp_redirect( site_url('login-register'));
    // exit;

    $coursearray    = maybe_unserialize(get_user_meta($userid,$meta_key,true));

    }else{
        $ipaddress     = getRealIpAddr();
        $coursearray = maybe_unserialize(get_course_wislist($ipaddress));
        $coursearray = maybe_unserialize($coursearray[0]);
    }

    if(!empty($coursearray )){
    ?>
    <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="booking-mainbox">
                <div class="rows cf">
            <?php
                $the_query = new WP_Query( array(
                    'posts_per_page' => -1,
                    'order' => 'DESC',
                    'orderby' =>'date',
                    'post_type'     =>'course',
                    'post__in' => $coursearray,
                    
                    )
                );
                while ($the_query -> have_posts()) : $the_query -> the_post(); 
                        // print_r($the_query);
                    do_action('courses_html_actions');
                endwhile;
                ?>
                    <?php /* ?>
                    <div class="three-block">
                        <div class="booking-blok">
                            <div class="booking-imgblk cf">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/booking-img1.jpg" alt="">
                                <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt="">
                                    <p>Course Review</p>
                                </span>
                            </div>
                            <div class="booking-desbox equal-height shorlist-blk">
                                <h3>London School of Commerce</h3>
                                <div class="small-headingbox">MBA, International Business</div>
                                <div class="inqri-and-addcartbox cf">
                                    <a style="display:none" href="#" class="addto-cartlink"><i class="fa fa-shopping-cart"></i>Add to Cart</a>
                                    <a href="#" class="enqure-link">Enqure now</a>
                                </div>
                                <div class="write-cour-box" style="display:none">
                                    <a href="#" class="write-btnlink">Write a course review</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="three-block">
                        <div class="booking-blok">
                            <div class="booking-imgblk cf">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/booking-img1.jpg" alt="">
                                <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt="">
                                    <p>Course Review</p>
                                </span>
                            </div>
                            <div class="booking-desbox equal-height shorlist-blk">
                                <h3>London School of Commerce</h3>
                                <div class="small-headingbox">MBA, International Business</div>
                                <div class="inqri-and-addcartbox cf">
                                    <a href="#" class="addto-cartlink"><i class="fa fa-shopping-cart"></i>Add to Cart</a>
                                    <a href="#" class="enqure-link">Enqure now</a>
                                </div>
                                <div class="write-cour-box">
                                    <a href="#" class="write-btnlink">Write a course review</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="three-block">
                        <div class="booking-blok">
                            <div class="booking-imgblk cf">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/booking-img1.jpg" alt="">
                                <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt="">
                                    <p>Course Review</p>
                                </span>
                            </div>
                            <div class="booking-desbox equal-height shorlist-blk">
                                <h3>504 Absolutely Essential
                                    English Words</h3>
                                <div class="small-headingbox">Murray Bromberg</div>
                                <div class="course-datebox">
                                    <div class="cf"><span>Enroiment date:</span><span>09 September 2014</span></div>
                                    <div class="cf"><span>Completion date:</span><span>03 July 2016</span></div>
                                    <div class="cf"><span>Course price:</span><span>4,985.00</span></div>
                                </div>
                                <div class="inqri-and-addcartbox cf">
                                    <a href="#" class="addto-cartlink"><i class="fa fa-shopping-cart"></i>Add to Cart</a>
                                    <a href="#" class="enqure-link">Enqure now</a>
                                </div>
                                <div class="write-cour-box">
                                    <a href="#" class="write-btnlink">Write a course review</a>
                                </div>

                            </div>
                        </div>
                    </div>

                   <?php */ ?>

                </div>
            </div>
        </div>
    </section>
<?php }else{ ?>

    <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
                <h2> shortlist not Founds</h2>
            </div>
    </section>
 <?php } ?>   

<?php get_footer();

