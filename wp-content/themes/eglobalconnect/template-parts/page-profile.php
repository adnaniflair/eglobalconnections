<?php /* Template Name: My Profile/account Page Template */ 

if(!is_user_logged_in()){
    wp_redirect( site_url('login-register'));
    exit;
}

get_header(); 
wp_reset_postdata();
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}

get_header(); 

//User Manament Details :
if(is_user_logged_in()){ 
    $current_user = wp_get_current_user();
    /**
     * @example Safe usage:
     * $current_user = wp_get_current_user();
     * if ( ! $current_user->exists() ) {
     *     return;
     * }
     */
      // echo "<pre>";
      // print_r($current_user);
      // echo "</pre>";

      $user_id          =  $current_user->ID; 
      $user_login       =  $current_user->user_login; 
      $user_email       =  $current_user->user_email; 
      $user_firstname   =  $current_user->user_firstname; 
      $user_lastname    =  $current_user->user_lastname;
      $user_displayname =  $current_user->display_name; 
      
      $user_gender      =  get_user_meta( $user_id, 'gender',true);
      $user_town_or_city=  get_user_meta( $user_id, 'town_city',true);
      $user_address     =  get_user_meta( $user_id, 'address',true);
      $user_phone       =  get_user_meta( $user_id, 'phone',true);
      $user_zipcode     =  get_user_meta( $user_id,'zipcode',true);
      $user_country     =  get_user_meta( $user_id,'country',true);
      $user_dateofbirth =  get_user_meta( $user_id,'dateofbirth',true);
      if(!empty($user_dateofbirth)){
        $user_dateofbirth = date("d-m-Y", strtotime($user_dateofbirth));
      }
    // $metas = array( 
    //     'facebookurls'  => $facebookurls, 
    //     'twitterurls'   => $twitterurls ,
    //     'linkidurl'     => $linkidurl ,
    //     'googleurls'    => $googleurls ,
    // );

    $user_facebookurl       =  get_user_meta( $user_id, 'facebookurls',true);
    $use_twitterurl         =  get_user_meta( $user_id, 'twitterurls',true);
    $use_linkdinurl         =  get_user_meta( $user_id, 'linkidinurls',true);
    $use_googleurl          =  get_user_meta( $user_id, 'googleurls',true);
   
    //print_r($user_displayname);
    //get_user_meta( $user_id, $key, $value );

    //     'first_name' => $firstname, 
    // 'last_name'  => $lastname ,
    // 'town_city'  => $towncity ,
    // 'gender'     => $gener,
    // 'address'     => $address,
    // 'zipcode'     => $zipcode,
    // 'country'     => $country,
}

            
global $wpdb;
$userid = get_current_user_id();
$curpreveducations       = $wpdb->get_results( "SELECT * FROM `ege_prv_cur_educ_users`  WHERE `futurestatus` = 0 and `userid` = '$userid' ORDER BY `ege_prv_cur_educ_users`.`syear` DESC " );

$futpreveducations       = $wpdb->get_results( "SELECT * FROM `ege_prv_cur_educ_users`  WHERE `futurestatus` = 1 and `userid` = '$userid' ORDER BY `ege_prv_cur_educ_users`.`syear` DESC" );
                // echo "<pre>";
                // print_r($curpreveducations);
                // echo "</pre>";

// MY REVIEW GET USING FUNCTIONS

?>
    <section class="inner-bannerbox topgreen-border profile-diff-bg">
        <div class="wrapper">
            <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">
                    <ul class="cf">
                        <li><a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">My Profile</a></li>
                        <li><label><?php echo $user_firstname.' '.$user_lastname; ?></label></li>
                    </ul>
                </div>
                <div class="innerbanner-block profi-topbox cf">
                    <div class="inrbanner-leftimg">
                        <?php echo get_avatar($userid ,100); ?>
                        <a target="_blank" href="https://en.gravatar.com/">Edit</a> 
                        <?php /*
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/profile-img.jpg" alt="" /> */ ?>
                    </div>
                    <div class="inrbaner-rightcont">
                        <div class="baner-titbox">
                            <h3>Hello!,<?php echo $user_firstname.' '.$user_lastname; ?></h3>
                        </div>
                        <div style="display:none" class="profile-ratingmain-box">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/profile-ratingbox.png" alt="" />
                        </div>
                        <div class="country-locationbox">
                                   <ul>
                                <li>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/country-flag-icon.png" alt="" />
                                    <?php if(!empty($user_town_or_city)){ ?>
                                    <span><?php echo ucfirst($user_town_or_city).','.$user_country; ?></span>
                                    <?php } ?>
                                </li>
                                <li>
                           
                                    <?php
                                    if(!empty($curpreveducations)){
                                      
                                    foreach ($curpreveducations as $key) { 
                                        $educatorid     =  $key->eductorid;
                                        $courseid       =  $key->courseid; 
                                        $coursetitle    =  $key->course_name; 
                                        $educatortitle  =  $key->educator_name; 
                                        $presentstatus  = $key->presentstatus;
                                       
                                        if(empty($educatortitle)){
                                        $educatortitle  = get_the_title($educatorid);
                                        }
                                        if(empty($coursetitle)){
                                        $coursetitle    = get_the_title($courseid);
                                        }
                                        if($presentstatus){
                                            $hlable = 'Current';
                                        }else{
                                            $hlable ='Previous';
                                        }
                                        ?>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/current-icon.png" alt="" />    
                                    <span><?php echo $hlable;  ?>: <?php echo $educatortitle.'('.$coursetitle.')'; ?></span>

                                    <?php } ?>
                                <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="innerpage-menu-box greenbgcolor">
        <div class="wrapper cf">
            
            <div class="enquiry-rightbox">
                <?php $isservice = get_my_services_review($userid);
                if(empty($isservice)){ ?>
                  <a href="<?php echo site_url().'/service-review/'; ?>" class="btn booknow-btn removebtn-icon">Add service review</a>
               <?php } ?>
                <a href="<?php echo site_url().'/publicprofile/?userid='.get_current_user_id(); ?>" class="btn booknow-btn removebtn-icon">View your public profile</a>
                <span class="enqu-lang-dropdown" style="display:none">
                    <select class="chosen-select">
                        <option>Another Language</option>
                        <option>Another Language1</option>
                        <option>Another Language2</option>
                    </select>
                </span>
            </div>
            <div class="sticky-imgtit">
                <div class="stikimg-small imgroundedbox">
                        <?php echo get_avatar($userid ,60); ?>
                </div>
                <div class="sticky-tibox">
                   <div class="table">
                       <div class="table-cell">
                            <h4>Hello!,<?php echo $user_firstname; ?></h4>
                       </div>
                    </div>
                </div>
            </div>
            <div class="iner-left-menubox" data-aos="fade-right" data-aos-duration="1200">
                <a href="javascript:void(0);" id="innermenu-click" class="innermenu-linkbox">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <ul class="cf in-responsive">
                    <li><a href="#profile-id">Personal Information</a></li>
                    <li><a href="#education-id">education</a></li>
                    <li><a href="#booking-id">Book History</a></li>
                    <li><a href="#review-id">Reviews</a></li>
                    <li><a href="#shortlist-id">Favourites</a></li>
                </ul>
            </div>
        </div>
    </section>

    <section class="profile-main-box" id="profile-id">
        <div class="wrapper cf">
            <div class="rows cf"  data-aos="fade-left" data-aos-duration="1500">
                <div class="two-blocks personal-box">
                    <div class="paddingbox shadowbox equal-height">
                        <div class="protile-tit-withbtn">
                            <h3>Personal Information</h3>
                            <a href="javascript:void(0);" class="openpersonalinfo profile-editlink">Edit</a>
                            <div style="display:none" class="enquiry-rightbox"><a href="javascript:void(0);" class="btn enquiry-btn openpersonalinfo">Edit</a></div>
                        </div>
                <!-- Example Html For Profile Page -->
                 <div class="wpcf7 form-design-comon" style="display:none">
                    <form  method="post" id="social-informtions-form-two">
                       <input type="hidden" name="action" value="update_socialmedia_informations" />
                        <div class="input-name">
                            <label>Face Book Url: </label>    
                            <input type="text"  class="form-control"  name="facebook-urls" value="<?php echo $user_facebookurl; ?>" >
                         </div>   
                        <div class="input-name">
                            <label>Twitter Url : </label>    
                            <input type="text" name="twitter-urls" value="<?php echo $twitterurls; ?>"  class="form-control">      
                        </div>
                        <div class="input-name">
                            <label>Linkedin Url: </label>    
                            <input type="text"  class="form-control" name="linkiedin-url" value="<?php echo $user_linkedinurl; ?>" >
                         </div>
                        <div class="input-name">
                            <label>Google url: </label>    
                            <input type="text"  class="form-control" name="google-urls" value="<?php echo $user_googleurl; ?>" >
                        </div>    
                        <div class="Submit-btn">
                          <input  class="submit-socialmedia-infomations submit-socialmedia-infomationsclick"  type="button" value="Submit">
                        </div>
                    </form> 
                   </div> 
                        <div class="personal-infobox">
                            <div class="pers-blkbox cf">
                                <label>First name:</label>
                                <span><?php echo $user_firstname; ?></span>
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Last name:</label>
                                <span><?php echo $user_lastname; ?></span>
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Date of Birth:</label>
                                <?php if(!empty( $user_dateofbirth )){ ?>
                                <span><?php echo $user_dateofbirth;  ?></span>
                                <?php }else{ ?>
                                <span><a href="#">+ Add Date of Birth</a></span>
                                <?php } ?>   
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Gender:</label>
                                <?php if(empty($user_gender)){ ?>
                                <span><a href="#">+ Add Gender</a></span>
                                <?php }else{ ?>
                                <span><?php echo $user_gender; ?></span>
                                 <?php } ?>
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Email:</label>
                                <span><?php echo $user_email;   ?></span>
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Phone:</label>
                                <?php if(empty($user_phone)){ ?>
                                <span><a href="#">+ Add phone</a></span>
                                <?php }else{ ?>
                                <span><?php echo $user_phone; ?></span>
                                 <?php } ?>   
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Address:</label>
                               <?php if(empty($user_address)){ ?>
                                <span><a href="#">+ Add address</a></span>
                               <?php }else{ ?>
                                <span><?php echo $user_address; ?></span>
                               <?php } ?>
                            </div>
                            <div class="pers-blkbox cf">
                                <label>ZIP code:</label>
                                 <?php if(empty($user_zipcode)){ ?>
                                <span><a href="#">+ Add zip code</a></span>
                                <?php }else{ ?>
                                    <span><?php echo $user_zipcode; ?></span>
                                 <?php } ?>   
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Town/City:</label>
                                <?php if(empty($user_town_or_city)){ ?>
                                <span><a href="#">+ Add town/city</a></span>
                                <?php }else{ ?>
                                <span><?php echo $user_town_or_city; ?></span>
                                <?php } ?>    
                            </div>
                            <div class="pers-blkbox cf">
                                <label>Country:</label>
                                <?php if(empty($user_country)){ ?>
                                <span><a href="#">+ Add country</a></span>
                                <?php }else{ ?>
                                 <span class="informations-name"><?php echo $user_country; ?></span>
                                <?php } ?>   
                            </div>
                        </div>
                    </div>
                </div>

                <div class="two-blocks personal-box">
                    <div class="paddingbox shadowbox equal-height">
                        <div class="protile-tit-withbtn">
                            <h3>Social links</h3>
                            <a href="#" class="profile-editlink open-social-informations">Edit</a>
                        </div>
                        <div class="personal-infobox personal-social-link">
                            <div class="pers-blkbox cf">
                                <label><i class="fa fa-facebook"></i>Facebook :</label>
                                <?php if(empty($user_facebookurl)) {?>
                                <span><a href="#">+ Add Facebook link</a></span>
                                <?php }else{ ?>
                                <span><a href="#"><?php echo $user_facebookurl; ?></a></span>
                                <?php } ?>
                            </div>
                            <div class="pers-blkbox cf">
                                <label><i class="fa fa-twitter"></i>Twitter :</label>
                                <?php if(empty($use_twitterurl)) {?>
                                <span><a href="#">+ Add Twitter link</a></span>
                                <?php }else{ ?>
                                <span><a href="#"><?php echo $use_twitterurl; ?></a></span>
                                <?php } ?>
                            </div>
                            <div class="pers-blkbox cf">
                                <label><i class="fa fa-linkedin"></i>Linkedin :</label>
                                <?php if(empty($use_linkdinurl)) {?>
                                <span><a href="#">+ Add Linkedin link</a></span>
                                <?php }else{ ?>
                                <span><a href="#"><?php echo $use_linkdinurl; ?></a></span>
                                <?php } ?>
                            </div>
                            <div class="pers-blkbox cf">
                                <label><i class="fa fa-google-plus"></i>Google+ :</label>
                                <?php if(empty($use_googleurl)) {?>
                                <span><a href="#">+ Add Google+ link</a></span>
                                <?php }else{ ?>
                                <span><a href="#"><?php echo $use_googleurl; ?></a></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="course-section education-blok-section border-bottom" id="education-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox">
                <h2>Education</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>

            <div class="content-mainbox">
                <div class="education-blocks cf">

                    <div class="educat-titbox">
                        <h3>Previous and Current Education</h3><span>(Tell us about your education history)</span>
                    </div>
                    <div class="education-blockbox cf" style="display:none">
                        <div class="educat-leftimg"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/educaiton-img1.jpg" alt=""></div>
                        <div class="educat-rightcont">
                            <div class="educat-tit">
                                <h4>London School of Commerce</h4><span>( MBA, International Business )</span>
                            </div>
                            <p>Praesent congue suscipit velit in faucibus lorem varius id duis feugiat odio ac quam feugiat, malesuada luctus ligula luctus pellentesque a hendrerit metus phasellus nec sem id nisl tincidunt placerat Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
                            <div class="educat-twobutton">
                                <a href="#" class="present-btn">2015 - present</a>
                                <a href="#" class="educat-edit-btn">Edit</a>
                            </div>
                        </div>
                    </div>
                    <?php
                    if(!empty($curpreveducations)){
                        foreach ($curpreveducations as $key) {
                                $formid         =  $key->id;
                                $educatorid     =  $key->eductorid;
                                $courseid       =  $key->courseid;
                                $educatoridother = '';
                                if($educatorid == 1){
                                    $educatoridother = $key->educator_name;
                                }
                                $courseididother = '';
                                if($courseid == 1){
                                    $courseididother = $key->course_name;
                                }
                                $Descriptions   =  $key->Descriptions;
                                $smonth         =  $key->smonth;
                                $syear          =  $key->syear;
                                $emonth         =  $key->emonth;
                                $eyear          =  $key->eyear;
                                $bannerimage    =  get_the_post_thumbnail_url($educatorid,'bannerimage-educators-list');
                                if(empty($bannerimage)){
                                 $bannerimage  = get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                                }
                                $presentstatus  =  $key->presentstatus;
                                if($presentstatus){
                                    $pndetails =  $syear.'- present';
                                }else{
                                    $pndetails =  $syear.'-'.$eyear;
                                }
                                //$eyear          =  $key->futurestatus;
                            # code...
                                $educator_title = get_the_title($educatorid);
                                if(!empty($educatoridother)){
                                    $educator_title = $educatoridother;
                                }
                                $course_title = get_the_title($educatorid);
                                if(!empty($educatoridother)){
                                    $course_title = $courseididother;
                                }
                            ?>

                        <div id="currentloop<?php echo $formid; ?>" class="education-blockbox cf">
                                <div class="educat-leftimg"><img src="<?php echo $bannerimage; ?>" alt=""></div>
                                <div class="educat-rightcont">
                                    <div class="educat-tit">
                                        <h4><?php echo $educator_title; ?></h4><span>(<?php echo $course_title; ?>)</span>
                                    </div>
                                    <p> <?php echo  $Descriptions;  ?></p>
                                    <div class="educat-twobutton">
                                        <a href="#" class="present-btn"><?php echo $pndetails; ?></a>
                                        <a href="javascript:void(0)" data-id="<?php echo $formid ?>" class="educat-edit-btn eductor-delete-code">Delete</a>
                                    </div>
                                </div>
                            </div>

                            <?php
 
                        }

                    }

                    ?>

                    <div class="add-univertiy-box">
                        <a href="javascript:void(0)" class="add-univer-btn openeductions-informations">Add university or school you attended</a>

                    </div>
                    <section class="form">
                        

                    </section>

                </div>
            </div>
        </div>
    </section>

    <section class="course-section education-blok-section border-bottom">
        <div class="wrapper cf"  data-aos="fade-left" data-aos-duration="1500">
            <div class="content-mainbox">
                <div class="education-blocks cf">
                    <div class="educat-titbox">
                        <h3>Future Education</h3><span>(Tell us about your future plans for education)</span>
                    </div>
                    <?php
                    if(!empty($futpreveducations )){
                        foreach ($futpreveducations   as $key) {
                                $formid         =  $key->id;
                                $educatorid     =  $key->eductorid;
                                $courseid       =  $key->courseid;
                                $Descriptions   =  $key->Descriptions;
                                $smonth         =  $key->smonth;
                                $syear          =  $key->syear;
                                $emonth         =  $key->emonth;
                                $futurestatus   =  $key->futurestatus;
                                $eyear          =  $key->eyear;
                                $notdecidedstatus =  $key->notdecidedstatus;
                                $bannerimage    =  get_the_post_thumbnail_url($educatorid,'bannerimage-educators-list');
                                if(empty($bannerimage)){
                                 $bannerimage  = get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                                }
                                $futurestatus  =  $key->futurestatus;
                                if($presentstatus){
                                    $pndetails =  $syear.'- present';
                                }else{
                                    $pndetails =  $syear;
                                }

                                if($notdecidedstatus){
                                     $pndetails = 'Not Decided Yet';
                                }
                                //$eyear          =  $key->futurestatus;
                            # code...
                            ?>

                        <div id="currentloop<?php echo $formid; ?>" class="education-blockbox cf">
                                <div class="educat-leftimg"><img src="<?php echo $bannerimage; ?>" alt=""></div>
                                <div class="educat-rightcont">
                                    <div class="educat-tit">
                                        <h4><?php echo get_the_title($educatorid); ?></h4><span>(<?php echo get_the_title($courseid); ?>)</span>
                                    </div>
                                    <p> <?php echo  $Descriptions;  ?></p>
                                    <div class="educat-twobutton">
                                        <a href="#" class="present-btn"><?php echo $pndetails; ?></a>
                                        <a href="javascript:void(0)" data-id="<?php echo $formid ?>" class="educat-edit-btn eductor-deletefutrue-code">Delete</a>
                                    </div>
                                </div>
                            </div>

                            <?php
 
                        }

                    }

                    ?>

                    <div style="display:none" class="education-blockbox cf">
                        <div class="educat-leftimg"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/educaiton-img1.jpg" alt=""></div>
                        <div class="educat-rightcont">
                            <div class="educat-tit">
                                <h4>London School of Commerce</h4><span>( MBA, International Business )</span>
                            </div>
                            <p>Praesent congue suscipit velit in faucibus lorem varius id duis feugiat odio ac quam feugiat, malesuada luctus ligula luctus pellentesque a hendrerit metus phasellus nec sem id nisl tincidunt placerat Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
                            <div class="educat-twobutton">
                                <a href="#" class="present-btn">2015 - present</a>
                                <a href="#" class="educat-edit-btn">Edit</a>
                            </div>
                        </div>
                    </div>
                    <?php if(!empty($futpreveducations)){ 
                        $datastyle = 'none';
                     } ?>  
                    <div class="add-univertiy-box fut-add-univertiy-box" style="display:<?php echo $datastyle; ?>">
                        <a href="javascript:void(0)" class="add-univer-btn openeductions-informationstwo">Add where you would like to study</a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section  style="display:none" class="course-section greybg" id="booking-id" >
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox">
                <h2>Booking</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
                    <div class="three-block">
                        <div class="booking-blok">
                            <div class="booking-imgblk cf">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/booking-img1.jpg" alt="">
                                <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt="">
                                    <p>Pre-Bachelor</p>
                                </span>
                            </div>
                            <div class="booking-desbox equal-height">
                                <h3>London School of Commerce</h3>
                                <div class="small-headingbox">MBA, International Business</div>
                                <div class="course-datebox">
                                    <div class="cf"><span>Enroiment date:</span><span>09 September 2014</span></div>
                                    <div class="cf"><span>Completion date:</span><span>03 July 2016</span></div>
                                    <div class="cf"><span>Course price:</span><span>4,985.00</span></div>
                                </div>
                                <div class="policy-box">
                                    <span>Canceletion policy</span>
                                </div>
                                <div class="boking-btnlink">
                                    <div class="boking-edtlink"><a href="#" class="bok-edtlink">Edit</a></div>
                                    <div class="boking-wricou"><a href="#" class="bok-coureview">Write a course review</a></div>
                                    <div class="boking-canclebox"><a href="#" class="bok-cancle">Cancle</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="three-block">
                        <div class="booking-blok">
                            <div class="booking-imgblk cf">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/booking-img1.jpg" alt="">
                                <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt="">
                                    <p>Pre-Bachelor</p>
                                </span>
                            </div>
                            <div class="booking-desbox equal-height">
                                <h3>London School of Commerce</h3>
                                <div class="small-headingbox">MBA, International Business</div>
                                <div class="course-datebox">
                                    <div class="cf"><span>Enroiment date:</span><span>09 September 2014</span></div>
                                    <div class="cf"><span>Completion date:</span><span>03 July 2016</span></div>
                                    <div class="cf"><span>Course price:</span><span>4,985.00</span></div>
                                </div>
                                <div class="policy-box">
                                    <span>Canceletion policy</span>
                                </div>
                                <div class="boking-btnlink">
                                    <div class="boking-edtlink"><a href="#" class="bok-edtlink">Edit</a></div>
                                    <div class="boking-wricou"><a href="#" class="bok-coureview">Write a course review</a></div>
                                    <div class="boking-canclebox"><a href="#" class="bok-cancle">Cancle</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="three-block">
                        <div class="booking-blok">
                            <div class="booking-imgblk cf">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/booking-img1.jpg" alt="">
                                <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt="">
                                    <p>Pre-Bachelor</p>
                                </span>
                            </div>
                            <div class="booking-desbox equal-height">
                                <h3>London School of Commerce</h3>
                                <div class="small-headingbox">MBA, International Business</div>
                                <div class="course-datebox">
                                    <div class="cf"><span>Enroiment date:</span><span>09 September 2014</span></div>
                                    <div class="cf"><span>Completion date:</span><span>03 July 2016</span></div>
                                    <div class="cf"><span>Course price:</span><span>4,985.00</span></div>
                                </div>
                                <div class="policy-box">
                                    <span>Canceletion policy</span>
                                </div>
                                <div class="boking-btnlink">
                                    <div class="boking-edtlink"><a href="#" class="bok-edtlink">Edit</a></div>
                                    <div class="boking-wricou"><a href="#" class="bok-coureview">Write a course review</a></div>
                                    <div class="boking-canclebox"><a href="#" class="bok-cancle">Cancle</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
        $myreveiw = get_my_reveiw($userid);
        // echo "<pre>";
        // print_r($myreveiw);
        // echo "</pre>";
    ?>
    <section class="course-section whitebg" id="review-id">
        <div class="wrapper cf"  data-aos="fade-left" data-aos-duration="1500">
            <div class="top-title-contbox">
                <h2>Reviews</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
                    <?php
                        if(!empty($myreveiw)){
                        foreach ($myreveiw as $value) {
                            # code...
                             $id     =  $value['id'];
                             $educatorid     =  $value['educators_id'];
                             $courseid       =  $value['course_id'];
                             $datetetime     = $value['create_datetime'];
                             $title          = $value['Title'];
                             $overalrating   = $value['overal_rating'];
                                $overalratingarg = array(
                                'rating' =>  $overalrating,
                                'type' => 'rating',
                                'number' => 5,
                                ); 
                                
                            $bannerimages        = get_the_post_thumbnail_url($courseid ,'bannerimage-courses-list');
                            if(empty($bannerimages)){
                                $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/course-block-img1.png';
                            } 
                            ?>

                    <div class="three-block review-block-profile">
                        <div class="booking-blok">
                            <?php /*
                            <div class="booking-imgblk cf">
                             
                                <img src="<?php echo $bannerimages; ?>" alt="">
                                <span class="pre-bachelr-label"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/pre-bachlar-icon.png" alt="">
                                    <p>Course Review</p>
                                </span>
                            </div>
                            */ ?>
                            <div class="booking-desbox equal-height">
                                <h3><?php echo get_the_title( $educatorid )  ?></h3>
                                <div class="small-headingbox"><?php echo get_the_title( $courseid )  ?></div>
                                <div class="bookreview-box cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star cf">
                                             <?php wp_star_rating($overalratingarg); ?>
                                            <label class="<?php echo egobal_get_the_review_label_color($overalrating) ?> "><?php  echo egobal_get_the_review_label_two($overalrating); ?></label>
                                        </div>
                                        <div class="review-texts fullblock">
                                            <span>Reviewed 8 February 2018</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="policy-box bold-text">
                                    <span>"<?php echo $title; ?>"</span>
                                </div>
                                <div class="boking-btnlink review-btnlinks">
                                    <div class="reviewlinks"><a href="<?php echo site_url().'/review-details/?id='.$id .'#go-aboutid' ?>">About</a></div>
                                    <div class="ratinglinks"><a href="<?php echo site_url().'/review-details/?id='.$id .'#go-rating' ?>">Rating</a></div>
                                    <div class="photoslinks"><a href="<?php echo site_url().'/review-details/?id='.$id .'#go-imgid' ?>">Photos</a></div>
                                    <div class="videwoslinks"><a href="<?php echo site_url().'/review-details/?id='.$id .'#go-videoid' ?>">Video</a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="write-cour-box">
                <center>
                <a href="<?php echo site_url().'/review' ?>" class="write-btnlink">Add Review</a>
                </center>
            </div>
        </div>
    </section>


    <?php
    $userid         = get_current_user_id();
    $meta_key       = 'course_short_list';
    //$educators_list_unique = array($educatorid);
    $coursearray    = maybe_unserialize(get_user_meta($userid,$meta_key,true));
    if(!empty($coursearray)){
    ?>
    <section  id="favourites"></section>
    <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox">

                <h2>Favorite Courses</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
            <?php
                $the_query = new WP_Query( array(
                    'posts_per_page' => 3,
                    'order' => 'DESC',
                    'orderby' =>'date',
                    'post_type'     =>'course',
                    'post__in' => $coursearray,
                    
                    )
                );
                while ($the_query -> have_posts()) : $the_query -> the_post(); 
                // print_r($the_query);
                    do_action('courses_html_actions');
                endwhile;   
                    ?>
                </div>
            </div>
            <center>
            <div class="write-cour-box" >
               <center>
             <a  href="<?php echo site_url()  ?>/course-shortlist/" class="write-btnlink">
                view all</a>
            </center> 
            </div>
            </center>
        </div>
    </section>
<?php
}
$userid         = get_current_user_id();
$meta_key       = 'educator_short_list';
//$educators_list_unique = array($educatorid);
$educatorarry    = maybe_unserialize(get_user_meta($userid,$meta_key,true));
if(!empty($educatorarry)){
?>
    <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox">
                <h2>Favorite Institutions</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
             <?php
                $the_query = new WP_Query( array(
                    'posts_per_page' => 2,
                    'order' => 'DESC',
                    'orderby' =>'date',
                    'post_type'     =>'educator',
                    'post__in' => $educatorarry,
                    
                    )
                );
                while ($the_query -> have_posts()) : $the_query -> the_post(); 
                        // print_r($the_query);
                            do_action('educator_html_actions');
                endwhile;
                ?>

                </div>
            </div>
            <center>
            <div class="write-cour-box" >
             <a  href="<?php echo site_url()  ?>/educators-shortlist/" class="write-btnlink">
                view all</a>
            </div>
            </center>

        </div>
    </section>
<?php } ?>
        <?php       
        $userid         = get_current_user_id();
        $meta_key       = 'product_short_list';
        //$educators_list_unique = array($educatorid);
        $educatorarrys    = maybe_unserialize(get_user_meta($userid,$meta_key,true));
        if(!empty($educatorarrys)){
        ?>
        <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox">
                <h2>Favorite Books</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
                <?php
        
                $the_query = new WP_Query( array(
                    'posts_per_page' => 4,
                    'order' => 'DESC',
                    'orderby' =>'date',
                    'post_type'     =>'product',
                    'post__in' => $educatorarrys,
                    
                    )
                );
                // echo "</pre>";
                if($the_query -> have_posts()){
                ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    $productid       = get_the_id();
                    $producttitle    = get_the_title();
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'bannerimage-product-list');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                    $countview       = pvc_get_post_views($productid);
                    $bestseller      = get_field('best_seller',$productid);

                    $productcontent  = get_the_excerpt();
                    $terms           = get_the_terms($productid,'product-categories');
                    if(!empty($terms)){
                    $categoryname    = $termsname = $terms[0]->name;
                    $termsid         = $terms[0]->term_id;
                    // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    }
                    $productprice    = get_field('product_price',get_the_id());
                    //Rating Code

                    //Review Sections  Code
                    global $wpdb;
                    $table_name = $wpdb->prefix . "review";
                    $totalproductreviws = 0;
                    $args = '';
                    $avarage =0;
                    $totalaverage =0;
                    $productview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE 
                        status = 1 AND `product_id` = ".$productid );
                    if(!empty($productview)){
                        $totalproductreviws =  count($productview);
                        $totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE  status = 1 AND `product_id` =".$productid);
                        $avarage                    = $totalrating->avarage;
                        $totalaverage               = wp_star_rating_avarage_float($avarage);
                    }
                      $args = array(
                       'rating' => $avarage,
                       'type' => 'rating',
                       'number' => 5,
                    );
                    ?>
                    <div class="three-block fourblock pagepro-block">
                        <div class="white-bg ourcourse-block">
                            <div class="course-imgblock">
                                <span class="imgmain-box"><img src="<?php echo $bannerimages; ?>" alt=""></span>
                                <?php if( !empty($bestseller)){ ?>
                                <div class="bachlabl-leftbox">
                                    <span class="pre-bachelr-label">
                                          <p>Bestseller</p>
                                    </span>
                                </div> 
                                <?php } ?> 

                                <div class="inner-titwid-img">
                                   <div class="ourcourse-contbox cf">
                                       <h4><?php echo $producttitle;?></h4>
                                        <?php if(!empty($productcontent)){ ?>
                                        <p><?php echo substr($productcontent,'0','172').'..'; ?> </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php  echo do_action('product_short_list_actions'); ?>

                            <div class="our-courseblk-content">
                                <a href="<?php echo get_permalink(); ?>"><h3><?php echo $producttitle;?> </h3></a>
                                <?php if(!empty($categoryname)) { ?>
                                    <div class="ourcor-smlcont-box"><?php echo $categoryname; ?> 
                                    </div>
                                <?php } ?>
                                <div class="cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                            <a class="ratenum-like" href="javascript:void(0)" title="like">
                                                <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                            </a>
                                        </div>
                                        <?php if(!empty($totalproductreviws)){ ?>
                                        <div class="review-texts">
                                            <span><?php echo $totalproductreviws; ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="year-view-box">
                                    <?php if(!empty($productprice)) { ?>
                                    <span class="rate-leftbox">From <i>£<?php echo $productprice; ?> </i></span>
                                    <?php } ?>

                                    <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assettwo/images/view-ico.png">views: <i><?php echo $countview; ?></i></span>
                                </div>
                            </div>
                        </div>
                </div>
                <?php  endwhile; ?>
                <?php }else{
                    echo "No Products Founds";
                    }
                    wp_reset_postdata(); ?>

                </div>
            </div>
            <center>
                <div class="write-cour-box" >
                <a  href="<?php echo site_url()  ?>/book-shortlist/" class="write-btnlink">
                view all</a>
           
                </div>
            </center>
        </div>
    </section>
    <?php 
    }
    $orderhistoryarry  = get_my_orderhistory();
    if(!empty($orderhistoryarry)){ 
    ?>
     <section class="course-section greybg" id="shortlist-id" style="display:none">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox">
                 <h2>Books History</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
                <?php
        
                $the_query = new WP_Query( array(
                    'posts_per_page' => 4,
                    'order' => 'DESC',
                    'orderby' =>'date',
                    'post_type'     =>'product',
                    'post__in' => $orderhistoryarry,
                    
                    )
                );
                // echo "</pre>";
                if($the_query -> have_posts()){
                ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    $productid       = get_the_id();
                    $producttitle    = get_the_title();
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'bannerimage-product-list');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                    $countview       = pvc_get_post_views($productid);
                    $bestseller      = get_field('best_seller',$productid);

                    $productcontent  = get_the_excerpt();
                    $terms           = get_the_terms($productid,'product-categories');
                    if(!empty($terms)){
                    $categoryname    = $termsname = $terms[0]->name;
                    $termsid         = $terms[0]->term_id;
                    // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    }
                    $productprice    = get_field('product_price',get_the_id());
                    //Rating Code

                    //Review Sections  Code
                    global $wpdb;
                    $table_name = $wpdb->prefix . "review";
                    $totalproductreviws = 0;
                    $args = '';
                    $avarage =0;
                    $totalaverage =0;
                    $productview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE 
                        status = 1 AND `product_id` = ".$productid );
                    if(!empty($productview)){
                        $totalproductreviws =  count($productview);
                        $totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE  status = 1 AND `product_id` =".$productid);
                        $avarage                    = $totalrating->avarage;
                        $totalaverage               = wp_star_rating_avarage_float($avarage);
                    }
                      $args = array(
                       'rating' => $avarage,
                       'type' => 'rating',
                       'number' => 5,
                    );
                    ?>
                    <div class="three-block fourblock pagepro-block">
                        <div class="white-bg ourcourse-block">
                            <div class="course-imgblock">
                                <span class="imgmain-box"><img src="<?php echo $bannerimages; ?>" alt=""></span>
                                <?php if( !empty($bestseller)){ ?>
                                <div class="bachlabl-leftbox">
                                    <span class="pre-bachelr-label">
                                          <p>Bestseller</p>
                                    </span>
                                </div> 
                                <?php } ?> 

                                <div class="inner-titwid-img">
                                   <div class="ourcourse-contbox cf">
                                       <h4><?php echo $producttitle;?></h4>
                                        <?php if(!empty($productcontent)){ ?>
                                        <p><?php echo substr($productcontent,'0','172').'..'; ?> </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php  echo do_action('product_short_list_actions'); ?>

                            <div class="our-courseblk-content">
                                <a href="<?php echo get_permalink(); ?>"><h3><?php echo $producttitle;?> </h3></a>
                                <?php if(!empty($categoryname)) { ?>
                                    <div class="ourcor-smlcont-box"><?php echo $categoryname; ?> 
                                    </div>
                                <?php } ?>
                                <div class="cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                            <a class="ratenum-like" href="javascript:void(0)" title="like">
                                                <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                            </a>
                                        </div>
                                        <?php if(!empty($totalproductreviws)){ ?>
                                        <div class="review-texts">
                                            <span><?php echo $totalproductreviws; ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="year-view-box">
                                    <?php if(!empty($productprice)) { ?>
                                    <span class="rate-leftbox">From <i>£<?php echo $productprice; ?> </i></span>
                                    <?php } ?>

                                    <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assettwo/images/view-ico.png">views: <i><?php echo $countview; ?></i></span>
                                </div>
                            </div>
                        </div>
                </div>
                <?php  endwhile; ?>
                <?php }else{
                    echo "No Products Founds";
                    }
                    wp_reset_postdata(); ?>

                </div>
            </div>
            <center>
                <div class="write-cour-box" >
                <a  href="<?php echo site_url()  ?>/product-shortlist/" class="write-btnlink">
                view all</a>
           
                </div>
            </center>
        </div>
    </section>


<?php } ?>
        <?php 
        $orderhistoryarry  = get_my_orderhistory();
        if(!empty($orderhistoryarry)){ 
        ?>
        <section class="course-section greybg" id="shortlist-id">
        <div class="wrapper cf"  data-aos="fade-right" data-aos-duration="1500">
            <div class="top-title-contbox">
                <h2>Books History</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br />interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="booking-mainbox">
                <div class="rows cf">
                <?php
        
                $the_query = new WP_Query( array(
                    'posts_per_page' => 4,
                    'order' => 'DESC',
                    'orderby' =>'date',
                    'post_type'     =>'product',
                    'post__in' =>  $orderhistoryarry ,
                    
                    )
                );
                // echo "</pre>";
                if($the_query -> have_posts()){
                ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    $productid       = get_the_id();
                    $producttitle    = get_the_title();
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'bannerimage-product-list');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                    $countview       = pvc_get_post_views($productid);
                    $bestseller      = get_field('best_seller',$productid);

                    $productcontent  = get_the_excerpt();
                    $terms           = get_the_terms($productid,'product-categories');
                    if(!empty($terms)){
                    $categoryname    = $termsname = $terms[0]->name;
                    $termsid         = $terms[0]->term_id;
                    // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    }
                    $productprice    = get_field('product_price',get_the_id());
                    //Rating Code

                    //Review Sections  Code
                    global $wpdb;
                    $table_name = $wpdb->prefix . "review";
                    $totalproductreviws = 0;
                    $args = '';
                    $avarage =0;
                    $totalaverage =0;
                    $productview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE 
                        status = 1 AND `product_id` = ".$productid );
                    if(!empty($productview)){
                    $totalproductreviws =  count($productview);
                    $totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE  status = 1 AND `product_id` =".$productid);
                    $avarage                    = $totalrating->avarage;
                    $totalaverage               = wp_star_rating_avarage_float($avarage);
                    }
                      $args = array(
                       'rating' => $avarage,
                       'type' => 'rating',
                       'number' => 5,
                    );
                $downloadbookurls = get_field('product_pdf',$productid );
                ?>
                    <div class="three-block fourblock pagepro-block">
                        <div class="white-bg ourcourse-block">
                            <div class="course-imgblock">
                                <span class="imgmain-box"><img src="<?php echo $bannerimages; ?>" alt=""></span>
                                <?php if( !empty($bestseller)){ ?>
                                <div class="bachlabl-leftbox">
                                    <span class="pre-bachelr-label">
                                          <p>Bestseller</p>
                                    </span>
                                </div> 
                                <?php } ?> 

                                <div class="inner-titwid-img">
                                   <div class="ourcourse-contbox cf">
                                       <h4><?php echo $producttitle;?></h4>
                                        <?php if(!empty($productcontent)){ ?>
                                        <p><?php echo substr($productcontent,'0','172').'..'; ?> </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php  echo do_action('product_short_list_actions'); ?>

                            <div class="our-courseblk-content">
                                <a href="<?php echo get_permalink(); ?>"><h3><?php echo $producttitle;?> </h3></a>
                                <?php if(!empty($categoryname)) { ?>
                                    <div class="ourcor-smlcont-box"><?php echo $categoryname; ?> 
                                    </div>
                                <?php } ?>
                                <div class="cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                            <a class="ratenum-like" href="javascript:void(0)" title="like">
                                                <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                            </a>
                                        </div>
                                        <?php if(!empty($totalproductreviws)){ ?>
                                        <div class="review-texts">
                                            <span><?php echo $totalproductreviws; ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                         <div class="order-history-add inqri-and-addcartbox cf">
                            <?php if(is_user_logged_in()){
                                        
                            ?>
                            <?php if(!empty($downloadbookurls)) { ?>
                            <a  download title="download" class="download-pdf<?php echo $productid; ?>"  href="<?php echo $downloadbookurls; ?>" > <input type="submit" href="javascript:void(0)" class="btn booknow-btn" value="Download" name="submit"> </a>  
                            <?php } } ?>

                            <a style="display:none" href="#" class="addto-cartlink"><i class="fa fa-shopping-cart"></i>Add to Cart</a>
                            <a style="display:none" href="#" class="enqure-link">Enqure now</a>
                            <a  href="<?php echo site_url().'/book-review/?reviewid='.$productid.''?>" class="reveiw-btn-add write-btnlink">Add Review</a>
                        </div>
                                <div class="year-view-box">
                                    <?php if(!empty($productprice)) { ?>
                                    <span class="rate-leftbox">From <i>£<?php echo $productprice; ?> </i></span>
                                    <?php } ?>

                                    <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assettwo/images/view-ico.png">views: <i><?php echo $countview; ?></i></span>
                                </div>
                            </div>
                        </div>
                </div>
                <?php  endwhile; ?>
                <?php }else{
                    echo "No Products Founds";
                    }
                    wp_reset_postdata(); ?>

                </div>
            </div>
   
        </div>
    </section>
<?php } ?>
    <section class="wpcf7 form-design-comon paddingbox"  style="display:none;" >
               <div class="wrapper maxwidth-form cf">
                    <form  method="post" id="review-informtions-form" 
                    enctype="multipart/form-data">
                        <div class="input-name">
                            <label>Select Educator Name: </label>
                            <?php
                            $universityposts = get_posts(
                            array(
                            'post_type' => 'educator', // Post type
                            'posts_per_page' => -1,
                            'orderby'       => 'menu_order',
                            'order'         => 'ASC'
                            )
                            );
                            ?>

                            <select name="educator-name" id="educators-drodown" class="form-control chosen-serach-dropdown">

                                    <option value=" ">Institution name</option>
                                    <?php
                                     $selected ='';
                                    if(!empty($universityposts)){
                                        foreach ($universityposts as $postvalue) {
                                            $universitysessisonid = '';
                                            // if(!empty($_SESSION['educatorid'])){
                                            //    echo  $universitysessisonid = $_SESSION['educatorid'];
                                            // }

                                            if($universitysessisonid == $postvalue->ID){
                                                 $selected = 'selected= selected';
                                            }
                                            # code...
                                            echo '<option value="'.$postvalue->ID.'" '.$selected.'>'.$postvalue->post_title.'</option>'
                                          
                                            ?>
                                            <?php

                                        }
                                    }
                                    ?> 
                      
                                 </select>
                         </div> 
                        <div class="input-name">
                            <label>Course Name: </label>
                            <?php
                            $universityposts = get_posts(
                            array(
                            'post_type' => 'course', // Post type
                            'posts_per_page' => -1,
                            'orderby'       => 'menu_order',
                            'order'         => 'ASC'
                            )
                            );
                            ?>
                            <select name="course-name" id="course-drodown" class="form-control chosen-serach-dropdown">
                                    <option value=" ">Select Course</option>
                                    <?php
                                     $selected ='';
                                    if(!empty($universityposts)){
                                        foreach ($universityposts as $postvalue) {
                                            $universitysessisonid = '';
                                            // if(!empty($_SESSION['educatorid'])){
                                            //    echo  $universitysessisonid = $_SESSION['educatorid'];
                                            // }

                                            if($universitysessisonid == $postvalue->ID){
                                                 $selected = 'selected= selected';
                                            }
                                            # code...
                                            echo '<option value="'.$postvalue->ID.'" '.$selected.'>'.$postvalue->post_title.'</option>'
                                          
                                            ?>
                                            <?php

                                        }
                                    }
                                    ?> 
                      
                                 </select>
                         </div> 
                          <div class="input-name">
                            <label>Other Course : </label>
                            <input type="text" name="other">
                         </div>  
                        <div class="input-name">
                            <label>Add review title: </label>    
                            <input   class="form-control review-title-form" type="text" name="review-title" value="<?php echo $user_phone; ?>" >
                            <input type="hidden" name="action" value="update_submit-reveiew-sections">
                         </div>
                        <div class="input-name">
                            <label>Write your opinion: </label> 
                            <textarea class="form-control user-opinion"  name="address"><?php echo $user_address; ?></textarea>
                        </div>
                        <div class="input-name right-gorating">
                            <label> select Overal rating: </label> 
                            <div class="rate-star">
                                <span class="like-count overal-rating-js">
                                    <i data-rate='1' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='2' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='3' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='4' class="overal-rating fa fa-star-o"></i>
                                    <i data-rate='5' class="overal-rating fa fa-star-o"></i>
                                </span>
                                <div class="overal-rating-label">Good</div>
                            </div>
                        
                        </div>
                       <div class="input-name cf">
                            <label> select More rating: </label> 
                            <div class="sub-input-name righ-ratingmainbox">
                               <div class="cf">
                                    <label> Teaching Quality: </label> 
                                    <div class="rate-star">
                                        <span class="like-count overal-teach-quality-js">
                                            <i data-rate='1' class="overal-tq-rating fa fa-star-o"></i>
                                            <i data-rate='2' class="overal-tq-rating fa fa-star-o"></i>
                                            <i data-rate='3' class="overal-tq-rating fa fa-star-o"></i>
                                            <i data-rate='4' class="overal-tq-rating fa fa-star-o"></i>
                                            <i data-rate='5' class="overal-tq-rating fa fa-star-o"></i>
                                        </span>
                                       <div class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                                <div class="cf">
                                    <label> School Facilties: </label> 
                                    <div class="rate-star">
                                        <span class="like-count overal-school-facilties-js">
                                            <i data-rate='1' class="overal-sf-rating fa fa-star-o"></i>
                                            <i data-rate='2' class="overal-sf-rating fa fa-star-o"></i>
                                            <i data-rate='3' class="overal-sf-rating fa fa-star-o"></i>
                                            <i data-rate='4' class="overal-sf-rating fa fa-star-o"></i>
                                            <i data-rate='5' class="overal-sf-rating fa fa-star-o"></i>
                                        </span>
                                        <div class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                                <div class="cf">
                                    <label> Locations: </label> 
                                    <div class="rate-star">
                                            <span class="like-count overal-locations-js">
                                                <i data-rate='1' class="overal-ol-rating fa fa-star-o"></i>
                                                <i data-rate='2' class="overal-ol-rating fa fa-star-o"></i>
                                                <i data-rate='3' class="overal-ol-rating fa fa-star-o"></i>
                                                <i data-rate='4' class="overal-ol-rating fa fa-star-o"></i>
                                                <i data-rate='5' class="overal-ol-rating fa fa-star-o"></i>
                                            </span>
                                            <div class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                                <div class="cf">
                                    <label> Value for Money : </label> 
                                    <div class="rate-star">
                                            <span class="like-count overal-value-money-js">
                                                <i data-rate='1' class="overal-vm-rating fa fa-star-o"></i>
                                                <i data-rate='2' class="overal-vm-rating fa fa-star-o"></i>
                                                <i data-rate='3' class="overal-vm-rating fa fa-star-o"></i>
                                                <i data-rate='4' class="overal-vm-rating fa fa-star-o"></i>
                                                <i data-rate='5' class="overal-vm-rating fa fa-star-o"></i>
                                            </span>
                                            <div class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                                <div class="cf">
                                    <label> Career assistance: </label> 
                                    <div class="rate-star">
                                            <span class="like-count overal-career-js">
                                                <i data-rate='1' class="overal-ca-rating fa fa-star-o"></i>
                                                <i data-rate='2' class="overal-ca-rating fa fa-star-o"></i>
                                                <i data-rate='3' class="overal-ca-rating fa fa-star-o"></i>
                                                <i data-rate='4' class="overal-ca-rating fa fa-star-o"></i>
                                                <i data-rate='5' class="overal-ca-rating fa fa-star-o"></i>
                                            </span>
                                            <div class="overal-rating-labe">Good</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-name">
                            <label>Add Photo:</label>
                            <input class="form-control" type="file" id="review_images" name="files[]" multiple="multiple"/>
                         </div>
                          <div class="input-name">
                            <label>Add Video:</label>
                            <input class="form-control" type="file" name="review-video[]" multiple="multiple" >
                          </div>
                          <div class="Submit-btn">
                              <input  class="submit-review-infomations"  type="button" value="Submit">
                        </div>
                    </form> 
                </div>
        </section>
    <!-- Popup Code Html -->
    <div id="personal-informtions" class="enquiry-form-popup myprofile-informations">
         <div class="maxwidth-box">
             <div class="padding whitecolor">
                 <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                    <div class="wpcf7 form-design-comon" >
                    <form  method="post" id="personal-informtions-form">
                      <input type="hidden" name="action" value="update_personal_informations" />
                        <div class="input-name">
                            <label>First name: </label>
                            <input class="form-control" type="text" name="firstname" value="<?php echo  $user_firstname; ?>" >    
                         </div> 
                        <div class="input-name">
                            <label>Last name: </label>
                            <input class="form-control" type="text" name="lastname" value="<?php echo  $user_lastname; ?>" >
                         </div>   
                        <div class="input-name">
                            <label>Email: </label>    
                            <input class="form-control" readonly="readonly" disabled type="email" name="email"  value="<?php echo $user_email;   ?>">
                         </div>   
                        <div class="input-name">
                        <label>Date of Birth : </label>   
                        <?php $user_dateofbirth =  get_user_meta( $user_id,'dateofbirth',true); ?> 
                        <input  class="form-control" type="date" name="date-of-birth" 
                        value="<?php echo $user_dateofbirth;  ?>" class="wpcf7-form-control wpcf7-date wpcf7-validates-as-date" >         
                        </div>
                        <div class="input-name">
                            <label>Phone: </label>    
                            <input  class="form-control" type="text" name="phone" value="<?php echo $user_phone; ?>" >
                         </div>
                        <div class="input-name">
                            <label>Gender: </label> 
                            <span class="from-span-class"><input type="radio"  name="gender" value="male" <?php if($user_gender == 'male'){ echo "checked"; } ?> > Male</span> 
                            <span  class="from-span-class"><input type="radio"  name="gender" value="female" <?php if($user_gender == 'female'){ echo "checked"; } ?> > Female</span> 
                        </div>    
                        <div class="input-name">
                            <label>Address: </label> 
                            <textarea class="form-control"  name="address"><?php echo $user_address; ?></textarea>
                        </div>
                        <div class="input-name">
                            <label> Zip code: </label>
                                <input class="form-control" type="text" name="zipcode" value="<?php echo $user_zipcode; ?>" > 

                        </div>
                        <div class="input-name">
                            <label>Town/city:</label>  
                            <input class="form-control" type="text" name="town-city" value="<?php echo $user_town_or_city; ?>">
                         </div>
                          <div class="input-name">
                            <label>Country:</label>
                            <input class="form-control" type="text" name="country" value="<?php echo $user_country; ?>" >
                          </div>
                          <div class="Submit-btn">
                              <input  class="submit-personal-infomations"  type="button" value="Submit">
                        </div>
                        <span class="success-alert personal-success-message" style="display:none"></span>
                    </form> 
                   </div> 
             </div>    
      </div>
    </div>
   <div id="personal-informtions-social" class="enquiry-form-popup myprofile-informations">
         <div class="maxwidth-box">
             <div class="padding whitecolor">
                 <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                    <div class="wpcf7 form-design-comon">
                    <form  method="post" id="social-informtions-forms">
                       <input type="hidden" name="action" value="update_socialmedia_informations" />
                        <div class="input-name">
                            <label>Face Book Url: </label>
                            <input class="form-control" type="text" name="facebookurls" 
                            value="<?php echo $user_facebookurl; ?>" >
                         </div>   
                        <div class="input-name">
                            <label>Twitter Url : </label>    
                            <input  class="form-control" type="text" name="twitterurls" 
                            value="<?php echo $use_twitterurl; ?>" >      
                        </div>
                        <div class="input-name">
                            <label>Linkedin Url: </label>    
                            <input class="form-control" type="text" name="linkiedinurl" 
                            value="<?php echo $use_linkdinurl; ?>" >
                         </div>
                        <div class="input-name">
                            <label>Google url: </label>    
                            <input  class="form-control" type="text" name="googleurls" 
                            value="<?php echo $use_googleurl; ?>" >
                        </div>    
                        <div class="Submit-btn">
                          <input  class="submit-socialmedia-infomations submit-socialmedia-infomations-click"  type="button" value="Submit">
                        </div>
                        <span class="success-alert social-success-message" style="display:none"></span>
                    </form> 
                   </div> 
             </div>    
      </div>
    </div> 
       <div id="personal-informtions-eductions" class="enquiry-form-popup myprofile-educations-informations">
         <div class="maxwidth-box">
             <div class="padding whitecolor">
                    <div class="header-titheaing-box cur-prve-heading"  >
                        <h4>Add University And School You Attend</h4>
                    </div>
                    <div class="header-titheaing-box fure-prve-heading" >
                        <h4>Where would you like to study</h4>
                    </div>
                 <div class="close-popbox"><a href="javascript:void(0);" class="clspopup"><i class="fa fa-times"></i></a></div>
                    <div class="wpcf7 form-design-comon">
                    <form  method="post" class="curt-prev-educations-forms">
                       <input type="hidden" name="action" value="update_cpeducations_informations" />
                       <input type="hidden" class="other-value" name="other-value" value="">
                       <input type="hidden" class="future-data-val" name="fut-data" value="">
                        <div class="input-name fu-country-name">
                            <?php 
                                $country_args = array(
                                'orderby'       => 'name', 
                                'order'         => 'ASC',
                                'parent'        => 0,
                                'hide_empty'    => true, 
                                );
                                $countrylist = get_terms('Country',$country_args);
                            ?>
                            <label>Country : </label>
                            <select id="future-data-country" name='fu-country-id' class="profile-country-select-dropdown  form-control">
                                <option value="">Country</option>
                                <?php
                                foreach ($countrylist as $country ) {
                                # code...
                                    echo '<option value="'.$country->slug.'">'.$country->name.'</option>';
                                }   
                                ?>
                            </select>
                            <span class="error error-country" style="display:none">this filed is required</span>
                         </div>
                        <div class="input-name ac-country-name">
                            <?php 
                                $country_args = array(
                                'orderby'       => 'name', 
                                'order'         => 'ASC',
                                'parent'        => 0,
                                'hide_empty'    => false, 
                                );
                                $countrylist = get_terms('Country',$country_args);
                            ?>
                            <label>Country : </label>
                            <select id="current-data-country" name='country-id ' class="profile-country-select-dropdown  form-control">
                                <option value="">Country</option>
                                <?php
                                foreach ($countrylist as $country ) {
                                # code...
                                    echo '<option value="'.$country->slug.'">'.$country->name.'</option>';
                                }   
                                ?>
                                 <option value="Afghanistan">Afghanistan</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="American Samoa">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="Anguilla">Anguilla</option>
                                <option value="Antartica">Antarctica</option>
                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="Aruba">Aruba</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                                <option value="Bermuda">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                <option value="Botswana">Botswana</option>
                                <option value="Bouvet Island">Bouvet Island</option>
                                <option value="Brazil">Brazil</option>
                                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Burundi">Burundi</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cayman Islands">Cayman Islands</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Chad">Chad</option>
                                <option value="Chile">Chile</option>
                                <option value="China">China</option>
                                <option value="Christmas Island">Christmas Island</option>
                                <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Comoros">Comoros</option>
                                <option value="Congo">Congo</option>
                                <option value="Congo">Congo, the Democratic Republic of the</option>
                                <option value="Cook Islands">Cook Islands</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                <option value="Croatia">Croatia (Hrvatska)</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Czech Republic">Czech Republic</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Djibouti">Djibouti</option>
                                <option value="Dominica">Dominica</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="East Timor">East Timor</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Egypt">Egypt</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                <option value="Eritrea">Eritrea</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                <option value="Faroe Islands">Faroe Islands</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Finland">Finland</option>
                                <option value="France Metropolitan">France, Metropolitan</option>
                                <option value="French Guiana">French Guiana</option>
                                <option value="French Polynesia">French Polynesia</option>
                                <option value="French Southern Territories">French Southern Territories</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Gambia">Gambia</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Ghana">Ghana</option>
                                <option value="Gibraltar">Gibraltar</option>
                                <option value="Greece">Greece</option>
                                <option value="Greenland">Greenland</option>
                                <option value="Grenada">Grenada</option>
                                <option value="Guadeloupe">Guadeloupe</option>
                                <option value="Guam">Guam</option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="Guinea">Guinea</option>
                                <option value="Guinea-Bissau">Guinea-Bissau</option>
                                <option value="Guyana">Guyana</option>
                                <option value="Haiti">Haiti</option>
                                <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                <option value="Holy See">Holy See (Vatican City State)</option>
                                <option value="Honduras">Honduras</option>
                                <option value="Hong Kong">Hong Kong</option>
                                <option value="Hungary">Hungary</option>
                                <option value="Iceland">Iceland</option>
                                <option value="India">India</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Iran">Iran (Islamic Republic of)</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Ireland">Ireland</option>
                                <option value="Israel">Israel</option>
                                <option value="Italy">Italy</option>
                                <option value="Jamaica">Jamaica</option>
                                <option value="Japan">Japan</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="Kenya">Kenya</option>
                                <option value="Kiribati">Kiribati</option>
                                <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                <option value="Korea">Korea, Republic of</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Lao">Lao People's Democratic Republic</option>
                                <option value="Latvia">Latvia</option>
                                <option value="Lebanon" >Lebanon</option>
                                <option value="Lesotho">Lesotho</option>
                                <option value="Liberia">Liberia</option>
                                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                <option value="Liechtenstein">Liechtenstein</option>
                                <option value="Lithuania">Lithuania</option>
                                <option value="Luxembourg">Luxembourg</option>
                                <option value="Macau">Macau</option>
                                <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                <option value="Madagascar">Madagascar</option>
                                <option value="Malawi">Malawi</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Maldives">Maldives</option>
                                <option value="Mali">Mali</option>
                                <option value="Malta">Malta</option>
                                <option value="Marshall Islands">Marshall Islands</option>
                                <option value="Martinique">Martinique</option>
                                <option value="Mauritania">Mauritania</option>
                                <option value="Mauritius">Mauritius</option>
                                <option value="Mayotte">Mayotte</option>
                                <option value="Mexico">Mexico</option>
                                <option value="Micronesia">Micronesia, Federated States of</option>
                                <option value="Moldova">Moldova, Republic of</option>
                                <option value="Monaco">Monaco</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Montserrat">Montserrat</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Mozambique">Mozambique</option>
                                <option value="Myanmar">Myanmar</option>
                                <option value="Namibia">Namibia</option>
                                <option value="Nauru">Nauru</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Netherlands">Netherlands</option>
                                <option value="Netherlands Antilles">Netherlands Antilles</option>
                                <option value="New Caledonia">New Caledonia</option>
                                <option value="New Zealand">New Zealand</option>
                                <option value="Nicaragua">Nicaragua</option>
                                <option value="Niger">Niger</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="Niue">Niue</option>
                                <option value="Norfolk Island">Norfolk Island</option>
                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                <option value="Norway">Norway</option>
                                <option value="Oman">Oman</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="Palau">Palau</option>
                                <option value="Panama">Panama</option>
                                <option value="Papua New Guinea">Papua New Guinea</option>
                                <option value="Paraguay">Paraguay</option>
                                <option value="Peru">Peru</option>
                                <option value="Philippines">Philippines</option>
                                <option value="Pitcairn">Pitcairn</option>
                                <option value="Poland">Poland</option>
                                <option value="Portugal">Portugal</option>
                                <option value="Puerto Rico">Puerto Rico</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Reunion">Reunion</option>
                                <option value="Romania">Romania</option>
                                <option value="Russia">Russian Federation</option>
                                <option value="Rwanda">Rwanda</option>
                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
                                <option value="Saint LUCIA">Saint LUCIA</option>
                                <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                <option value="Samoa">Samoa</option>
                                <option value="San Marino">San Marino</option>
                                <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Senegal">Senegal</option>
                                <option value="Seychelles">Seychelles</option>
                                <option value="Sierra">Sierra Leone</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                <option value="Slovenia">Slovenia</option>
                                <option value="Solomon Islands">Solomon Islands</option>
                                <option value="Somalia">Somalia</option>
                                <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                <option value="Span">Spain</option>
                                <option value="SriLanka">Sri Lanka</option>
                                <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                <option value="Sudan">Sudan</option>
                                <option value="Suriname">Suriname</option>
                                <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                <option value="Swaziland">Swaziland</option>
                                <option value="Sweden">Sweden</option>
                                <option value="Switzerland">Switzerland</option>
                                <option value="Syria">Syrian Arab Republic</option>
                                <option value="Taiwan">Taiwan, Province of China</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Tanzania">Tanzania, United Republic of</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Togo">Togo</option>
                                <option value="Tokelau">Tokelau</option>
                                <option value="Tonga">Tonga</option>
                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                <option value="Tunisia">Tunisia</option>
                                <option value="Turkey">Turkey</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                <option value="Tuvalu">Tuvalu</option>
                                <option value="Uganda">Uganda</option>
                                <option value="Ukraine">Ukraine</option>
                                <option value="United Arab Emirates">United Arab Emirates</option>
                                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                <option value="Uruguay">Uruguay</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="Vanuatu">Vanuatu</option>
                                <option value="Venezuela">Venezuela</option>
                                <option value="Vietnam">Viet Nam</option>
                                <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                <option value="Western Sahara">Western Sahara</option>
                                <option value="Yemen">Yemen</option>
                                <option value="Yugoslavia">Yugoslavia</option>
                                <option value="Zambia">Zambia</option>
                                <option value="Zimbabwe">Zimbabwe</option>
                                <option value="other">Other</option>
                            </select>
                            <span class="error error-country" style="display:none">this filed is required</span>
                         </div>
                        <div  class="input-name other-country" style="display:none;" >
                            <label>Other Country : </label>
                            <input   type="text" class="other-coutry-value form-control"  name="other-country"  >
                            <span class="error error-country" style="display:none">this filed is required</span>
                         </div>    
                        <div class="input-name ac-eductaor-name" >
                            <label>Edu. Institutes : </label> 
                                 <select disabled="disabled" name='educator-id' class="profile-educator-select-dropdown  form-control">
                                 </select>   
                                     <span class="error error-edu" style="display:none">this filed is required</span>
                        </div>
                        <div class="input-name educator-name-other" style="display:none">
                            <label>Educator Other Name : </label> 
                            <input type="text" class="other-educator-name form-control"   name="other-educator"> 
                            <span class="error error-edu" style="display:none">this filed is required</span>
                        </div>
                        <div class="input-name" style="display:none">
                            <label>Study Level : </label>    
                            <?php
                            /*
                            $study_args = array(
                            'orderby'       => 'name', 
                            'order'         => 'ASC',
                            'parent'        => 0,
                            'hide_empty'    => true, 

                            );

                            $studylevllist = get_terms('studylevel',$study_args);

                            ?>
                                <select name='studylevel-id' class="form-control">
                                         <option value="">Study level</option>
                                        <?php
                                        foreach ($studylevllist as $studylevel ) {
                                        # code...
                                        echo '<option value="'.$studylevel->term_id.'">'.$studylevel->name.'</option>';
                                        }   
                                        ?>
                                 </select>
                                 */ ?>
                         </div>
                        <div class="input-name ac-course-name">
                            <label>Course Name : </label> 
                            <select  disabled="disabled" name='course-id' class="profile-course-select-dropdown  form-control">
                            </select>
                            <span class="error error-course" style="display:none">this filed is required</span>
                        </div>
                        <div class="input-name other-course-name">
                            <label>Other Course Name : </label> 
                            <input type="text" class="other-course-name-val form-control"  name="other-coursename"> 
                            <span class="error error-course" style="display:none">this filed is required</span>
                        </div>

                        <?php /*
                        <div class="input-name" >
                            <label>Edu. Institutes  Other: </label> 
                            <input class="form-control" class="educator-other-name" type="text" name="educator-other-name" value="">
                             <span class="error error-edu" style="display:none">this filed is required</span>
                        </div>
                         <div class="input-name">
                            <label>Course Name  Other: </label> 
                            <input class="form-control" class="course-other-name" type="text" name="course-other-name" value="">
                            <span class="error error-course" style="display:none">this filed is required</span>
                        </div>
                        */ ?>
                         <div class="input-name">
                            <label>Descriptions : </label> 
                            <textarea name="course-descriptions" class="form-control"></textarea>
                         </div>
                        <div class="input-name" style="display:none">
                            <label>Subject Area</label> 
                            <?php
                            /*
                            $subject_args = array(
                            'orderby'       => 'name', 
                            'order'         => 'ASC',
                            'parent'        => 0,
                            'hide_empty'    => true, 

                            );

                            $subjectlist = get_terms('subject',$subject_args);

                            ?>
                            <select name='subject-id' class="form-control">
                            <option value="">Subject area</option>
                            <?php
                            foreach ($subjectlist as $subject ) {
                            # code...
                            echo '<option value="'.$subject->term_id.'">'.$subject->name.'</option>';
                            }   
                            ?>
                            </select>
                            */ ?>
                         </div>

                        <div class="input-name cf">
                            <label>Starting To : </label>    
                            <div class="rit-dropdow-box">
                                <div class="dropdn-inline-box">
                                    <?php
                              $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'Jun', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                                    ?>
                                    <select name="s-month"  class="form-control">
                                        <?php
                                        foreach ($months as $num => $name) {
                                        printf('<option value="%u">%s</option>', $num, $name);
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php 
                                    $years = array_combine(range(date("Y"), 1980), range(date("Y"), 1980));
                                   $yearstwo = array_combine(range(2019, 2050), range(date("Y"), 2050));
                                ?>
                                <div class="dropdn-inline-box current-star-down" >
                                        <select name="s-year"  class="cuurent-start-box form-control">
                                        <?php
                                        foreach ($years as $num => $name) {
                                        printf('<option value="%u">%s</option>', $num, $name);
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="dropdn-inline-box future-start-box" style="display:none">
                                     <select name="sf-year"  class="cuurent-start-box form-control">
                                        <?php
                                        foreach ($yearstwo as $num => $name) {
                                            printf('<option value="%u">%s</option>', $num, $name);
                                        }
                                        ?>
                                      </select>  
                                </div>
                                <div class="dropdn-inline-box not-decided-box" style="display:none">
                                    <input value="1"  name="notdecided" type="checkbox"><span>I have not decided yet</span>
                                </div>
                            </div>
                        </div>    
                        <div class="input-name starting-form-data cf">
                            <label>Starting Form : </label>    
                            <div class="rit-dropdow-box">
                                <div class="dropdn-inline-box">
                                      <select name="e-month"  class="form-control">
                                        <?php
                                        foreach ($months as $num => $name) {
                                        printf('<option value="%u">%s</option>', $num, $name);
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="dropdn-inline-box">
                                    <select name="e-year"  class="country-wise-drodown form-control">
                                        <?php
                                        foreach ($years as $num => $name) {
                                        printf('<option value="%u">%s</option>', $num, $name);
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="dropdn-inline-box">
                                    <input value='1' class="present-status" name="present-status" type="checkbox"><span>present</span>
                                </div>
                            </div>
                        </div>    
                        <div class="Submit-btn right-align-btnlink">
                          <a href="javascript:void(0)" class="close-popbox cancel-links">Cancel</a>
                          <input  class="submit-socialmedia-infomations submit-educations-infomations"  type="button" value="Save changes">
                        </div>
                        <span class="cur-prv-success-message" style="display:none"></span>
                    </form> 
                   </div> 
             </div>    
      </div>
    </div> 


<?php 
get_footer();
?>
<script type="text/javascript">
jQuery(document).ready(function($) {
    jQuery('.other-course-name').hide(); 
    
   jQuery(".eductor-delete-code").live("click",function(){
            var deleteid = jQuery(this).data('id');
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                    'action'    : 'delete_cur_prev_eductor',
                    'deleteid'  :  deleteid,
                },
                success:function(data) {
                    // This outputs the result of the ajax request
                    if(data == 'success'){
                        jQuery('#currentloop'+deleteid).css("border", "3px solid red");
                        jQuery('#currentloop'+deleteid).fadeOut(500);
                        jQuery('#currentloop'+deleteid).fadeOut(500);
                    }else{
                        alert('Someting Problem To delete');
                    }
                    //jQuery('.profile-educator-select-dropdown').html(data);
                },
                error: function(errorThrown){
                     console.log(errorThrown);
                }
            });
});     

   jQuery(".eductor-deletefutrue-code").live("click",function(){
            var deleteid = jQuery(this).data('id');
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                    'action'    : 'delete_cur_prev_eductor',
                    'deleteid'  :  deleteid,
                },
                success:function(data) {
                    // This outputs the result of the ajax request
                    if(data == 'success'){
                        jQuery('#currentloop'+deleteid).css("border", "3px solid red");
                        jQuery('#currentloop'+deleteid).fadeOut(500);
                        jQuery('#currentloop'+deleteid).fadeOut(500);
                        setTimeout(function(){
                            jQuery('.fut-add-univertiy-box').fadeIn(500);
                          },1000);
                    }else{
                        alert('Someting Problem To delete');
                    }

                    //jQuery('.profile-educator-select-dropdown').html(data);
                },
                error: function(errorThrown){
                     console.log(errorThrown);
                }
            });
});     

jQuery(".submit-educations-infomations").on("click",function(){
            var editpersonalfrm      = $('.curt-prev-educations-forms').serialize();
            var othervalue           = jQuery('.other-value').val();
            //var  countrydropdown     = jQuery.trim(jQuery('.profile-country-select-dropdown').val());
            var  educatorid          = jQuery.trim(jQuery('.profile-educator-select-dropdown').val());
            var  courseid            = jQuery.trim(jQuery('.profile-course-select-dropdown').val());
            if(othervalue == 1){
              var countrydropdown = jQuery.trim(jQuery('#current-data-country').val());
            }else{
               var countrydropdown = jQuery.trim(jQuery('#future-data-country').val());
            }
            if(countrydropdown == 'other'){
                var countrydropdown = jQuery.trim(jQuery('.other-coutry-value').val());
                var  educatorid     = jQuery.trim(jQuery('.other-educator-name').val());
                var  courseid       = jQuery.trim(jQuery('.other-course-name-val').val());
                //var  othervalue         = jQuery.trim(jQuery('.other-value').val());
            }
            if(educatorid == 'other'){
                var  educatorid     = jQuery.trim(jQuery('.other-educator-name').val());
                var  courseid       = jQuery.trim(jQuery('.other-course-name-val').val());
            }
            if(courseid == 'other'){
                var  courseid       = jQuery.trim(jQuery('.other-course-name-val').val());
            }

            //alert(countrydropdown);
           jQuery('.error-country').hide();
           jQuery('.error-edu').hide();
           jQuery('.error-course').hide();
            if(educatorid  == '' ){
                jQuery('.error-edu').show();
            }
            if(courseid == ''){
              jQuery('.error-course').show();
            }
            if(countrydropdown == ''){
                 jQuery('.error-country').show();
            }

           if(educatorid !='' && courseid !='' &&  countrydropdown !=''){
                $.ajax({
                    asyc:false,
                    url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                    method:'Post',
                    data: editpersonalfrm,
                    success:function(data) {
                        // This outputs the result of the ajax request
                        console.log(data);
                        jQuery('.cur-prv-success-message').show('');
                        jQuery('.social-success-message').text(data);
                        if(data == 'success'){
                            setTimeout(function(){   location.reload(); }, 3000);
                        }
                    },
                    error: function(errorThrown){
                        console.log(errorThrown);
                    }
                });
            }else{
                return false;
            }
    });
    jQuery(".profile-country-select-dropdown").live("change", function(){
            $countrydropdown    = jQuery(this).val();
            $othervalue         = jQuery('.other-value').val();
            if($countrydropdown == 'other'){
                jQuery('.other-country').show();
                jQuery('.ac-course-name').hide();
                jQuery('.ac-eductaor-name').hide();
                jQuery('.educator-name-other').show();
                jQuery('.other-course-name').show();
                jQuery('.other-course-name').show();

            }else{
                jQuery('.other-country').hide();
                jQuery('.ac-course-name').show();
                jQuery('.ac-eductaor-name').show();
                jQuery('.educator-name-other').hide();
                jQuery('.other-course-name').hide();
                jQuery('.other-course-name').hide();
            }
            if($countrydropdown !=''){
                jQuery('.profile-educator-select-dropdown').removeAttr('disabled')
            }else{
                jQuery('.profile-educator-select-dropdown').attr('disabled','disabled');
                jQuery('.profile-educator-select-dropdown').html('');
                jQuery('.profile-course-select-dropdown').attr('disabled','disabled');
                jQuery('.profile-course-select-dropdown').html('');
                return false;
            }
            ajaxurl             =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                'action': 'educator_listing_drodpown_two',
                'countrydropdown':$countrydropdown
            },
            success:function(data) {
            // This outputs the result of the ajax request
                    if($othervalue  == 0){
                        jQuery('.profile-educator-select-dropdown').html(data);
                    }else{   
                        jQuery('.profile-educator-select-dropdown').html(data+'<option val="other" >other</option>');
                    }
            },
            error: function(errorThrown){
                    console.log(errorThrown);
            }
        });
    }); 
   jQuery(".profile-course-select-dropdown").live("change",function(){
        $educatorsdrodown    = jQuery(this).val();
        if($educatorsdrodown == 'other'){
                //jQuery('.ac-course-name').hide();
                jQuery('.other-course-name').show();
            }else{
                jQuery('.other-country').hide();
               // jQuery('.ac-course-name').show();
                //jQuery('.other-course-name').hide();
            }
   });
   jQuery(".profile-educator-select-dropdown").live("change", function(){
        $educatorsdrodown    =  jQuery.trim(jQuery('.profile-educator-select-dropdown').val());
        $educatorsdrodown    = jQuery(this).val();
            if($educatorsdrodown == 'other'){
                jQuery('.ac-course-name').hide();
               // jQuery('.ac-eductaor-name').hide();
                jQuery('.educator-name-other').show();
                jQuery('.other-course-name').show();
            }else{
                jQuery('.ac-course-name').show();
                //jQuery('.ac-eductaor-name').show();
                jQuery('.educator-name-other').hide();
                jQuery('.other-course-name').hide();
            }
            //alert($educatorsdrodown);
            if($educatorsdrodown !=''){
                jQuery('.profile-course-select-dropdown').removeAttr('disabled')
            }else{
                jQuery('.profile-course-select-dropdown').attr('disabled','disabled');
                jQuery('.profile-course-select-dropdown').html('');
                return false;
            }
            ajaxurl             =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                'action': 'course_listing_drodpown',
                'educatordropdown':$educatorsdrodown
            },
            success:function(data) {
            // This outputs the result of the ajax request
               if($othervalue  == 0){
                        jQuery('.profile-course-select-dropdown').html(data);
                    }else{   
                        jQuery('.profile-course-select-dropdown').html(data+'<option val="other" >other</option>');
                    }

            },
            error: function(errorThrown){
                    console.log(errorThrown);
            }
        });
    }); 
  function overalratingjs($thisclass,$mainclass){
        var rateclass = jQuery($thisclass).data( "rate" );
        var inactive  = jQuery($thisclass).hasClass( "fa-star-o" );
       // alert(rateclass);


       // var mainclass = $mainclass;
        if(inactive == true){
            //alert('work');
            for (i = 0; i <rateclass; i++) {
                jQuery($mainclass).children().eq(i).removeClass('fa-star-o')
                jQuery($mainclass).children().eq(i).addClass('fa-star')
            }
        }else{
            if(rateclass == 1){
                    jQuery(this).removeClass( "fa-star" );
                    jQuery(this).addClass( "fa-star-o" );
            }
            // jQuery(this).removeClass( "fa-star" );
            // jQuery(this).addClass( "fa-star-o" );
           // alert('ada');
            for (i = 5; i >=rateclass; i--) {
               // alert(i);
                 var ig= i;
                jQuery($mainclass).children().eq(ig).removeClass('fa-star')
                jQuery($mainclass).children().eq(ig).addClass('fa-star-o')
            }
        }
  }  

    //jQuery('.overal-rating-js').children('.fa-star').length 
  jQuery(".overal-rating").on( "hover", function(){
        var rateclass = jQuery(this).data( "rate" );
        var inactive  = jQuery(this).hasClass( "fa-star-o" );
        var mainclass = '.overal-rating-js';
        if(inactive == true){
            //alert('work');
            for (i = 0; i <rateclass; i++) {
                jQuery(mainclass).children().eq(i).removeClass('fa-star-o')
                jQuery(mainclass).children().eq(i).addClass('fa-star')
            }
        }else{
            // if(rateclass == 1){
            //     jQuery(this).removeClass( "fa-star" );
            // }
            // jQuery(this).removeClass( "fa-star" );
            // jQuery(this).addClass( "fa-star-o" );
           // alert('ada');
            for (i = 5; i >=rateclass; i--) {
                var ig= i;
                jQuery('.overal-rating-js').children().eq(ig).removeClass('fa-star')
                jQuery('.overal-rating-js').children().eq(ig).addClass('fa-star-o')
            }
        }
      });  
        // jQuery(".overal-rating").on( "click", function(){
        //     var mainclass = '.overal-rating-js';
        //     $thisclass    = jQuery(this);
        //     overalratingjs($thisclass,mainclass);
        // }); 
    jQuery(".overal-tq-rating").on( "hover", function(){
           var mainclass = '.overal-teach-quality-js';
            $thisclass    = jQuery(this);
            overalratingjs($thisclass,mainclass);
        //alert(rateclass);
    });
    jQuery(".overal-sf-rating").on( "hover", function(){
        var mainclass = '.overal-school-facilties-js';
        $thisclass    = jQuery(this);
        overalratingjs($thisclass,mainclass);
    //alert(rateclass);
    });
    jQuery(".overal-ol-rating").on( "hover", function(){
        var mainclass = '.overal-locations-js';
        $thisclass    = jQuery(this);
        overalratingjs($thisclass,mainclass);
    //alert(rateclass);
    });
    jQuery(".overal-ca-rating").on( "hover", function(){
        var mainclass = '.overal-career-js';
        $thisclass    = jQuery(this);
        overalratingjs($thisclass,mainclass);
    //alert(rateclass);
    });
    jQuery(".overal-vm-rating").on( "hover", function(){
        var mainclass = '.overal-value-money-js';
        $thisclass    = jQuery(this);
        overalratingjs($thisclass,mainclass);
    //alert(rateclass);
    });  

// jQuery('.submit-personal-infomations').on("click",function(){
//             $.ajax({
//                 url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
//                 method:'Post',
//                  data: {
//                     'action': 'submit-reveiew-sections',
//                 },
//                 success:function(data) {
//                     // This outputs the result of the ajax request
//                     console.log(data);
//                     jQuery('.personal-success-message').show();
//                     jQuery('.personal-success-message').text(data);
//                 },
//                 error: function(errorThrown){
//                     console.log(errorThrown);
//                 }
//             }); 

// });
$(".submit-socialmedia-infomations-click").on( "click", function(){
        jQuery('.social-success-message').hide();
        var editpersonalfrm = $('#social-informtions-forms').serialize();
        //ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: editpersonalfrm,
                success:function(data) {
                    // This outputs the result of the ajax request
                    console.log(data);
                    jQuery('.social-success-message').show();
                    jQuery('.social-success-message').text(data);
                    setTimeout(function(){   location.reload(); }, 3000);
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
     });
  

    $(".submit-review-infomations").on( "click", function(){
        //var editpersonalfrm = $('#personal-informtions-form').serialize();
            var editpersonalfrm = $('#review-informtions-form').serialize();
            var educatorid  = jQuery('#educators-drodown').val();
            var courseid    = jQuery('#course-drodown').val();
            var useropnion  = jQuery('.user-opinion').val();
            var useropnion  = jQuery('.review-title').val();
            var form_data   = new FormData();
            form_data.append('action', 'update_submit-reveiew-sections');
            var fileList = jQuery('#review_images').get(0).files;
            var files_data1 = [];
            var count = fileList.length;
            for( var x=0;x<fileList.length;x++ ){
             form_data.append( 'gallery_image_'+x,fileList.item(x) );
            }
            form_data.append('educatorid', educatorid);
            form_data.append('useropnion', courseid);
            form_data.append('courseid', courseid);
            // var ins = document.getElementById('multiFiles').files.length;
            // for (var x = 0; x < ins; x++) {
            //     form_data.append("files[]", document.getElementById('multiFiles').files[x]);
            // }
          //  form_data.append('action', 'update_submit-reveiew-sections');  
            console.log(form_data);
            //var abc ="ada";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data:form_data,
                success:function(data) {
                    // This outputs the result of the ajax request
                    console.log(data);
                
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
     });   

    $(".submit-personal-infomations").on( "click", function(){
        var editpersonalfrm = $('#personal-informtions-form').serialize();
    
        //ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";
        //var abc ="ada";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: editpersonalfrm,
                success:function(data) {
                    // This outputs the result of the ajax request
                    console.log(data);
                    jQuery('.personal-success-message').show();
                    jQuery('.personal-success-message').text(data);
                    setTimeout(function(){   location.reload(); }, 3000);
                
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
     });   

    jQuery(".openpersonalinfo").on( "click", function(){
        jQuery("#personal-informtions").fadeIn();
        jQuery("body").addClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });

    jQuery(".openeductions-informations").on( "click", function(){
       // alert('ok');
        jQuery('.ac-country-name').show();
        jQuery('.fu-country-name').hide();
        jQuery('.other-country').hide();
        jQuery('.ac-course-name').show();
        jQuery('.ac-eductaor-name').show();
        jQuery('.educator-name-other').hide();
        jQuery('.other-course-name').hide();

        jQuery("#personal-informtions-eductions").fadeIn();
    //    jQuery('.profile-country-select-dropdown ').append('<option value="other">other</option>');
        jQuery('.other-value').val(1);
        jQuery('.future-data-val').val(0)
        jQuery(".cur-prve-heading").show();
        jQuery(".fure-prve-heading").hide();
        jQuery(".curt-prev-educations-forms span.error").hide();
        // jQuery('.not-decided-box').hide();
        // jQuery(".starting-form-data").show();
        jQuery('.curt-prev-educations-forms')[0].reset();
        jQuery(".future-start-box").hide();
        jQuery(".current-star-down").show();
        jQuery('.not-decided-box').hide();
        jQuery(".starting-form-data").show();
        jQuery('.profile-educator-select-dropdown').attr('disabled','disabled');
        jQuery('.profile-educator-select-dropdown').html('');
        jQuery('.profile-course-select-dropdown').attr('disabled','disabled');
        jQuery('.profile-course-select-dropdown').html('');
        jQuery("body").addClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });

    // Future Educations JS added on client
    jQuery(".openeductions-informationstwo").on( "click", function(){
        jQuery('.ac-country-name').hide();
        jQuery('.fu-country-name').show();
       // jQuery('.profile-country-select-dropdown ').children().last().remove();
        jQuery('.other-country').hide();
        jQuery('.other-value').val(0);
        jQuery('.ac-course-name').show();
        jQuery('.ac-eductaor-name').show();
        jQuery('.educator-name-other').hide();
        jQuery('.other-course-name').hide();
        jQuery(".cur-prve-heading").hide();
        jQuery('.future-data-val').val(1)
        jQuery(".curt-prev-educations-forms span.error").hide();
        jQuery(".fure-prve-heading").show();
        jQuery("#personal-informtions-eductions").fadeIn();
        jQuery('.curt-prev-educations-forms')[0].reset();
        jQuery('.not-decided-box').show();
        jQuery(".starting-form-data").hide();
        jQuery(".future-start-box").show();
        jQuery(".current-star-down").hide();
        jQuery('.profile-educator-select-dropdown').attr('disabled','disabled');
        jQuery('.profile-educator-select-dropdown').html('');
        jQuery('.profile-course-select-dropdown').attr('disabled','disabled');
        jQuery('.profile-course-select-dropdown').html('');
        jQuery("body").addClass("no-scroll");
    //$("html, body").animate({ scrollTop: 0 }, 500);
    });

    jQuery(".open-social-informations").on( "click", function(){
        jQuery("#personal-informtions-social").fadeIn();
        jQuery("body").addClass("no-scroll");
    //$("html, body").animate({ scrollTop: 0 }, 500);
    });
    jQuery(".close-popbox .clspopup,.cancel-links").on( "click", function(){
        jQuery("#personal-informtions").fadeOut();
        jQuery("#personal-informtions-eductions").fadeOut(500);
        jQuery("body").removeClass("no-scroll");
    //$("html, body").animate({ scrollTop: 0 }, 500);
    });

  

});
</script>
