<?php

get_header(); 
$producteviews = pvc_get_post_views(get_the_id());
update_post_meta(get_the_id(),'post_views_counter',$producteviews);

$unvisityid 	   =  get_field('select_university',get_the_id());
$universityname    = get_the_title($unvisityid);
$terms  		   = get_the_terms($unvisityid,'Country');
$unvisitylogo      = get_field('university_logo',$unvisityid);
//$countryname    = $termsname = $terms[0]->name;
$countryname =  '';
$countryflag = '';
if(!empty($terms)){
    $countryname =  $termsname = $terms[0]->name;
    $termsid     = $terms[0]->term_id;
    $countryflag = ege_countryflagurl($termsid);
}

wp_reset_postdata(); 
//Review Sections 
global $wpdb;
$table_name = $wpdb->prefix . "review";

$avarage            = 0;
$totalaverage       = 0;
$totalproductreviws = 0;
$productid = get_the_id();
//SELECT * FROM `ege_review` WHERE `course_id` = 15
$coursereview = $wpdb->get_results( "SELECT * FROM $table_name WHERE status = 1 AND `product_id` =".$productid );
if(!empty($coursereview)){
$totalproductreviws =  count($coursereview);
$totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE status = 1 AND `product_id` = ".$productid);
$avarage                    = $totalrating->avarage;
$totalaverage               = wp_star_rating_avarage_float($avarage);
}

$args = array(
   'rating' => $avarage,
   'type' => 'rating',
   'number' => 5,
);


$bannerimages        = get_the_post_thumbnail_url( get_the_id(),'full');
if(empty($bannerimages)){
    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
} 

//$ratings = round( $avarage / 10, 0 ) / 2;
//$average = 3;

if(is_user_logged_in()) {
    $userid         = get_current_user_id();
    $meta_key       = 'product_short_list';
    $educatorarray  = maybe_unserialize(get_user_meta($userid,$meta_key,true));
    $wishclass      = 'add';
    $wishsortlistclass   = 'fa-heart-o';
    if(is_array($educatorarray)){
        if(in_array(get_the_id(), $educatorarray)){
            $wishclass = 'remove';
            $wishsortlistclass = 'fa-heart';
        }
    }
}

//Product Category Get Code
$terms               = get_the_terms($productid,'product-categories');
$categoryname        = '';
if(!empty($terms)){
    $categoryname    = $termsname = $terms[0]->name;
}
$productprice    = get_field('product_price',get_the_id());

?>

    <section class="inner-bannerbox topgreen-border greenbg-color">
        <div class="wrapper">
            <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">
                    <ul class="cf">
                        <li><a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a></li>
                        <li><a href="<?php echo site_url().'/products';  ?>">Products</a></li>
                       <?php /*
                        <li><a href="#">University</a></li>
                        <li><a href="#">UK</a></li>
                        */ ?>
                        <li><label> <?php echo get_the_title(); ?></label></li>

                    </ul>
                </div>
                <div class="innerbanner-block cf">
                    <?php if(!empty($unvisitylogo)){ ?>
                    <div class="inrbanner-leftimg">
                        <img src="<?php echo $unvisitylogo; ?>" alt="" />
                    </div>
                    <?php } ?>
                    <div class="inrbaner-rightcont">
                        <div class="baner-titbox">
                            <h3><?php echo get_the_title(); ?></h3>
                        </div>                                               
                    </div>
                </div>
            </div>            
        </div>
    </section>
    <section>
        <div class="wrapper cf">
            <div class="prod-signpage-box whitebg">
                <div class="pro-imag-contbox cf">
                    <!--<div class="signprod-leftimg equal-height" style="background-image:url(<?php // echo $bannerimages; ?>;"></div>-->
                    <div class="signprod-leftimg">
                        <div class="product-imgbox">
                            <img src="<?php echo get_template_directory_uri(); ?>/assettwo/images/product-detail-bgimg.jpg" alt="">
                        </div>
                        <div class="slidershow-box desktop-layout">
                            <div class="table-showcont-box kindle-edition" id="kindle-edition">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Kindle Edition</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr> 	
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr> 	 	
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr> 	 	 	
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>  		 	 	
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="table-showcont-box audiobook" id="audiobook" style="display:none;">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Audio Book</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr> 	
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr> 	 	
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr> 	 	 	
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>  		 	 	
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="table-showcont-box hardcover" id="hardcover" style="display:none;">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Hard Cover</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr> 	
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr> 	 	
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr> 	 	 	
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>  		 	 	
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="table-showcont-box paperback" id="paperback" style="display:none;">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Paper Back</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr> 	
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr> 	 	
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr> 	 	 	
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>  		 	 	
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="table-showcont-box audio-cd" id="audio-cd" style="display:none;">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Audio CD</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr> 	
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr> 	 	
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr> 	 	 	
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>  		 	 	
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>  	   		 	 	
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="signpror-rightblock equal-height">
                        <div class="signupro-rittop-box cf">
                            <div class="sign-lefttit-box">
                                <h3><?php echo get_the_title(); ?></h3>
                                <?php if(!empty($categoryname)){?>
                                <span><?php echo $categoryname; ?></span>
                                <?php } ?>
                                <div class="cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php wp_star_rating($args); ?>
                                            <a class="ratenum-like" href="javascript" title="like">
                                                <span class="rating-number-box"><?php echo $totalaverage; ?></span>
                                            </a>
                                        </div>
                                        <?php if($totalproductreviws != 0 ){ ?>
                                        <div class="review-texts">
                                            <span><?php echo  $totalproductreviws ?> reviews</span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php if(is_user_logged_in()) { ?>
                                <div class="signpag-headdiv" style="display:none">
                                    
                                    <span data-wish="<?php echo $wishclass; ?>" class="products-wish-list"><i class="fa <?php echo $wishsortlistclass; ?>"></i></span>
                                </div>
                            <?php } ?>

                            </div>
                            <?php if(!empty( $productprice)){ ?>
                            <div class="sign-rit-prictbox enquiry-rightbox">
                                <span>£<?php echo $productprice; ?></span>
                        <?php if(is_user_logged_in()){
                         echo  do_shortcode('[wpecpp name="'.get_the_title().'" price="'.$productprice.'" ]');   
                        }else{ ?>
                        <input type="submit"  class="book-product-btn btn booknow-btn" value="Buy Now" name="submit">
                         <?php } ?>   
                            <?php /*    <a href="#" class="btn booknow-btn">Book now</a> */ ?>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="produ-detail-sliderbox owl-carousel" id="singleproduct-slider">
                            <div class="prod-slider-blk active">
                               <a href="javascript:void(0);" class="click-sldprod" data-id="kindle-edition">
                                <span><img src="<?php echo get_template_directory_uri(); ?>/assettwo/images/Kindle-edition.png" alt=""></span>
                                <div class="prodslir-rigt">
                                    <h4>Kindle Edition</h4>
                                    <p>&#8364; 7.99</p>
                                </div>
                                </a>
                            </div>
                            <div class="prod-slider-blk">
                               <a href="javascript:void(0);" class="click-sldprod" data-id="audiobook">
                                    <span><img src="<?php echo get_template_directory_uri(); ?>/assettwo/images/Audio-book.png" alt=""></span>
                                    <div class="prodslir-rigt">
                                        <h4>Audiobook</h4>
                                        <p>&#8364; 0.00</p>
                                    </div>
                                </a>
                            </div>
                            <div class="prod-slider-blk">
                               <a href="javascript:void(0);" class="click-sldprod" data-id="hardcover">
                                    <span><img src="<?php echo get_template_directory_uri(); ?>/assettwo/images/Hard-cover.png" alt=""></span>
                                    <div class="prodslir-rigt">
                                        <h4>Hardcover</h4>
                                        <p>&#8364; 33.00</p>
                                    </div>
                                </a>
                            </div>
                            <div class="prod-slider-blk">
                               <a href="javascript:void(0);" class="click-sldprod" data-id="paperback">
                                <span><img src="<?php echo get_template_directory_uri(); ?>/assettwo/images/Paper-back.png" alt=""></span>
                                <div class="prodslir-rigt">
                                    <h4>Paperback</h4>
                                    <p>&#8364; 33.00</p>
                                </div>
                                </a>
                            </div>
                            <div class="prod-slider-blk">
                               <a href="javascript:void(0);" class="click-sldprod" data-id="audio-cd">
                                <span><img src="<?php echo get_template_directory_uri(); ?>/assettwo/images/Audio-cd.png" alt=""></span>
                                <div class="prodslir-rigt">
                                    <h4>AudioCD</h4>
                                    <p>&#8364; 33.00</p>
                                </div>
                                </a>
                            </div>                            
                        </div>
                        <div class="signupro-contbox">
                        <div class="slidershow-box mobile-layout">
                            <div class="table-showcont-box kindle-edition" id="kindle-edition">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Kindle Edition</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr>    
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr>        
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr>            
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>                
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="table-showcont-box audiobook" id="audiobook" style="display:none;">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Audio Book</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr>    
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr>        
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr>            
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>                
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="table-showcont-box hardcover" id="hardcover" style="display:none;">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Hard Cover</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr>    
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr>        
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr>            
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>                
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="table-showcont-box paperback" id="paperback" style="display:none;">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Paper Back</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr>    
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr>        
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr>            
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>                
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="table-showcont-box audio-cd" id="audio-cd" style="display:none;">
                                <table>
                                    <tr>
                                        <th>Format:</th>
                                        <td>Audio CD</td>
                                    </tr>
                                    <tr>
                                        <th>File Size:</th>
                                        <td>7667 KB</td>
                                    </tr>
                                    <tr>
                                        <th>Print Length:</th>
                                        <td>320 Pages</td>
                                    </tr>
                                    <tr>
                                        <th>Publisher:</th>
                                        <td>Cornerstone Digital 2018)</td>
                                    </tr>
                                    <tr>    
                                        <th>Sold by:</th>
                                        <td>Amazon Media EU S.à r.l.</td>
                                    </tr>
                                    <tr>        
                                        <th>Language:</th>
                                        <td>English</td>
                                    </tr>
                                    <tr>            
                                        <th>ASIN:</th>
                                        <td>B01N5AX61W</td>
                                    </tr>
                                    <tr>                
                                        <th>Text-to-Speech:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>X-Ray:</th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Word Wise: </th>
                                        <td>Enabled</td>
                                    </tr>
                                    <tr>                    
                                        <th>Enhanced Typesetting: </th>
                                        <td>Enabled</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                            <?php the_content(); ?>
                        </div>

                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="innerpage-menu-box greenbgcolor" style="display:none;">
        <div class="wrapper cf">
            <div class="sticky-imgtit titimgsticky">
                <?php if(!empty($unvisitylogo)) { ?>
                <div class="stikimg-small">
                  <img src="<?php echo $unvisitylogo ?>" alt="" width="60"/>
                </div>
                <?php } ?>
                <div class="sticky-tibox">
                   <div class="table">
                       <div class="table-cell">
                            <h4><?php echo get_the_title(); ?></h4>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <?php
               $categories = get_the_terms(get_the_id(),'product-categories' );
                if(!empty($categories)){
                $realtedcategories = array();
                foreach ($categories as $value) {
                # code...
                $realtedcategories[] =  $value->slug;
                }
                $subject_array = array(
                'taxonomy' => 'product-categories',
                'field'    => 'slug',
                'terms' => $realtedcategories
                );

                $the_query = new WP_Query( array(
                'posts_per_page' => 8,
                'order' => 'DESC',
                'orderby' =>'date',
                'post_type'     =>'product',
                'tax_query' => array($subject_array),
                'post__not_in' => array(get_the_id())
                )
                ); 
        // echo "</pre>";
        if($the_query -> have_posts()){
        ?>    
        <section class="inner-blocks-slider">
        <div class="wrapper cf">
            <div class="top-title-contbox normal-conte aos-init aos-animate" data-aos="fade-right" data-aos-duration="1200">
                <h2>Related Books</h2>                
            </div>

            <div class="school-detail-slider page-product-mainbox">
                <div id="" class="rows cf owl-carousel books-sliderbox">

                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    $productid       = get_the_id();
                    $producttitle    = get_the_title();
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'bannerimage-product-list');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                    $countview       = pvc_get_post_views($productid);
                    $bestseller      = get_field('best_seller',$productid);

                    $productcontent  = get_the_excerpt();
                    $terms           = get_the_terms($productid,'product-categories');
                    if(!empty($terms)){
                    $categoryname    = $termsname = $terms[0]->name;
                    $termsid         = $terms[0]->term_id;
                    // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    }
                    $productprice    = get_field('product_price',get_the_id());
                    //Rating Code

                    //Review Sections  Code
                    global $wpdb;
                    $table_name = $wpdb->prefix . "review";
                    $totalproductreviws = 0;
                    $args = '';
                    $avarage =0;
                    $totalaverage =0;
                    $productview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE 
                        status = 1 AND `product_id` = ".$productid );
                    if(!empty($productview)){
                        $totalproductreviws =  count($productview);
                        $totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE  status = 1 AND `product_id` =".$productid);
                        $avarage                    = $totalrating->avarage;
                        $totalaverage               = wp_star_rating_avarage_float($avarage);
                    }
                      $args = array(
                       'rating' => $avarage,
                       'type' => 'rating',
                       'number' => 5,
                    );
                    ?>
                    <div class="three-block fourblock pagepro-block two-blcoks">
                        <div class="white-bg ourcourse-block">
                            <div class="course-imgblock">
                                <span class="imgmain-box"><img src="<?php echo $bannerimages; ?>" alt=""></span>
                                <?php if( !empty($bestseller)){ ?>
                                <div class="bachlabl-leftbox">
                                    <span class="pre-bachelr-label">
                                          <p>Bestseller</p>
                                    </span>
                                </div> 
                                <?php } ?> 

                                <div class="inner-titwid-img">
                                   <div class="ourcourse-contbox cf">
                                       <h4><?php echo $producttitle;?></h4>
                                        <?php if(!empty($productcontent)){ ?>
                                        <p><?php echo substr($productcontent,'0','172').'..'; ?> </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php  echo do_action('product_short_list_actions'); ?>

                            <div class="our-courseblk-content">
                                <a href="<?php echo get_permalink(); ?>"><h3><?php echo $producttitle;?> </h3></a>
                                <?php if(!empty($categoryname)) { ?>
                                    <div class="ourcor-smlcont-box"><?php echo $categoryname; ?> 
                                    </div>
                                <?php } ?>
                                <div class="cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                            <a class="ratenum-like" href="javascript:void(0)" title="like">
                                                <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                            </a>
                                        </div>
                                        <?php if(!empty($totalproductreviws)){ ?>
                                        <div class="review-texts">
                                            <span><?php echo $totalproductreviws; ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="year-view-box">
                                    <?php if(!empty($productprice)) { ?>
                                    <span class="rate-leftbox">From <i>£<?php echo $productprice; ?> </i></span>
                                    <?php } ?>

                                    <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assettwo/images/view-ico.png">views: <i><?php echo $countview; ?></i></span>
                                </div>
                            </div>
                        </div>
                </div>
                <?php  endwhile; ?>
                <?php  
                    wp_reset_postdata(); ?>
       
                </div>
            </div>
        </div>
    </section>
    <?php }  }?>
<?php if(!empty($coursereview)) { ?>
    <section class="our-review-section" id="go-reviewid">
        <div class="wrapper cf">
            <div class="top-title-contbox normal-conte" data-aos="fade-right" data-aos-duration="1200">
                <h2>Students' recent reviews</h2>               
            </div>
            <div class="ourreiv-blockbox owl-carousel" id="singleprod-reivew" data-aos="fade-left" data-aos-duration="1200">

           
                    <?php  foreach ($coursereview as $value) {
                    # code...
                   $userid          = $value->userid;
                   $reveiwtitle          = $value->Title;
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                  // $user            = get_user_by( 'email',$useremail);
                   //print_r($user);
                   $firstname       =  $user_info->first_name;
                   $lastname        =  $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $descriptions    = $value->descriptions;
                   $overal_rating   = $value->overal_rating;
                  // echo get_avatar( $userid ); 
                   ?>
                <div class="reviwebox-block singpro-review-block  cf">
                    <div class="left-riveimg-box equal-height">
                        <span class="rie-imgicons"><?php echo get_avatar( $userid ,85); ?></span>
                        <div class="revrat-box">
                            <h4><?php echo $userlogin; ?></h4>
                            <div class="cf">
                                <div class="school-detail-bottom">
                                    <div class="rate-star">
                                        <?php if(!empty($overal_rating) ){
                                        $totalavaragereaming = 5;
                                        ?>
                                        <span class="like-count">
                                        <?php for($i=1; $i<=$overal_rating; $i++ ){ ?>
                                        <i class="fa fa-star"></i>
                                        <?php } ?>
                                        <?php for($i=$overal_rating; $i<$totalavaragereaming; $i++ ){ ?>
                                        <i class="fa fa-star-o"></i>
                                        <?php  } ?>
                                        </span>
                                        <?php  } ?>
                                    </div>
                                    <div class="price-box-text"><?php echo $reveiwtitle; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-rivecontbox equal-height">
                        <div class="table">
                            <div class="table-cell">
                                <p><?php echo substr($descriptions,'0','500'); ?></p>
                            </div>
                        </div>                        
                    </div>
                </div>
                   <?php
                    }
                   ?>
            </div>
        </div>
    </section>
    <?php } 

do_action('loginbox_html_code_product'); 
    ?>   

<?php get_footer();