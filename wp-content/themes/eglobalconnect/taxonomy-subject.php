<?php

/* Template Name: Category Page Template */
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
//session_start();
$queried_object = get_queried_object();
$subject_array = '';
$educator_array = '';
//print_r($_SESSION);
//Sessison code  For Courses Mangement.
if(!empty($_REQUEST['subject-id']) || !empty($_REQUEST['studylevel-id']) ||  !empty($_REQUEST['country-id']) || !empty($queried_object->term_id)){
    // $_SESSION['termid']          = $_REQUEST['subject-id'];
    // $_SESSION['studylevel']      = $_REQUEST['studylevel-id'];
    // $_SESSION['countrydropdown'] = $_REQUEST['country-id'];
    // $_SESSION['educatorid']      = '';

   // $sessionstudylevel = $_SESSION['studylevel'];
}
/*
	<section id="popular-subjects" class="popular-subjects">

<div class="wrap">
*/

//print_r($queried_object);
if(!empty($_REQUEST['subject-id'])){
    $term_id = $_REQUEST['subject-id'];
    //$_SESSION['termid'] = $term_id; 
              
}else{    
        $term_id = $queried_object->term_id;
        if(!empty($term_id)){
       //   $_SESSION['termid']  =  $term_id;

        }else{
        // Session Subject Initalize. 
         // $term_id = $_SESSION['termid'];
        }
}    
$term = get_term_by( 'id', $term_id, 'subject');
if(!empty($queried_object->term_id)  || !empty($_REQUEST['subject-id']) ){
    $termsname = $term->name;
    $termdescription = $term->description;
}


// echo "<pre>";
// print_r($_REQUEST);
// echo "</pre>";
    if(!empty($_REQUEST['country-id'])  ) {
        //|| !empty($_SESSION['countrydropdown'] 
    if(!empty($_REQUEST['country-id'])){
         $countryhomeslug = $_REQUEST['country-id'];
    }else{
        // if(!empty($_SESSION['countrydropdown'])){
        //     $countryhomeslug  = $_SESSION['countrydropdown'];
        // }
    }


    $universityposts_home = get_posts(
    array(
    'post_type' => 'educator', // Post type
    'tax_query' => array(
    array(
    'taxonomy' => 'Country', // Taxonomy name
    'field' => 'slug',
    'terms' => $countryhomeslug 
    )
    ),
    'posts_per_page' => -1,
    'orderby'       => 'menu_order',
    'order'         => 'ASC'
    )
    );  
    // echo "<pre>";
    // print_r($universitypostss);
    // echo "</pre>";
   if(!empty($universityposts_home)){
        foreach ($universityposts_home as $educator) {
        # code...
        $educatorid[] =  $educator->ID;

        }
   }
    $educator_array = array(  
    'key' => 'select_university',
    'value' =>$educatorid,
    'compare' => "IN",
    );
 }
    if(!empty($term)){
    	$subject_array = array(
    						'taxonomy' => 'subject',
    						'field'    => 'slug',
    						'terms' => $term->slug
    						);
    }
    //if(isset($_REQUEST['studylevel-id'])){
        if(!empty($_REQUEST['studylevel-id'])  )
        {

           // ||  !empty($_SESSION['studylevel'])

            if(!empty($_REQUEST['studylevel-id']))
            {
                $studylevel_home = $_REQUEST['studylevel-id'];
            }else{   
                // if(!empty( $_SESSION['studylevel'])){
                //    $studylevel_home =  $_SESSION['studylevel']; 
                // }
            }
            $studylevel = get_term_by( 'id',$studylevel_home, 'studylevel');
                if(!empty($studylevel)){
                    $subject_array = array(
                    'taxonomy' => 'studylevel',
                    'field'    => 'slug',
                    'terms' => $studylevel->slug
                    );
            }
        }
    // }


    //$studylevel = '';
    if(!empty($term) && !empty($studylevel)) {
         $subject_array = array( // (array) - use taxonomy parameters (available with Version 3.1).
    'relation' => 'AND', // (string) - The logical relationship between each inner taxonomy array when there is more than one. Possible values are 'AND', 'OR'. Do not use with a single inner taxonomy array. Default value is 'AND'.
    array(
      'taxonomy' => 'subject', // (string) - Taxonomy.
      'field' => 'slug', // (string) - Select taxonomy term by Possible values are 'term_id', 'name', 'slug' or 'term_taxonomy_id'. Default value is 'term_id'.
      'terms' => $term->slug // (int/string/array) - Taxonomy term(s).
    ),
    array(
      'taxonomy' => 'studylevel',
      'field' => 'slug',
      'terms' => $studylevel->slug,
      //'include_children' => false,
      //'operator' => 'NOT IN'
    )
    );

    }
$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
$the_query = new WP_Query( array(
					'posts_per_page' => 12,
					'order' => 'DESC',
					'orderby' =>'date',
					'paged' => $paged,
					'post_type'		=>'course',
					'tax_query' => array($subject_array),
                    'meta_query' => array($educator_array),
                    
                    )
                ); 


//Advertisment Sections
$ad_title               = get_field('ad_title',get_the_id());
$descriptions           = get_field('descriptions',get_the_id());
$download_pdf           = get_field('download_pdf',get_the_id());
$advertisement_image    = get_field('advertisement_image',get_the_id());
if(empty($advertisement_image)){
    $advertisement_image = get_stylesheet_directory_uri() .'/assettwo/images/united-kingdom-img.png';
}

//Header image and Title
$banner_image    = get_field('banner_image',get_the_id());
$header_title    = get_field('header_title',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}

$classname = '';
$classname2='';
$coursetitlehead = $termsname;
$wordlenght =  strlen($coursetitlehead);
 if($wordlenght >= 55){
    $classname =  'full-course-width-three';
    $classname2 =  'full-course-width-two';
 }
?>  
<script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/js/select2.min.js"></script>
<link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/select2.min.css" rel="stylesheet" />
<section class="inner-bannerbox green-bgcover topgreen-border" style="background-image: url(<?php echo $banner_image; ?>">
        <div class="wrapper">
            <div class="inner-baner-cont <?php echo $classname; ?>" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">
                    <ul class="cf">
                        <li><a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a></li>
                        <li><a href="<?php echo site_url('/subject') ?>">Courses</a></li>
                      <?php if(!empty($termsname)  && !empty($term)) { ?>
                        <li><label><?php echo $termsname; ?></label></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="innerbanner-block cf ">
                    <div class="pagebanner-content ">
                        <div class="pagetitle-box cf">
                            <span class="banerpage-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/page-business-icon.png" alt=""></span>
                            <?php if(!empty($termsname)  && !empty($term)) { ?>
                            <h2><?php echo $termsname; ?></h2>
                            <?php }else{ ?>
                            <h2>Courses</h2>
                            <?php } ?>   
                        </div>
                        <div class="pagebaner-allconte cf">
                            <?php if(!empty($termsname)  && !empty($term)) { ?>
                              <p>
                                <?php echo $the_query->found_posts; ?> <?php echo $termsname; ?> Courses</p>
                            <?php }else{ ?>
                                <p>
                                <?php echo $the_query->found_posts; ?> business courses </p>
                           <?php } ?> 
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section class="business-sectionbox">
        <div class="wrapper cf">
            <div class="minusmartin-top cf">
                <div class="leftbusinessbox whitebg equal-height">
                    <?php if(!empty($termsname)  && !empty( $term_id)) { ?>
                    <h3><?php echo $termsname; ?></h3>
                    <?php }else{ ?>
                    <h3><?php echo $header_title; ?></h3>
                     <?php } ?> 

                    <?php 
                    wp_reset_postdata();
                    if(!empty($termdescription ) || !empty($term )) {
                        echo wpautop($termdescription);
                        }else{ 
                                echo  wpautop(get_the_content());
                            
                        }
                    ?>
                </div>
                <div class="rightbusiness-box cf greybg equal-height">
                    <div class="rightleft-textbox">
                        <h4><?php echo $ad_title;  ?></h4>
                        <p><?php echo $descriptions;  ?></p>
                        <a download="download" href="<?php echo $download_pdf; ?>" class="download-links">Download</a>
                    </div>
                    <div class="rightrit-imgbox">
                        <img src="<?php echo $advertisement_image; ?>" alt="" />
                    </div>
                </div>
            </div>           
        </div>
    </section>

    <section class="course-section greybg" id="go-courseid">

        <div class="wrapper cf">
                    
            <div class="toggle-buttonbox cf">
                <div class="left-toptip-contbox">
                        <span  > Click to search the things that are most important to you, and we'll show you what we've got.</span> 
                </div>
                <div class="right-dro-dowbox">
                    <div class="selectfor-by">
                        <span class="selecte-serachby"><i class="fa fa-align-left"></i>Search By</span>

                    </div>   
     
                    <div class="select-sorbybtn">
                        <span class="selecte-sortby"><i class="fa fa-sliders"></i>Sort By</span>
                        <div class="sorby-main-drodown">
                            <ul  class="sortby-dropdown-ul">
                                <li  class="sortby-dropdown" ><a data-value="worldrank">World Ranking</a></li>
                                <li class="sortby-dropdown" ><a  data-value="price">Price</a></li>
                                <li class="sortby-dropdown" ><a  data-value="popularity">Popularity</a></li>
                                <li class="sortby-dropdown" ><a  data-value="durations">Durations</a></li>                        
                                <li class="sortby-dropdown" ><a  data-value="scholarship">Scholarship</a></li>                        
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dropdownmain-box serach-by-dropdown cf" data-aos="fade-left" data-aos-duration="1200" style="display:none">
                <div class="drop-labelbox tool-tip-content"><label>Search by</label></div>
                <div class="dropdown-blocks">
                    <?php
                    // session_start();
                    //$_SESSION["favcolor"] = "green";

                    // echo "<pre>";    
                    // print_r($_SESSION);
                    // echo "</pre>";
                    $studylevelid = '';
                    $universityposts = get_posts(
                       array(
                            'posts_per_page' => -1,
                            'order' => 'ASC',
                            //'orderby' =>'date',
                            'paged' => $paged,
                            'post_type'     =>'course',
                            'tax_query' => array($subject_array),
                            'meta_query' => array($educator_array), 
                           )
                        ); 
                        if(!empty($universityposts)){ 
                            $countryids = array();
                            $studylevelid = array();
                            $unvisityid = array();

                            foreach ($universityposts as $postvalue) {
                            # code...
                             $courseid     =  $postvalue->ID;
                             $universityid = get_field('select_university',$courseid);
                             $studylevel   = wp_get_post_terms($courseid,'studylevel');

                              //print_r($studylevel);  
                             $universityid = get_field('select_university',$courseid);
                             $country   = wp_get_post_terms($universityid,'Country');

                                                      
                            // echo $studylevel[0]['term_id'];
                         
                            if(!empty($studylevel[0])){
                                $studylevelid[] = $studylevel[0]->term_id;
                            }  
                            // country dropdown
                             //$countryids = array();   
                            if(!empty($country[0])){
                                $countryids[] = $country[0]->term_id;
                            } 
                             //print_r($studylevel); 
                             if(!empty( $universityid)){
                               $unvisityid[]   = $universityid;
                             }
                             //echo '<br>';
                            //echo $universityname = get_the_title($unvisityid);
                            }
                        }
                          if(!empty($unvisityid)){  
                            $educators_list = (array_unique($unvisityid));
                          }
                         // print_r($studylevelid);
                         if(!empty($studylevelid)){  
                            $studylevel_list = (array_unique($studylevelid));
                          }
                          if(!empty($countryids)){
                            $coutrylist    = (array_unique($countryids));
                          }

                    ?>
                    <?php 
                    $country_args = array(
                    'orderby'       => 'name', 
                    'order'         => 'ASC',
                    'parent'        => 0,
                    'hide_empty'    => true, 

                    );

                    $countrylist = get_terms('Country',$country_args);

                    ?>
                    <select name='country-dropdown' id="country-dropdown" class="chosen-select chosen-serach-dropdown">
                        <option value="">Country</option>
                        <?php
                        foreach ($countrylist as $country ) {
                            $countryslug = $countryhomeslug;
                            $selected  ='';

                            if($countryslug == $country->slug){
                              $selected = 'selected= selected';
                            }
                        # code...
                            echo '<option value="'.$country->slug.'" '.$selected.' >'.$country->name.'</option>';
                        }   
                        ?>
                    </select>
                    <?php /*
                    <select id="country-dropdown" class="chosen-select chosen-serach-dropdown">
                        <option value="">Country</option>
                        <?php
                        if(!empty($coutrylist)){
                            foreach ($coutrylist as $key) {
                                # code...
                                 $term = get_term_by( 'id', $key, 'Country');
                                 echo '<option value="'.$term->slug.'">'.$term->name.'</option>'
                                 ?>
                                <?php

                                }
                            }
                        ?> 
                    </select>
                    */ ?>
                </div>
                       <?php
                        $subject_args = array(
                        'orderby'       => 'name', 
                        'order'         => 'ASC',
                        'parent'        => 0,
                        'hide_empty'    => false, 

                        );

                        $subjectlist = get_terms('subject',$subject_args);

                        ?>
                        <select name='subject-id'   id="subject-id-dropdown" class="custom-select chosen-serach-dropdown">
                            <option value=" ">Subject Area</option>
                            <?php
                            foreach ($subjectlist as $subject ) {
                            # code...
                                $selected  ='';
                                if($term_id == $subject->term_id){
                                    $selected = 'selected= selected';
                                }
                                $subjectitle = str_replace("'",'&apos;',$subject->name);
                                $subjectitle = str_replace("’",'&apos;',$subject->name);
                                echo '<option value="'.$subject->term_id.'" '.$selected.' >'.$subjectitle.'</option>';
                            }   
                            ?>
                        </select>
                           <div class="dropdown-blocks">
                                    <?php
                        $study_args = array(
                        'orderby'       => 'name', 
                        'order'         => 'ASC',
                        'parent'        => 0,
                        'hide_empty'    => true, 

                        );

                        $studylevllist = get_terms('studylevel',$study_args);

                        ?>
                        <select  id="studylevel-dropdown" class="chosen-select chosen-serach-dropdown"> 
                            <option value="">Study level</option>
                            <?php
                            foreach ($studylevllist as $studylevel ) {
                                $selected = '';
                                if($studylevel_home == $studylevel->term_id){
                                    $selected = 'selected= selected';

                                }
                            # code...
                                echo '<option value="'.$studylevel->term_id.'" '.$selected .'>'.$studylevel->name.'</option>';
                            }   
                            ?>
                        </select>
                        <?php /*
                    <select  id="studylevel-dropdown" class="chosen-select chosen-serach-dropdown">
                        <option value="">Study Level</option>
                         <?php
                            foreach ($studylevel_list as $key) {
                                # code...
                                $term = get_term_by( 'id', $key, 'studylevel');
                                echo '<option value="'.$key.'">'.$term->name.'</option>'
                              
                                ?>
                                <?php

                            }
                        ?> 
                    </select>
                    */ ?>
                </div>  
                <div class="dropdown-blocks" id="change-dropdown-educator" style="display:none;">     </div>
                    <?php
                     $universityposts  ='';
                    if(isset($_REQUEST['country-id']) ||  !empty($countryhomeslug)){
                        $universityposts = get_posts(
                        array(
                        'post_type' => 'educator', // Post type
                        'tax_query' => array(
                        array(
                        'taxonomy' => 'Country', // Taxonomy name
                        'field' => 'slug',
                        'terms' => $countryhomeslug
                        )
                        ),
                        'posts_per_page' => -1,
                        'orderby'       => 'menu_order',
                        'order'         => 'ASC'
                        )
                        );
                    }else{
                        $universityposts = get_posts(
                            array(
                            'post_type' => 'educator', // Post type
                            'posts_per_page' => -1,
                            'orderby'       => 'menu_order',
                            'order'         => 'ASC'
                            )
                        );
                    }
                    ?>

                    <select id="educators-drodown" class="custom-select chosen-serach-dropdown">

                            <option value=" ">Institution name</option>
                            <?php
                             $selected ='';
                            if(!empty($universityposts)){
                                foreach ($universityposts as $postvalue) {
                                    $universitysessisonid = '';
                                    // if(!empty($_SESSION['educatorid'])){
                                    //    echo  $universitysessisonid = $_SESSION['educatorid'];
                                    // }

                                    if($universitysessisonid == $postvalue->ID){
                                         $selected = 'selected= selected';
                                    }
                                    # code...
                                    $posttitle = str_replace("'",'&apos;', $postvalue->post_title);
                                      $posttitle = str_replace("’",'&apos;',$postvalue->post_title);
                                    echo '<option value="'.$postvalue->ID.'" '.$selected.'>'.$posttitle.'</option>'
                                  
                                    ?>
                                    <?php

                                }
                            }
                            ?> 
                           <?php /*
                                foreach ($educators_list as $key) {
                                    # code...
                                    echo '<option value="'.$key.'">'.get_the_title($key).'</option>'
                                  
                                    ?>
                                    <?php

                                }
                                */
                            ?> 
                    </select>
           
     
  
              <?php /*
                <div class="dropdown-blocks" >
                    <select id="sort-by-dropdown" class="chosen-select chosen-serach-dropdown">
                        <option value="date">Sort By</option>
                        <option value="price">Price</option>
                        <option value="popularity">Popularity</option>
                        <option value="durations">Durations</option>
                    </select>
                </div>
                */ ?>
            
            </div>

             <input type="hidden" name="term-id-value" class="term-id-value" value="<?php echo $term_id  ?>">
             <input type="hidden" name="term-id-value" class="country-id-value" value="<?php echo $educatorslug   ?>">
             <input type="hidden" name="term-id-value" class="studylevel-id" value="<?php echo $studylevel_home  ?>">
            <div class="rows cf"  id="course-listing-data" >
              
                <div class="loading overlay-bgcolor" style="display:none">
                <div class="all-middleicon-loader">  <img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect"></div>
                </div>
               <?php
				$queried_object = get_queried_object();
			    $term_id = $queried_object->term_id;
				$term = get_term_by( 'id', $term_id, 'subject');
				//print_r($term);
				// $the_query = new WP_Query( array(
				// 'posts_per_page' =>4,
				// 'post_type'		=>'course',
				// 	 )	
				// ); 
				if($the_query -> have_posts()){
                    $count = 0;
                ?>
				<?php while ($the_query -> have_posts()) : $the_query -> the_post() ;
                    $count++;

                    do_action('courses_html_actions',$count);
                    if($count >= 3){
                       $count = 0;
                    }
			     endwhile;
				}else{
					echo "No Courses Founds";
				}

				wp_reset_postdata(); ?>
                <div class="fullwidth-block">
                <div class="page-linkox cf">
                        <?php

                        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                         ?>
                </div>
                </div>
      
            </div>
        </div>
    </section>
<?php do_action('loginbox_html_code'); ?>    
<?php get_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/js/subject-custom.js"></script>
