<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
wp_reset_postdata();

// $to = 'adnan.limdiwala@iflair.com';
// $Educator type = 'The Educator type';
// $body = '';
// $headers = array('Content-Type: text/html; charset=UTF-8');
 
//wp_mail( $to, $Educator type, $body, $headers );
/*
  $the_query = new WP_Query( array('posts_per_page'=>2,
                                 'post_type'=>'course',
                                 'paged' => get_query_var('paged') ? get_query_var('paged') : 1) 
                            ); 
                            ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
*/ ?>
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="javascript-void(0)"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title fullwidth-tit">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="blog-details-page" >
    <div id="primary" class="wrapper cf">
        <div class="inner-detailbox">
                <?php the_content(); ?>

        </div>      
    </div><!-- #primary -->
</section><!-- .wrap -->
<?php /*
endwhile;

$big = 999999999; // need an unlikely integer
 echo paginate_links( array(
    'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $the_query->max_num_pages
) );

wp_reset_postdata();



//echo get_user_meta( 1,  'nickname_1',  true );
$user_query = new WP_User_Query( array( 'meta_key' => 'nickname_1') );
$results    = $user_query->get_results();

// echo "<pre>";
// print_r($results);
// echo "</pre>";
// User Loop
if ( ! empty( $user_query->get_results() ) ) {
	foreach ( $user_query->get_results() as $user ) {
		echo '<p>' . $user->display_name . '</p>';
	}
} else {
	echo 'No users found.';
}
// echo "<pre>";
// print_r($results);
// echo "</pre>";
//print_r(get_users('Administrator'));

get_field('select_university');
*/

//echo getRealIpAddr();
/*
    <!-- Calendly inline widget begin -->
<div class="calendly-inline-widget" data-url="https://calendly.com/test1-iflair" style="min-width:320px;height:550px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
<!-- Calendly inline widget end -->
//$response = wp_remote_get( 'https://calendly.com/api/v1/users/me?action=foo', array( 'timeout' => 120, 'X-TOKEN' => 'GKMINPNMFEVBTO47YNCBE5FBEXS4NT4E' ) ); 
*/ 
?>


<?php get_footer();

