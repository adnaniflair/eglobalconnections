<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
wp_reset_postdata();

// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}
$eductaortitlehead = get_the_title();
$wordlenght =  strlen($eductaortitlehead);
  $classname = '';
  $classname2='';
 if($wordlenght >= 60){
    $classname =  'full-course-blog-width';
    $classname2 =  'full-course-blog-width2';
 }

?>
 
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox detail-bannerbox" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper">
        <div class="inner-baner-cont <?php echo $classname; ?>" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="JavaScript:Void(0);"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>

            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                    	<span class="date-filed"> <?php echo get_the_time('d/m/Y',get_the_id()); ?></span>
                        <h2 class="educator-main-title fullwidth-tit">
                         	<?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="blog-details-page">
	<div id="primary" class="wrapper cf">

		<div class="inner-detailbox " data-aos="fade-left" data-aos-duration="1500">
		    
            <?php
       /*
            // check if the repeater field has rows of data
            if( have_rows('blog_listing') ):
             $count =1;       
            // loop through the rows of data
            while ( have_rows('blog_listing') ) : the_row();
                // display a sub field value
                $class = '';
               if($count%2){
                $class = 'left-img';
               // $animationsclass = 'fade-left'; 
                $add = 1;
                }else{
                 $class = 'right-img'; 
                // $animationsclass = 'fade-right'; 
                 $add= 2;  
                }
                 $imagesurl = get_sub_field('image');
                 if(empty($imagesurl)){
                    $url = get_stylesheet_directory_uri().'/assettwo/images/Business-1-1-768x531.jpg';
                 }
                 $content = get_sub_field('descriptions');
                ?>               
                 <div class="allservic-blocks <?php echo $class; ?> cf ">
                <?php      if($add == 1){ ?>
                    <div class="service-ritimgbox  aos-init" data-aos="fade-left" data-aos-duration="700" style="background-image:url('<?php echo $imagesurl; ?>"></div>
                <?php } ?>
                    <div class="left-serv-contbox aos-init" data-aos="fade-left" data-aos-duration="700">
                       <div class="serv-add-content">
                            <?php echo $content; ?>
                        </div>                   
                
                    </div>
                    <?php      if($add == 2){ ?>
                    <div class="service-ritimgbox  aos-init" data-aos="fade-right" data-aos-duration="700" style="background-image:url('<?php echo $imagesurl; ?>"></div>
                <?php } ?>
                </div>
                <div  style="display:none" class="allservic-blocks left-img cf">
                    <div class="service-ritimgbox equal-height" data-aos="fade-left" data-aos-duration="700" style="background-image:url('<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/material-img.jpg')"></div>
                    <div class="left-serv-contbox equal-height" data-aos="fade-right" data-aos-duration="700">
                       <div class="serv-add-content">
                            <?php echo $provide_materials; ?>
                        </div>                   
                        <div class="serv-cont-btnbox">
                            <a href="<?php echo site_url('/contact') ?>">Contact us</a>
                        </div>
                    </div>
                </div>

                <?php

            $count++;
            endwhile;

            else :

            // no rows found

            endif;
            */
            ?>
                <?php the_content(); ?>
        </div>		
	</div><!-- #primary -->
</section><!-- .wrap -->

<?php get_footer();
