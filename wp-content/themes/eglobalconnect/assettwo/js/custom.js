jQuery(document).ready(function ($) {
    ///jQuery('#wplc_start_chat_btn').text('Start the Chat');
    jQuery('.user-login').children('a').addClass('logoutclass');
    $('.sub-toggle').click(function (event) {
        event.preventDefault();
        $('.sub-toggle').not(this).siblings("ul").slideUp();
        $(this).siblings("ul").slideToggle();

    });
    
    if (jQuery('.chosen-select').length > 0) {
        jQuery('.chosen-select').chosen({
            width: "100%"
        });
    }
    $(".drp_value").click(function () {
        $('.sub-toggle').not(this).siblings("ul").slideUp();
    });
    $('#popular-subjects.owl-carousel').owlCarousel({
        loop: true,
        items: 3,
        nav: true,
        dots: false,
        margin: 0,
        responsiveClass: true,
        responsive: {
            991: {
                items: 3
            },
            767: {
                items: 2
            },            
            0: {
                items: 1
            }
        }
    });
    $('#singleproduct-slider.owl-carousel').owlCarousel({
        loop: false,
        items: 5,
        nav: true,
        dots: false,
        margin: 0,
        responsiveClass: true,
        responsive: {
            1150: {
                items: 5
            },
            1024: {
                items: 3
            },            
            767: {
                items: 2 
            },
            550: {
                items: 2 
            },
            0: {
                items: 1 
            }
        }
    });
    $('#home-bnr.owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 8000,  
        items: 1,
        nav: true,
        dots: true,
        margin: 0,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    })
    
    $('.desti-sliderbox').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:10000,        
        margin:30,
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            640:{
                items:2
            },
            768:{
                items:3
            },
            1025:{
                items:4
            }
        }
    });
    
    $('.books-sliderbox').owlCarousel({
        loop: false,
        items: 4,
        nav: true,
        dots: true,
        autoHeight: true,
        responsive: {
            1024: {
                items: 4
            },
            800: {
                items: 3
            },
            550: {
                items: 2
            },
            0: {
                items: 1
            }
        }
    });
    
     $('.course-sliderbox').owlCarousel({
        loop: false,
        items: 2,
        nav: true,
        dots: false,
        margin: 0,
        responsiveClass: true,
        responsive: {
            992: {
                items: 2
            },            
            0: {
                items: 1
            }
        }
    });
    $('#reviews-slider.owl-carousel').owlCarousel({
        loop: true,
        items: 1,
        nav: false,
        dots: true,
        margin: 0
    })
    
    var windowsize = $(window).width();

    if (windowsize < 480) {
        $('.bxslider').bxSlider({
            auto: true,
            slideWidth: 320,
            slideMargin: 30,
            speed: 2000,
            autoControls: true,
            autoHover: true,
            stopAutoOnClick: true,
            pager: true,
            minSlides: 2,
            maxSlides: 1
        });

    } else {
        $('.bxslider').bxSlider({
            auto: true,
            slideWidth: 265,
            slideMargin: 30,
            speed: 20000,
            autoHover: true,
            autoControls: true,
            stopAutoOnClick: true,
            pager: true,
            minSlides: 2,
            maxSlides: 3
        });
    }


    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 88
                }, 1500);
                return false;
            }
        }
    });
    $("#button_push_left").on("click", function () {
        $("#push_left").toggleClass("push_left_open");
        $("body").toggleClass("body_open_left");
        if ($("#push_left").hasClass("push_left_open")) { /*If the menu is open, then:*/
            $("#bpleft").attr('src', 'img/close_left.png'); /*Change the menu button icon*/
        } else {
            $("#bpleft").attr('src', 'img/menu_icon.png'); /*If the menu is closed, change to the original button icon*/
        }
    });
    $(".has-submenu ul").hide();
    $(".has-submenu").click(function () {
        /*$(".has-submenu ul").slideUp();
        $(this).children("ul").slideToggle();*/
        var n = $(this).children("ul").css("display");
        if (n == 'none') {
            $('.has-submenu').children("ul").slideUp();
            $(this).children("ul").slideDown();
        } else
            $(this).children("ul").slideUp();
    });
    AOS.init({
        easing: 'ease-in-out-sine'
    });
    // $(".course-heart-icon .course-heart-click").click(function () {
    //     $(this).parent('.course-heart-icon').find(".addtoshort-btn").toggle();
    // });
   jQuery(".course-heart-icon .course-heart-click").hover(function () {
        jQuery(this).parent('.course-heart-icon').find(".addtoshort-btn").toggle();
    });

    jQuery(".product-heart-icon .course-heart-o").hover(function () {
        jQuery(this).parent('.product-heart-icon').find(".addtoshort-btn").toggle();
    });
    
    $("#innermenu-click").click(function () {
        $(this).parent('.iner-left-menubox').toggleClass('open-menu');
        
    });
    
    $('.gallery-sld').owlCarousel({
        loop: true,
        items: 4,
        nav: true,
        dots: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            992: {
                items: 4
            },
            767: {
                items: 3
            },
            480: {
                items: 2
            },
            0: {
                items: 1
            }
        }
    });
    $('.gallery-sldtwo').owlCarousel({
        loop: false,
        items: 3,
        nav: true,
        dots: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            
            767: {
                items: 3
            },
            480: {
                items: 2
            },
            0: {
                items: 1
            }
        }
    });
    
    $('#singleprod-reivew.owl-carousel').owlCarousel({
        loop: false,
        items: 2,
        nav: true,
        dots: true,
        autoHeight: true,
        responsive: {
            768: {
                items: 2
            },
            0: {
                items: 1
            }
        }
        
    });
    
    $('#mega-menu-wrap-top .mega-menu-toggle').click(function (e) {
        $('#mega-menu-top').animate({ "left" : "0"}, 500);
        e.preventDefault();        
    });
    /*$('#mega-menu-wrap-top .mega-menu-toggle').click(function (e) {
        $('#mega-menu-top').toggle("slide");
        e.preventDefault();        
    });*/
    
    /*jQuery('#mega-menu-wrap-top .mega-menu-open .mega-toggle-blocks-right').click(function (e) {
        $('#mega-menu-top').animate({ "left" : "-300"}, 500);
        e.preventDefault();        
    });*/
        
    $(window).resize(function() {
        // This will fire each time the window is resized:
        if($(window).width() >= 768) {
            // if larger or equal
            $('.slider_circle_10').EasySlides({'autoplay': false, 'show': 3});
        } else {
            // if smaller
            $('.slider_circle_10').EasySlides({'autoplay': false, 'show': 1});
        }
    }).resize(); // This will simulate a resize to trigger the initial run.
       // language dropdown

       /* sticky header */
    $(document).scroll(function () { // \n
        var y = $(this).scrollTop(); // \n
        if (y > 300) { // \n
            $( '.innerpage-menu-box.greenbgcolor' ).addClass( "fixmenubox" ); // \n
            $( '.innerpage-menu-box.greenbgcolor .sticky-imgtit' ).addClass( "titimgsticky" ); // \n
            $( '.single-course header,.single-educator header,.page-template-page-educator-profile header' ).addClass( "remove-sticky" ); // \n
        } else {
            $( '.innerpage-menu-box.greenbgcolor' ).removeClass( "fixmenubox" ); // \n
            $( '.innerpage-menu-box.greenbgcolor .sticky-imgtit' ).removeClass( "titimgsticky" ); // \n
            $( '.single-course header,.single-educator header,.page-template-page-educator-profile header' ).removeClass( "remove-sticky" ); // \n
        } // \n
    }); // \n
    
    $("#loadmore").click(function () {
        $('.hidecont-box').slideToggle();
        $(this).parent('.seemore-btnbox').toggleClass('remove-btn-onclick');
    });
    $(".enquiry-rightbox .openpopup").on( "click", function(){
        $(".enquiry-form-popup").fadeIn();
        $("body").addClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });
    $(".book-product-btn").on( "click", function(){
        $(".enquiry-form-popup").fadeIn();
        //$("body").addClass("no-scroll");
        $("html, body").animate({ scrollTop: 0 }, 500);
    });
    $(".enquiry-rightbox .enquiry-btn").on( "click", function(){
        $(".course-form-popup").fadeIn();
        $("body").addClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });
    $(".enquiry-rightbox .openpopup1").on( "click", function(){
        $("#eductors-popup").fadeIn();
        $("body").addClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });
     $(".close-popbox .clspopup").on( "click", function(){
        $(".enquiry-form-popup").fadeOut();
        $("body").removeClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });
    $(".clspopup-box").on( "click", function(){
        $(".enquiry-form-popup").fadeOut();
        $("body").removeClass("no-scroll");
        //$("html, body").animate({ scrollTop: 0 }, 500);
    });
    
    // var mySwiper = new Swiper(".swiper-container", {
    //   direction: "vertical",
    //   loop: true,
    //   pagination: ".swiper-pagination",
    //   grabCursor: true,
    //   speed: 1000,
    //   paginationClickable: true,
    //   parallax: true,
    //   autoplay: false,
    //   effect: "slide",
    //   mousewheelControl: 1
    // });
    
    /*$('.custom-fileup-control label').click(function () {
        $('#review_images').change(function () {
            var fileinput = $(this).val();
            var path = fileinput;
            var file = path.split('\\').pop();
            $('.custom-fileup-control.revie-img span').html(file);
        });
    });
    
    $('.custom-fileup-control.revie-vid label').click(function () {
        $('#review_images').change(function () {
            var fileinput = $(this).val();
            var path = fileinput;
            var file = path.split('\\').pop();
            $('.custom-fileup-control.revie-vid span').html(file);
        });
    });*/
    
    jQuery('a.click-sldprod').click(function () {
        jQuery(".audioh-playh-on").show();
        jQuery(".audio-playh-off").hide();
        jQuery(".audiobook-start").show();
        jQuery(".audiobook-stop").hide();
        jQuery(".audiocd-playh-on").show();
        jQuery(".audiocd-playh-off").hide();
        var x = document.getElementById("myAudio"); 
        x.pause();

        // get id
        jQuery('.prod-slider-blk').removeClass('active');
        //jQuery(this).addClass('active');
        jQuery(this).parent().addClass('active')
        var clickedId = jQuery(this).attr('data-id');
      //  jQuery('.a.click-sldprod').hide();
        //jQuery(".inner-readmore-popubox").toggle();
        jQuery(".table-showcont-box").hide();
        jQuery("."+clickedId).show();      
      
    });
        
});


/* Script on load
----------------------------------*/
jQuery(window).load(function () {
    equalheight('.equal-height');
    equalheight('.three-block .ourcourse-block .course-imgblock' );   
    equalheight('.blocks-rows .aboutblock-box');
    equalheight('.popular-subjects #popular-subjects .subject-block-inner .sub-bottom');
    equalheight('.ourcourse-block .our-courseblk-content a h3');
    equalheight('.reviwebox-block.singpro-review-block');
    equalheight('.school-details-wrap');
    equalheight('.school-details-wrap h3');
    equalheight('.three-block.fourblock.pagepro-block');
    equalheight('section.recomended-school .bxslider.university-slider a.item.university-block .university-detail .detail-inner');
    equalheight('.team-main-box .four-block');
    equalheight('.team-main-box .team-socia-contetbox');
});

/* Script on resize
---------------------------------*/
jQuery(window).resize(function () {
   equalheight('.equal-height');
    equalheight('.three-block .ourcourse-block .course-imgblock');
    equalheight('.blocks-rows .aboutblock-box');
    equalheight('.popular-subjects #popular-subjects .subject-block-inner .sub-bottom');
    equalheight('.ourcourse-block .our-courseblk-content a h3');
    equalheight('.reviwebox-block.singpro-review-block');
    equalheight('.school-details-wrap');
    equalheight('.school-details-wrap h3');
    equalheight('.three-block.fourblock.pagepro-block');
    equalheight('section.recomended-school .bxslider.university-slider a.item.university-block .university-detail .detail-inner');
    equalheight('.team-main-box .four-block');
    equalheight('.team-main-box .team-socia-contetbox');
});


// equal height testimonials block
equalheight = function (container) {

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    jQuery(container).each(function () {

        $el = jQuery(this);
        jQuery($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}


