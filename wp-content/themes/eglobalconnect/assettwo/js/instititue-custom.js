    jQuery(document).ready(function($) {

        jQuery("#select-country-name").live("change", function(){
            $countrydropdown    = jQuery('#select-country-name').val();

            $.ajax({
            url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
            method:'Post',
            data: {
                 'action': 'educator_listing_drodpown_two',
                 'countrydropdown':$countrydropdown
            },
            success:function(data) {
                // This outputs the result of the ajax request
                jQuery('#selected-educator-id').html(data);


            },
            error: function(errorThrown){
            console.log(errorThrown);
            }
            });
        }); 

        //Jqury_ajax_call();
        $(".custom-select").select2({
            tags: "true",
            placeholder: "Select an option",
            allowClear: true,
            width: '100%',
            createTag: function(params) {
                var term = $.trim(params.term);

                if (term === '') {
                    return null;
                }

                return {
                    id: term,
                    text: term,
                    value: true // add additional parameters
                }
            }
        });

        jQuery(".selecte-sortby").live("click", function() {
            jQuery(".sortby-dropdown-ul").slideToggle("100", function() {
            // Animation complete
            });

        });

        jQuery(".selecte-serachby").live("click", function() {
            jQuery(".serach-by-dropdown").slideToggle("100", function() {
                // Animation complete
            });
            // jQuery('.sort-by-dropdown').fadeIn('slow');
            jQuery('.sort-by-dropdown').hide();

        });

        jQuery(".sortby-dropdown").live("click", function() {
            jQuery('.sortby-dropdown').removeClass('active');
            jQuery(this).addClass('active');
            Jqury_ajax_call();
        });
    
    function Jqury_ajax_call(){
            jQuery('.loading').show();
            jQuery('.page-template-page-educator').addClass('loader-body');
            //jQuery('.tax-subject').addClass('loader-body');

            $pagenumber = 1;
            $termid = '';
            $educatortype = jQuery('#select-educator-type').val();
            $studylevel = '';
            $countrydropdown = jQuery('#select-country-name').val();
            $sortdropdown    = jQuery('ul.sortby-dropdown-ul').find('li.active').children().data('value');
            if($sortdropdown == 'undefined'){
                $sortdropdown = 'date';
            }           
            $sortbyvalue     = $sortdropdown;
            $termid = jQuery('#subject-id-dropdown').val();
            $educatortypeid = jQuery.trim(jQuery('#selected-educator-id').val());

            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method: 'Post',
                data: {
                    'action': 'educators_listing_sections',
                    'page': $pagenumber,
                    'termid': $termid,
                    'studylevel': $studylevel,
                    'educatortype': $educatortype,
                    'educatortypeid': $educatortypeid,
                    'countrydropdown': $countrydropdown,
                    'sortbyvaluedropdown': $sortbyvalue,
                },
                success: function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#educators-listing-data').html(data);
                    jQuery('.loading').hide();
                    jQuery('.page-template-page-educator').removeClass('loader-body');
                    equalheight('.school-details-wrap');
                    equalheight('.school-details-wrap h3');
                    // equal height testimonials block
   
                    // jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
                    //jQuery('.tax-subject').removeClass('loader-body');

                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
    }
   
        jQuery(".chosen-serach-dropdown").live("change", function() {
                   Jqury_ajax_call(); 
        });
        jQuery(".nav-links").live("click", function() {
            jQuery('.loading').show();
            jQuery('.page-template-page-educator').addClass('loader-body');

            $pagenumber = jQuery(this).data('page');
            $termid = jQuery('.term-id-value').val();
            $educatorname = '';
            $studylevel = '';
            $countrydropdown = jQuery('#select-country-name').val();
            $sortbyvalue = '';

            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method: 'Post',
                data: {
                    'action': 'educators_listing_sections',
                    'page': $pagenumber,
                    'termid': $termid,
                    'studylevel': $studylevel,
                    'educatorname': $educatorname,
                    'countrydropdown': $countrydropdown,
                    'sortbyvaluedropdown': $sortbyvalue,
                },
                success: function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#educators-listing-data').html(data);
                    //jQuery('.loading').hide();
                    jQuery('.loading').hide();
                    jQuery('.page-template-page-educator').removeClass('loader-body');
                    equalheight('.school-details-wrap');
                    equalheight('.school-details-wrap h3');

                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
    });