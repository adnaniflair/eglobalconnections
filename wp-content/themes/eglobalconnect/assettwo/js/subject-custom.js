jQuery(document).ready(function ($) {
    
    $(".custom-select").select2({
            tags: "true",
            placeholder: "Select an option",
            allowClear: true,
            width: '100%',
            createTag: function(params) {
                var term = $.trim(params.term);

                if (term === '') {
                    return null;
                }

                return {
                    id: term,
                    text: term,
                    value: true // add additional parameters
                }
            }
        });

     jQuery(".selecte-serachby").live("click", function() {
        jQuery(".serach-by-dropdown").slideToggle("100", function() {
            // Animation complete
        });
        // jQuery('.sort-by-dropdown').fadeIn('slow');
        jQuery('.sort-by-dropdown').hide();
    });

     
    jQuery("#subject-id-dropdown").live("change", function() {
           jQuery('.term-id-value').val('');     
    });

     jQuery(".selecte-sortby").live("click", function() {
              jQuery(".sortby-dropdown-ul").slideToggle("100", function() {
            // Animation complete
        });
        // jQuery('.sort-by-dropdown').fadeIn('slow');
      //  jQuery('.sortby-dropdown-ul').hide();
    });

     jQuery(".sortby-dropdown").live("click", function() {
        jQuery('.sortby-dropdown').removeClass('active');
        jQuery(this).addClass('active');
        Jqury_ajax_call();
    });
    jQuery("#country-dropdown").live("change", function(){
        $countrydropdown    = jQuery('#country-dropdown').val();
            $.ajax({
            url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
            method:'Post',
            data: {
                'action': 'educator_listing_drodpown',
                'countrydropdown':$countrydropdown
            },
            success:function(data) {
                // This outputs the result of the ajax request
                //jQuery('#change-dropdown-educator').html(data);
                jQuery('#educators-drodown').html(data);

                jQuery('.chosen-select').chosen({
                width: "100%"
                });
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });
    });  

   function Jqury_ajax_call(){
     jQuery('.loading').show();
            jQuery('.page-template-taxonomy-subject-php').addClass('loader-body');
            jQuery('.tax-subject').addClass('loader-body');
            $educatorname    = jQuery('#educators-drodown').val();
            $studylevel      = jQuery('#studylevel-dropdown').val();
            $countrydropdown = jQuery('#country-dropdown').val();
            $sortdropdown    = jQuery('ul.sortby-dropdown-ul').find('li.active').children().data('value');
            if($sortdropdown == 'undefined'){
                $sortdropdown = 'date';
            }           
            $sortbyvalue     = $sortdropdown;
            $study = jQuery('.studylevel-id').val();
            if($study !=''){
                $studylevel     =   $study;
            }
            $coutryid = jQuery('.country-id-value').val();
            if($coutryid !=''){
                $countrydropdown    =  $coutryid;
            }
            //alert($educatorname);
            $pagenumber =1;
           // $termid = jQuery('.term-id-value').val();
            $termid = jQuery('#subject-id-dropdown').val().trim();
            if($termid == ''){
                $termid    = jQuery('.term-id-value').val();
            }

            $.ajax({
            url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
            method:'Post',
            data: {
                'action': 'course_listing_sections',
                'page' : $pagenumber,
                'termid' : $termid,
                'studylevel': $studylevel,
                'educatorname':$educatorname,
                'countrydropdown':$countrydropdown,
                'sortbyvaluedropdown':$sortbyvalue,
            },
            success:function(data) {
                // This outputs the result of the ajax request
                jQuery('#course-listing-data').html(data);
                jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
                jQuery('.tax-subject').removeClass('loader-body');
                AOS.init({
                easing: 'ease-in-out-sine'
                });
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        }); 

   }

    jQuery(".chosen-serach-dropdown").live("change", function(){
        Jqury_ajax_call();
    }); 
    jQuery(".nav-links").live("click", function(){
        jQuery('.loading').show();
        jQuery('.page-template-taxonomy-subject-php').addClass('loader-body');
        jQuery('.tax-subject').addClass('loader-body');

        $pagenumber = jQuery(this).data('page');
        $termid = jQuery('#subject-id-dropdown').val().trim();
        if($termid == ''){
            $termid    = jQuery('.term-id-value').val();
        }
        $educatorname    = jQuery('#educators-drodown').val();
        $studylevel      = jQuery('#studylevel-dropdown').val();
        $countrydropdown = jQuery('#country-dropdown').val();
        $sortdropdown    = jQuery('ul.sortby-dropdown-ul').find('li.active').children().data('value');
        if($sortdropdown == 'undefined'){
            $sortdropdown = 'date';
        }           
        $sortbyvalue     = $sortdropdown;
        $study = jQuery('.studylevel-id').val();
        if($study !=''){
        $studylevel     =   $study;
        }
        $coutryid = jQuery('.country-id-value').val();
        if($coutryid !=''){
        $countrydropdown    =  $coutryid;
        }

        $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method:'Post',
                data: {
                    'action': 'course_listing_sections',
                    'page' : $pagenumber,
                    'termid' : $termid,
                    'studylevel': $studylevel,
                    'educatorname':$educatorname,
                    'countrydropdown':$countrydropdown,
                    'sortbyvaluedropdown':$sortbyvalue,
                },
                success:function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#course-listing-data').html(data);
                    jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
                    jQuery('.tax-subject').removeClass('loader-body');
                    AOS.init({
                    easing: 'ease-in-out-sine'
                    });
                    jQuery('html, body').animate({
                    scrollTop: jQuery(".business-sectionbox").offset().top
                    }, 1000);
                
                },
                error: function(errorThrown){
                    console.log(errorThrown);
                }
            }); 
    }); 
});  
