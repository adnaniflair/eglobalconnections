<?php
/* Template Name: Educator List Template */

get_header(); 
//session_start();
$term_id ='';
//print_r($_REQUEST);
$countrydropdown        = '';
if(isset($_REQUEST['country-id'])){ 
    $countrydropdown        = $_REQUEST['country-id'];
}

if(isset($_REQUEST['subject-id'])){ 
    if(!empty($_REQUEST['subject-id'])){
        $term_id = $_REQUEST['subject-id'];
    }
}
if(isset($_REQUEST['educatoridtype-id'])){
    if(!empty($_REQUEST['educatoridtype-id'])){
        $educatortypeid = $_REQUEST['educatoridtype-id'];
    }
}


$term           = get_term_by( 'slug',$countrydropdown, 'Country');
$termsname      = $term->name;
$termsuid       = $term->term_id;
$countryflag    = ege_countryflagurl($termsuid,'educator-list');

$termdescription = $term->description;
$paged = 1;
if(!empty($_REQUEST['etype'])){
    $educatortypeid    = $_REQUEST['etype'];
}
if(!empty($countrydropdown)){
    $countrydropdownsarray = array(
    'taxonomy' => 'Country', // Taxonomy name
    'field' => 'slug',
    'terms' => $countrydropdown
    );
}

//Study Level Type Done
if(!empty($educatortypeid)){
        $countrydropdownsarray = array( // (array) - use taxonomy parameters (available with Version 3.1).
        array(
        'taxonomy' => 'educator-type',
        'field' => 'slug',
        'terms' => $educatortypeid,
        //'include_children' => false,
        //'operator' => 'NOT IN'
        )
        );
}

if(!empty($countrydropdown) && !empty($educatortypeid)) {
$countrydropdownsarray = array( // (array) - use taxonomy parameters (available with Version 3.1).
'relation' => 'AND', // (string) - The logical relationship between each inner taxonomy array when there is more than one. Possible values are 'AND', 'OR'. Do not use with a single inner taxonomy array. Default value is 'AND'.
array(
'taxonomy' => 'Country', // (string) - Taxonomy.
'field' => 'slug', // (string) - Select taxonomy term by Possible values are 'term_id', 'name', 'slug' or 'term_taxonomy_id'. Default value is 'term_id'.
'terms' => $countrydropdown // (int/string/array) - Taxonomy term(s).
//'include_children' => true, // (bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
//'operator' => 'IN' // (string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS'. Default value is 'IN'.
),
array(
'taxonomy' => 'educator-type',
'field' => 'slug',
'terms' => $educatortypeid,
//'include_children' => false,
//'operator' => 'NOT IN'
)
);

}


if(!empty($educatortypeid)){
            $educatortypeterm   = get_term_by( 'slug',$educatortypeid, 'educator-type');
            $educatorname       = $educatortypeterm->name;
            $educatormaintitle  = $educatorname;
}else{
    $educatormaintitle = get_the_title();
}

//'page_id' => 119, 
//'post__in' =>array(119, 184, 79),

//dad
$term = get_term_by( 'id',$term_id, 'subject');
if(!empty($term)){
$subject_array = array(
    'taxonomy' => 'subject',
    'field'    => 'slug',
    'terms' => $term->slug
    );

$universityposts = get_posts(
    array(
    'posts_per_page' => -1,
    'order' => 'ASC',
    //'orderby' =>'date',
    'paged' => $paged,
    'post_type'     =>'course',
    'tax_query' => array($subject_array),
    )
);
if(!empty($universityposts)){ 
    $unvisityid = array();
    foreach ($universityposts as $postvalue) {
        # code...
         $courseid     =  $postvalue->ID;
         $universityid = get_field('select_university',$courseid);

         //print_r($studylevel); 
         if(!empty( $universityid)){
           $unvisityid[]   = $universityid;
         }
         //echo '<br>';
    //echo $universityname = get_the_title($unvisityid);
    }
}

if(!empty($unvisityid)){  
    $educators_list = (array_unique($unvisityid));
}
}

// 'tax_query' => array($countrydropdownsarray),
//    'tax_query' => array($countrydropdownsarray),
//'post__in' => $educators_list,
if(!empty($educator_dropdown_id) ){
    $educators_list = array($educator_dropdown_id);
    $the_query = new WP_Query( array(
    'posts_per_page' => 8,
    'order' => 'DESC',
    'post__in' => $educators_list,
    'orderby' =>'date',
    'paged' => $paged,
    'post_type'     =>'educator',
       ) 
    );
}else{
    $the_query = new WP_Query( array(
    'posts_per_page' => 8,
    'order' => 'DESC',
    'post__in' => $educators_list,
    'orderby' =>'date',
    'paged' => $paged,
    'post_type'     =>'educator',
    'tax_query' => array($countrydropdownsarray),
   
    )
    );
}

$banner_image    = get_field('banner_image',get_the_id());
$header_title    = get_field('header_title',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}

//Advertisment Sections
$ad_title               = get_field('ad_title',get_the_id());
$descriptions           = get_field('descriptions',get_the_id());
$download_pdf           = get_field('download_pdf',get_the_id());
$advertisement_image    = get_field('advertisement_image',get_the_id());
if(empty($advertisement_image)){
    $advertisement_image = get_stylesheet_directory_uri() .'/assettwo/images/united-kingdom-img.png';
}
?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/js/select2.min.js"></script>
<link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/select2.min.css" rel="stylesheet" />
<section class="inner-bannerbox green-bgcover topgreen-border" style="background-image: url(<?php echo $banner_image;  ?>">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="#"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <span class="banerpage-icon" style="display:none;">
                        <?php  if(!empty($countryflag)) { ?>
                            <img src="<?php echo $countryflag; ?>" alt="">
                        <?php } ?>
                        </span>
                        <?php if($educatormaintitle == 'University'){
                                $educatormaintitle = 'Universitie';
                             }
                            ?>
                        <h2 class="educator-main-title">
                            <?php echo get_the_title(); ?>
                        </h2>
                    </div>
                    <div class="pagebaner-allconte cf" style="display:none" >
                        <p>
                            <?php echo $the_query->found_posts; ?>
                            <?php /*
                            <?php echo $educatormaintitle.'s'; ?> in
                            <?php echo $termsname; ?>
                            */ ?>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="business-sectionbox">
    <div class="wrapper cf">
        <div class="minusmartin-top cf">
            <div class="leftbusinessbox whitebg equal-height" id="country-name-html">
                <h3><?php echo $header_title?></h3>
                <p>
                    <?php 
                    wp_reset_postdata();
                    echo get_the_content(); 
                    ?>
                </p>
            </div>
            <div class="rightbusiness-box cf greybg equal-height">
                <div class="rightleft-textbox">
                    <h4><?php echo $ad_title;  ?></h4>
                    <p><?php echo $descriptions;  ?></p>
                    <a download="download" href="<?php echo $download_pdf; ?>" class="download-links">Download</a>
                </div>
                <div class="rightrit-imgbox">
                    <img src="<?php echo $advertisement_image; ?>" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>

<section class="course-section greybg remove30padingtop">
    <div class="wrapper cf">
        <div class="toggle-buttonbox cf">
            <div class="left-toptip-contbox">
              <span  >Click to search the things that are most important to you, and we'll show you what we've got.</span> 
            </div>
            <div class="selectfor-by">
                <span class="selecte-serachby"><i class="fa fa-align-left"></i>Search By</span>
            </div>
            <div class="select-sorbybtn">
                <span class="selecte-sortby"><i class="fa fa-sliders"></i>Sort By</span>
                <div class="sorby-main-drodown">
                   <ul  class="sortby-dropdown-ul">
                       <li  class="sortby-dropdown" ><a data-value="worldrank">World Ranking</a></li>
                        <li class="sortby-dropdown" ><a  data-value="popularity">Popularity</a></li>
                      
                    </ul>
                </div>
            </div>
        </div>

        <!--<div class="dropdownmain-box sort-by-dropdown cf" data-aos="fade-left" data-aos-duration="1200" style="display:none">
            <div class="drop-labelbox"><label>Sort by</label></div>
            <div class="dropdown-blocks">
                <select id="sortby-dropdown" class="chosen-serach-dropdown  chosen-select">
                    <option value="date">Sort By</option>
                    <option value="popularity">Popularity</option>
                </select>
            </div>
        </div>-->
        <div class="dropdownmain-box serach-by-dropdown cf" data-aos="fade-left" data-aos-duration="1200" style="display:none">
            <div class="drop-labelbox"><label>Search by</label></div>
            <div class="dropdown-blocks">
                <?php 
                        // echo "<pre>";    
                        // print_r($_SESSION);
                        // echo "</pre>"; 

                        //   echo "<pre>";
                        //   print_r($educators_list);
                        //   echo "<pre>";


  
                        $country_args = array(
                        'orderby'       => 'name', 
                        'order'         => 'ASC',
                        'parent'        => 0,
                        'hide_empty'    => true, 

                        );

                        $countrylist = get_terms('Country',$country_args);

                        ?>
                <select name='country-id' id="select-country-name" class="chosen-serach-dropdown chosen-select">
                    <option value="">Country</option>
                    <?php

                            foreach ($countrylist as $country ) {
                            # code...
                                $selected = '';
                                if($country->slug == $countrydropdown){
                                    echo $selected = 'selected="selected"';
                                }
                                echo '<option value="'.$country->slug.'" '.$selected.' >'.$country->name.'</option>';
                            }   
                            ?>
                </select>
            </div>
            <?php
            $subject_args = array(
            'orderby'       => 'name', 
            'order'         => 'ASC',
            'parent'        => 0,
            'hide_empty'    => false, 

            );

            $subjectlist = get_terms('subject',$subject_args);

            ?>
            <select name='subject-id' id="subject-id-dropdown" class="custom-select  chosen-serach-dropdown">
                <option value=" ">Subject Area</option>
                <?php
                        foreach ($subjectlist as $subject ) {
                        # code...
                            $subjectitle = str_replace("'",'&apos;',$subject->name);
                            $subjectitle = str_replace("’",'&apos;',$subject->name);
                            $selected  ='';
                            if($term_id == $subject->term_id){
                                $selected = 'selected= selected';
                            }
                            echo '<option value="'.$subject->term_id.'" '.$selected.' >'.$subjectitle.'</option>';
                        }   
                        ?>
            </select>
        
            <div class="dropdown-blocks">
                <select name='educator-type-id' id="select-educator-type" class="chosen-serach-dropdown chosen-select">
                    <option value="">Institution Type</option>
                    <?php 
                        $educator_type_args = array(
                        'orderby'       => 'name', 
                        'order'         => 'ASC',
                        'parent'        => 0,
                        'hide_empty'    => true, 

                        );

                        $educator_typelist = get_terms('educator-type',$educator_type_args);
                        ?>
                    <?php
                        foreach ($educator_typelist as $educator_type ) {
                        # code...
                            $selected = '';
                            if($educator_type->slug == $educatortypeid){
                                    $selected = 'selected= selected';
                            }
                            echo '<option value="'.$educator_type->slug.'" '.$selected.'  >'.$educator_type->name.'</option>';
                        }   
                        ?>
                </select>
                </select>
            </div>
            <select id="selected-educator-id" class="custom-select chosen-serach-dropdown ">
                <?php
                        //     array(
                        // 'taxonomy' => 'Country', // Taxonomy name
                        // 'field' => 'slug',
                        // 'terms' => $_REQUEST['countrydropdown']
                        // )

                        $universityposts = get_posts(
                        array(
                        'post_type' => 'educator', // Post type
                        'tax_query' => array($countrydropdownsarray
                        ),
                        'posts_per_page' => -1,
                        'orderby'       => 'menu_order',
                        'order'         => 'ASC'
                        )
                        );


                    ?>

                <option value=" ">Institution name</option>
                <?php
                            if(!empty($universityposts)){

                                foreach ($universityposts as $postvalue) {
                                    # code...
                                    $selected = '';
                                    $subjectitle = str_replace("'",'&apos;',$postvalue->post_title);
                                     $subjectitle = str_replace("’",'&apos;',$postvalue->post_title);

                                  

                                    if($postvalue->ID  == $educator_dropdown_id){
                                        $selected = 'selected= selected';
                                    }
                                    echo '<option value="'.$postvalue->ID.'" '.$selected.' >'.$postvalue->post_title.'</option>'
                                  
                                    ?>
                <?php

                                }
                            }
                            ?>
            </select>
       

        </div>
        <?php


                    // $universityposts = get_posts(
                    //     array(
                    //     'post_type' => 'educator', // Post type
                    //     // 'tax_query' => array(
                    //     // array(
                    //     // 'taxonomy' => 'Country', // Taxonomy name
                    //     // 'field' => 'slug',
                    //     // 'terms' => 'south-africa'
                    //     // )
                    //     // ),
                    //     'posts_per_page' => -1,
                    //     'orderby'       => 'menu_order',
                    //     'order'         => 'ASC'
                    //     )
                    // );
                    // echo "<pre>";
                    // print_r($universityposts);
                    // echo "</pre>";
            ?>
        <div class="cf">
            <div class="school-detail-slider">
                <div class="rows cf" id="educators-listing-data">
                
                    <?php  
                    // echo "<pre>";
                    // print_r($the_query );
                    // echo "</pre>";
                    if($the_query -> have_posts()){
                ?>
                    <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                            // print_r($the_query);   
                      
                        do_action('educator_html_actions');
                    
                    endwhile;
                    }else{
                    echo "No Educators Founds";
                    }
                    wp_reset_postdata(); ?>
                    <div class="fullwidth-block">
                        <div class="page-linkox cf">
                            <?php

                        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
do_action('loginbox_html_code'); 
do_action('loginbox_html_code_two'); 
?>
<?php 
 get_footer();
?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/js/instititue-custom.js"></script>