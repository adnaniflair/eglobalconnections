<?php
/* Template Name: Product List Template */

get_header(); 
//session_start();
// $term_id ='';
// $countrydropdown        = 'united-kingdom';
// $educatortypeid         = $_REQUEST['etype'];
// $educator_dropdown_id   = '';
// if(empty($educatortypeid)){
//     if(!empty($_SESSION['educatorcountrydropdown'])){
//         $countrydropdown = $_SESSION['educatorcountrydropdown'];
//         if(!empty($_SESSION['educatortermid'])){
//             $term_id = $_SESSION['educatortermid'];
//         }

//         if(!empty($_SESSION['educatoreducatortype'])){
//             $educatortypeid = $_SESSION['educatoreducatortype'];
//         }

//         if(!empty($_SESSION['educatoreducatortypeid'])){
//             $educator_dropdown_id = $_SESSION['educatoreducatortypeid'];
//         }
//     }
// }
// $term           = get_term_by( 'slug',$countrydropdown, 'Country');
// $termsname      = $term->name;
// $termsuid       = $term->term_id;
// $countryflag    = ege_countryflagurl($termsuid,'educator-list');

// $termdescription = $term->description;
// $paged = 1;
// if(!empty($_REQUEST['etype'])){
//     $educatortype    = $_REQUEST['etype'];
// }
// $countrydropdownsarray = array(
// 'taxonomy' => 'Country', // Taxonomy name
// 'field' => 'slug',
// 'terms' => $countrydropdown
// );

// if(!empty($countrydropdown) && !empty($educatortypeid)) {
// $countrydropdownsarray = array( // (array) - use taxonomy parameters (available with Version 3.1).
// 'relation' => 'AND', // (string) - The logical relationship between each inner taxonomy array when there is more than one. Possible values are 'AND', 'OR'. Do not use with a single inner taxonomy array. Default value is 'AND'.
// array(
// 'taxonomy' => 'Country', // (string) - Taxonomy.
// 'field' => 'slug', // (string) - Select taxonomy term by Possible values are 'term_id', 'name', 'slug' or 'term_taxonomy_id'. Default value is 'term_id'.
// 'terms' => $countrydropdown // (int/string/array) - Taxonomy term(s).
// //'include_children' => true, // (bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
// //'operator' => 'IN' // (string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND', 'EXISTS' and 'NOT EXISTS'. Default value is 'IN'.
// ),
// array(
// 'taxonomy' => 'educator-type',
// 'field' => 'slug',
// 'terms' => $educatortypeid,
// //'include_children' => false,
// //'operator' => 'NOT IN'
// )
// );

// }
// if(!empty($educatortypeid)){
//             $educatortypeterm   = get_term_by( 'slug',$educatortypeid, 'educator-type');
//             $educatorname       = $educatortypeterm->name;
//             $educatormaintitle  = $educatorname;
// }else{
//     $educatormaintitle = 'Educator';
// }

// //'page_id' => 119, 
// //'post__in' =>array(119, 184, 79),

// //dad
// $term = get_term_by( 'id',$term_id, 'subject');
// if(!empty($term)){
// $subject_array = array(
//     'taxonomy' => 'subject',
//     'field'    => 'slug',
//     'terms' => $term->slug
//     );

// $universityposts = get_posts(
//     array(
//     'posts_per_page' => -1,
//     'order' => 'ASC',
//     //'orderby' =>'date',
//     'paged' => $paged,
//     'post_type'     =>'course',
//     'tax_query' => array($subject_array),
//     )
// );
// if(!empty($universityposts)){ 
//     $unvisityid = array();
//     foreach ($universityposts as $postvalue) {
//         # code...
//          $courseid     =  $postvalue->ID;
//          $universityid = get_field('select_university',$courseid);

//          //print_r($studylevel); 
//          if(!empty( $universityid)){
//            $unvisityid[]   = $universityid;
//          }
//          //echo '<br>';
//     //echo $universityname = get_the_title($unvisityid);
//     }
// }

// if(!empty($unvisityid)){  
//     $educators_list = (array_unique($unvisityid));
// }
// }
//'post__in' => $educators_list,
/**
Reset Post Data Due to  the_content Warning
**/
wp_reset_postdata();
/**
Product array wp query 
**/
$the_query = new WP_Query( array(
    'posts_per_page' => 10,
    'order'          => 'DESC',
    'orderby'        =>'date',
    'paged'          => $paged,
    'post_type'      =>'product',
    )
);
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
$header_title    = get_field('header_title',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}
?>
<section class="inner-bannerbox green-bgcover topgreen-border" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper">
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i></a></li>
                    <li><a href="#"><?php echo get_the_title(); ?></a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title">
                         <?php echo get_the_title(); ?>
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
<section class="business-sectionbox">
    <div class="wrapper cf">
        <div class="minusmartin-top cf">
            <div class="leftbusinessbox whitebg equal-height product-listings-page" id="country-name-html" style="width:100%">
                <p>
                    <?php the_content(); ?>
                </p>
            </div>
        </div>
    </div>
</section>

<section class="course-section greybg remove30padingtop">
    <div class="wrapper cf">
        <div class="toggle-buttonbox cf">
            <div class="selectfor-by" style="display:none">
                <span class="selecte-serachby"><i class="fa fa-align-left"></i>Search By</span>
            </div>
            <div class="select-sorbybtn">
                <span class="selecte-sortby"><i class="fa fa-sliders"></i>Sort By</span>
                <div class="sorby-main-drodown">
                   <ul  class="sortby-dropdown-ul">
                       <li  class="sortby-dropdown" ><a data-value="price">Price</a></li>
                        <li class="sortby-dropdown" ><a  data-value="popularity">Popularity</a></li>
                      
                    </ul>
                </div>
            </div>
      

        </div>

        <!--<div class="dropdownmain-box sort-by-dropdown cf" data-aos="fade-left" data-aos-duration="1200" style="display:none">
            <div class="drop-labelbox"><label>Sort by</label></div>
            <div class="dropdown-blocks">
                <select id="sortby-dropdown" class="chosen-serach-dropdown  chosen-select">
                    <option value="date">Sort By</option>
                    <option value="popularity">Popularity</option>
                </select>
            </div>
        </div>-->
        <div class="dropdownmain-box serach-by-dropdown cf" data-aos="fade-left" data-aos-duration="1200" style="display:none">
            <div class="drop-labelbox"><label>Search by</label></div>
            <div class="dropdown-blocks">
                <?php 
                        // echo "<pre>";    
                        // print_r($_SESSION);
                        // echo "</pre>"; 

                        //   echo "<pre>";
                        //   print_r($educators_list);
                        //   echo "<pre>";



                        $country_args = array(
                        'orderby'       => 'name', 
                        'order'         => 'ASC',
                        'parent'        => 0,
                        'hide_empty'    => true, 

                        );

                        $countrylist = get_terms('Country',$country_args);

                        ?>
                <select name='country-id' id="select-country-name" class="chosen-serach-dropdown chosen-select">
                    <?php
                            foreach ($countrylist as $country ) {
                            # code...
                                $selected = '';
                                if($country->slug == $countrydropdown){
                                    echo $selected = 'selected="selected"';
                                }
                                echo '<option value="'.$country->slug.'" '.$selected.' >'.$country->name.'</option>';
                            }   
                            ?>
                </select>
            </div>
            <div class="dropdown-blocks">
                <?php
                        $subject_args = array(
                        'orderby'       => 'name', 
                        'order'         => 'ASC',
                        'parent'        => 0,
                        'hide_empty'    => false, 

                        );

                        $subjectlist = get_terms('subject',$subject_args);

                        ?>
                <select name='subject-id' id="subject-id-dropdown" class="chosen-select chosen-serach-dropdown">
                    <option value="">Subject area</option>
                    <?php
                            foreach ($subjectlist as $subject ) {
                            # code...
                                $selected  ='';
                                if($term_id == $subject->term_id){
                                    $selected = 'selected= selected';
                                }
                                echo '<option value="'.$subject->term_id.'" '.$selected.' >'.$subject->name.'</option>';
                            }   
                            ?>
                </select>
            </div>
            <div class="dropdown-blocks">
                <select name='educator-type-id' id="select-educator-type" class="chosen-serach-dropdown chosen-select">
                    <option value="">Educator Type</option>
                    <?php 
                        $educator_type_args = array(
                        'orderby'       => 'name', 
                        'order'         => 'ASC',
                        'parent'        => 0,
                        'hide_empty'    => true, 

                        );

                        $educator_typelist = get_terms('educator-type',$educator_type_args);
                        ?>
                    <?php
                        foreach ($educator_typelist as $educator_type ) {
                        # code...
                            $selected = '';
                            if($educator_type->slug == $educatortypeid){
                                    $selected = 'selected= selected';
                            }
                            echo '<option value="'.$educator_type->slug.'" '.$selected.'  >'.$educator_type->name.'</option>';
                        }   
                        ?>
                </select>
                </select>
            </div>
            <select style="display:none" id="selected-educator-id" class="custom-select chosen-serach-dropdown ">
                <?php
                        //     array(
                        // 'taxonomy' => 'Country', // Taxonomy name
                        // 'field' => 'slug',
                        // 'terms' => $_REQUEST['countrydropdown']
                        // )
                        $universityposts = get_posts(
                        array(
                        'post_type' => 'educator', // Post type
                        'tax_query' => array(
                        array(
                        'taxonomy' => 'Country', // Taxonomy name
                        'field' => 'slug',
                        'terms' => $countrydropdown,
                        )
                        ),
                        'posts_per_page' => -1,
                        'orderby'       => 'menu_order',
                        'order'         => 'ASC'
                        )
                        );  
                    ?>

                <option value=" ">Select Educator Name</option>
                <?php
                            if(!empty($universityposts)){

                                foreach ($universityposts as $postvalue) {
                                    # code...
                                    $selected = '';
                                    if($postvalue->ID  == $educator_dropdown_id){
                                        $selected = 'selected= selected';
                                    }
                                    echo '<option value="'.$postvalue->ID.'" '.$selected.' >'.$postvalue->post_title.'</option>'
                                  
                                    ?>
                <?php

                                }
                            }
                            ?>
            </select>
       

        </div>
        <?php


                                // $universityposts = get_posts(
                    //     array(
                    //     'post_type' => 'educator', // Post type
                    //     // 'tax_query' => array(
                    //     // array(
                    //     // 'taxonomy' => 'Country', // Taxonomy name
                    //     // 'field' => 'slug',
                    //     // 'terms' => 'south-africa'
                    //     // )
                    //     // ),
                    //     'posts_per_page' => -1,
                    //     'orderby'       => 'menu_order',
                    //     'order'         => 'ASC'
                    //     )
                    // );
                    // echo "<pre>";
                    // print_r($universityposts);
                    // echo "</pre>";
            ?>
        <div class="cf">
            <div class="school-detail-slider page-product-mainbox">
                <div  id="product-listings-html" class="rows cf">
                <div class="loading overlay-bgcolor" style="display:none">
                <div class="all-middleicon-loader"> <img width="150" src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/default.svg" alt="eGlobalconnect"></div>
                </div>   
                <?php
                // echo "</pre>";
                if($the_query -> have_posts()){
                ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    $productid       = get_the_id();
                    $producttitle    = get_the_title();
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'bannerimage-product-list');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                    $countview       = pvc_get_post_views($productid);
                    $bestseller      = get_field('best_seller',$productid);

                    $productcontent  = get_the_excerpt();
                    $terms           = get_the_terms($productid,'product-categories');
                    if(!empty($terms)){
                    $categoryname    = $termsname = $terms[0]->name;
                    $termsid         = $terms[0]->term_id;
                    // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    }
                    $productprice    = get_field('product_price',get_the_id());
                    //Rating Code

                    //Review Sections  Code
                    global $wpdb;
                    $table_name = $wpdb->prefix . "review";
                    $totalproductreviws = 0;
                    $args = '';
                    $avarage =0;
                    $totalaverage =0;
                    $productview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE 
                        status = 1 AND `product_id` = ".$productid );
                    if(!empty($productview)){
                        $totalproductreviws =  count($productview);
                        $totalrating = $wpdb->get_row("SELECT AVG(`overal_rating`) as avarage FROM $table_name WHERE  status = 1 AND `product_id` =".$productid);
                        $avarage                    = $totalrating->avarage;
                        $totalaverage               = wp_star_rating_avarage_float($avarage);
                    }
                      $args = array(
                       'rating' => $avarage,
                       'type' => 'rating',
                       'number' => 5,
                    );
                    ?>
                    <div class="three-block fourblock pagepro-block">
                        <div class="white-bg ourcourse-block">
                            <div class="course-imgblock">
                                <span class="imgmain-box"><img src="<?php echo $bannerimages; ?>" alt=""></span>
                                <?php if( !empty($bestseller)){ ?>
                                <div class="bachlabl-leftbox">
                                    <span class="pre-bachelr-label">
                                          <p>Bestseller</p>
                                    </span>
                                </div> 
                                <?php } ?> 

                                <div class="inner-titwid-img">
                                   <div class="ourcourse-contbox cf">
                                       <h4><?php echo $producttitle;?></h4>
                                        <?php if(!empty($productcontent)){ ?>
                                        <p><?php echo substr($productcontent,'0','172').'..'; ?> </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php  echo do_action('product_short_list_actions'); ?>

                            <div class="our-courseblk-content">
                                <a href="<?php echo get_permalink(); ?>"><h3><?php echo $producttitle;?> </h3></a>
                                <?php if(!empty($categoryname)) { ?>
                                    <div class="ourcor-smlcont-box"><?php echo $categoryname; ?> 
                                    </div>
                                <?php } ?>
                                <div class="cf">
                                    <div class="school-detail-bottom">
                                        <div class="rate-star">
                                            <?php if(!empty($args)) { wp_star_rating($args); } ?>
                                            <a class="ratenum-like" href="javascript:void(0)" title="like">
                                                <span class="rating-number-box"><?php echo $totalaverage ?></span>
                                            </a>
                                        </div>
                                        <?php if(!empty($totalproductreviws)){ ?>
                                        <div class="review-texts">
                                            <span><?php echo $totalproductreviws; ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="year-view-box">
                                    <?php if(!empty($productprice)) { ?>
                                    <span class="rate-leftbox">From <i>£<?php echo $productprice; ?> </i></span>
                                    <?php } ?>

                                    <span class="rate-ritbox"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assettwo/images/view-ico.png">views: <i><?php echo $countview; ?></i></span>
                                </div>
                            </div>
                        </div>
                </div>
                <?php  endwhile; ?>
                <?php }else{
                    echo "No Products Founds";
                    }
                    wp_reset_postdata(); ?>
                        <div class="fullwidth-block">
                        <div class="page-linkox cf">
                            <?php
                            if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php do_action('loginbox_html_code'); ?>    

<?php 
 get_footer();
?>

<script type='text/javascript'>
    jQuery(document).ready(function($) {

        jQuery("#select-country-name").live("change", function(){
        $countrydropdown    = jQuery('#select-country-name').val();
        ajaxurl =  "<?php echo admin_url( 'admin-ajax.php' ); ?>";

        // if($countrydropdown != ''){
        // jQuery('#educators-drodown').find('option:first').attr('selected', 'selected');
        // jQuery('#educators_drodown_chosen').children('a').children('span').text('Educator Name');
        // }

        $.ajax({
        url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
        method:'Post',
        data: {
        'action': 'educator_listing_drodpown_two',
        'countrydropdown':$countrydropdown
        },
        success:function(data) {
        // This outputs the result of the ajax request
        jQuery('#selected-educator-id').html(data);
        // jQuery('.chosen-select').chosen({
        // width: "100%"
        // });

        // alert(data);
        // jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
        //jQuery('.tax-subject').removeClass('loader-body');

        },
        error: function(errorThrown){
        console.log(errorThrown);
        }
        });
        }); 

        //Jqury_ajax_call();
        // $(".custom-select").select2({
        //     tags: "true",
        //     placeholder: "Select an option",
        //     allowClear: true,
        //     width: '100%',
        //     createTag: function(params) {
        //         var term = $.trim(params.term);

        //         if (term === '') {
        //             return null;
        //         }

        //         return {
        //             id: term,
        //             text: term,
        //             value: true // add additional parameters
        //         }
        //     }
        // });

        jQuery(".selecte-sortby").live("click", function() {
        jQuery(".sortby-dropdown-ul").slideToggle("100", function() {
        // Animation complete
        });
        // jQuery('.sort-by-dropdown').fadeIn('slow');
        //  jQuery('.sortby-dropdown-ul').hide();
        });

        jQuery(".selecte-serachby").live("click", function() {
            jQuery(".serach-by-dropdown").slideToggle("100", function() {
                // Animation complete
            });
            // jQuery('.sort-by-dropdown').fadeIn('slow');
            jQuery('.sort-by-dropdown').hide();


        });

    jQuery(".sortby-dropdown").live("click", function() {
        jQuery('.sortby-dropdown').removeClass('active');
        jQuery(this).addClass('active');
        Jqury_ajax_call();
    });
    
    function Jqury_ajax_call(){
            jQuery('.loading').show();
            jQuery('.loading').show();
            jQuery('.page-template-page-product').addClass('loader-body');
            //jQuery('.tax-subject').addClass('loader-body');
            $pagenumber      = 1;
            $termid          = '';
            $educatortype    = jQuery('#select-educator-type').val();
            $studylevel      = '';
            $countrydropdown = jQuery('#select-country-name').val();
            $sortdropdown    = jQuery('ul.sortby-dropdown-ul').find('li.active').children().data('value');
            if($sortdropdown == 'undefined'){
                $sortdropdown = 'date';
            }           
            $sortbyvalue     = $sortdropdown;
            $termid          = jQuery('#subject-id-dropdown').val();
            $educatortypeid  = jQuery.trim(jQuery('#selected-educator-id').val());

            ajaxurl          = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method: 'Post',
                data: {
                    'action': 'product_listing_sections',
                    'page': $pagenumber,
                    'termid': $termid,
                    'studylevel': $studylevel,
                    'educatortype': $educatortype,
                    'educatortypeid': $educatortypeid,
                    'countrydropdown': $countrydropdown,
                    'sortbyvaluedropdown': $sortbyvalue,
                },
                success: function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#product-listings-html').html(data);
                    //jQuery('#country-name-html').html('TEXT IS COMing');
                    // jQuery('.page-template-taxonomy-subject-php').removeClass('loader-body');
                    //jQuery('.tax-subject').removeClass('loader-body');
                    jQuery('.loading').hide();
                    jQuery('.page-template-page-product').removeClass('loader-body');
                    equalheight('.three-block.fourblock.pagepro-block');

                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
    }
        // jQuery("#educators-drodown").live("change", function(){

        //       $educatorname    = jQuery('#educators-drodown').val();
        //         if($educatorname != ''){
        //             jQuery('#country-dropdown').find('option:first').attr('selected', 'selected');
        //             jQuery('#country_dropdown_chosen').children('a').children('span').text('Country');
        //         }
        //  }); 
        // jQuery("#country-dropdown").live("change", function(){
        //     $countrydropdown    = jQuery('#country-dropdown').val();
        //     if($countrydropdown != ''){
        //     jQuery('#educators-drodown').find('option:first').attr('selected', 'selected');
        //     jQuery('#educators_drodown_chosen').children('a').children('span').text('Educator Name');
        //     }
        // });     
        jQuery(".chosen-serach-dropdown").live("change", function() {
                   Jqury_ajax_call(); 
        });
        jQuery(".nav-links").live("click", function() {
            jQuery('.loading').show();
            jQuery('.page-template-page-product').addClass('loader-body');
            jQuery('.tax-subject').addClass('loader-body');

            $pagenumber = jQuery(this).data('page');
            $termid = jQuery('.term-id-value').val();
            $educatorname = '';
            $studylevel = '';
            $countrydropdown = jQuery('#select-country-name').val();
            $sortbyvalue = '';

            ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method: 'Post',
                data: {
                    'action': 'product_listing_sections',
                    'page': $pagenumber,
                    'termid': $termid,
                    'studylevel': $studylevel,
                    'educatorname': $educatorname,
                    'countrydropdown': $countrydropdown,
                    'sortbyvaluedropdown': $sortbyvalue,
                },
                success: function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#product-listings-html').html(data);
                    jQuery('.loading').hide();
                    jQuery('.page-template-page-product').removeClass('loader-body');
                    equalheight('.three-block.fourblock.pagepro-block');


                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
    });

</script>
