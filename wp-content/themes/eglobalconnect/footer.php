<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
	<footer>
		<div class="footer-top">
			<div class="wrapper">
				<div class="footer-column newsletter">
				<?php
				if (!empty(get_field('community_title','option'))) {
					echo "<h3>". get_field('community_title','option') ."</h3>";
				}
				if (!empty(get_field('community_short_description','option'))) {
					echo "<p>". get_field('community_short_description','option') ."</p>";
				}
					echo  do_shortcode('[mc4wp_form id="2606"]'); 
				?>
				</div>
				<div class="footer-column">
					<h3><?php echo __('contact us' ,'eglobaleducation') ?></h3>
					<?php
					wp_nav_menu( array(
					'theme_location' => 'Contact-us',
					'container'=>false,
					'menu_class'=>''
					) );
					?>
				
				</div>
				<div class="footer-column">
<?php
// $locations = get_nav_menu_locations(); //get all menu locations
// $menu = wp_get_nav_menu_object($locations['Policies']);//get the menu object
// $policiesmenuname =  $menu->name;
?>
					<h3><?php echo __('policies' ,'eglobaleducation') ?></h3>
					<?php
					wp_nav_menu( array(
					'theme_location' => 'Policies',
					'container'=>false,
					'menu_class'=>''
					) );
					?>
				</div>
				<div class="footer-column">
					<h3><?php echo __('About Us' ,'eglobaleducation') ?></h3>
					<?php
					wp_nav_menu( array(
					'theme_location' => 'abouts-us',
					'container'=>false,
					'menu_class'=>''
					) );
					?>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="wrapper">
				<div class="footer-logo">
					<a href="#" title="eGlobalconnect">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/logo.png" alt="eGlobalconnect">
					</a>
				</div>
				<div class="copyrigh-text">
					<p>
					Copyright © <?php echo date('Y'); ?> eglobaleducation.co.uk | All rights reserved
					</p>
				</div>
				<div class="social-icons">
					<?php
						$facebook_url = get_field('facebook_url','option');
						$twitter_url = get_field('twitter_url','option');
						$you_tube_url = get_field('you_tube_url','option');
						$insta_gram_url = get_field('insta_gram_url','option');
					?>
					<ul>
						<li>
							<a target="_blank" href="<?php echo $facebook_url; ?>" title="facebook">
								<i class="fa fa-facebook"></i>
							</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo $linkedin_url; ?>" title="linkedin">
								<i class="fa fa-linkedin"></i>
							</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo $twitter_url; ?>" title="twitter">
								<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo $you_tube_url; ?>" title="youtube">
								<i class="fa fa-youtube"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
	</footer>

<?php wp_footer(); ?>

</div>	

</body>
</html>
