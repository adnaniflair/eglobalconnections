<?php /* Template Name: Home Page Template */ 
get_header(); 
$checkvideoorimage   = get_field('choose_slider_options');
$banner_title 		 = get_field('banner_title');
$banner_descriptions = get_field('banner_descriptions');
$recommended_schools_and_universities = get_field('recommended_schools_and_universities');
$recommended_schools_and_universities_sub_title = get_field('recommended_schools_and_universities_sub_title');
$review_title      = get_field('review_title');
$popular_subjects  = get_field('popular_subjects');
$videostyle        = 'display:none';
$imageblock        = 'display:none';
if($checkvideoorimage == 'Image'){
	$imageblock = 'display:block';
}
if($checkvideoorimage == "Video"){
	$videostyle = 'display:block';
}


?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/js/select2.min.js"></script>
<link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/select2.min.css" rel="stylesheet" />
	<section class="banner">
        <div class="home-bannerbox owl-carousel owl-theme" id="home-bnr"  style="<?php echo $imageblock; ?>"  >
			<?php

			// check if the repeater field has rows of data
			if( have_rows('home_page_slider') ):

			// loop through the rows of data
			while ( have_rows('home_page_slider') ) : the_row();
		    $bannerimage =  get_sub_field('banner_image');

			// display a sub field value
		    ?>
		    <?php if(!empty($bannerimage)) {?>
			<div class="home-slider item" style="background-image: url(<?php  echo get_sub_field('banner_image'); ?>">

               <div class="table"><div class="table-cell">
                <div class="wrapper">
                    <div class="banner-text">
                        <h1 data-aos="fade-up" data-aos-duration="300" class="aos-init aos-animate"><?php  echo get_sub_field('title'); ?></h1>
                          <p data-aos="fade-up" data-aos-duration="500" class="aos-init"><?php  echo get_sub_field('descriptions'); ?></p>
                    </div>
                    <div class="banner-course-filter aos-init aos-animate" data-aos="fade-up" data-aos-duration="700">
                        <?php do_action('serach_filter_code'); ?>
                    </div>
                </div>
                </div></div>	
            </div>
	        <?php } ?>
		    <?php

			endwhile;

			else :

			// no rows found

			endif;

			?>            
        </div>
        <?php 
			$videourl = get_field('home_page_video');
			if(!empty($videourl)){
        ?>	
        <div id="video-viewport" style="<?php echo $videostyle; ?>">
            <video autoplay="" muted="" loop="" playsinline="">
	            <source src="<?php echo $videourl; ?>" type="video/mp4">
	            <source src="<?php echo $videourl; ?>" type="video/webm">
            </video>
            <div class="video-contentbox">
                <div class="table">
                    <div class="table-cell">
                        <div class="wrapper">
                            <div class="banner-text">
                            	<?php if(!empty($banner_title)){ ?>
                                <h1 data-aos="fade-up" data-aos-duration="300" class="aos-init aos-animate"><?php echo $banner_title; ?></h1>
                                 <?php } ?>
                                  <?php if(!empty($banner_descriptions)) {?>
                                  <p data-aos="fade-up" data-aos-duration="500" class="aos-init">
                                  	<?php echo $banner_descriptions; ?>
                                  </p>
	                              <?php } ?>
                            </div>
                            <div class="banner-course-filter aos-init aos-animate" data-aos="fade-up" data-aos-duration="700">
                                <?php do_action('serach_filter_code'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	    <?php } ?>
		<a href="#popular-subjects" class="click-down"><i class="fa fa-angle-double-down"></i></a>	
        
	</section>
	<div   class="mobile-div-block banner-course-filter aos-init aos-animate" data-aos="fade-up" data-aos-duration="700">
		<div class="wrapper">
				<?php do_action('serach_filter_code'); ?>
		</div>		
	</div>
	<?php  do_action('popular_subject_html'); ?>
	<section class="recomended-school" style="display:none;">
		<div class="wrapper">
			<div class="head-text">
				<h2><?php echo $recommended_schools_and_universities;  ?></h2>
				<p><?php echo $recommended_schools_and_universities_sub_title; ?> </p>
			</div>
			<div class="bxslider university-slider">
					<?php  
					$cat_args = array(
					'orderby'       => 'term_id', 
					'order'         => 'ASC',
					'hide_empty'    => false, 
					);

					$universityterms = get_terms('Country',$cat_args);
					$firstcont = $universityterms[0];
					$firstcatid = $firstcont->term_id;
					//echo get_field('add_image','Country_9');

					//$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type ) );
			
					foreach ($universityterms as  $value) {
						# code...
						$countryname = 	$value->name;
						$countryslug = 	$value->slug;
					 	$countrydescriptions = $value->description;
						$image = get_field('add_image',$value->taxonomy . '_' . $value->term_id);
						?>
						<a href="<?php echo site_url().'/'.$countryslug ?>" class="item university-block university-block-sections" data-name="<?php echo $countryslug;  ?>" data-id="<?php echo $value->term_id; ?>">
							<div class="block-image">
								<div class="image-overlay"></div>
								<img src="<?php echo $image; ?>">
							</div>
							<div class="university-detail">
								<div class="detail-inner">
									<h3><?php echo $countryname; ?></h3>
									<p><?php echo $countrydescriptions; ?></p>
								</div>
							</div>
						</a>
						<?php
					}
					?>
			</div>
			<div class="school-detail-slider" style="display:none">
				<!-- <p><?php //echo $recommended_schools_and_universities_sub_title; ?></p> -->

			<?php
						
						$universityposts = get_posts(
						array(
						'post_type' => 'educator', // Post type
						'tax_query' => array(
						array(
						'taxonomy' => 'Country', // Taxonomy name
						'field' => 'slug',
						'terms' => 'south-africa'
						)
						),
						'posts_per_page' => 2,
						'orderby'       => 'menu_order',
						'order'         => 'ASC'
						)
						);	

						//print_r($universityposts);
						if(!empty($universityposts)){
						echo '<p>' . category_description( $firstcatid ) .'</p>';
						echo '<div class="subslide">';
						foreach ($universityposts as $postvalue) {
							# code...
							$universityid       =  $postvalue->ID;
                            $unvisitylogo       = get_field('university_logo',$universityid );
							$universitytitle    = $postvalue->post_title;
							$universitycontent  = $postvalue->post_content;
							$terms              = get_the_terms($universityid,'Country');
							$countryname        =  $termsname = $terms[0]->name;
							$termid             =  $terms[0]->id;
							$countview          = pvc_get_post_views($universityid);
							$bannerimage        = get_the_post_thumbnail_url( get_the_id(),  'bannerimage-educators-list' );
                            if(empty($bannerimage)){
                              $bannerimages     =   get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                            }
                          $wordranktooltipcontent = get_field('world_rank_tooltip',$universityid );
							//print_r($terms);	
							?>
						<div class="school-detail-block  educator-blocks">
						 <div class="school-image">
							     <?php  if(!empty($unvisitylogo)) { ?>
	                                <span class="min-imgbox educatort-logo-image"><img src="<?php echo $unvisitylogo ?>" width="65" /></span>
	                               <?php }  ?>
						     <?php
	                                if(!empty($bannerimage)){
	                                    echo get_the_post_thumbnail($universityid,'bannerimage-educators-list');
	                                }else{
	                               ?>
	                                <img src="<?php echo $bannerimages; ?>">
	                                <?php
	                             } ?>
	                                <?php 
	                                    echo do_action('educator_short_list_actions')
	                                 ?>
							<div class="image-overlay">
								<div class="image-overlay-inner">
									<p><?php echo $universitycontent;  ?></p>
									<span style="display:none;">there is a course in english</span>
								</div>
							</div>
						</div>
					<div class="school-details-wrap">
						<a href="<?php echo get_permalink($universityid); ?>"><h3><?php echo $universitytitle; ?></h3></a>
						<span class="school-flag">
							<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/school-flag.png">
							<p><?php echo $countryname; ?></p>
						</span>
						<div class="school-detail-bottom">
                            <div class="rate-star">
                                <span class="like-count">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </span>
                                <a class="rate-like" href="javascript" title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
                                  <div class="rate-discr" ><p>Total  Rating 4 out of 5 - Good</p></div>
                            </div>
							<div class="world-ranks">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/graph-ico.png">
								<p>World Ranking:124</p>
								<?php if(!empty($wordranktooltipcontent)){ ?>
								  <div class="worldrank-desc" ><p><?php echo   $wordranktooltipcontent; ?></p></div>
								<?php } ?>
							</div>
							<div class="views-count">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png">
								<p>Views:<?php echo $countview ?></p>
							</div>
						</div>
					</div>
				</div>
			<?php
						}
					echo '</div>';
					}

			?>
		</div>
	
	</div>
	
	</section>
	
	
	<!-- new banner with slider -->
	<section class="recomm-slider-box">
	    <div class="wrapper cf" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/home-sliderbanner.png);">
	        <h2><?php echo $recommended_schools_and_universities;  ?></h2>
	        <div class="desti-sliderbox owl-carousel owl-theme">
	        	<?php /*
	            <div  class="destina-blocks">
	                <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/united-kindom-img.jpg" alt=""></span>
	                <div class="detai-titi-box">
	                    <h3>United Kingdom</h3>	                    
	                </div>
	            </div>
	            <div class="destina-blocks">
	                <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/germany-image.jpg" alt=""></span>
	                <div class="detai-titi-box">
	                    <h3>Germany</h3>	                   
	                </div>
	            </div>
	            <div class="destina-blocks">
	                <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/canada-image.jpg" alt=""></span>
	                <div class="detai-titi-box">
	                    <h3>Canada</h3>	                    
	                </div>
	            </div>
                <div class="destina-blocks">
	                <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/germany-image.jpg" alt=""></span>
	                <div class="detai-titi-box">
	                    <h3>Germany</h3>	                   
	                </div>
	            </div>
	            <div class="destina-blocks">
	                <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/united-kindom-img.jpg" alt=""></span>
	                <div class="detai-titi-box">
	                    <h3>United Kingdom</h3>	                   
	                </div>
	            </div>
	            */ ?>
	  
	            	<?php  
					$cat_args = array(
					'orderby'       => 'term_id', 
					'order'         => 'ASC',
					'hide_empty'    => false, 
					);
					$universityterms = get_terms('Country',$cat_args);
					$firstcont = $universityterms[0];
					$firstcatid = $firstcont->term_id;
					//ec
					foreach ($universityterms as  $value) {
						# code...
						$countryname = 	$value->name;
						$countryslug = 	$value->slug;
					 	$countrydescriptions = $value->description;
						$image = get_field('add_image',$value->taxonomy . '_' . $value->term_id);
						if(empty($image)){
							$image = get_stylesheet_directory_uri().'/assettwo/images/germany-image.jpg';
						}
						?>
						<div data-name="<?php echo $countryslug;  ?>" data-id="<?php echo $value->term_id; ?>" class="destina-blocks">
							<a  data-name="<?php echo $countryslug;  ?>" data-id="<?php echo $value->term_id; ?>" class="destina-blocks-two" href="<?php echo site_url().'/'.$countryslug ?>"><span><img src="<?php echo $image ?>" alt="<?php echo $countryname;  ?>"></span></a>
							<div data-name="<?php echo $countryslug;  ?>" data-id="<?php echo $value->term_id; ?>" class="detai-titi-box destina-blocks-two" >
								<a  data-name="<?php echo $countryslug;  ?>" data-id="<?php echo $value->term_id; ?>" class="destina-blocks-two" href="<?php echo site_url().'/'.$countryslug ?>"><h3><?php echo $countryname;  ?></h3></a>	                   
							</div>
						</div>
						<?php

					}
					?>
	        </div>
	            <?php

    			foreach ($universityterms as  $value) {
				# code...
				$countryname = 	$value->name;
				$countryslug = 	$value->slug;


			 // 	$countrydescriptions = $value->description;
				// $image = get_field('add_image',$value->taxonomy . '_' . $value->term_id);
				// if(empty($image)){
				// 	$image = get_stylesheet_directory_uri().'/assettwo/images/germany-image.jpg';
				// }
		    	//$countryslud = 'uk'; // Added by PK (22-04-2019)
				//$countryterm  = get_the_terms($countryid,'Country');
			    $school      = ege_count_educator_base_on_etype('school',$countryslug );
			    $university  = ege_count_educator_base_on_etype('university',$countryslug );
			    $college     = ege_count_educator_base_on_etype('college',$countryslug );
				$term 		 = get_term_by('slug',$countryslug, 'Country');
				$description = $term->description;


				?>
	        <div style="display:none" class="country-slider-infobox <?php echo $countryslug;  ?>">
		            <div id="<?php $countryslud ?>" class="countr-ullibox">
	                <ul>
	                    <li>
	                        <span><?php echo $university; ?></span>
	                        <p>Universities</p>
	                    </li>
	                    <li>
	                        <span><?php echo $college; ?></span>
	                        <p>Colleges</p>
	                    </li>
	                    <li>
	                        <span><?php echo $school; ?></span>
	                        <p>High schools</p>
	                    </li>
	                    
	                </ul>
	            </div>
	            <div class="countr-detai-box">
	                <p><?php echo $description; ?></p>
	            </div>
	   
	     
	        </div>
		    <?php } ?>
	    </div>
	</section>
	
	
	
	<section class="recomended-school" style="display:none">
		<div class="wrapper">
			<div class="head-text">
				<h2><?php echo $recommended_schools_and_universities;  ?></h2>
				<p>Browse our most popular universities and schools. Just click on the link for the country in which you most like to study to get started!</p>
			</div>
			<div class="bxslider university-slider">
					<?php  
					$cat_args = array(
					'orderby'       => 'term_id', 
					'order'         => 'ASC',
					'hide_empty'    => false, 
					);

					$universityterms = get_terms('Country',$cat_args);
					$firstcont = $universityterms[0];
					$firstcatid = $firstcont->term_id;
					//echo get_field('add_image','Country_9');

					//$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type ) );
			
					foreach ($universityterms as  $value) {
						# code...
						$countryname = 	$value->name;
						$countryslug = 	$value->slug;
					 	$countrydescriptions = $value->description;
						$image = get_field('add_image',$value->taxonomy . '_' . $value->term_id);
						?>
						<a class="item university-block university-block-sections" data-name="<?php echo $countryslug;  ?>" data-id="<?php echo $value->term_id; ?>">
							<div class="block-image">
								<div class="image-overlay"></div>
								<img src="<?php echo $image; ?>">
							</div>
							<div class="university-detail">
								<div class="detail-inner">
									<h3><?php echo $countryname; ?></h3>
									<p><?php echo $countrydescriptions; ?></p>
								</div>
							</div>
						</a>
						<?php
					}
					?>
			</div>
			<div class="school-detail-slider">
				<!-- <p><?php //echo $recommended_schools_and_universities_sub_title; ?></p> -->

			<?php
						
						$universityposts = get_posts(
						array(
						'post_type' => 'educator', // Post type
						'tax_query' => array(
						array(
						'taxonomy' => 'Country', // Taxonomy name
						'field' => 'slug',
						'terms' => 'south-africa'
						)
						),
						'posts_per_page' => 2,
						'orderby'       => 'menu_order',
						'order'         => 'ASC'
						)
						);	

						//print_r($universityposts);
						if(!empty($universityposts)){
						echo '<p>' . category_description( $firstcatid ) .'</p>';
						echo '<div class="subslide">';
						foreach ($universityposts as $postvalue) {
							# code...
							$universityid       =  $postvalue->ID;
                            $unvisitylogo       = get_field('university_logo',$universityid );
							$universitytitle    = $postvalue->post_title;
							$universitycontent  = $postvalue->post_content;
							$terms              = get_the_terms($universityid,'Country');
							$countryname        =  $termsname = $terms[0]->name;
							$termid             = $terms[0]->id;
							$countview          = pvc_get_post_views($universityid);
							$bannerimage        = get_the_post_thumbnail_url( get_the_id(),  'bannerimage-educators-list' );
                            if(empty($bannerimage)){
                              $bannerimages     =   get_stylesheet_directory_uri().'/assettwo/images/lsc.png';
                            }
                        $wordranktooltipcontent = get_field('world_rank_tooltip',$universityid );
							//print_r($terms);	
							?>
						<div class="school-detail-block  educator-blocks">
						 <div class="school-image">
							     <?php  if(!empty($unvisitylogo)) { ?>
	                                <span class="min-imgbox educatort-logo-image"><img src="<?php echo $unvisitylogo ?>" width="65" /></span>
	                               <?php }  ?>
						     <?php
	                                if(!empty($bannerimage)){
	                                    echo get_the_post_thumbnail($universityid,'bannerimage-educators-list');
	                                }else{
	                               ?>
	                                <img src="<?php echo $bannerimages; ?>">
	                                <?php
	                             } ?>
	                                <?php 
	                                    echo do_action('educator_short_list_actions')
	                                 ?>
							<div class="image-overlay">
								<div class="image-overlay-inner">
									<p><?php echo $universitycontent;  ?></p>
									<span style="display:none;">there is a course in english</span>
								</div>
							</div>
						</div>
					<div class="school-details-wrap">
						<a href="<?php echo get_permalink($universityid); ?>"><h3><?php echo $universitytitle; ?></h3></a>
						<span class="school-flag">
							<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/school-flag.png">
							<p><?php echo $countryname; ?></p>
						</span>
						<div class="school-detail-bottom">
                            <div class="rate-star">
                                <span class="like-count">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </span>
                                <a class="rate-like" href="javascript" title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
                                  <div class="rate-discr" ><p>Total  Rating 4 out of 5 - Good</p></div>
                            </div>
							<div class="world-ranks">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/graph-ico.png">
								<p>World Ranking:124</p>
								<?php if(!empty($wordranktooltipcontent)){ ?>
								  <div class="worldrank-desc" ><p><?php echo   $wordranktooltipcontent; ?></p></div>
								<?php } ?>
							</div>
							<div class="views-count">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/view-ico.png">
								<p>Views:<?php echo $countview ?></p>
							</div>
						</div>
					</div>
				</div>
			<?php
						}
					echo '</div>';
					}

			?>
		</div>
	
	</div>
	
	</section>
<?php /*
    <section class="introduction">
        <div class="wrapper">
            <h2 data-aos="fade-right" data-aos-duration="600">Let us introduce ourselves</h2>
<div class="introdu-box left" data-aos="flip-right"
data-aos-easing="ease-in-cubic"
data-aos-duration="1200">
                 <div class="intro-block" >
                    <div class="intro-img-wrap">
                        <img src="images/intro-1.png">
                    </div>
                    <h4>Help you choose a course</h4>
                    <p>From being there to chat as you browse courses, to interviews with experts and real students, we'll guide you all the way to finding your perfect course.</p>
                </div>
            </div>
            <div class="introdu-box middlebox" data-aos="fade-up" data-aos-duration="1200">
                <div class="intro-block">
                    <div class="intro-img-wrap">
                        <img src="images/intro-2.png">
                    </div>
                    <h4>Provide materials</h4>
                    <p>From being there to chat as you browse courses, to interviews with experts and real students, we'll guide you all the way to finding your perfect course.</p>
                </div>
            </div>
            <div class="introdu-box right" data-aos="flip-left"
     data-aos-easing="ease-in-cubic"
     data-aos-duration="1200">
                <div class="intro-block">
                    <div class="intro-img-wrap">
                        <img src="images/intro-3.png">
                    </div>
                    <h4>Support after of the course</h4>
                    <p>From being there to chat as you browse courses, to interviews with experts and real students, we'll guide you all the way to finding your perfect course.</p>
                </div>
            </div>
        </div>
    </section>
 */ ?>
	<section class="introduction">
		<div class="wrapper">
			<?php
			$intromaintitle = get_field('introductions_main_tilte');
			if(!empty($intromaintitle)){ 
			?>
			<h2 data-aos="fade-right" data-aos-duration="600" class="aos-init aos-animate"><?php echo $intromaintitle; ?></h2>
			<?php
			}
			// check if the repeater field has rows of data
			if( have_rows('introductions_sections') ):
				// loop through the rows of data
				$count =1;
				while ( have_rows('introductions_sections') ) : the_row();
					//echo "string".$count;
					$intro_image 		=	get_sub_field('intro_image');
					$intro_title 		=	get_sub_field('intro_title');
					$intro_descriptions = 	get_sub_field('intro_descriptions');
					if($count == 1){
						$data_animate = 'data-aos="flip-right" data-aos-easing="ease-in-cubic" data-aos-duration="1200"';
						$animationclass = 'left';
					}
					if($count == 2){

						$data_animate = 'data-aos="fade-up" data-aos-duration="1200"';
						$animationclass = 'middlebox';
						
					}
					if($count == 3){
						$data_animate ='data-aos="flip-left" data-aos-easing="ease-in-cubic" data-aos-duration="1200"';
						$animationclass = 'right';
					}
 				?>
			   <div class="introdu-box <?php echo $animationclass; ?>" <?php echo $data_animate; ?>>
				<div class="intro-block">
					<?php if(!empty($intro_image)){ ?>
					<div class="intro-img-wrap">
						<img src="<?php echo $intro_image  ?>">
					</div>
					<?php } if(!empty($intro_title)){ ?>
					<h4><?php echo $intro_title;  ?></h4>
				    <?php } if(!empty($intro_descriptions)){ ?>
					<p><?php echo $intro_descriptions?></p>
					<?php } ?>
				</div>	
				</div>
				<?php
				$count++;
				// display a sub field value
				endwhile;

			else :
					echo '<h2> No Rows Founds </h2>';
			// no rows found
			endif;

			?>
		</div>
	</section><!-- introduction -->
	<?php
	$servicereveiw = get_my_services_review(); 
	$review_title = get_field('review_title');

	if (!empty($servicereveiw)){ ?>
	<section class="reviews">
	    <div class="wrapper">
		
	    <h2 data-aos="fade-right" data-aos-duration="600"><?php echo $review_title; ?></h2>
	        <div id="reviews-slider" data-aos="fade-left" data-aos-duration="600" class="testmonial-block owl-carousel owl-theme">
	     	 <?php  foreach ($servicereveiw as $value) {
	                # code...
	               $userid          = $value['userid'];
	               $user_info       = get_userdata($userid);
	               $useremail       = $user_info->user_email;
	              // $user            = get_user_by( 'email',$useremail);
	               //print_r($user);
	               $firstname       = $user_info->first_name;
	               $lastname        = $user_info->last_name;
	               $userlogin       = $user_info->user_login;
	               $descriptions    = $value['descriptions'];
	               $id              = $value->id;
	               $overal_rating   = $value['overal_rating'];;
	              // echo get_avatar( $userid ); 

	               ?>
	                <div class="item testmonial-block-innner" data-animate="bounceIn animated">
	                <div class="profile-wrap">
	                    <?php echo get_avatar( $userid ,52); ?>
	                </div>
	                <h4><?php echo $userlogin; ?></h4>
	                <div class="rate-star">
						<?php if(!empty($overal_rating) ){
						$totalavaragereaming = 5;
						?>
						<span class="like-count">
						<?php for($i=1; $i<=$overal_rating; $i++ ){ ?>
						<i class="fa fa-star"></i>
						<?php } ?>
						<?php for($i=$overal_rating; $i<$totalavaragereaming; $i++ ){ ?>
						<i class="fa fa-star-o"></i>
						<?php  } ?>
						</span>
						<?php  } ?>
	                </div>
	                   <p><?php echo substr($descriptions,'0','1000'); ?></p>
	            </div>
	        <?php } ?>
	   
	        </div>
	    </div>
	</section>
	<?php } ?>
<?php /*
<script async defer src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=403915283748078&autoLogAppEvents=1"></script>

<section class="feedback-feed-section" >
    <div class="wrapper cf">
        <div class="twoblocks">	
        	<?php
        	if (!empty(get_field('facebook_title','option'))) {
					echo "<h2>". get_field('facebook_title','option') ."</h2>";
			}
			if (!empty(get_field('facebook_short_description','option'))) {
					echo "<p>". get_field('facebook_short_description','option') ."</p>";
			}
        	?>
            <div  class="fb-page" data-href="https://www.facebook.com/eGlobalConnections/" data-tabs="timeline" data-width="500" data-height="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/eGlobalConnections/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/eGlobalConnections/">eGlobal Connections</a></blockquote></div>
        </div>
        <div class="twoblocks">
            <?php
        	if (!empty(get_field('twitter_title','option'))) {
					echo "<h2>". get_field('twitter_title','option') ."</h2>";
			}
			if (!empty(get_field('twitter_short_description','option'))) {
					echo "<p>". get_field('twitter_short_description','option') ."</p>";
			}
        	?>
            <a class="twitter-timeline" data-width="500" data-height="520" data-theme="light" href="https://twitter.com/eGconnections?ref_src=twsrc%5Etfw">Tweets by eGconnections</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    </div>
</section>

*/ ?>
<?php do_action('loginbox_html_code'); ?>    

<?php get_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/js/home-page.js"></script>
