<link rel='stylesheet prefetch' href='<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/ress.min.css'>
<link href="<?php echo get_stylesheet_directory_uri() ?>/assettwo/css/verticle-slider-css.css" rel="stylesheet">
<?php
/* Template Name: Educator Profile Template */
get_header(); 

$pageid = $_REQUEST['educatoid'];
$universityid = $pageid; 
$terms  = get_the_terms($universityid,'Country');
$countryname =  $termsname = $terms[0]->name;
$unvisitylogo = get_field('university_logo',$pageid);

$unvisitybannerimages = get_field('banner_image',$pageid);
if(empty($unvisitybannerimages)){
$unvisitybannerimages =	site_url().'/wp-content/uploads/2018/12/inner-banner.jpg';
}

// Page Sub Details Sections Fileds
$content_post = get_post($pageid);
$educatorcontent = $content_post->post_content;
$educatorcontent = apply_filters('the_content', $educatorcontent);

$why_de_montfort = get_field('why_de_montfort',$pageid);
$why_de_montfort_banner_image = get_field('why_de_montfort_banner_image',$pageid);
if(empty($why_de_montfort_banner_image)){
   $why_de_montfort_banner_image = get_stylesheet_directory_uri().'/assettwo/images/bannerimg1.jpg';
}

$entry_requirements = get_field('entry_requirements',$pageid);
$entry_requirements_banner_image = get_field('entry_requirements_banner_image',$pageid);
if(empty($entry_requirements_banner_image)){
   $entry_requirements_banner_image = get_stylesheet_directory_uri().'/assettwo/images/bannerimg2.jpg';
}
$fees_and_funding = get_field('fees_and_funding',$pageid);
$fees_funding_image = get_field('fees_funding_image',$pageid);
if(empty($fees_funding_image)){
   $fees_funding_image = get_stylesheet_directory_uri().'/assettwo/images/bannerimg2.jpg';
}

$facilities = get_field('facilities',$pageid);
$facilities_banner_image = get_field('facilities_banner_image',$pageid);
if(empty($facilities_banner_image)){
   $facilities_banner_image = get_stylesheet_directory_uri().'/assettwo/images/bannerimg3.jpg';
}

$campus_culture = get_field('campus_culture',$pageid);
$culture_banner_image = get_field('culture_banner_image',$pageid);
if(empty($culture_banner_image)){
   $culture_banner_image = get_stylesheet_directory_uri().'/assettwo/images/bannerimg4.jpg';
}

$location = get_field('location',$pageid);
$locations_image = get_field('locations_image',$pageid);
if(empty($locations_image)){
   $locations_image = get_stylesheet_directory_uri().'/assettwo/images/bannerimg1.jpg';
}

// Educator Serach Dropdown 
$educatorid = array($pageid);
$educator_array = array(  
'key' => 'select_university',
'value' =>$educatorid,
'compare' => "IN",
);

//Rating 
$average = 2;
$coursereview = $wpdb->get_results( "SELECT * FROM `ege_review` WHERE `course_id` = 15 " );
$educator_background_image = get_field('educator_background_image',$pageid);
if(empty($educator_background_image)){
  $educator_background_image = 'http://www.eglobaleducation.co.uk/wp-content/uploads/2019/04/health.jpg';
}
?>
    <section  class="inner-bannerbox topgreen-border" style="background-image: url(<?php echo $unvisitybannerimages; ?>);display:none;">
        <div class="wrapper">

            <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
                <div class="breadcrum-box cf">

                    <ul class="cf">
                        <li><a href="<?php echo get_permalink($pageid); ?>"><button>Back</button></a></li>
                    </ul>
                </div>
                <div class="innerbanner-block cf">

                	<?php if(!empty($unvisitylogo)){ ?>
                    <div class="inrbanner-leftimg">
                        <img src="<?php echo $unvisitylogo; ?>" alt="" />
                    </div>
	                <?php } ?>
                    <div class="inrbaner-rightcont">
                        <div class="baner-titbox">
                            <h3><?php echo get_the_title($pageid); ?></h3>
                            <span><i class="fa fa-heart"></i></span>
                        </div>
                        <div class="banner-country-flag">
                            <span><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/small-flag1.jpg"></span>
                            <span class="dottd-btmborder"><?php echo $countryname; ?></span>
                        </div>
                        <div class="school-detail-bottom">
                            <div class="rate-star">
                                <?php if(!empty($average) ){
                                    $totalavaragereaming = 5;
                                 ?>
                                <span class="like-count">
                                    <?php for($i=1; $i<=$average; $i++ ){ ?>
                                    <i class="fa fa-star"></i>
                                    <?php } ?>
                                    <?php for($i=$average; $i<$totalavaragereaming; $i++ ){ ?>
                                    <i class="fa fa-star-o"></i>
                                    <?php  } ?>
                                </span>
                                <?php  } ?>
                                <a class="rate-like" href="javascript:void(0)" title="like"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/like-icon.png"></a>
                            </div>
                            <div class="review-texts">
                                <span>136 reviews</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=" menu-slider-class innerpage-menu-box greenbgcolor" >
        <div class="wrapper cf">
            <div class="sticky-imgtit titimgsticky" >
                <div class="sticky-tibox">
                   <div class="table">
                       <div class="table-cell">
                            <h4>Concordia University Chicago University Chicago</h4>
                       </div>
                    </div>
                </div>
            </div>
            <div style="display:none" class="enquiry-rightbox"><a href="#" class="btn enquiry-btn">Enquire now</a></div>
            <div class="iner-left-menubox" data-aos="fade-right" data-aos-duration="1200">
               <a href="javascript:void(0);" id="innermenu-click" class="innermenu-linkbox">
                   <span></span>
                   <span></span>
                   <span></span>
                </a>
                <?php if(!empty($unvisitylogo)){ ?>
                <div class="stikimg-small">
                    <a href="<?php echo get_permalink($pageid); ?>"><img src="<?php echo $unvisitylogo; ?>" alt="" width="80" /></a>
                    <h2><?php echo get_the_title($pageid); ?></h2>

                </div>
                <?php } ?>
                <ul class="cf in-responsive">

                    <?php if(!empty($why_de_montfort)){ ?>
                        <li class="menu-id-slider  <?php
                    if($_REQUEST['menuid'] == '0') echo "current-active";
                  ?>" data-id='0' >
                  <a  href="#why-de-montfort"><span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/montfort.png" width="45"></span>

                  </a></li>
                    <?php } ?>
                    <?php if(!empty($facilities)){ ?>
                    <li class="menu-id-slider <?php
                    if($_REQUEST['menuid'] == '1') echo "current-active";
                  ?>" data-id='1' ><a  href="#facilities"><span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/facilities-icon.png" width="45"></span></a></li>
                    <?php } ?>
                     <?php if(!empty($campus_culture)){ ?>
                        <li class="menu-id-slider <?php
                    if($_REQUEST['menuid'] == '2') echo "current-active";
                  ?>" data-id='2' ><a  href="#campus-culcture"><span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/campus-culcture.png" width="45"></span></a></li>
                    <?php } 
                    if(!empty($entry_requirements)){ ?>
                        <li class="menu-id-slider <?php
                    if($_REQUEST['menuid'] == '3') echo "current-active";
                  ?>" data-id='3' ><a  href="#entry-requirements"><span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/requirement-icon.png" width="45"></span></a></li>
                    <?php } ?>
                    <?php if(!empty($fees_and_funding)){ ?>
                        <li class="menu-id-slider <?php
                    if($_REQUEST['menuid'] == '4') echo "current-active";
                  ?>" data-id='4' ><a  href="#fees-and-funding"><span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/fees-and-funding.png" width="45"></span></a></li>
                    <?php } ?>
                    <?php if(!empty($location)){ ?>
                       <li class="menu-id-slider <?php
                    if($_REQUEST['menuid'] == '5')  echo "current-active";
                  ?>" data-id='5' ><a href="#location"><span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/location-icon.png" width="45"></span></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
    
    <div class="page-wrap-fullheight">

      <div id="home-slider">
        <div class="swiper-container" style="background-image: url(<?php echo $educator_background_image; ?>);">
          <div class="swiper-wrapper">
            <div class="swiper-slide swiper-slide-one">
              <div class="swiper-image" data-swiper-parallax-y="35%">
                <div class="swiper-image-inner swiper-image-right swiper-image-two left-detail-img" style="background-image: url(<?php echo $why_de_montfort_banner_image; ?>">
                
                </div>
                <div class="swiper-image-inner swiper-image-right swiper-image-two right-detai-content">
                    <div class="wrapper cf">
                        <div class="table"><div class="table-cell">
                          <div class="title-icon">
                            <span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/montfort.png" width="45"></span>
                           <h2> Why this university ? </h2>
                         </div>
                          <?php echo $why_de_montfort; ?>
                          </div></div>
                    </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide swiper-slide-two">
              <div class="swiper-image" data-swiper-parallax-y="35%">
                <div class="swiper-image-inner swiper-image-right swiper-image-four left-detail-img" style="background-image: url(<?php echo $facilities_banner_image; ?>">
                </div>
                <div class="swiper-image-inner swiper-image-right swiper-image-four right-detai-content">
                   <div class="wrapper cf">
                   <div class="table"><div class="table-cell">
                      <div class="title-icon">
                        <span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/facilities-icon.png" width="45"></span>
                        <h2>Facilities</h2>
                      </div>
                      <?php echo $facilities; ?>
                       </div></div>
                    </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide swiper-slide-three">
              <div class="swiper-image" data-swiper-parallax-y="35%">
                <div class="swiper-image-inner swiper-image-right swiper-image-six left-detail-img" style="background-image: url(<?php echo $culture_banner_image; ?>">                  
                </div>
                
                <div class="swiper-image-inner swiper-image-right swiper-image-six right-detai-content">
                  <div class="wrapper cf">
                      <div class="table"><div class="table-cell">
                      <div class="title-icon">
                      <span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/campus-culcture.png" width="45"></span> 
                      <h2>Campus Culture</h2>
                      </div>
                      <?php echo $campus_culture; ?> 
                          </div></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide swiper-slide-three">
              <div class="swiper-image" data-swiper-parallax-y="35%">
                <div class="swiper-image-inner swiper-image-right swiper-image-six left-detail-img" style="background-image: url(<?php echo $entry_requirements_banner_image; ?>">
                
                </div>
                <div class="swiper-image-inner swiper-image-right swiper-image-six right-detai-content">
                <div class="wrapper cf">
                 <div class="table"><div class="table-cell">
                   <div class="title-icon">
                    <span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/requirement-icon.png" width="45"></span> 
                    <h2>Entry Requirements</h2>
                  </div>
                   <?php echo $entry_requirements; ?>
                     </div></div>
                    </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide swiper-slide-three">
              <div class="swiper-image" data-swiper-parallax-y="35%">
                <div class="swiper-image-inner swiper-image-right swiper-image-six left-detail-img" style="background-image: url(<?php echo $fees_funding_image; ?>)">
                
                </div>
                <div class="swiper-image-inner swiper-image-right swiper-image-six right-detai-content">
                <div class="wrapper cf">
                   <div class="table"><div class="table-cell">
                    <div class="title-icon">
                      <span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/fees-and-funding.png" width="45"></span>
                      <h2>Fees & Funding </h2>
                    </div>
                    <?php echo $fees_and_funding; ?>
                       </div></div> 
                    </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide swiper-slide-three">
              <div class="swiper-image" data-swiper-parallax-y="35%">
                <div class="swiper-image-inner swiper-image-right swiper-image-six left-detail-img" style="background-image: url(<?php echo $locations_image; ?>">
                
                </div>
                <div class="swiper-image-inner swiper-image-right swiper-image-six right-detai-content">
                <div class="wrapper cf">
                  <div class="table"><div class="table-cell">
                      <div class="title-icon">
                      <span class="sub-details-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/location-icon.png" width="45"></span>
                      <h2>Location</h2>
                    </div>
                    <?php echo $location; ?> 
                      </div></div>  
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </div>
    </div>
    <section class="about-top-contebox mobile-div-991" id="go-aboutid" style="display:none;">
        <div class="wrapper cf">
            <div style="display:none;" class="title-contentbox" data-aos="fade-right" data-aos-duration="600">
                <h1>About</h1>
                 <?php  echo $educatorcontent; ?>
            </div>
            <?php if(!empty($why_de_montfort)){  ?>
            
            <div id="why-de-montfort" class="title-contentbox main-prof-box cf">
                <div class="educ-leftpro-img ">
                  <img width="600" src="<?php echo $why_de_montfort_banner_image; ?>">
                </div>
                <div class="educ-ritpro-cont ">
                   <div class="table"><div class="table-cell">
                    <h2>Why <?php echo get_the_title($pageid); ?></h2>
                    <?php echo $why_de_montfort; ?>
                    </div></div>
                </div>
                
            </div>
            <?php } ?>
            <?php if(!empty($facilities)){  ?>
            <div id="facilities" class="title-contentbox main-prof-box right cf" >
                             <div class="educ-leftpro-img ">
                  <img width="600" src="<?php echo $why_de_montfort_banner_image; ?>">
                </div>

                <div class="educ-ritpro-cont">
                <div class="table"><div class="table-cell">
                <h2>FACILITIES</h2>
                <?php echo $facilities; ?>  
                </div> 
                    </div></div>
            </div>
            <?php } ?>
            <?php if(!empty($campus_culture)){  ?>
            <div id="campus-culcture" class="title-contentbox main-prof-box cf">
                               <div class="educ-leftpro-img">
                  <img width="600"  src="<?php echo $culture_banner_image; ?>">
                </div>

                <div class="educ-ritpro-cont">
                <div class="table"><div class="table-cell">
                <h2>CAMPUS CULTURE</h2>
                <?php echo $campus_culture; ?> 
                </div>  
                    </div></div>
            </div>
            <?php } ?>
            <?php if(!empty($entry_requirements)){  ?>
            <div id="entry-requirements" class="title-contentbox main-prof-box cf right">
                              <div class="educ-leftpro-img">
                  <img width="600" src="<?php echo $entry_requirements_banner_image; ?>">
                </div>

                <div class="educ-ritpro-cont">
                <div class="table"><div class="table-cell">
                <h2>ENTRY REQUIREMENTS</h2>
                <?php echo $entry_requirements; ?>
                </div>
                    </div></div>
            </div>
            <?php } ?>
            <?php if(!empty($fees_and_funding)){  ?>
            <div id="fees-and-funding" class="title-contentbox main-prof-box cf">
                              <div class="educ-leftpro-img ">
                  <img width="600" src="<?php echo $fees_funding_image; ?>">
                </div>

                <div class="educ-ritpro-cont ">
                <div class="table"><div class="table-cell">
                <h2>FEES & FUNDING</h2>
                 <?php echo $fees_and_funding; ?> 
                </div>  
                    </div></div>
            </div>
            <?php } ?>
            <?php if(!empty($location)){  ?>
             <div id="location" class="title-contentbox main-prof-box cf right">
                               <div class="educ-leftpro-img ">
                  <img width="600" src="<?php echo $locations_image; ?>">
                </div>

                <div class="educ-ritpro-cont">
                <div class="table"><div class="table-cell">                
                 <h2>LOCATION</h2>
                 <?php echo $location; ?>   
                 </div>
                    </div></div>
            </div>
            <?php } ?>
        </div>
    </section>

<?php
$unvisitylogo = get_field('university_logo',$pageid);
?>

<section class="innerpage-menu-box greenbgcolor" style="display:none !important;">
        <div class="wrapper cf">
            <div class="sticky-imgtit titimgsticky">
              <?php if(!empty($unvisitylogo)){ ?>      
                <div class="stikimg-small">
                    <img src="<?php echo $unvisitylogo; ?>" alt="" width="60">
                </div>
                <?php } ?>

                    
                <div class="sticky-tibox">
                   <div class="table">
                       <div class="table-cell">
                            <h4><?php echo get_the_title($pageid); ?></h4>
                       </div>
                    </div>
                </div>
            </div>
            
        
            <div style="display:none" class="iner-left-menubox aos-init aos-animate" data-aos="fade-right" data-aos-duration="1200">
               <a href="javascript:void(0);" id="innermenu-click" class="innermenu-linkbox">
                   <span></span>
                   <span></span>
                   <span></span>
                </a>
                <ul class="cf in-responsive">
                    <li><a href="#go-aboutid">about</a></li>
                    <li><a href="#go-courseid">Courses</a></li>
                    <li ><a href="#go-reviewid">Reviews</a></li>
                    <li><a href="#go-rankid">Ranking</a></li>
                    <li><a href="#go-imgid">Images</a></li>
                    <li><a href="#go-videoid">Videos</a></li>
                </ul>
            </div>
        </div>
    </section>
    <section class="our-review-section" id="go-reviewid" style="display:none;">
        <div class="wrapper cf">
            <div class="top-title-contbox" data-aos="fade-right" data-aos-duration="1200">
                <h2>Our review</h2>
                <p>Consectetur adipiscing proin vitae sapien amet neque euismod vulputate<br/>interdum auctor velit in laoreet tincidunt elit.</p>
            </div>
            <div class="ourreiv-blockbox" data-aos="fade-left" data-aos-duration="1200">
                <?php  foreach ($coursereview as $value) {
                    # code...
                   $userid          = $value->userid;
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                  // $user            = get_user_by( 'email',$useremail);
                   //print_r($user);
                   $firstname       =  $user_info->first_name;
                   $lastname        =$user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $descriptions    = $value->descriptions;
                   $overal_rating    = $value->overal_rating;
                  // echo get_avatar( $userid ); 

                   ?>
                <div class="reviwebox-block cf">
                    <div class="left-riveimg-box equal-height">
                        <span class="rie-imgicons"><?php echo get_avatar( $userid ,52); ?></span>
                        <div class="revrat-box">
                            <h4><?php echo $userlogin; ?></h4>
                            <div class="cf">
                                <div class="school-detail-bottom">
                                    <div class="rate-star">
                                            <?php if(!empty($overal_rating) ){
                                            $totalavaragereaming = 5;
                                            ?>
                                            <span class="like-count">
                                            <?php for($i=1; $i<=$overal_rating; $i++ ){ ?>
                                            <i class="fa fa-star"></i>
                                            <?php } ?>
                                            <?php for($i=$overal_rating; $i<$totalavaragereaming; $i++ ){ ?>
                                            <i class="fa fa-star-o"></i>
                                            <?php  } ?>
                                            </span>
                                            <?php  } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-rivecontbox equal-height">
                        <div class="table" >
                            <div class="table-cell">
                                <p><?php echo substr($descriptions,'0','172').'...'; ?><a href="" class="arrow-links">read more</a></p>
                            </div>
                        </div>
                        <a  style="display:none;" href="#" class="helpfull-btn">Helpful</a>
                    </div>
                </div>
                   <?php
                } ?>

                <div class="reviwebox-block cf">
                    <div class="left-riveimg-box equal-height">
                        <span class="rie-imgicons"><img src="<?php echo get_stylesheet_directory_uri() ?>/assettwo/images/review-imgicon.png"></span>
                        <div class="revrat-box">
                            <h4>Victoriya Markova</h4>
                            <div class="cf">
                                <div class="school-detail-bottom">
                                    <div class="rate-star">
                                        <span class="like-count">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-rivecontbox equal-height">
                        <div class="table">
                            <div class="table-cell">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation... <a href="" class="arrow-links">read more</a></p>
                            </div>
                        </div>
                        <a  style="display:none" href="#" class="helpfull-btn">Helpful</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
<script src='<?php echo get_stylesheet_directory_uri() ?>/assettwo/js/swiper.jquery.min.js'></script>  
<script type="text/javascript">
  jQuery(document).ready(function ($) {
    setTimeout(function(){  
      jQuery('.swiper-pagination-bullet').eq(0).append('<span>Why this University ?</span>');
      jQuery('.swiper-pagination-bullet').eq(1).append('<span>Facilities</span>');
      jQuery('.swiper-pagination-bullet').eq(2).append('<span>Campus Culture </span>');
      jQuery('.swiper-pagination-bullet').eq(3).append('<span>Entry & Credit </span>');
      jQuery('.swiper-pagination-bullet').eq(4).append('<span>Fees & Funding </span>');
      jQuery('.swiper-pagination-bullet').eq(5).append('<span>location</span>');
    }, 1000);

       jQuery('.menu-id-slider a').click(function(){ 
     $menuid = jQuery(this).parent().data('id');
     jQuery('.menu-id-slider').removeClass('current-active');
     jQuery(this).parent().addClass('current-active');
     jQuery('.swiper-pagination-clickable').children().eq( $menuid ).trigger( "click" );
  });


  var mySwiper = new Swiper(".swiper-container", {
      direction: "vertical",
      loop: false,
      pagination: ".swiper-pagination",
      grabCursor: true,
      speed: 1000,
      paginationClickable: true,
      parallax: true,
      autoplay: false,
      initialSlide:<?php echo $_REQUEST['menuid'] ?>,
      effect: "slide",
      mousewheelControl: 1

    })

  jQuery('.swiper-pagination-bullet').click(function(){
     var indexnumber = jQuery(this).index();
     //alert(indexnumber);
     jQuery('.menu-id-slider').removeClass('current-active');
     jQuery('.menu-id-slider').eq(indexnumber).addClass('current-active');
  });

//   $( "li" ).each(function( index ) {
//   console.log( index + ": " + $( this ).text() );
// });

  jQuery(".swiper-slide-three").mouseover(function(){
    var indexnumber = jQuery('.swiper-pagination-bullet-active').index();
    jQuery('.menu-id-slider').removeClass('current-active');
    jQuery('.menu-id-slider').eq(indexnumber).addClass('current-active');

  });
    jQuery(".swiper-slide-one").mouseover(function(){
    var indexnumber = jQuery('.swiper-pagination-bullet-active').index();
    jQuery('.menu-id-slider').removeClass('current-active');
    jQuery('.menu-id-slider').eq(indexnumber).addClass('current-active');

  });

  jQuery(".swiper-slide-two").mouseover(function(){
    var indexnumber = jQuery('.swiper-pagination-bullet-active').index();
    jQuery('.menu-id-slider').removeClass('current-active');
    jQuery('.menu-id-slider').eq(indexnumber).addClass('current-active');

  });
  jQuery(".swiper-image-inner").mouseover(function(){
    var indexnumber = jQuery('.swiper-pagination-bullet-active').index();
    jQuery('.menu-id-slider').removeClass('current-active');
    jQuery('.menu-id-slider').eq(indexnumber).addClass('current-active');

  });
  jQuery(".swiper-slide").mouseover(function(){
    var indexnumber = jQuery('.swiper-pagination-bullet-active').index();
    jQuery('.menu-id-slider').removeClass('current-active');
    jQuery('.menu-id-slider').eq(indexnumber).addClass('current-active');

  });

  });
</script>

