<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 
$the_query = new WP_Query( array(
    'posts_per_page' => 5,
    'order'          => 'DESC',
    'orderby'        =>'date',
    'paged'          => $paged,
    'post_type'      =>'post',
    )
);
// Banner Image Code Define
$banner_image    = get_field('banner_image',get_the_id());
if(empty($banner_image)){
    $banner_image = get_stylesheet_directory_uri().'/assettwo/images/course-bannerimage.jpg';
}
?>
<section class="inner-bannerbox green-bgcover topgreen-border blog-bannerbox" style="background-image: url(<?php echo $banner_image; ?>)">
    <div class="wrapper"> 
        <div class="inner-baner-cont" data-aos="fade-left" data-aos-duration="1500">
            <div class="breadcrum-box cf">
                <ul class="cf">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="#">Blogs</a></li>
                </ul>
            </div>
            <div class="innerbanner-block cf">
                <div class="pagebanner-content">
                    <div class="pagetitle-box cf">
                        <h2 class="educator-main-title">
                         Blog
                        </h2>
                    </div>
          
                </div>

            </div>
        </div>
    </div>
</section>
  


<section class="course-section greybg remove30padingtop">

	<div class="wrapper cf" id="post-listings-html">
                    <?php
                    if($the_query -> have_posts()){
                    ?>
                    <?php while ($the_query -> have_posts()) : $the_query -> the_post(); 
                    $productid       = get_the_id();
                    $producttitle    = get_the_title();
                    $bannerimages    = get_the_post_thumbnail_url( get_the_id(),'bannerimage-product-list');
                    if(empty($bannerimages)){
                    $bannerimages    = get_stylesheet_directory_uri().'/assettwo/images/keep-calm-image.jpg';
                    } 
                     $productcontent = get_the_content();
                    // $productcontent = preg_replace("/<img[^>]+\>/i", "", $productcontent);  
                    // $productcontent = apply_filters('the_content', $productcontent);
                    // $productcontent = str_replace(']]>', ']]>', $productcontent);
                   //$productcontent  = get_the_excerpt();
                  
                    // $terms           = get_the_terms($productid,'product-categories');
                    // if(!empty($terms)){
                    // $categoryname    =  $termsname = $terms[0]->name;
                    // $termsid         = $terms[0]->term_id;
                    // // $countryflag = ege_countryflagurl($termsid,'educator-list');
                    // }

                    //Review Sections  Code
                    ?>
                    <div class="blog-mainbox cf">
                        <div class="left-blog-imgbox equal-height" style="background-image:url(<?php echo $bannerimages; ?>);"></div>
                        <div class="right-blog-box equal-height">
                            <div class="table">
                                <div class="table-cell">
                                    <div class="ritblog-content aos-init aos-animate" data-aos="fade-left" data-aos-duration="600">
                                        <div class="blog-datebox"> <?php echo get_the_time('d/m/Y', $productid); ?></div>
                                        <h4><?php echo get_the_title(); ?> </h4>
                                        <?php echo substr(strip_tags(trim($productcontent)),'0','325').'..'; ?>
                                        <div class="blog-viewmor-box">
                                            <a href="<?php echo get_permalink(); ?>">read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php  endwhile; ?>
                    <?php }else{
                    echo "No Post Founds";
                    }?>

        <div class="fullwidth-block">
        <div class="page-linkox cf">
        <?php
        if (function_exists('custom_pagination')) { custom_pagination($the_query->max_num_pages,"",$paged); }
        ?>
        </div>
        </div>  
    </div>		
</section>

<?php get_footer(); ?>
<script type='text/javascript'>
    jQuery(document).ready(function($) {
        jQuery(".nav-links").live("click", function() {
            jQuery('.loading').show();
            jQuery('.page-template-taxonomy-subject-php').addClass('loader-body');
            jQuery('.tax-subject').addClass('loader-body');

            $pagenumber = jQuery(this).data('page');

            ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
            $.ajax({
                url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                method: 'Post',
                data: {
                    'action': 'posts_listing_sections',
                    'page': $pagenumber,
                },
                success: function(data) {
                    // This outputs the result of the ajax request
                    jQuery('#post-listings-html').html(data);
                    equalheight('.equal-height');

                    //jQuery('.loading').hide();
                    //jQuery('.page-template-page-educator').removeClass('loader-body');

                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
    });

</script>