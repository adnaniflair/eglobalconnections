<?php
/**
 * Eglobal Connect MY Profile back compat functionality
 *
 *
 * @package WordPress
 * @subpackage egobal connect
 * @since egobal connect My account Funcationality Start date 26.2.2019
 */

add_action( 'wp_ajax_update_personal_informations', 'ege_update_personal_informations' );
add_action( 'wp_ajax_nopriv_update_personal_informations', 'ege_update_personal_informations' );
/**
*Update Profile code.
**/
function ege_update_personal_informations(){
	$user_id =  get_current_user_id();
	// echo '<pre>';
	// print_r($_REQUEST);
	// echo '</pre>';
	$firstname     = sanitize_text_field($_REQUEST['firstname']);
	$lastname      = sanitize_text_field($_REQUEST['lastname']);
	$phone         = sanitize_text_field($_REQUEST['phone']);
	$gener         = sanitize_text_field($_REQUEST['gender']);
	$towncity      = sanitize_text_field($_REQUEST['town-city']);
	$address       = sanitize_text_field($_REQUEST['address']);
	$zipcode       = sanitize_text_field($_REQUEST['zipcode']);
	$country       = sanitize_text_field($_REQUEST['country']);
	$dateofbirth   = sanitize_text_field($_REQUEST['date-of-birth']);

	$metas = array( 
		'first_name'  => $firstname, 
		'last_name'   => $lastname ,
		'town_city'   => $towncity ,
		'dateofbirth' => $dateofbirth ,
		'phone'  	  => $phone ,
		'gender'      => $gener,
		'address'     => $address,
		'zipcode'     => $zipcode,
		'country'     => $country,
	);
	if(!empty($metas)){
		foreach($metas as $key => $value) {
			update_user_meta( $user_id,$key,$value);
		}
		echo "Your Profile is Updated";
	}
	wp_die();
}

// Ajax Actions For social Media update Method.
add_action( 'wp_ajax_update_socialmedia_informations', 'ege_update_socialmedia_informations' );
add_action( 'wp_ajax_nopriv_update_socialmedia_informations', 'ege_update_socialmedia_informations' );
/**
* //Social Method Udate.
**/
function ege_update_socialmedia_informations(){
	$user_id =  get_current_user_id();
	$facebookurls     = sanitize_text_field($_REQUEST['facebookurls']);
	$twitterurls      = sanitize_text_field($_REQUEST['twitterurls']);
	$linkidurl        = sanitize_text_field($_REQUEST['linkiedinurl']);
	$googleurls       = sanitize_text_field($_REQUEST['googleurls']);
	$metas = array( 
		'facebookurls'  => $facebookurls, 
		'twitterurls'   => $twitterurls ,
		'linkidinurls'  => $linkidurl ,
		'googleurls'    => $googleurls ,
	);
	if(!empty($metas)){
		foreach($metas as $key => $value) {
			update_user_meta( $user_id, $key, $value );
		}
		echo "Socials Media Updated";
	}
	wp_die();
}

// Ajax Actions For Review Sections.
add_action( 'wp_ajax_update_submit-reveiew-sections', 'ege_submit_reveiew_sections' );
add_action( 'wp_ajax_nopriv_update_submit-reveiew-sections', 'ege_submit_reveiew_sections' );


// Your upload code coming here.
function ege_submit_reveiew_sections(){
		$userid             = get_current_user_id();
		$educatorid    	 	= sanitize_text_field($_REQUEST['educatorid']);
		$courseid      		= sanitize_text_field($_REQUEST['courseid']);
		$reviewtitle      	= sanitize_text_field($_REQUEST['reviewtitle']);
		$descriptions      	= sanitize_text_field($_REQUEST['useropnion']);
		$overalrate         = sanitize_text_field($_REQUEST['overalrate']);
		$tqrate         	= sanitize_text_field($_REQUEST['tqrate']);
		$sfrate      		= sanitize_text_field($_REQUEST['sfrate']);
		$lqrate       		= sanitize_text_field($_REQUEST['lqrate']);
		$vmoney       		= sanitize_text_field($_REQUEST['vmoney']);
		$ovcareer       	= sanitize_text_field($_REQUEST['ovcareer']);
		$totalimages     	= sanitize_text_field($_REQUEST['totalimages']);
		$totalvideo         = sanitize_text_field($_REQUEST['totalvideo']);
		$dateofbirth        = sanitize_text_field($_REQUEST['date-of-birth']);
    	$valid_formats      = array("jpg", "png", "gif", "bmp", "jpeg"); // Supported file types
    	$datecreated 		= date('Y-m-d H:i:s');
    	$status 			= '3';
    	// change upload directory path here folders
		add_filter( 'upload_dir', 'wpse_183245_upload_dir' );
		add_filter('intermediate_image_sizes_advanced', 'add_image_insert_override' );
 		//add_filter( 'intermediate_image_sizes', '__return_empty_array' );
 		//add_filter('intermediate_image_sizes', 'misha_deactivate_medium_large');
		$imagearray ='';
		if( !empty( $_FILES ) && $_REQUEST['totalimages'] != 0 ):

			for( $i=0;$i<$_REQUEST['totalimages'];$i++ ):
					$files = $_FILES['gallery_image_'.$i];
					$attachment_id = upload_user_file( $files );
					$attachments[] =  $attachment_id;
			endfor;
			$attach_ids = array_unique( $attachments );
			$imagearray = maybe_serialize($attach_ids);
		endif;
		$videoarray ='';
		if( !empty( $_FILES ) && $_REQUEST['totalvideo'] != 0 ):
			for( $i=0;$i<$_REQUEST['totalvideo'];$i++ ):
			$files = $_FILES['gallery_video_'.$i];
			$attachment_videoid = upload_user_file( $files );
			$attachmentsvideo[] =  $attachment_videoid;
			endfor;
			$attach_videoids = array_unique( $attachmentsvideo );
			$videoarray = maybe_serialize($attach_videoids);

		endif;

		remove_filter( 'upload_dir', 'wpse_183245_upload_dir' );
		remove_filter('intermediate_image_sizes_advanced', 'add_image_insert_override' );
 		//remove_filter( 'intermediate_image_sizes', '__return_empty_array' );
 		//remove_filter('intermediate_image_sizes', 'misha_deactivate_medium_large');
		global $wpdb;
		$table = $wpdb->prefix.'review';
		$data = array('Title'=>$reviewtitle,'descriptions'=>$descriptions,'userid' =>$userid, 'course_id' =>$courseid,'educators_id'=>$educatorid,'overal_rating'=>$overalrate,'career_assistance'=>$ovcareer,'value_of_money'=>$vmoney,'teching_quality'=>$tqrate,'school_facilites'=>$sfrate,'locations'=>$lqrate,'images'=>$imagearray,'videos'=>$videoarray,'status'=>$status ,'create_datetime'=>$datecreated ,'update_datetime'=>$datecreated ,'review_type'=>'course');
		$format = array('%s','%s','%d','%d','%d','%d','%d','%d','%d','%d','%d','%s','%s','%s','%s','%s');
		$insert_id =$wpdb->insert($table,$data,$format);
		if($insert_id){
			$lastid = $wpdb->insert_id;
			echo $lastid;
		}else{
			echo "error";
		}
		wp_die();
} 


/**
Upload USer Profile  Mangement
**/


function upload_user_file( $file = array() ) {

  require_once( ABSPATH . 'wp-admin/includes/admin.php' );
      $file_return = wp_handle_upload( $file, array('test_form' => false ) );
      if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
          return false;
      } else {
          $filename = $file_return['file'];
          $attachment = array(
              'post_mime_type' => $file_return['type'],
              'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
              'post_content' => '',
              'post_status' => 'inherit',
              'guid' => $file_return['url']
          );
          $attachment_id = wp_insert_attachment( $attachment, $file_return['url'] );
          require_once(ABSPATH . 'wp-admin/includes/image.php');
          $attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
          wp_update_attachment_metadata( $attachment_id, $attachment_data );
          if( 0 < intval( $attachment_id ) ) {
            return $attachment_id;
          }
      }
      return false;
}
/**
upload directory to folder.
*/
function wpse_183245_upload_dir( $dirs ) {
    $dirs['subdir'] = '/users/'.get_current_user_id().'/';
    $dirs['path'] = $dirs['basedir'] . '/users/'.get_current_user_id().'/';
    $dirs['url'] = $dirs['baseurl'] . '/users/'.get_current_user_id().'/';
    return $dirs;
}

function add_image_insert_override($sizes){
    unset( $sizes['thumbnail']);
    unset( $sizes['medium']);
    unset( $sizes['medium_large']);
    unset( $sizes['bannerimage-educators-list']);
    unset( $sizes['bannerimage-product-list']);
    unset( $sizes['country-flag']);
    unset( $sizes['eglobaleducation-featured-image']);
    unset( $sizes['large']);
    unset( $sizes['eglobaleducation-thumbnail-avatar']);
    unset( $sizes['bannerimage-courses-list']);
    return $sizes;
}


// Add product Reveiw sections.
add_action( 'wp_ajax_add_product_reveiw', 'ege_add_product_reveiw' );
add_action( 'wp_ajax_nopriv_add_product_reveiw', 'ege_add_product_reveiw' );

function ege_add_product_reveiw(){
		$userid             = get_current_user_id();
		$reviewtitle    	= sanitize_text_field($_REQUEST['reviewtitle']);
		$productid    	 	= sanitize_text_field($_REQUEST['productid']);
		$overalrate      	= sanitize_text_field($_REQUEST['overalrate']);
		$descriptions      	= sanitize_text_field($_REQUEST['useropnion']);
		$datecreated 		= date('Y-m-d H:i:s');
		$status 			= '0';
		global $wpdb;
		$table = $wpdb->prefix.'review';
		$data = array('Title'=>$reviewtitle,'descriptions'=>$descriptions,'userid' =>$userid, 'product_id' =>$productid,'review_type'=>'Product','overal_rating'=>$overalrate,'status'=>$status ,'create_datetime'=>$datecreated ,'update_datetime'=>$datecreated );
		$format = array('%s','%s','%d','%d','%s','%d','%d','%s','%s');
		$insert_id =$wpdb->insert($table,$data,$format);
		if($insert_id){
			echo 'success';
		}else{
			echo "error";
		}
	wp_die();
}


// Add Service Reveiw sections.
add_action( 'wp_ajax_add_service_reveiw', 'ege_add_service_reveiw' );
add_action( 'wp_ajax_nopriv_add_service_reveiw', 'ege_add_service_reveiw' );

function ege_add_service_reveiw(){
		$userid             = get_current_user_id();
		$reviewtitle    	= sanitize_text_field($_REQUEST['reviewtitle']);
		$productid    	 	= 0;
		$overalrate      	= sanitize_text_field($_REQUEST['overalrate']);
		$descriptions      	= sanitize_text_field($_REQUEST['useropnion']);
		$datecreated 		= date('Y-m-d H:i:s');
		$status 			= '0';
		global $wpdb;
		$table = $wpdb->prefix.'review';
		$data = array('Title'=>$reviewtitle,'descriptions'=>$descriptions,'userid' =>$userid, 'product_id' =>$productid,'review_type'=>'services','overal_rating'=>$overalrate,'status'=>$status ,'create_datetime'=>$datecreated ,'update_datetime'=>$datecreated );
		$format = array('%s','%s','%d','%d','%s','%d','%d','%s','%s');
		$insert_id =$wpdb->insert($table,$data,$format);
		if($insert_id){
			echo 'success';
		}else{
			echo "error";
		}
	wp_die();
}

add_action( 'wp_ajax_course_listing_drodpown', 'ege_course_listing_drodpown' );
add_action( 'wp_ajax_nopriv_course_listing_drodpown', 'ege_course_listing_drodpown' );

function ege_course_listing_drodpown(){
	if(isset($_REQUEST['educatordropdown'])){
	//session_start();
    //$_SESSION['countrydropdowneducator'] = $_REQUEST['countrydropdown'];	
	     	$educatorname = $_REQUEST['educatordropdown'];
	        $checkcourseidarray = checkcourseidarray();

			$educator_array = array();
			if(!empty($educatorname)){
			   	$educator_array = array(  
				'key' => 'select_university',
				'value' =>$educatorname,
				'compare' => "LIKE",
				);
			}


			$universityposts = get_posts(
			array(
			'post_type' => 'course', // Post type
			'meta_query' => array($educator_array),
			'posts_per_page' => -1,
			)
			);	


			// echo "<pre>";
			// print_r($universityposts);
			// echo "</pre>";
	?>                    
				<option value="">Select Course</option>
				<?php
				if(!empty($universityposts)){
				foreach ($universityposts as $postvalue) {

					if(in_array($postvalue->ID,$checkcourseidarray)){
						continue;
					}
				    # code...
				    echo '<option value="'.$postvalue->ID.'">'.$postvalue->post_title.'</option>'
				  
				    ?>
				    <?php

				}
				}
              ?> 
    <?php        
    }        
    wp_die();

}

// Add Current/prev Educations Details 
add_action( 'wp_ajax_update_cpeducations_informations', 'ege_insert_cpeducations' );
add_action( 'wp_ajax_nopriv_update_cpeducations_informations', 'ege_insert_cpeducations' );
function ege_insert_cpeducations(){
	global $wpdb;
	$userid             = get_current_user_id();
	$countryid    	 	= sanitize_text_field($_REQUEST['country-id']);
	$othervalue    	 	= sanitize_text_field($_REQUEST['other-value']);
	$courseid      		= sanitize_text_field($_REQUEST['course-id']);
	$educatorid      	= sanitize_text_field($_REQUEST['educator-id']);
	if($othervalue == 1){
		if(!empty($_REQUEST['course-id'])){
			$courseid      		= sanitize_text_field($_REQUEST['course-id']);
		}else{
			 $courseid = 'other';
		}
		$educatorid      	= sanitize_text_field($_REQUEST['educator-id']);
		if($educatorid == 'other' || !empty($_REQUEST['educator-id']) ){
			 $educatorid      		= 1;
		}
		if($courseid == 'other' ){
			 $courseid      		= 1;
		}
	}

	$coursedescriptions = sanitize_text_field($_REQUEST['course-descriptions']);
	$smonth         	= sanitize_text_field($_REQUEST['s-month']);
	$syear         		= sanitize_text_field($_REQUEST['s-year']);
	$emonth      		= sanitize_text_field($_REQUEST['e-month']);
	$eyear       		= sanitize_text_field($_REQUEST['e-year']);
	$presentstatus      = sanitize_text_field($_REQUEST['present-status']);
	$futurestatus       = sanitize_text_field($_REQUEST['fut-data']);
	$course_name   		= sanitize_text_field($_REQUEST['other-coursename']);
	$educator_name   	= sanitize_text_field($_REQUEST['other-educator']);
	$notdecidedstatus   = sanitize_text_field($_REQUEST['notdecided']);
	$date 				= date('Y-m-d H:i:s');
	//echo $date;
	$table = $wpdb->prefix.'prv_cur_educ_users';
	//if(!empty($courseid)){
		$data = array('userid'=>$userid,'countryname'=>$countryid,'eductorid' =>$educatorid,
		 'courseid' =>$courseid,'smonth'=>$smonth,'syear'=>$syear,'emonth'=>$emonth,
		 'eyear'=>$eyear,'Descriptions'=>$coursedescriptions,
		 'presentstatus'=>$presentstatus,'futurestatus'=>$futurestatus,'notdecidedstatus'=>$notdecidedstatus,'createdate'=>$date,
		 'updatedate'=>$date,'course_name'=>$course_name,'educator_name'=>$educator_name);

		//print_r($data );
		$format = array('%d','%s','%d','%d','%s','%s','%s','%s','%s','%d','%d','%s','%s','%s','%s','%s');
		$insert_id =$wpdb->insert($table,$data,$format);
	//}else{
	//	$insert_id='';
	//}

	if($insert_id){
		echo "success";
	}else{
		echo "error";
		$wpdb->print_error();
	}
  	 wp_die();
}

// Add  Future Educations Details 
add_action( 'wp_ajax_update_fpeducations_informations', 'ege_insert_fpeducations' );
add_action( 'wp_ajax_nopriv_update_fpeducations_informations', 'ege_insert_fpeducations' );
function ege_insert_fpeducations(){
	global $wpdb;
	$userid             = get_current_user_id();
	$countryid    	 	= sanitize_text_field($_REQUEST['country-id']);
	$courseid      		= sanitize_text_field($_REQUEST['course-id']);
	$educatorid      	= sanitize_text_field($_REQUEST['educator-id']);
	$coursedescriptions = sanitize_text_field($_REQUEST['course-descriptions']);
	$emonth      		= sanitize_text_field($_REQUEST['e-month']);
	$eyear       		= sanitize_text_field($_REQUEST['e-year']);
	$presentstatus      = sanitize_text_field($_REQUEST['present-status']);
	$futurestatus       = sanitize_text_field($_REQUEST['fut-data']);
	$notdecidedstatus   = sanitize_text_field($_REQUEST['notdecided']);
	if($futurestatus === 1){
		$syear         		= sanitize_text_field($_REQUEST['s-year']);
	}else{
		$syear         		= sanitize_text_field($_REQUEST['sf-year']);
	}
	$date 				= date('Y-m-d H:i:s');
	$table = $wpdb->prefix.'prv_cur_educ_users';
	if(!empty($courseid)){
		$data = array('userid'=>$userid,'countryname'=>$countryid,
			'eductorid' =>$educatorid,
		 'courseid' =>$courseid,'smonth'=>$smonth,'syear'=>$syear,'emonth'=>$emonth,
		 'eyear'=>$eyear,'Descriptions'=>$coursedescriptions,
		 'presentstatus'=>$presentstatus,'notdecidedstatus'=>$notdecidedstatus,'futurestatus'=>$futurestatus,'createdate'=>$date,
		 'updatedate'=>$date);
		$format = array('%d','%s','%d','%d','%s','%s','%s','%s','%s','%d','%s','%s','%s','%s');
		$insert_id =$wpdb->insert($table,$data,$format);
	}else{
		$insert_id='';
	}
	if($insert_id){
		echo "success";
	}else{
		echo "error";

	}
  	 wp_die();
}

// Delete Future Educations Details 
add_action( 'wp_ajax_delete_cur_prev_eductor', 'ege_delete_cur_prev_eductor' );
add_action( 'wp_ajax_nopriv_delete_cur_prev_eductor', 'ege_delete_cur_prev_eductor' );

function  ege_delete_cur_prev_eductor(){
	global $wpdb;
	if(isset($_REQUEST['deleteid'])){
		if(!empty($_REQUEST['deleteid'])){
			$id    = $_REQUEST['deleteid'];
			$table = $wpdb->prefix.'prv_cur_educ_users';
			$deleteid   = $wpdb->delete( $table, array( 'id' => $id));
			if($deleteid){
				echo 'success';
			}
		}	
	}

	wp_die();
}


// Delete Future Educations Details 
// add_action('cure_prev_educations_show','cur_prev_educations_html',1,10);

// function cur_prev_educations_html(){
	
// }

/**
 * Register a custom menu page.
 */
function wpdocs_register_my_custom_menu_page(){
   add_menu_page( 
        __( 'Order history', 'textdomain' ),
        'Order history',
        'manage_options',
        'custompage',
        'edu_order_history_page',
        'dashicons-cart',
        6
    ); 

	add_menu_page( __( 'Review', 'menu-test' ), __( 'Review', 'mspecs-menu' ), 'null', 'review-menu','','dashicons-admin-users',9);
	// Add a submenu to the custom top-level menu:
	add_submenu_page( 'review-menu', __( 'Review', 'egobalconnect' ), __( 'Review', 'menu-test' ), 'manage_options', 'review-page', 'ede_sublevel_page_review' );
	//IMPORT PROPERTY
	add_submenu_page( 'review-menu', __( 'Review', 'egobalconnect' ), __( 'Product Review', 'menu-test' ), 'manage_options', 'product-review-page', 'ede_sublevel_page_reviewtwo' );
	add_submenu_page( 'review-menu', __( 'Service Review', 'egobalconnect' ), __( 'Service Review', 'menu-test' ), 'manage_options', 'service-review-page', 'ede_sublevel_page_reviewthree' );
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' ,10	);
 
/**
 * Display a custom menu page
 */
/**
 * Display a custom menu page
 */
function ede_sublevel_page_review(){
global $wpdb;
if(isset($_REQUEST['ida'])  ||  isset($_REQUEST['idr'])){
			$table_name  = $wpdb->prefix . "review";
			if(isset($_REQUEST['ida'])){
				$id          = $_REQUEST['ida'];
				$status      = 1;
			}else{
				$id          = $_REQUEST['idr'];
				$status      = 2;
			}
			$where       = "`id` = '" . $id . "'";
			$update_data = "status='" . $status . "'";
			$sql_update  = "UPDATE " . $table_name . " SET " . $update_data . "  WHERE " . $where . " ";
			if ( $wpdb->query( $sql_update ) ) {
				?>
				<div class="acf-admin-notice notice is-dismissible updated "><p>Options Updated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
				<?php

					$user_id=$userlogin=$user_email= $user_firstname= $user_lastname=$user_displayname = '';
					if(isset($_REQUEST['userid'])){
					$current_user = get_userdata($_REQUEST['userid']);
						/**
						* @example Safe usage:
						* $current_user = wp_get_current_user();
						* if ( ! $current_user->exists() ) {
						*     return;
						* }
						*/
						$user_id          =  $current_user->ID; 
						$user_login       =  $current_user->user_login; 
						$user_email       =  $current_user->user_email; 
						$user_firstname   =  $current_user->user_firstname; 
						$user_lastname    =  $current_user->user_lastname;
						$user_displayname =  $current_user->display_name; 
					}

					//$to = $broker_email_address;
					$to = $user_email;
					if(isset($_REQUEST['ida'])){
					$subject = "Accepted  of your submitted review";
					$message = "Our team has reviewed your submitted feedback and we are glad to inform you it is now approved by the site administration.<br>

					Further, your submitted review is now started showing in the list of reviews under respective course and institution page and the same on My Profile page.<br>

					We appreciate your effort for the same and keep posting.";
					}else{
					$subject = "Rejection of your submitted review";
					$message = "Our team has reviewed your submitted feedback on the institution and unfortunately, it can't be approved by the site administration this time.<br><br>

						But, we appreciate your effort for the same and you can try once again by deleting your previously sent review and submit again with a proper content,<br><br>

						For more information you can read tip and help which is appeared on right box of Review form.";
					 }
				?>
			<?php
				ob_start();
			?>
			<table border="0" cellpadding="10" cellspacing="0" style=
			"background-color: #FFFFFF" width="100%">
			<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" class=
			"content" style="background-color: #FFFFFF; border-collapse:collapse; width:100%; max-width:600px">
			  <tr>
			    <td id="templateContainerHeader" valign="top" mc:edit="welcomeEdit-01" style="font-size:14px; padding-top:2.429em; padding-bottom:.929em">
			      <p style="text-align:center;margin:0;padding:0; color:#545454; display:block; font-family:Helvetica; font-size:16px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal;"><img  src="http://www.eglobaleducation.co.uk/wp-content/themes/eglobalconnect/assettwo/images/logo.png"
			      style="display:inline-block;height:50%;width:30%;"></p>
			    </td>
			  </tr>
			  <tr>
			    <td align="center" valign="top">
			      <table border="0" cellpadding="0" cellspacing="0" class=
			      "brdBottomPadd-two" id="templateContainer" width="100%" style="border-collapse:collapse; border-top:1px solid #e2e2e2;border-left:1px solid #e2e2e2; border-right:1px solid #e2e2e2;border-radius:4px 4px 0 0; background-clip:padding-box; border-spacing:0; border-bottom:1px solid #f0f0f0">
			        <tr>
			          <td class="bodyContent" valign="top" mc:edit="welcomeEdit-02" style="color:#505050; font-family:Helvetica; font-size:14px; line-height:150%; padding-top:3.143em; padding-right:3.5em; padding-left:3.5em; text-align:left; padding-bottom:.857em;">
			            <h4 style="color:#4E4E4E; display:block; font-family:Helvetica; font-size:16px; line-height:1.154em; font-style:normal; font-weight:400; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0; text-align:left;"><b>Dear <?php echo $user_displayname; ?>,</h4> 

			            <p style="color:#2e2e2e; display:block; font-family:Helvetica; font-size:14px;line-height:1.385em; font-style:normal; font-weight:300; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;
			text-align:left"><?php echo $message;  ?></p><p style="color:#2e2e2e; display:block; font-family:Helvetica; font-size:14px;line-height:1.385em; font-style:normal; font-weight:300; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;
			text-align:left">
						
			       <a class="blue-btn" href=
			            "http://blog.autopilothq.com/lead-nurturing-secrets/">Click here</a> to go to reveiw page.</p>
			                  <h4 style="color:#4E4E4E; display:block; font-family:Helvetica; font-size:16px; line-height:1.154em; font-style:normal; font-weight:400; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0; text-align:left;"><b>Thank you!</h4> 

			          </td>
			        </tr>
			      </table>
			    </td>
			  </tr>
			<tr>
			    <td align="center" class="unSubContent" id="bodyCellFooter" valign=
			    "top" style="margin:0;padding:0;width:100%!important;padding-top:10px;padding-bottom:15px">
			      <table border="0" cellpadding="0" cellspacing="0" id=
			      "templateContainerFooter" width="100%" style="border-collapse:collapse;">
			        <tr>
			          <td valign="top" width="100%" mc:edit="welcomeEdit-11">
			            <h6 style="text-align:center;margin-top: 9px; color:#a1a1a1; display:block; font-family:Helvetica; font-size:12px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal; margin-right:0; margin-bottom:0; margin-left:0;">Eglobal Education</h6>
			                <h6 style="text-align:center; color:#a1a1a1; display:block; font-family:Helvetica; font-size:12px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal; margin-right:0; margin-top:0; margin-bottom:0; margin-left:0;">121 King Street ,London 3000 United Kingdom</h6>
			          </td>
			        </tr>
			      </table>
			    </td>
			  </tr>
			</table>
			</td>
			</tr>
			</table>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
			$headers = array('Content-Type: text/html; charset=UTF-8');
			wp_mail($to, $subject, $html, $headers);
		} else {
			?>
<div class="acf-admin-notice notice is-dismissible updated "><p>Options Not Updated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
				<?php
			}
}
	$table_name = $wpdb->prefix . "review";

	$avarage = 0;
	$totalaverage =0;
	$totalcoursereviws =0;
	//SELECT * FROM `ege_review` WHERE `course_id` = 15
	$coursereview = $wpdb->get_results( "SELECT * FROM $table_name WHERE `review_type` LIKE 'course' AND  `status` != 3 ORDER BY `descriptions` ASC" );
    ?> 
        <html>
    <head>
        <link rel="stylesheet" type="text/css"
              href="<?php echo get_stylesheet_directory_uri()  ?>/css/admin.css">
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css"
              href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
<h2> <?php esc_html_e( 'Course Review', 'textdomain' ); ?></h2>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>id</th>
                <th>User Full Name</th>
                <th>course title</th>
                <th>Educators title</th>
                <th>View</th>
                <th>Review Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        	    <?php 
        	     $count=0;
        	     foreach ($coursereview as $value) {
                    # code...
                   $count++;
                   $userid          = $value->userid;
                   $courseid        = $value->course_id;
                   $educatorid      = $value->educators_id;
                   $reviewid        = $value->id;
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                   $firstname       = $user_info->first_name;
                   $lastname        = $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $status       	= $value->status;
                   $descriptions    = $value->descriptions;
                   $create_datetime = date("d F Y", strtotime($value->create_datetime)); 
                  // echo get_avatar( $userid );
                   if($status == 1){
                   		$label 	  = 'Accepted';
               		}else{
               			$label  = 'Accept';
               		}	
               		 if($status == 2){
               			$labeltwo = 'Reject';
               		}else{
               			$labeltwo = 'Rejected';
               		}
                   ?>
		            <tr>
		            	<td><?php echo $count; ?></td>
		                <td><?php echo $firstname.' '.$lastname; ?></td>
		               <td width="220"><?php echo get_the_title($educatorid); ?></td>
		                <td width="220"><?php echo get_the_title($courseid); ?></td>
		                <td width="10"><a class="btn btn-info" target="_blank" href="<?php echo site_url().'/'.'review-details/?id='.$reviewid; ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"> </span></a></td>
		                <td><?php echo $create_datetime;  ?></td>
		                <td><a href="admin.php?page=review-page&userid=<?php echo $userid ?>&ida=<?php echo $reviewid;   ?>"><button class="accept-review btn"><?php echo $label; ?></button></a>
						<a href="admin.php?page=review-page&userid=<?php echo $userid ?>&idr=<?php echo $reviewid;   ?>"><button class="accept-review btn"><?php echo $labeltwo; ?></button></a>
		                </td>
		            </tr>
       		 <?php } ?>
        </tbody>
        <tfoot>
        	<?php
        	// echo "<pre>";
        	// print_r($coursereview);
        	// echo "</pre>";
        	?>
            <tr>
              	<th>id</th>
                <th>User Full Name</th>
                <th>course title</th>
                <th>Educators title</th>
                <th>View</th>
                <th>Review Date</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            jQuery('#example').DataTable();
        });
    </script>
    <?php
}

/**
 * Display a custom menu page
 */
function ede_sublevel_page_reviewtwo(){
    esc_html_e( 'Product Review', 'textdomain' );

global $wpdb;
if(isset($_REQUEST['ida'])  ||  isset($_REQUEST['idr'])){
			$table_name  = $wpdb->prefix . "review";
			if(isset($_REQUEST['ida'])){
				$id          = $_REQUEST['ida'];
				$status      = 1;
			}else{
				$id          = $_REQUEST['idr'];
				$status      = 2;
			}
			$where       = "`id` = '" . $id . "'";
			$update_data = "status='" . $status . "'";
			$sql_update  = "UPDATE " . $table_name . " SET " . $update_data . "  WHERE " . $where . " ";
			if ( $wpdb->query( $sql_update ) ) {
				?>
			<div class="acf-admin-notice notice is-dismissible updated "><p>Options Updated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
			<?php
				if(isset($_REQUEST['userid'])){
					$current_user = get_userdata($_REQUEST['userid']);
						/**
						* @example Safe usage:
						* $current_user = wp_get_current_user();
						* if ( ! $current_user->exists() ) {
						*     return;
						* }
						*/
						$user_id          =  $current_user->ID; 
						$user_login       =  $current_user->user_login; 
						$user_email       =  $current_user->user_email; 
						$user_firstname   =  $current_user->user_firstname; 
						$user_lastname    =  $current_user->user_lastname;
						$user_displayname =  $current_user->display_name; 
					}
					//$to = $broker_email_address;
					$to = $user_email;
					if(isset($_REQUEST['ida'])){
					$subject = "Accepted  of your submitted review";
					$message = "Our team has reviewed your submitted feedback and we are glad to inform you it is now approved by the site administration.<br><br>

					   We appreciate your effort for the same and keep posting.";
					}else{
					$subject = "Rejection of your submitted review";
					$message = "Our team has reviewed your submitted feedback on the  Product and unfortunately, it can't be approved by the site administration this time.<br><br>

						For more information you can read tip and help which is appeared on right box of Review form.";
					 }
				?>
			<?php
				ob_start();
			?>
			<table border="0" cellpadding="10" cellspacing="0" style=
			"background-color: #FFFFFF" width="100%">
			<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" class=
			"content" style="background-color: #FFFFFF; border-collapse:collapse; width:100%; max-width:600px">
			  <tr>
			    <td id="templateContainerHeader" valign="top" mc:edit="welcomeEdit-01" style="font-size:14px; padding-top:2.429em; padding-bottom:.929em">
			      <p style="text-align:center;margin:0;padding:0; color:#545454; display:block; font-family:Helvetica; font-size:16px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal;"><img  src="http://www.eglobaleducation.co.uk/wp-content/themes/eglobalconnect/assettwo/images/logo.png"
			      style="display:inline-block;height:50%;width:30%;"></p>
			    </td>
			  </tr>
			  <tr>
			    <td align="center" valign="top">
			      <table border="0" cellpadding="0" cellspacing="0" class=
			      "brdBottomPadd-two" id="templateContainer" width="100%" style="border-collapse:collapse; border-top:1px solid #e2e2e2;border-left:1px solid #e2e2e2; border-right:1px solid #e2e2e2;border-radius:4px 4px 0 0; background-clip:padding-box; border-spacing:0; border-bottom:1px solid #f0f0f0">
			        <tr>
			          <td class="bodyContent" valign="top" mc:edit="welcomeEdit-02" style="color:#505050; font-family:Helvetica; font-size:14px; line-height:150%; padding-top:3.143em; padding-right:3.5em; padding-left:3.5em; text-align:left; padding-bottom:.857em;">
			            <h4 style="color:#4E4E4E; display:block; font-family:Helvetica; font-size:16px; line-height:1.154em; font-style:normal; font-weight:400; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0; text-align:left;"><b>Dear <?php echo $user_displayname; ?>,</h4> 

			            <p style="color:#2e2e2e; display:block; font-family:Helvetica; font-size:14px;line-height:1.385em; font-style:normal; font-weight:300; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;
			text-align:left"> <?php echo $message;  ?> </p><p style="color:#2e2e2e; display:block; font-family:Helvetica; font-size:14px;line-height:1.385em; font-style:normal; font-weight:300; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;
			text-align:left">
						
			                  <h4 style="color:#4E4E4E; display:block; font-family:Helvetica; font-size:16px; line-height:1.154em; font-style:normal; font-weight:400; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0; text-align:left;"><b>Thank you!</h4> 

			          </td>
			        </tr>
			      </table>
			    </td>
			  </tr>
			<tr>
			    <td align="center" class="unSubContent" id="bodyCellFooter" valign=
			    "top" style="margin:0;padding:0;width:100%!important;padding-top:10px;padding-bottom:15px">
			      <table border="0" cellpadding="0" cellspacing="0" id=
			      "templateContainerFooter" width="100%" style="border-collapse:collapse;">
			        <tr>
			          <td valign="top" width="100%" mc:edit="welcomeEdit-11">
			            <h6 style="text-align:center;margin-top: 9px; color:#a1a1a1; display:block; font-family:Helvetica; font-size:12px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal; margin-right:0; margin-bottom:0; margin-left:0;">Eglobal Education</h6>
			                <h6 style="text-align:center; color:#a1a1a1; display:block; font-family:Helvetica; font-size:12px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal; margin-right:0; margin-top:0; margin-bottom:0; margin-left:0;">121 King Street ,London 3000 United Kingdom</h6>
			          </td>
			        </tr>
			      </table>
			    </td>
			  </tr>
			</table>
			</td>
			</tr>
			</table>
				<?php
				$html = ob_get_contents();
				ob_end_clean();
				$headers = array('Content-Type: text/html; charset=UTF-8');
				wp_mail($to, $subject,$html, $headers);
			} else {
				?>
						<div class="acf-admin-notice notice is-dismissible updated "><p>Options Updated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
				<?php
			}
}
$table_name = $wpdb->prefix . "review";

$avarage = 0;
$totalaverage =0;
$totalcoursereviws =0;
//SELECT * FROM `ege_review` WHERE `course_id` = 15
$coursereview = $wpdb->get_results( "SELECT * FROM $table_name WHERE `review_type` LIKE 'Product' ORDER BY `review_type` ASC" );
    ?> 
        <html>
    <head>
        <link rel="stylesheet" type="text/css"
              href="<?php echo get_stylesheet_directory_uri()  ?>/css/admin.css">
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css"
              href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
<h2> <?php esc_html_e( 'Product Review', 'textdomain' ); ?></h2>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>id</th>
                <th>User Full Name</th>
                <th>Product Title</th>
                <th>Title</th>
                <th>Descriptions</th>
                <th>Review Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        	    <?php 
        	     $count=0;
        	     foreach ($coursereview as $value) {
                    # code...
                   $count++;
                   $userid          = $value->userid;
                   $product_id      = $value->product_id;
                   $reviewid        = $value->id;
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                   $firstname       = $user_info->first_name;
                   $Title       	= $value->Title;
                   $lastname        = $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $status       	= $value->status;
                   $descriptions    = $value->descriptions;
                   $create_datetime = date("d F Y", strtotime($value->create_datetime)); 
                  // echo get_avatar( $userid );
                   if($status == 1){
                   		$label 	  = 'Accepted';
               		}else{
               			$label  = 'Accept';
               		}	
               		if($status == 2){
               			$labeltwo = 'Reject';
               		}else{
               			$labeltwo = 'Rejected';
               		}
                   ?>
		            <tr>
		            	<td><?php echo $count; ?></td>
		                <td><?php echo $firstname.' '.$lastname; ?></td>
		                <td width="150"><?php echo get_the_title($product_id); ?></td>
		                <td width="150"><?php echo $Title; ?></td>
		                <td width="300"><?php echo $descriptions; ?></td>
		                <td><?php echo $create_datetime;  ?></td>
		                <td><a href="admin.php?page=product-review-page&userid=<?php echo $userid;   ?>&ida=<?php echo $reviewid;   ?>"><button class="accept-review btn"><?php echo $label; ?></button></a>
						<a href="admin.php?page=product-review-page&userid=<?php echo $userid;   ?>&idr=<?php echo $reviewid;   ?>"><button class="accept-review btn"><?php echo $labeltwo; ?></button></a>
		                </td>
		            </tr>
       		 <?php } ?>
        </tbody>
        <tfoot>
        	<?php
        	// echo "<pre>";
        	// print_r($coursereview);
        	// echo "</pre>";
        	?>
            <tr>
                 <th>id</th>
                <th>User Full Name</th>
                <th>Product Title</th>
                <th>Title</th>
                <th>Descriptions</th>
                <th>Review Date</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            jQuery('#example').DataTable();
        });

    </script>
<?php
}

function ede_sublevel_page_reviewthree(){
global $wpdb;
if(isset($_REQUEST['ida'])  ||  isset($_REQUEST['idr'])){
			$table_name  = $wpdb->prefix . "review";
			if(isset($_REQUEST['ida'])){
				$id          = $_REQUEST['ida'];
				$status      = 1;
			}else{
				$id          = $_REQUEST['idr'];
				$status      = 2;
			}
			$where       = "`id` = '" . $id . "'";
			$update_data = "status='" . $status . "'";
			$sql_update  = "UPDATE " . $table_name . " SET " . $update_data . "  WHERE " . $where . " ";
			if ( $wpdb->query( $sql_update ) ) {
				?>
			<div class="acf-admin-notice notice is-dismissible updated "><p>Options Updated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
				<?php
											//$to = $broker_email_address;
					if(isset($_REQUEST['userid'])){
					$current_user = get_userdata($_REQUEST['userid']);
						/**
						* @example Safe usage:
						* $current_user = wp_get_current_user();
						* if ( ! $current_user->exists() ) {
						*     return;
						* }
						*/
						$user_id          =  $current_user->ID; 
						$user_login       =  $current_user->user_login; 
						$user_email       =  $current_user->user_email; 
						$user_firstname   =  $current_user->user_firstname; 
						$user_lastname    =  $current_user->user_lastname;
						$user_displayname =  $current_user->display_name; 
					}
					//$to = $broker_email_address;
					$to = $user_email;
					if(isset($_REQUEST['ida'])){
					$subject = "Accepted  of your submitted review";
					$message = "Our team has reviewed your submitted feedback and we are glad to inform you it is now approved by the site administration.<br><br>

					   We appreciate your effort for the same and keep posting.";
					}else{
					$subject = "Rejection of your submitted review";
					$message = "Our team has reviewed your submitted feedback on the  Service and unfortunately, it can't be approved by the site administration this time.<br><br>

						For more information you can read tip and help which is appeared on right box of Review form.";
					 }
				?>
			<?php
				ob_start();
			?>
			<table border="0" cellpadding="10" cellspacing="0" style=
			"background-color: #FFFFFF" width="100%">
			<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" class=
			"content" style="background-color: #FFFFFF; border-collapse:collapse; width:100%; max-width:600px">
			  <tr>
			    <td id="templateContainerHeader" valign="top" mc:edit="welcomeEdit-01" style="font-size:14px; padding-top:2.429em; padding-bottom:.929em">
			      <p style="text-align:center;margin:0;padding:0; color:#545454; display:block; font-family:Helvetica; font-size:16px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal;"><img  src="http://www.eglobaleducation.co.uk/wp-content/themes/eglobalconnect/assettwo/images/logo.png"
			      style="display:inline-block;height:50%;width:30%;"></p>
			    </td>
			  </tr>
			  <tr>
			    <td align="center" valign="top">
			      <table border="0" cellpadding="0" cellspacing="0" class=
			      "brdBottomPadd-two" id="templateContainer" width="100%" style="border-collapse:collapse; border-top:1px solid #e2e2e2;border-left:1px solid #e2e2e2; border-right:1px solid #e2e2e2;border-radius:4px 4px 0 0; background-clip:padding-box; border-spacing:0; border-bottom:1px solid #f0f0f0">
			        <tr>
			          <td class="bodyContent" valign="top" mc:edit="welcomeEdit-02" style="color:#505050; font-family:Helvetica; font-size:14px; line-height:150%; padding-top:3.143em; padding-right:3.5em; padding-left:3.5em; text-align:left; padding-bottom:.857em;">
			            <h4 style="color:#4E4E4E; display:block; font-family:Helvetica; font-size:16px; line-height:1.154em; font-style:normal; font-weight:400; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0; text-align:left;"><b>Dear <?php echo $user_displayname; ?>,</h4> 

			            <p style="color:#2e2e2e; display:block; font-family:Helvetica; font-size:14px;line-height:1.385em; font-style:normal; font-weight:300; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;
			text-align:left"> <?php echo $message;  ?> </p><p style="color:#2e2e2e; display:block; font-family:Helvetica; font-size:14px;line-height:1.385em; font-style:normal; font-weight:300; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0;
			text-align:left">
						
			                  <h4 style="color:#4E4E4E; display:block; font-family:Helvetica; font-size:16px; line-height:1.154em; font-style:normal; font-weight:400; letter-spacing:normal; margin-top:0; margin-right:0; margin-bottom:15px; margin-left:0; text-align:left;"><b>Thank you!</h4> 

			          </td>
			        </tr>
			      </table>
			    </td>
			  </tr>
			<tr>
			    <td align="center" class="unSubContent" id="bodyCellFooter" valign=
			    "top" style="margin:0;padding:0;width:100%!important;padding-top:10px;padding-bottom:15px">
			      <table border="0" cellpadding="0" cellspacing="0" id=
			      "templateContainerFooter" width="100%" style="border-collapse:collapse;">
			        <tr>
			          <td valign="top" width="100%" mc:edit="welcomeEdit-11">
			            <h6 style="text-align:center;margin-top: 9px; color:#a1a1a1; display:block; font-family:Helvetica; font-size:12px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal; margin-right:0; margin-bottom:0; margin-left:0;">Eglobal Education</h6>
			                <h6 style="text-align:center; color:#a1a1a1; display:block; font-family:Helvetica; font-size:12px; line-height:1.5em; font-style:normal; font-weight:400; letter-spacing:normal; margin-right:0; margin-top:0; margin-bottom:0; margin-left:0;">121 King Street ,London 3000 United Kingdom</h6>
			          </td>
			        </tr>
			      </table>
			    </td>
			  </tr>
			</table>
			</td>
			</tr>
			</table>
				<?php
				$html = ob_get_contents();
				ob_end_clean();
				$headers = array('Content-Type: text/html; charset=UTF-8');
				wp_mail($to, $subject,$html, $headers);
			} else {
				?>
			<div class="acf-admin-notice notice is-dismissible updated "><p>Options Not Updated</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
				<?php
			}
}
$table_name = $wpdb->prefix . "review";

$avarage = 0;
$totalaverage =0;
$totalcoursereviws =0;
//SELECT * FROM `ege_review` WHERE `course_id` = 15
$coursereview = $wpdb->get_results( "SELECT * FROM $table_name WHERE `review_type` LIKE 'services' ORDER BY `review_type` ASC" );
    ?> 
        <html>
    <head>
        <link rel="stylesheet" type="text/css"
              href="<?php echo get_stylesheet_directory_uri()  ?>/css/admin.css">
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css"
              href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
<h2> <?php esc_html_e( 'Service Review', 'textdomain' ); ?></h2>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>id</th>
                <th>User Full Name</th>
                <th>Title</th>
                <th>Descriptions</th>
                <th>Review Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        	    <?php 
        	     $count=0;
        	     foreach ($coursereview as $value) {
                    # code...
                   $count++;
                   $userid          = $value->userid;
                   $product_id      = $value->product_id;
                   $reviewid        = $value->id;
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                   $firstname       = $user_info->first_name;
                   $Title       	= $value->Title;
                   $lastname        = $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $status       	= $value->status;
                   $descriptions    = $value->descriptions;
                   $create_datetime = date("d F Y", strtotime($value->create_datetime)); 
                  // echo get_avatar( $userid );
                   if($status == 1){
                   		$label 	  = 'Accepted';
               		}else{
               			$label  = 'Accept';
               		}	
               	  if($status == 2){
               			$labeltwo = 'Reject';
               		}else{
               			$labeltwo = 'Rejected';
               		}
                   ?>
		            <tr>
		            	<td><?php echo $count; ?></td>
		                <td><?php echo $firstname.' '.$lastname; ?></td>
		                <td width="150"><?php echo $Title; ?></td>
		                <td width="300"><?php echo $descriptions; ?></td>
		                <td><?php echo $create_datetime;  ?></td>
		                <td><a href="admin.php?page=service-review-page&userid=<?php echo $userid;   ?>&ida=<?php echo $reviewid;   ?>"><button class="accept-review btn"><?php echo $label; ?></button></a>
						<a href="admin.php?page=service-review-page&userid=<?php echo $userid;   ?>&idr=<?php echo $reviewid;   ?>"><button class="accept-review btn"><?php echo $labeltwo; ?></button></a>
		                </td>
		            </tr>
       		 <?php } ?>
        </tbody>
        <tfoot>
        	<?php
        	// echo "<pre>";
        	// print_r($coursereview);
        	// echo "</pre>";
        	?>
            <tr>
                 <th>id</th>
                <th>User Full Name</th>
                <th>Title</th>
                <th>Descriptions</th>
                <th>Review Date</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            jQuery('#example').DataTable();
        });

    </script>
<?php
}

/**
Check Review already exits or not.
**/
function checkcourseidarray(){
	global $wpdb;
	$userid = get_current_user_id();
	$table_name = $wpdb->prefix . "review";
	$coursereview = $wpdb->get_results( "SELECT course_id FROM $table_name WHERE `userid` = ".$userid );
	$coursearray =array_column($coursereview,'course_id');
	return $coursearray;
}

/**
Get Review list user wise code.
**/
function get_my_reveiw($userid,$activeid=null){
	global $wpdb;
	$table_name = $wpdb->prefix . "review";
	$where = '';
	if($activeid){
		$where = " WHERE  `status` = 1 And userid = $userid And `review_type` LIKE 'course'";
	}else{
		$where = "WHERE `userid` = $userid AND `review_type` LIKE 'course'";
	}
	$coursereview = $wpdb->get_results( "SELECT * FROM $table_name $where GROUP by course_id" ,ARRAY_A );
	// $coursearray =array_column($coursereview);
	return $coursereview;
}

/**
Check order Text ID there or not.
**/
function checktxtidthere($txtid){
	global $wpdb;
	$userid = get_current_user_id();
	$table_name = $wpdb->prefix . "review";
	$coursereview = $wpdb->get_results( "SELECT * FROM `ege_order_history` WHERE `txn_id` LIKE '".$txtid."'");
	$coursearray =array_column($coursereview,'txn_id');
	return $coursearray;
}

/**
get order history using user id.
**/
function get_my_orderhistory(){
	global $wpdb;
	$userid = get_current_user_id();
	$table_name = $wpdb->prefix . "order_history";
	$where = '';
	$where = "WHERE userid = $userid";
	$order_history   =  $wpdb->get_results( "SELECT * FROM $table_name $where GROUP BY product_id ORDER BY `create_datetime` DESC" ,ARRAY_A );
	$order_history =array_column($order_history,'product_id');
	return $order_history;
}

/**
get order history using user id.
**/
function get_my_orderhistory_date($productid){
	global $wpdb;
	$userid = get_current_user_id();
	$table_name = $wpdb->prefix . "order_history";
	$where = '';
	$where = "WHERE product_id = $product_id";

	$order_historydate   =  $wpdb->get_results( "SELECT * FROM $table_name $where GROUP BY product_id ORDER BY `create_datetime` DESC" ,ARRAY_A );

	$order_historydate =array_column($order_history,'create_datetime');
	echo "<pre>";
		print_r($order_historydate);
	echo "</pre>";

	return $order_historydate;
	
}
/**
Check Review already exits or not.
**/
function checkproductidarray(){
	global $wpdb;
	$userid = get_current_user_id();
	$table_name = $wpdb->prefix . "review";
	$coursereview = $wpdb->get_results( "SELECT product_id FROM $table_name WHERE `review_type` LIKE 'Product' And `userid` = ".$userid  );
	$productarray =array_column($coursereview,'product_id');
	return $productarray;
}
/**
Check Review already exits or not.
**/
function checkreviewactiveornot($productid){
	global $wpdb;
	$userid = get_current_user_id();
	$table_name = $wpdb->prefix . "review";
	$coursereview = $wpdb->get_results( "SELECT product_id FROM $table_name WHERE `product_id` = $productid AND `status` = 1 And `userid` = ".$userid);
	$productarray =array_column($coursereview,'product_id');
	return $productarray;
}

function edu_order_history_page(){
global $wpdb;
if(isset($_REQUEST['ida'])  ||  isset($_REQUEST['idr'])){
			$table_name  = $wpdb->prefix . "review";
			if(isset($_REQUEST['ida'])){
				$id          = $_REQUEST['ida'];
				$status      = 1;
			}else{
				$id          = $_REQUEST['idr'];
				$status      = 2;
			}
			$where       = "`id` = '" . $id . "'";
			$update_data = "status='" . $status . "'";
			$sql_update  = "UPDATE " . $table_name . " SET " . $update_data . "  WHERE " . $where . " ";
			if ( $wpdb->query( $sql_update ) ) {
				echo "Record Updated";
								//$to = $broker_email_address;
				$to = "adnan.limdiwala@iflair.com";
				if(isset($_REQUEST['ida'])){

					$subject = "Your Review is Accepted";
					$message = "Your review has been approved by admin and then it can be accessible publicaly";

				}else{
					$subject = "Your Review is Reject";
					$message = "Your review has been Reject by admin You need to add New review for";
				}
				wp_mail($to, $subject, $message, $headers);
			} else {
				echo "Recored Not Updated";
			}
}
$table_name = $wpdb->prefix . "order_history";

$avarage = 0;
$totalaverage =0;
$totalcoursereviws =0;
//SELECT * FROM `ege_review` WHERE `course_id` = 15
$coursereview = $wpdb->get_results( "SELECT * FROM $table_name ORDER BY `create_datetime` ASC" );
    ?> 
        <html>
    <head>
        <link rel="stylesheet" type="text/css"
              href="<?php echo get_stylesheet_directory_uri()  ?>/css/admin.css">
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css"
              href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
<h2> <?php esc_html_e( 'Order history', 'textdomain' ); ?></h2>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
             	<th>id</th>
                <th>Customer Name</th>
                <th>Product Title</th>
                <th>Product Price</th>
                <th>Buyer Email ID</th>
                <th>Payment Status</th>
                <th>Purchased Date</th>
            </tr>
        </thead>
        <tbody>
        	    <?php 
        	     $count=0;
        	     foreach ($coursereview as $value) {
                    # code...
                   $count++;
                   $userid          = $value->userid;
                   $payer_email	    = $value->payer_email;
                   $product_id      = $value->product_id;
                   $payment_status  = $value->payment_status;
                   $reviewid        = $value->id;
                   $user_info       = get_userdata($userid);
                   $productprice    = get_field( "product_price" ,$product_id);
                 //  product_price
                   $useremail       = $user_info->user_email;
                   $firstname       = $user_info->first_name;
                   $Title       	= $value->Title;
                   $lastname        = $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $status       	= $value->status;
                   $descriptions    = $value->descriptions;
                   $create_datetime = date("d F Y", strtotime($value->create_datetime)); 
                  // echo get_avatar( $userid );
                   if($status == 1){
                   		$label 	  = 'Accepted';
               		}else{
               			$label  = 'Accept';
               		}	
               		$labeltwo = 'Reject';
                   ?>
		            <tr>
		            	<td><?php echo $count; ?></td>
		                <td><?php echo $firstname.' '.$lastname; ?></td>
		                <td width="300"><?php echo get_the_title($product_id); ?></td>
		                 <td><?php echo '£'. $productprice ; ?></td>
	                     <td width="150"><?php echo $payer_email; ?></td>
	                    <td><?php echo $payment_status;  ?></td>
		                <td><?php echo $create_datetime;  ?></td>
		            </tr>
       		 <?php } ?>
        </tbody>
        <tfoot>
        	<?php
        	// echo "<pre>";
        	// print_r($coursereview);
        	// echo "</pre>";
        	?>
            <tr>
                <th>id</th>
                <th>Customer Name</th>
                <th>Product Title</th>
                <th>Product Price</th>
                <th>Buyer Email ID</th>
                <th>Payment Status</th>
                <th>Purchased Date</th>
            </tr>
        </tfoot>
    </table>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            jQuery('#example').DataTable();
        });

    </script>
    <?php
}
/**
Get Service list user wise code/global wise.
**/
function get_my_services_review($userid=null){
	global $wpdb;
	$table_name = $wpdb->prefix . "review";
	$where = '';
	if($userid){
		$where = " WHERE  `status` = 1 And userid = $userid And `review_type` LIKE 'services'";
	}else{
		$where = "WHERE `review_type` LIKE 'services' AND `status` = 1";
	}

	$coursereview = $wpdb->get_results( "SELECT * FROM $table_name $where ORDER BY`update_datetime` DESC" ,ARRAY_A );
	// $coursearray =array_column($coursereview);
	return $coursereview;
}
/**
Get Service list user wise code/global wise.
**/
function get_my_recent_ebooks_review($productid=null){
	global $wpdb;
	$table_name = $wpdb->prefix . "review";
	$where = '';
	if($productid){
		$where = "WHERE `review_type`  LIKE 'Product' AND `status` = 1  AND `overal_rating` = 5 AND product_id =".$productid;
	}else{
		$where = " WHERE `review_type` LIKE 'Product' AND `status` = 1 AND `overal_rating`= 5";
	}
	$coursereview = $wpdb->get_results( "SELECT * FROM 	$table_name  $where   ORDER BY `create_datetime` DESC LIMIT 3" ,ARRAY_A );
	// $coursearray =array_column($coursereview);
	return $coursereview;
}
/**
Get Service list user wise code/global wise.
**/
function get_my_recent_services_review($userid=null){
	global $wpdb;
	$table_name = $wpdb->prefix . "review";
	$where = '';
	if($userid){
		$where = "WHERE `review_type` LIKE 'services' AND `status` = 1  AND `overal_rating` = 5 And userid".$userid;
	}else{
		$where = "WHERE `review_type` LIKE 'services' AND `status` = 1  AND `overal_rating`= 5";
	}
	$coursereview = $wpdb->get_results( "SELECT * FROM 	$table_name  $where ORDER BY `create_datetime` DESC LIMIT 3" ,ARRAY_A );
	// $coursearray =array_column($coursereview);
	return $coursereview;
}
// Service Reveiw Home Page Html
	add_action('ege_servicereveiw_html','ege_servicereveiw_html');

	function ege_servicereveiw_html(){
	$servicereveiw = get_my_services_review(); 
	$review_title = get_field('review_title');

	if (!empty($servicereveiw)){ ?>
    <section class="reviews">
        <div class="wrapper">
		
        <h2 data-aos="fade-right" data-aos-duration="600"><?php echo $review_title; ?></h2>
            <div id="reviews-slider" data-aos="fade-left" data-aos-duration="600" class="testmonial-block owl-carousel owl-theme">
         	 <?php  foreach ($servicereveiw as $value) {
                    # code...
                   $userid          = $value['userid'];
                   $user_info       = get_userdata($userid);
                   $useremail       = $user_info->user_email;
                  // $user            = get_user_by( 'email',$useremail);
                   //print_r($user);
                   $firstname       = $user_info->first_name;
                   $lastname        = $user_info->last_name;
                   $userlogin       = $user_info->user_login;
                   $descriptions    = $value['descriptions'];
                   $id              = $value->id;
                   $overal_rating   = $value['overal_rating'];;
                  // echo get_avatar( $userid ); 

                   ?>
                    <div class="item testmonial-block-innner" data-animate="bounceIn animated">
                    <div class="profile-wrap">
                        <?php echo get_avatar( $userid ,52); ?>
                    </div>
                    <h4><?php echo $userlogin; ?></h4>
                    <div class="rate-star">
						<?php if(!empty($overal_rating) ){
						$totalavaragereaming = 5;
						?>
						<span class="like-count">
						<?php for($i=1; $i<=$overal_rating; $i++ ){ ?>
						<i class="fa fa-star"></i>
						<?php } ?>
						<?php for($i=$overal_rating; $i<$totalavaragereaming; $i++ ){ ?>
						<i class="fa fa-star-o"></i>
						<?php  } ?>
						</span>
						<?php  } ?>
                    </div>
                       <p><?php echo substr($descriptions,'0','1000'); ?></p>
                </div>
            <?php } ?>
       
            </div>
        </div>
    </section>
<?php } }?>
